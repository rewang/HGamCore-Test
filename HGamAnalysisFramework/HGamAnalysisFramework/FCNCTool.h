#ifndef HGamAnalysisFramework_FCNCTool_H
#define HGamAnalysisFramework_FCNCTool_H

#include "HGamAnalysisFramework/EventHandler.h"
#include "HGamAnalysisFramework/TruthHandler.h"
#include "HGamAnalysisFramework/HgammaIncludes.h"

typedef std::vector<int> vint;
typedef std::vector<float> vfloat;

namespace HG {

  class FCNCTool {
  public:

    FCNCTool(HG::EventHandler *eventHandler);

    virtual ~FCNCTool() {};

    virtual EL::StatusCode initialize(Config &config);

    void saveFCNCInfo(xAOD::PhotonContainer    &photons,
                      xAOD::MuonContainer      &muons,
                      xAOD::ElectronContainer  &electrons,
                      xAOD::MissingETContainer &met,
                      xAOD::JetContainer       &jets);

    void truthHandler(HG::TruthHandler *th) { m_truthHandler = th; }

  private:

    HG::EventHandler         *m_eventHandler; //!
    virtual HG::EventHandler *eventHandler();

    HG::TruthHandler         *m_truthHandler; //!

    void saveMapsToEventInfo();

    unsigned int performSelection(xAOD::PhotonContainer    &photons,
                                  xAOD::MuonContainer      &muons,
                                  xAOD::ElectronContainer  &electrons,
                                  xAOD::MissingETContainer &met,
                                  xAOD::JetContainer       &jets);

    unsigned int LeptonAnaSel(xAOD::MissingETContainer &met,
                              xAOD::JetContainer       &jets);

    unsigned int HadronAnaSel(xAOD::JetContainer       &jets);

    double m_mTrueT1, m_pxTrueT1, m_pyTrueT1, m_pzTrueT1; // top Ex
    double m_mTrueT2, m_pxTrueT2, m_pyTrueT2, m_pzTrueT2; // top SM
    double m_T1qpid, m_ptTrueT1q, m_phiTrueT1q, m_etaTrueT1q, m_mTrueT1q; // top Ex c-quark
    double m_T2qpid, m_ptTrueT2q, m_phiTrueT2q, m_etaTrueT2q, m_mTrueT2q; // top SM b-quark
    double m_ptTrueT1qv, m_phiTrueT1qv, m_etaTrueT1qv; // top Ex c-jet
    double m_ptTrueT2qv, m_phiTrueT2qv, m_etaTrueT2qv; // top SM b-jet

    double m_ptTrueGam0, m_phiTrueGam0, m_etaTrueGam0; // top Ex leading pT photon
    double m_ptTrueGam1, m_phiTrueGam1, m_etaTrueGam1; // top Ex sub-leading pT photon

    double m_qpidWc0, m_ptTrueT2Wc0, m_phiTrueT2Wc0, m_etaTrueT2Wc0, m_mTrueT2Wc0; // top SM W boson decay up-type quark / neutrino
    double m_qpidWc1, m_ptTrueT2Wc1, m_phiTrueT2Wc1, m_etaTrueT2Wc1, m_mTrueT2Wc1; // top SM W boson decay do-type quark / lepton

    int getChannelNumber() { return m_eventHandler->mcChannelNumber(); };

    bool m_writeTruthInfo;
    bool findTrueTopEx();
    bool findTrueTopSM();
    const xAOD::TruthParticle *ThisParticleFinal(const xAOD::TruthParticle *p);

    void GetDaughter(const xAOD::TruthParticle *tp, std::vector<const xAOD::TruthParticle *> &vecDaughter);
    std::vector<double> m_mcidForMatch;

    std::vector<double> getNuPz(TLorentzVector &lep, const xAOD::MissingET *met, double &mT);

    bool isGoodJ(const xAOD::Jet *);

    TLorentzVector m_p4H;
    struct selLep {
      TLorentzVector p4;
      double sf;
      short id;
    } m_lep;

    // Configuration
    unsigned short m_maxNjet, m_maxNjetLep;
    double m_jptCC, m_jptCF;
    bool m_doLooseJ, m_debug;

    double m_mTop1HadLow, m_mTop1HadHigh, m_mTop1LepLow, m_mTop1LepHigh;
    double m_mTop2HadLow, m_mTop2HadHigh, m_mTop2LepLow, m_mTop2LepHigh;
    double m_ptelC, m_ptmuC;
    double m_mTC;

    // debug
    double m_mgg;

    // The info I want to store...
    std::map<TString, float>  m_eventInfoFloats;
    std::map<TString, int>    m_eventInfoInts;
    std::map<TString, vfloat> m_eventInfoVFloats;
    std::map<TString, vint>   m_eventInfoVInts;
    bool m_declared;
    void InitMap();


  };

}
#endif // HGamAnalysisFramework_FCNCTool_H
