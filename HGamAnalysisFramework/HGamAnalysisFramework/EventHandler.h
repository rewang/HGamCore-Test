#ifndef HGamAnalysisFramework_EventHandler_H
#define HGamAnalysisFramework_EventHandler_H

#include "HGamAnalysisFramework/HgammaIncludes.h"
#include <AsgTools/AsgMessaging.h>
#include <TruthWeightTools/ITruthWeightTool.h>
#include <TruthWeightTools/HiggsWeightTool.h>
#include "TriggerAnalysisInterfaces/ITrigGlobalEfficiencyCorrectionTool.h"
#include "EgammaAnalysisInterfaces/IAsgPhotonEfficiencyCorrectionTool.h"

namespace CP {
  class VertexPositionReweightingTool;
}

namespace HG {

  class EventHandler : public asg::AsgMessaging {
  protected:
    xAOD::TEvent              *m_event;
    xAOD::TStore              *m_store;
    GoodRunsListSelectionTool *m_grl;
    asg::AnaToolHandle<CP::IPileupReweightingTool> m_pileupRW;
    asg::AnaToolHandle<CP::IPileupReweightingTool> m_pileupRWdata;
    asg::AnaToolHandle<ITrigGlobalEfficiencyCorrectionTool> m_trigGlobalTool;
    std::vector<asg::AnaToolHandle<IAsgPhotonEfficiencyCorrectionTool>> m_trigFactory;
    std::vector<asg::AnaToolHandle<CP::IPileupReweightingTool> > m_extraPileupRW;
    std::vector<TString> m_prwExtra;
    Trig::IBunchCrossingTool  *m_bunchTool;
    CP::VertexPositionReweightingTool *m_vtxRW;
    TrigConf::xAODConfigTool  *m_configTool;
    Trig::TrigDecisionTool    *m_trigDecTool;
    // Trig::TrigMuonMatching    *m_trigMuonMatchTool;
    // Trig::TrigEgammaMatchingTool *m_trigElectronMatchTool;
    asg::AnaToolHandle<Trig::IMatchingTool> m_trigMatching;
    asg::AnaToolHandle<xAOD::HiggsWeightTool> m_higgsWeightTool;
    CP::MuonTriggerScaleFactors  *m_trigMuonScaleFactors;
    AsgElectronEfficiencyCorrectionTool  *m_trigElectronScaleFactors;
    AsgElectronEfficiencyCorrectionTool  *m_trigElectronMCEfficiency;
    AsgElectronEfficiencyCorrectionTool  *m_trigDiElectronScaleFactors;
    AsgElectronEfficiencyCorrectionTool  *m_trigDiElectronMCEfficiency;
    std::map<TString, SG::AuxElement::Decorator<char>* >     m_trigDec;
    std::map<TString, SG::AuxElement::ConstAccessor<char>* > m_trigAcc;
    std::map<TString, std::set<int> > m_trigRunNumbers;

    int                        m_mcWeightIndex;
    bool                       m_forceWeights;
    bool                       m_fixSherpa224;
    bool                       m_is50ns;
    bool                       m_checkDuplic;
    bool                       m_checkGRL;
    bool                       m_checkTile;
    bool                       m_checkLAr;
    bool                       m_checkCore;
    bool                       m_checkBkg;
    bool                       m_checkVertex;
    bool                       m_checkTrig;
    bool                       m_checkSCT;
    StrV                       m_requiredTriggers;
    std::map<TString, std::vector<double>> m_trigThresholds;
    std::map<TString, TString> m_trigMatch;
    TString                    m_truthPtclName;
    float                      m_jvt;
    float                      m_prwSF;
    std::map<unsigned int, std::set<unsigned int> > m_eventNumberSet;
    TString                    m_sysName;
    double                     m_trigMatchPhotondR;
    double                     m_trigMatchElectrondR;
    double                     m_trigMatchMuondR;

  public:
    static SG::AuxElement::Decorator<unsigned int> RandomRunNumber;
    static SG::AuxElement::Decorator<float> trigSF;


  public:
    EventHandler(const char *name, xAOD::TEvent *event, xAOD::TStore *store);
    virtual ~EventHandler();

    virtual EL::StatusCode initialize(Config &config);

    // Apply systematic variation to appropriate tools
    virtual CP::SystematicCode applySystematicVariation(const CP::SystematicSet &sys);

    // Global event pass (all selections)
    bool pass();

    // DQ selection (GRL+LAr+Tile+Core) + vertex
    bool passDQ();

    // Individual event selections
    bool isDalitz();
    bool isDuplicate();
    bool passGRL(const xAOD::EventInfo *eventInfo);
    bool passTile(const xAOD::EventInfo *eventInfo);
    bool passLAr(const xAOD::EventInfo *eventInfo);
    bool passCore(const xAOD::EventInfo *eventInfo);
    bool passBackground(const xAOD::EventInfo *eventInfo);
    bool passVertex(const xAOD::EventInfo *eventInfo);
    bool passSCT(const xAOD::EventInfo *eventInfo);
    bool passTriggers();

    bool isBadBatman();
    bool eventClean_LooseBad();

    // Event weights
    xAOD::HiggsWeights higgsWeights();
    double mcWeight();
    int mcChannelNumber();
    double pileupWeight();
    double vertexWeight();
    double triggerPrescaleWeight(TString triggerList = "HLT_g10_loose", bool muDependent = true);
    double triggerPrescale(TString triggerList = "HLT_g10_loose");
    unsigned long long pileupHash();

    // Helper functions
    int numberOfPrimaryVertices();
    double selectedVertexSumPt2();
    double selectedVertexZ();
    double selectedVertexPhi();
    double hardestVertexSumPt2();
    double hardestVertexZ();
    double hardestVertexPhi();
    double pileupVertexSumPt2();
    double pileupVertexZ();
    double pileupVertexPhi();
    double eventShapeDensity();
    double mu();
    void prwApply();
    int runNumber();
    double integratedLumi();

    Trig::IBunchCrossingTool *bunchTool();
    int bunchDistanceFromFront();
    int bunchGapBeforeTrain();

    float forwardEventShapeDensity();
    float centralEventShapeDensity();

    inline bool passTrigger(const TString &trig = "");
    bool passTriggerMatch_SinglePhoton(const TString &trig, const xAOD::Photon &photon1);
    bool passTriggerMatch_DiPhoton(const TString &trig, const xAOD::Photon &photon1, const xAOD::Photon &photon2);
    bool passTriggerMatch_SingleMuon(const TString &trig, const xAOD::Muon &muon);
    bool passTriggerMatch_DiMuon(const TString &trig, const xAOD::Muon &muon1, const xAOD::Muon &muon2);
    bool passTriggerMatch_SingleElectron(const TString &trig, const xAOD::Electron &el);
    bool passTriggerMatch_DiElectron(const TString &trig, const xAOD::Electron &el1, const xAOD::Electron &el2);
    StrV getPassedTriggers();
    StrV getRequiredTriggers();
    double triggerScaleFactor(xAOD::ElectronContainer *Electrons, xAOD::MuonContainer *Muons);
    void getElectronTriggerScaleFactorTwoLeptons(xAOD::ElectronContainer &Electrons, double &trigSF);
    void getMuonTriggerScaleFactor(xAOD::MuonContainer &muons, double &trigSF);
    bool getSingleMuonEfficiency(std::vector<Double_t> &eff, xAOD::MuonContainer &mucont, const TString &trigger, Bool_t dataType);
    bool getAsymDimuonEfficiency(std::vector<Double_t> &eff, xAOD::MuonContainer &mucont, const TString &trigger, Bool_t dataType);
    float getSF_g35_medium_g25_medium_L12EM20VH(const xAOD::PhotonContainer &photons);

    template<typename T>
    void storeVar(SG::AuxElement::Decorator<T> &dec, T value);

    template<typename T>
    void storeTruthVar(const char *name, T value);

    template<typename T>
    void storeVar(const char *name, T value);

    template<typename T>
    T getTruthVar(const char *name);

    template<typename T>
    T getVar(const char *name);

    virtual EL::StatusCode writeVars(TString name = "");
    virtual EL::StatusCode writeEventInfo();

    // these are deprecated!!
    template<typename T>
    void storeVariable(const char *name, T value);

    virtual EL::StatusCode write() { return writeEventInfo(); }
  };

  //______________________________________________________________________________
  bool EventHandler::passTrigger(const TString &trig)
  {
    // Define decorator/accessor if not already available
    if (m_trigDec.find(trig) == m_trigDec.end()) {
      m_trigAcc[trig] = new SG::AuxElement::ConstAccessor<char>(("passTrig_" + trig).Data());
      m_trigDec[trig] = new SG::AuxElement::Decorator    <char>(("passTrig_" + trig).Data());
    }

    const xAOD::EventInfo *eventInfo = nullptr;

    if (m_event->retrieve(eventInfo, "EventInfo").isFailure())
    { HG::fatal("EventHandler::passTrigger() : Cannot access EventInfo"); }

    // If the decision tool is defined, use it
    if (m_trigDecTool) {
      // In case it's not checked, default to false
      (*m_trigDec[trig])(*eventInfo) = false;

      // Check if only certain runs should be used for this trigger
      if (m_trigRunNumbers.find(trig) == m_trigRunNumbers.end()) {
        (*m_trigDec[trig])(*eventInfo) = m_trigDecTool->isPassed(trig.Data());
      } else {
        if (m_trigRunNumbers.at(trig).count(runNumber()))
        { (*m_trigDec[trig])(*eventInfo) = m_trigDecTool->isPassed(trig.Data()); }
      }

      return (*m_trigDec[trig])(*eventInfo);
    }

    // If there's no decision tool, check for decision in EventInfo
    if (m_trigAcc[trig]->isAvailable(*eventInfo))
    { return (*m_trigAcc[trig])(*eventInfo); }

    Error("EventHandler::passTrigger()", "Trigger '%s' could not be checked!", trig.Data());
    fatal("TrigDecisionTool not defined. This is either an MxAOD without this trigger stored, or initialize() went wrong. Exiting.");

    // Should never get here
    return false;
  }

}

#include "HGamAnalysisFramework/EventHandler.hpp"

#endif // HGamAnalysisFramework_EventHandler_H
