#ifndef HGamAnalysisFramework_ETmissHandler_H
#define HGamAnalysisFramework_ETmissHandler_H

#define __HGamMET__
#include "HGamAnalysisFramework/HgammaHandler.h"
#undef __HGamMET__

//#include "HGamAnalysisFramework/HgammaHandler.h"

// MET EDM
#include "xAODMissingET/MissingET.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODMissingET/MissingETAuxContainer.h"

#include "METInterface/IMETMaker.h"

#include "METUtilities/METSystematicsTool.h"
#include "METUtilities/METHelpers.h"

namespace HG {

  class ETmissHandler : public HgammaHandler<xAOD::MissingET, xAOD::MissingETContainer, xAOD::MissingETAuxContainer> {

  private:
    asg::AnaToolHandle<IMETMaker> m_metMakerTool; //!
    asg::AnaToolHandle<IMETMaker> m_metMakerHardTool; //!
    met::METSystematicsTool *m_metSysTool; //!

    std::string m_assocMapName;
    std::string m_coreName;
    std::string m_assocMapHardestPVName;
    std::string m_coreHardestPVName;
    std::vector<TString> m_metCst;
    std::vector<TString> m_metTypes;

    std::string m_uncalibMuons;
    std::string m_uncalibElecs;
    std::string m_configPrefix;

    bool m_doHardVertex;
    bool m_doPFlow;

  private:
    // Hide the function which doesn't inclue objects
    virtual xAOD::MissingETContainer getCorrectedContainer();

  public:
    ETmissHandler(const char *name, xAOD::TEvent *event, xAOD::TStore *store);
    virtual ~ETmissHandler();

    virtual EL::StatusCode initialize(Config &config);

    virtual void addGhostsToJets(xAOD::JetContainer   &calibJets);

    bool calculateRefGamma(const xAOD::PhotonContainer   *photons,
                           const xAOD::MissingETAssociationMap *metMap,
                           const xAOD::MissingETContainer *coreMet,
                           xAOD::MissingETContainer *outputMet);

    bool  calculateRefElectron(const xAOD::ElectronContainer   *electrons,
                               const xAOD::MissingETAssociationMap *metMap,
                               const xAOD::MissingETContainer *coreMet,
                               xAOD::MissingETContainer *outputMet);

    bool calculateRefMuon(const xAOD::MuonContainer   *muons,
                          const xAOD::MissingETAssociationMap *metMap,
                          const xAOD::MissingETContainer *coreMet,
                          xAOD::MissingETContainer *outputMet);

    bool  calculateRefJetandSoftTerms(const xAOD::JetContainer   *jets,
                                      const xAOD::MissingETAssociationMap *metMap,
                                      const xAOD::MissingETContainer *coreMet,
                                      xAOD::MissingETContainer *outputMet,
                                      bool doJvt);

    virtual StatusCode doHardVertex(const xAOD::PhotonContainer   *photons,
                                    const xAOD::ElectronContainer *electrons,
                                    const xAOD::MuonContainer     *muons,
                                    const xAOD::JetContainer      *jets,
                                    xAOD::MissingETContainer      *outMet);

    virtual StatusCode doDiphotonVertex(const xAOD::PhotonContainer   *photons,
                                        const xAOD::ElectronContainer *electrons,
                                        const xAOD::MuonContainer     *muons,
                                        const xAOD::JetContainer      *jets,
                                        xAOD::MissingETContainer      *outMet);

    virtual xAOD::MissingETContainer getCorrectedContainer(xAOD::PhotonContainer   *photons,
                                                           xAOD::JetContainer      *jets,
                                                           xAOD::ElectronContainer *electrons,
                                                           xAOD::MuonContainer     *muons);
    virtual xAOD::MissingETContainer applySelection(xAOD::MissingETContainer &container);
    virtual CP::SystematicCode applySystematicVariation(const CP::SystematicSet &sys);

    bool    m_useHardestVertex;
  };
}

#endif // HGamAnalysisFramework_ETmissHandler_H
