#ifndef HGamAnalysisFramework_JetHandler_H
#define HGamAnalysisFramework_JetHandler_H

// EDM include(s):
#include "JetInterface/IJetUpdateJvt.h"

// Local include(s):
#include "HGamAnalysisFramework/HgammaHandler.h"

class JetUncertaintiesTool;
class JERSmearingTool;
class JERTool;
namespace InDet { class InDetTrackSelectionTool; }
class JetOriginCorrectionTool;
class BTaggingEfficiencyTool;
class BTaggingSelectionTool;
namespace CP { class JetJvtEfficiency; }

namespace HG {
  class JetHandler : public HgammaHandler<xAOD::Jet, xAOD::JetContainer, xAOD::JetAuxContainer> {
  private:
    JetCalibrationTool     *m_jetCalibTool; //!
    JetCleaningTool        *m_jetCleaning; //!
    JetUncertaintiesTool   *m_jesProvider; //!
    JERTool                *m_jerTool; //!
    JERSmearingTool        *m_jerSmear; //!
    InDet::InDetTrackSelectionTool *m_trackTool; //!
    TH2F                   *m_jvtLikelihood; //!
    asg::AnaToolHandle<IJetUpdateJvt> m_jvtTool; //!
    asg::AnaToolHandle<IJetModifier> m_fJvtTool; //!
    CP::JetJvtEfficiency   *m_jvtSF; //!
    CP::JetJvtEfficiency   *m_fjvtSF; //!
    JetOriginCorrectionTool *m_jetOriginTool; //!
    StrV                    m_bTagNames; //!
    std::map<TString, StrV> m_bTagEffs; //!
    std::map<TString, StrV> m_bTagOPs; //!
    std::map<TString, NumV> m_bTagCuts; //!
    std::map<TString, std::vector<BTaggingEfficiencyTool *> > m_bTagEffTools; //!
    std::map<TString, std::vector<BTaggingSelectionTool *> > m_bTagSelTools; //!
    TString m_bTagEVReduction;

    double  m_rapidityCut;
    double  m_ptCut;
    double  m_jvf;
    double  m_jvt;
    bool    m_doFJVT;
    TString m_defaultBJetWP;
    double  m_bTagRapidityCut;
    double  m_bTagJvfCut;
    bool    m_enableBTagging;
    bool    m_correctVertex;
    bool    m_doCleaning;
    int     m_mcIndex;
    TString m_jetAlgo;
    std::vector<double> m_jvtEff;

  public:
    JetHandler(const char *name, xAOD::TEvent *event, xAOD::TStore *store);
    virtual ~JetHandler();

    virtual EL::StatusCode initialize(Config &config);

    static SG::AuxElement::Accessor<std::vector<float> > JVF;
    static SG::AuxElement::Accessor<float> Jvt, hardVertexJvt;
    static SG::AuxElement::Accessor<float> Jvf;
    static SG::AuxElement::Accessor<float> scaleFactor;
    static SG::AuxElement::Accessor<char>  isClean;
    static SG::AuxElement::Accessor<float> DetectorEta;

    virtual xAOD::JetContainer getCorrectedContainer();
    virtual xAOD::JetContainer applySelection(xAOD::JetContainer &container);
    virtual xAOD::JetContainer applySelectionNoJvt(xAOD::JetContainer &container);
    virtual xAOD::JetContainer applySelectionNoCleaning(xAOD::JetContainer &container);
    virtual xAOD::JetContainer applyBJetSelection(xAOD::JetContainer &container, TString wp = "");
    virtual CP::SystematicCode applySystematicVariation(const CP::SystematicSet &sys);

    /// applies kinematic preselection cuts: not-in-crack + pT cut
    bool passPtEtaCuts(const xAOD::Jet *jet);

    /// corrects JVT value
    void decorateRescaledJVT(xAOD::Jet &jet);
    void recalculateJVT(xAOD::Jet &jet);
    void decorateJVTExtras(xAOD::JetContainer &jets);

    /// return JVT event weight
    float weightJvt(const xAOD::JetContainer &jets);

    /// return JVT event weight
    float weightFJvt(const xAOD::JetContainer &jets);

    /// applies kinematic preselection cuts: not-in-crack + pT cut
    bool passJVTCut(const xAOD::Jet *jet);

    /// applies kinematic preselection cuts: not-in-crack + pT cut
    bool passJVFCut(const xAOD::Jet *jet, bool useBTagCut = false);

    /// calibrates and smears a jet
    void calibrateJet(xAOD::Jet *jet);

    /// returns flavor label
    TString getFlavorLabel(xAOD::Jet *jet);

    /// returns MC generator index
    void setMCGen(TString sampleName);

    /// apply btags to jets
    void decorateBJetReco(xAOD::Jet *jet);

    /// apply truth btags to jets
    void decorateBJetTruth(xAOD::Jet *jet);

    /// apply btags to jets
    void decorateJVF(xAOD::Jet *jet);

    /// print details about photon to the screen
    static void printJet(const xAOD::Jet *gam, TString comment = "");

    /// obtain combined JVT weights
    static double multiplyJvtWeights(const xAOD::JetContainer *jets_noJvtCut);

    /// obtain combined fJVT weights
    static double multiplyFJvtWeights(const xAOD::JetContainer *jets_noJvtCut);
  };
}

#endif // HGamAnalysisFramework_JetHandler_H
