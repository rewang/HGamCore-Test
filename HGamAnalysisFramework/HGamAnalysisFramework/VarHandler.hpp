#ifndef HGamAnalysisFramework_VarHandler_HPP
#define HGamAnalysisFramework_VarHandler_HPP

#include "xAODEventInfo/EventInfo.h"

#include <stdexcept>

namespace HG {

  template <class T>
  VarBase<T>::VarBase(const std::string &name)
    : m_name(name)
    , m_truthOnly(false)
    , m_recoOnly(false)
    , m_decVal(name)
    , m_accVal(name)
  { }

  template <class T>
  VarBase<T>::~VarBase()
  { }

  template <class T>
  void
  VarBase<T>::addToStore(bool truth)
  {
    if ((m_truthOnly && !truth) || (m_recoOnly && truth)) {
      if (m_truthOnly) { throw std::runtime_error(m_name + " (truth only variable) reco value being set, throwing exception"); }

      if (m_recoOnly) { throw std::runtime_error(m_name + " (reco only variable) truth value being set, throwing exception"); }
    }

    // Decorate to the TStore
    xAOD::EventInfo *eventInfo = nullptr;

    if (truth) { eventInfo = VarHandler::getInstance()->getTruthEventInfoFromStore(); }
    else       { eventInfo = VarHandler::getInstance()->getEventInfoFromStore(); }

    // First check TEvent, if not there then calculate it
    T value;

    if (checkInEvent(value, truth)) {
      m_decVal(*eventInfo) = value;
    } else {
      m_decVal(*eventInfo) = this->calculateValue(truth);
    }

  }

  template <class T>
  T
  VarBase<T>::calculateValue(bool /* truth */)
  {
    throw std::runtime_error(m_name + " not found in file, calculateValue() not overloaded, throwing exception");
    return m_default;
  }

  template <class T>
  T
  VarBase<T>::getValue(bool truth)
  {
    // Check in TStore and TEvent first.
    // Here TStore is assumed to have the more up to date value
    T value;

    if (checkInStore(value, truth)) { return value; }

    if (checkInEvent(value, truth)) { return value; }

    // Throw exception if calculating value before setting containers
    if ((truth && !VarHandler::getInstance()->checkTruthContainers()) ||
        (!truth && !VarHandler::getInstance()->checkContainers())) {
      throw std::runtime_error(m_name + " not found in file, containers not set, throwing exception");
    }

    // Otherwise, calculate the value, add to TStore, then return
    xAOD::EventInfo *eventInfo = nullptr;

    if (truth) { eventInfo = VarHandler::getInstance()->getTruthEventInfoFromStore(); }
    else       { eventInfo = VarHandler::getInstance()->getEventInfoFromStore(); }

    m_decVal(*eventInfo) = this->calculateValue(truth);

    return m_accVal(*eventInfo);
  }

  template <class T>
  T
  VarBase<T>::operator()()
  { return this->getValue(); }

  template <class T>
  T
  VarBase<T>::truth()
  { return this->getValue(true); }

  template <class T>
  bool
  VarBase<T>::checkInEvent(T &value, bool truth)
  {
    const xAOD::EventInfo *eventInfo = nullptr;

    if (truth) { eventInfo = VarHandler::getInstance()->getTruthEventInfoFromEvent(); }
    else       { eventInfo = VarHandler::getInstance()->getEventInfoFromEvent(); }

    if (eventInfo == nullptr)
    { return false; }

    if (m_accVal.isAvailable(*eventInfo)) {
      value = m_accVal(*eventInfo);
      return true;
    }

    return false;
  }

  template <class T>
  bool
  VarBase<T>::checkInStore(T &value, bool truth)
  {
    xAOD::EventInfo *eventInfo = nullptr;

    if (truth) { eventInfo = VarHandler::getInstance()->getTruthEventInfoFromStore(false); }
    else       { eventInfo = VarHandler::getInstance()->getEventInfoFromStore(false); }

    if (eventInfo == nullptr)
    { return false; }

    if (m_accVal.isAvailable(*eventInfo)) {
      value = m_accVal(*eventInfo);
      return true;
    }

    return false;
  }

  template <class T>
  void
  VarBase<T>::setValue(const T &value)
  {
    if (m_truthOnly) {
      throw std::runtime_error(m_name + " (truth only variable) reco value being set, throwing exception");
    }

    // Decorate to the TStore
    xAOD::EventInfo *eventInfo = VarHandler::getInstance()->getEventInfoFromStore();

    if (eventInfo == nullptr) { return; }

    m_decVal(*eventInfo) = value;
  }

  template <class T>
  void
  VarBase<T>::setTruthValue(const T &value)
  {
    if (m_recoOnly) {
      throw std::runtime_error(m_name + " (reco only variable) truth value being set, throwing exception");
    }

    // Decorate to the TStore
    xAOD::EventInfo *eventInfo = VarHandler::getInstance()->getTruthEventInfoFromStore();

    if (eventInfo == nullptr) { return; }

    m_decVal(*eventInfo) = value;
  }

  template <class T>
  bool
  VarBase<T>::exists()
  {
    T value;

    if (checkInStore(value)) { return true; }

    if (checkInEvent(value)) { return true; }

    return false;
  }

  template <class T>
  bool
  VarBase<T>::truthExists()
  {
    T value;

    if (checkInStore(value, true)) { return true; }

    if (checkInEvent(value, true)) { return true; }

    return false;
  }

}

#endif // HGamAnalysisFramework_VarHandler_HPP
