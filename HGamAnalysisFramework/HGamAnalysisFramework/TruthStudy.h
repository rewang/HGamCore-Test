#ifndef TRUTHSTUDY_H
#define TRUTHSTUDY_H

#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthParticleContainer.h"

#include <set>
#include <vector>

class TruthStudy {
public:
  // static TruthStudy& Instance() { return m_instance; } ;

  void Init(const xAOD::TruthParticleContainer *tpc, unsigned long long evt = 0);
  bool Init() { return m_Done[2]; };

  struct Dens {
    double rho, sig, area, nejet, eta;
    int njet, ieta;
  };
  struct Iso {
    double ptcone[3];
    double etcone[3];
    double partonetcone[3];
  };
  Iso TruthIso(const xAOD::TruthParticle *);
  std::pair<int, std::vector<double> > SherpaClass();
  Dens GetTrueDensity(double eta, int typePart = 1, int typeEta = 0);

  TruthStudy()           { m_trkPtCut = 1.; m_Done[0] = false; m_Done[1] = false; m_Done[2] = false; };
  TruthStudy(double pt)  { m_trkPtCut = pt; m_Done[0] = false; m_Done[1] = false; m_Done[2] = false; };
  ~TruthStudy() {};

private:

  // static TruthStudy m_instance;
  void GetTrueParticles();
  void GetTrueDensities();
  std::set<const xAOD::TruthParticle *> m_allPartons;
  std::set<const xAOD::TruthParticle *> m_allParticles;
  std::set<const xAOD::TruthParticle *> m_allHardPartons;
  std::vector<Dens> m_partonDensityVec, m_particleDensityVec;
  bool m_Done[3];
  double m_trkPtCut;
  unsigned long long m_evt;
  std::set<unsigned long long> m_EvtToDebug;
  const xAOD::TruthParticleContainer *m_truthParticles;

};
#endif
