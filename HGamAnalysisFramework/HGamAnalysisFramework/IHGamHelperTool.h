#ifndef HGamAnalysisFramework_IHGamHelperTool_h
#define HGamAnalysisFramework_IHGamHelperTool_h

// ASG framework includes
#include "AsgTools/IAsgTool.h"

// EDM includes
#include "xAODEgamma/PhotonContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODTruth/TruthParticleContainer.h"

namespace HG {

  class IHGamHelperTool : public virtual asg::IAsgTool {

    /// Declare the interface that the class provides
    ASG_TOOL_INTERFACE(HG::IHGamHelperTool)

  public:
    /// Save all variables only needed for systematic shifts
    virtual StatusCode saveSystematicVariables(xAOD::PhotonContainer    *photons   = nullptr,
                                               xAOD::ElectronContainer  *electrons = nullptr,
                                               xAOD::MuonContainer      *muons     = nullptr,
                                               xAOD::JetContainer       *jets      = nullptr,
                                               xAOD::MissingETContainer *mets      = nullptr) = 0;

    /// Save all standard variables not included in systematic subset
    virtual StatusCode saveStandardVariables(xAOD::PhotonContainer    *photons   = nullptr,
                                             xAOD::ElectronContainer  *electrons = nullptr,
                                             xAOD::MuonContainer      *muons     = nullptr,
                                             xAOD::JetContainer       *jets      = nullptr,
                                             xAOD::MissingETContainer *mets      = nullptr) = 0;

    /// Save extra variables necessary for possible detailed studies
    virtual StatusCode saveDetailedVariables(xAOD::PhotonContainer    *photons   = nullptr,
                                             xAOD::ElectronContainer  *electrons = nullptr,
                                             xAOD::MuonContainer      *muons     = nullptr,
                                             xAOD::JetContainer       *jets      = nullptr,
                                             xAOD::MissingETContainer *mets      = nullptr) = 0;

    /// Save extra variables necessary for possible detailed studies
    virtual StatusCode saveTruthVariables(xAOD::TruthParticleContainer *photons   = nullptr,
                                          xAOD::TruthParticleContainer *electrons = nullptr,
                                          xAOD::TruthParticleContainer *muons     = nullptr,
                                          xAOD::JetContainer           *jets      = nullptr,
                                          xAOD::MissingETContainer     *mets      = nullptr) = 0;

  }; // class IHGamHelperTool

} // namespace HG


#endif // HGamAnalysisFramework_IHGamHelperTool_h
