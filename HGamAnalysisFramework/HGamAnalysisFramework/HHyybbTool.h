#ifndef HGAMANALYSISFRAMEWORK_HHYYBBTOOL_H
#define HGAMANALYSISFRAMEWORK_HHYYBBTOOL_H

// STL includes
#include <vector>
// ATLAS/ROOT includes
#include <AsgTools/AsgMessaging.h>
#include <TLorentzVector.h>
#include <TString.h>


// Forward declarations
namespace TMVA { class Reader; }
namespace HG { class EventHandler; class TruthHandler; }
namespace xAOD { class hhWeightTool; }

namespace HG {
  /*!
     @class HHyybbTool
     @brief The main class for the HH2yybb analysis.

     This tool, included in the HGamAnalysisFramework, contains all the functions designed
     specifically for the HH2yybb analysis.
   */
  class HHyybbTool : public asg::AsgMessaging {
  public:
    /**
     * @brief Constructor
     * @param eventHandler The HGam eventHandler to write information to EventInfo
     * @param truthHandler The HGam truthHandler to read truth information
     */
    HHyybbTool(const char *name, HG::EventHandler *eventHandler, HG::TruthHandler *truthHandler, const bool &isMC);

    /*! Destructor, deallocates resources used by class instance. */
    virtual ~HHyybbTool();

    /*!
       @fn virtual EL::StatusCode initialize(Config &config)
       @brief Set or fetch default values for members from config
       @param config
     */
    virtual EL::StatusCode initialize(Config &config);

    /*!
       @enum muCorrTypeEnum
       @brief Muon correction category enumeration
       @var allMu
       @brief 0 = correct the jet's 4v using all muons in it (i.e. with dR < 0.4)
       @var clMu
       @brief 1 = correct the jet's 4v using only the muon that is closest to the jet axis (and dR < 0.4)
       @var pTMu
       @brief 2 = correct the jet's 4v using only the highest pT muon (and dR < 0.4)
     */
    enum muCorrTypeEnum {allMu = 0, clMu = 1, pTMu = 2, none = 3};

    /*!
       @enum bTagCatEnum
       @brief Number of jets in the event that pass the b-tagging working point cut
       @var noTag
       @brief -1 = there are < 2 jets in the event
       @var zeroTag
       @brief 0  = there are at least 2 jets in the event, all of which are untagged
       @var oneTag
       @brief 1  = there are at least 2 jets in the event, only one of them is b-tagged
       @var twoTag
       @brief 2  = there are at least 2 jets in the event, two of them are b-tagged
     */
    enum bTagCatEnum {noTag = -1, zeroTag = 0, oneTag = 1, twoTag = 2};

    /*!
       @enum massCatEnum
       @brief Which set of mass/pT/etc. cuts to use
       @var lowMass
       @brief 0 = use the low-mass selection
       @var highMass
       @brief 1 = use the high-mass selection
     */
    enum massCatEnum {lowMass = 0, highMass = 1};

    /*!
       @enum yybbCutflowEnum
       @brief Latest step in the cutflow that is passed by the event
     */
    enum yybbCutflowEnum {CENJETS = 0, TAGGING = 1, BPT = 2, BBMASS = 3, YYMASS = 4, PASSYYBB = 5};

    /*!
       \fn void decorateMuonCorrections(xAOD::JetContainer &jets, xAOD::MuonContainer muons)
       \brief Decorate muons to jets
     */
    void decorateMuonCorrections(xAOD::JetContainer &jets, xAOD::MuonContainer muons);

    /*!
       \fn void getMuonCorrJet4Vs(xAOD::JetContainer &canJets, HHyybbTool::muCorrTypeEnum corrType = allMu)
       \brief Returns the TLorentzVectors of jets with muon correction applied

     */
    std::vector<TLorentzVector> getMuonCorrJet4Vs(xAOD::JetContainer &canJets, HHyybbTool::muCorrTypeEnum corrType = allMu);

    /**
     * @brief Function to save all the yybb information (Nominal by default)
     * @details
     *
     * @param photons Container with all photons
     * @param muons Container with all muons
     * @param jets Container with all jets
     */
    void saveHHyybbInfo(xAOD::PhotonContainer photons, xAOD::MuonContainer muons, xAOD::JetContainer &jets, xAOD::JetContainer &jets_noJVT);

  private:
    HG::EventHandler *m_eventHandler; //!
    HG::TruthHandler *m_truthHandler; //!
    xAOD::hhWeightTool *m_hhWeightTool = nullptr;

    // Algorithm setup
    bool m_detailedHHyybb;       //! Are we running a fully detailed HHyybb analysis?
    TString m_MVA_xml_low_mass;  //! Which XML file should be used for categorising 1-tag events (low-mass)?
    TString m_MVA_xml_high_mass; //! Which XML file should be used for categorising 1-tag events (high-mass)?
    TString m_1BTagWP;           //! Which b-tagging WP should be used for 1-tag events?
    TString m_2BTagWP;           //! Which b-tagging WP should be used for 2-tag events?

    // User-configurable: jet selection and muon corrections
    muCorrTypeEnum m_muCorrType; //! Which muon correction to use
    double m_minMuonPT;          //! Minimum pT of muons
    double m_maxMuonJetDR;       //! Maximum deltaR between muons and the jet
    float m_jet_eta_max;         //! Maximum eta of jets

    // Low-mass selection
    float m_bJet1_pTMin_low_mass; //! (in GeV)
    float m_bJet2_pTMin_low_mass; //! (in GeV)
    float m_mbb_min_low_mass;     //! lower bound of mbb window (in GeV)
    float m_mbb_max_low_mass;     //! upper bound of mbb window (in GeV)

    // High-mass selection
    float m_bJet1_pTMin_high_mass; //! (in GeV)
    float m_bJet2_pTMin_high_mass; //! (in GeV)
    float m_mbb_min_high_mass;     //! lower bound of mbb window (in GeV)
    float m_mbb_max_high_mass;     //! upper bound of mbb window (in GeV)

    // yy mass selections
    float m_myy_min_low_mass;                    //! lower bound of tight myy window (in GeV) -- low mass
    float m_myy_max_low_mass;                    //! upper bound of tight myy window (in GeV) -- low mass
    float m_myy_min_high_mass;                   //! lower bound of tight myy window (in GeV) -- high mass
    float m_myy_max_high_mass;                   //! upper bound of tight myy window (in GeV) -- high mass
    static constexpr float m_massHiggs = 125.09; //! what we consider the exact Higgs mass to be (in GeV)

    // MVA reader variables
    TMVA::Reader *m_reader_low_mass = nullptr; //!
    TMVA::Reader *m_reader_high_mass = nullptr; //!
    float m_abs_eta_j; //!
    float m_abs_eta_jb; //!
    float m_Delta_eta_jb; //!
    float m_idx_by_mH; //!
    float m_idx_by_pT; //!
    float m_idx_by_pT_jb; //!
    float m_jb; //!
    float m_m_jb; //!
    float m_passes_WP77; //!
    float m_passes_WP85; //!
    float m_pT_j; //!
    float m_pT_jb; //!
    int nEventsOneTagJetChanged; //!
    int nEventsOneTag; //!

    /**
     * @brief Accessor for event handler
     */
    virtual HG::EventHandler *eventHandler();

    /**
     * @brief Accessor for truth handler
     */
    virtual HG::TruthHandler *truthHandler();

    /*!
       \fn void saveMapsToEventInfo(std::map<TString, float> &eventInfoFloats, std::map<TString,char> &eventInfoChars, std::map<TString,int> &eventInfoInts)
       \brief Write maps to eventInfo
     */
    void saveMapsToEventInfo(std::map<TString, float> &eventInfoFloats, std::map<TString, char> &eventInfoChars, std::map<TString, int> &eventInfoInts);

    /**
     * @brief Function which performs full yybb selection and saves info to info maps (nominal by default)
     * @param photons Container with all photons
     * @param muons Container with all muons
     * @param jets Container with all jets
     * @param eventInfoFloats Map which has selected float info appended to it for saving to file
     * @param eventInfoChars Map which has selected chars info appended to it for saving to file
     * @param eventInfoInts Map which has selected ints info appended to it for saving to file
     * @param label String to include in the addtional information
     */
    void performSelection(xAOD::PhotonContainer photons,
                          xAOD::JetContainer &jets,
                          xAOD::JetContainer &jets_noJVT,
                          std::map<TString, float> &eventInfoFloats,
                          std::map<TString, char> &eventInfoChars,
                          std::map<TString, int> &eventInfoInts,
                          HHyybbTool::massCatEnum massCat);

    /*!
       \fn void HHyybbTool::muonCorrection(xAOD::Jet *jet, xAOD::MuonContainer muons, HHyybbTool::muCorrTypeEnum corrType)
       \brief Calls the selected muon correction strategy, saves results of correction in jet branches ending in `_muonsInJ`
       \param jet The Jet object under consideration.
       \param muons The muon container with all muons for that event.
       \param corrType The muon correction strategy to be applied (options: allMu, clMu, pTMu)
     */
    void decorateMuonCorrectionToJet(xAOD::Jet *jet, xAOD::MuonContainer muons, HHyybbTool::muCorrTypeEnum corrType);

    /*!
       \fn void decorateWithMVAInputs(xAOD::Jet* bJet, xAOD::JetContainer& nonbJets)
       \brief Decorates jets with kinematic inputs needed for MVA
       \param bJet The b-tagged Jet object under consideration.
       \param nonbJets The jet container with all non-tagged jets for that event.
     */
    void decorateWithMVAInputs(xAOD::Jet *bJet, xAOD::JetContainer &nonbJets);

    /*!
       \fn double weightBTagging(xAOD::JetContainer &canJets, TString bTagWorkPoint)
       \brief Fetch flavour tagging SF
     */
    double weightBTagging(xAOD::JetContainer &canJets, HHyybbTool::bTagCatEnum bTagCat, const bool &useDiscreteSFs = false);

    /*!
       \fn double weightJVT(HHyybbTool::massCatEnum massCat, xAOD::JetContainer &jets_noJVT)
       \brief Fetch JVT (in)efficiency SF
       \param massCat The mass category.
       \param jets_noJVT The jet container for jets before JVT is applied.
     */
    double weightJVT(HHyybbTool::massCatEnum massCat, xAOD::JetContainer &jets_noJVT);

    /*!
       \fn HHyybbTool::weightNLO()
       \brief do truth hh reweighting to NLO where appropriate
     */
    double weightNLO();

    /*!
       \fn HHyybbTool::bTagCatEnum getBTagCategory(xAOD::JetContainer jets_passing_2tag_WP, xAOD::JetContainer jets_passing_1tag_WP, xAOD::JetContainer jets_failing_1tag_WP)
       \brief Figure out which b-tag category this event belongs to
     */
    bTagCatEnum getBTagCategory(xAOD::JetContainer jets_passing_2tag_WP, xAOD::JetContainer jets_passing_1tag_WP, xAOD::JetContainer jets_failing_1tag_WP);

    /*!
       \fn xAOD::JetContainer getCandidateJets(xAOD::JetContainer jets_passing_2tag_WP, xAOD::JetContainer jets_passing_1tag_WP, xAOD::JetContainer allUntaggedJets, HHyybbTool::bTagCatEnum bTagCat)
       \brief
     */
    xAOD::JetContainer getCandidateJetsWithMVAScore(xAOD::JetContainer jets_passing_2tag_WP, xAOD::JetContainer jets_passing_1tag_WP, xAOD::JetContainer jets_failing_1tag_WP, xAOD::JetContainer jets_central, HHyybbTool::bTagCatEnum bTagCat, HHyybbTool::massCatEnum massCat, float &MVA_score);

    /*!
       \fn yybbCutflowEnum getHHyybbCutflow(xAOD::PhotonContainer selPhotons, std::vector<TLorentzVector> jet4Vs, HHyybbTool::bTagCatEnum bTagCat, float pt1, float pt2)
       \brief Figure out what's the latest step in the cutflow that this event passes
     */
    yybbCutflowEnum getHHyybbCutflow(xAOD::PhotonContainer selPhotons, std::vector<TLorentzVector> jet4Vs, HHyybbTool::bTagCatEnum bTagCat, HHyybbTool::massCatEnum massCat);

    /*!
       \fn void getMuonCorrJet4Vs(xAOD::Jet* jet, HHyybbTool::muCorrTypeEnum corrType = allMu)
       \brief Returns the TLorentzVector of a jet with muon correction applied

     */
    TLorentzVector getMuonCorrJet4V(xAOD::Jet *jet, HHyybbTool::muCorrTypeEnum corrType);

    /*!
       \fn TString getMuonCorrPrefix(muCorrTypeEnum corrType
       \brief Fetch prefix for muon correction type
     */
    TString getMuonCorrPrefix(muCorrTypeEnum corrType);

    /*!
       \fn void evaluateOneTagMVAs(xAOD::Jet *bJet, xAOD::JetContainer untaggedJets);
       \brief Evaluate 1-tag MVA for all untagged jets.
     */
    void evaluateOneTagMVAs(xAOD::Jet *bJet, xAOD::JetContainer untaggedJets);

    /*!
       \fn void evaluateOutputKinematicQuantities(xAOD::PhotonContainer selPhotons, xAOD::JetContainer canJets, std::map<TString, float> &eventInfoFloats)
       \brief Set four object information to maps
     */
    void evaluateOutputKinematicQuantities(xAOD::PhotonContainer selPhotons, std::vector<TLorentzVector> corrJets4V, std::vector<TLorentzVector> unCorrJets4V, std::map<TString, float> &eventInfoFloats, TString label = "");

    /*!
       \fn TLorentzVector muonCorrectionClosest(xAOD::Jet *jet, xAOD::MuonContainer muons, int &muonInJCount)
       \brief Get correction from closest muon to the jet among those that overlap with the jet itself
     */
    TLorentzVector muonCorrectionClosest(xAOD::Jet *jet, xAOD::MuonContainer muons);

    /*!
       \fn TLorentzVector muonCorrectionHighestPt(xAOD::Jet *jet, xAOD::MuonContainer muons, int &muonInJCount)
       \brief Get correction from highest pT muon which overlaps with jet
     */
    TLorentzVector muonCorrectionHighestPt(xAOD::MuonContainer muons);

    /*!
       \fn TLorentzVector muonCorrectionAll(xAOD::Jet *jet, xAOD::MuonContainer muons, int &muonInJCount)
       \brief Get correction from all muons which overlap with jet
     */
    TLorentzVector muonCorrectionAll(xAOD::MuonContainer muons);
  };

}
#endif // HGAMANALYSISFRAMEWORK_HHYYBBTOOL_H
