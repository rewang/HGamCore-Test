#ifndef HGAMANALYSISFRAMEWORK_HGAMVLQTOOL_H
#define HGAMANALYSISFRAMEWORK_HGAMVLQTOOL_H

// STL includes
#include <vector>
// ATLAS/ROOT includes
#include <AsgTools/AsgMessaging.h>
#include <TLorentzVector.h>
#include <TString.h>


// Forward declarations
namespace HG { class EventHandler; class TruthHandler; }

namespace HG {
  /*!
     @class HGamVLQTool
     @brief The main class for the B->bH->byy VLQ analysis.

     This tool, included in the HGamAnalysisFramework, contains all the functions designed
     specifically for the B->bH->byy VLQ analysis.
   */
  class HGamVLQTool : public asg::AsgMessaging {
  public:
    /**
     * @brief Constructor
     * @param eventHandler The HGam eventHandler to write information to EventInfo
     * @param truthHandler The HGam truthHandler to read truth information
     */
    HGamVLQTool(const char *name, HG::EventHandler *eventHandler, HG::TruthHandler *truthHandler, const bool &isMC);

    /*! Destructor, deallocates resources used by class instance. */
    virtual ~HGamVLQTool();

    /*!
       @fn virtual EL::StatusCode initialize(Config &config)
       @brief Set or fetch default values for members from config
       @param config
     */
    virtual EL::StatusCode initialize(Config &config);

    /*!
       @enum yybCutflowEnum
       @brief Latest step in the cutflow that is passed by the event
     */
    enum yybCutflowEnum {PHOTONS = 0, LEPVETO = 1, CENJET = 2, BTAG = 3, FORWARDJET = 4, YYBMASS = 5, YSTAR = 6, PASSYYB = 7};
    enum btagWP {WP100 = 0, WP85 = 1, WP77 = 2, WP70 = 3, WP60 = 4}; // WP100 corresponds to untagged
    enum m_yy_cat {SIDEBANDS = 0, VR = 1, SR = 2}; // m_yy in sidebands, validation region, signal region

    /**
     * @brief Function to save all the yyb information
     * @details
     *
     * @param photons Container with all photons
     * @param jets Container with all jets
     * @param jets_noJVT Container with all jets, no JVT cut
     * @param muons Container with all muons
     * @param electron Container with all electrons
     */
    void saveHGamVLQInfo(xAOD::PhotonContainer photons,
                         xAOD::JetContainer &jets,
                         xAOD::JetContainer &jets_noJVT,
                         xAOD::MuonContainer &muons,
                         xAOD::ElectronContainer &electrons);

  private:
    HG::EventHandler *m_eventHandler; //!
    HG::TruthHandler *m_truthHandler; //!

    // Algorithm setup
    TString m_BTagWP;           //! Which b-tagging WP should be used?

    // User-configurable selection options;
    double m_minMuonPT;          //! Minimum pT of muons for lepton veto (in GeV)
    double m_minElectronPT;      //! Minimum pT of electrons for lepton veto (in GeV)
    float m_centraljet_eta_max;  //! Maximum eta of central jets
    float m_centraljet_pt_min;   //! Minimum  pt of central jets (in GeV)
    float m_forwardjet_eta_max;  //! Maximum eta of forward jets
    float m_forwardjet_eta_min;  //! Minimum eta of forward jets
    float m_forwardjet_pt_min;   //! Maximum pt of foeward jets (in GeV)
    float m_yyb_min;             //! Minimum yyb mass to pass cut (in GeV)
    float m_ystar_max;           //! Maximum y*
    bool m_pt_priority_bjet;      //! Prioritize highest pt for btagged jet (passing WP) as opposed to first using "tightest btag WP". Use an integer
    static constexpr float m_massHiggs = 125.09; //! what we consider the exact Higgs mass to be (in GeV)

    btagWP btagWPMin; //! WP that is the min that we require

    /**
     * @brief Accessor for event handler
     */
    virtual HG::EventHandler *eventHandler();

    /**
     * @brief Accessor for truth handler
     */
    virtual HG::TruthHandler *truthHandler();

    /*!
       \fn void saveMapsToEventInfo(std::map<TString, float> &eventInfoFloats, std::map<TString,int> &eventInfoInts)
       \brief Write maps to eventInfo
     */
    void saveMapsToEventInfo(std::map<TString, float> &eventInfoFloats, std::map<TString, int> &eventInfoInts);

    /*!
       \fn void performSelection(xAOD::PhotonContainer photons, xAOD::JetContainer& jets, xAOD::JetContainer& jets_noJVT, xAOD::MuonContainer& muons, xAOD::ElectronContainer& electrons, std::map<TString, float> &eventInfoFloats, std::map<TString, int> &eventInfoInts);
       \brief Function which performs full yyb selection and saves info to info maps (nominal by default)
       \param photons Container with all photons
       \param jets Container with all jets
       \param jets_noJVT Container with all jets and no JVT cut
       \param muons Container with all muons
       \param electron Container with all electrons
       \param eventInfoFloats Map which has selected float info appended to it for saving to file
       \param eventInfoInts Map which has selected ints info appended to it for saving to file
       \param label String to include in the addtional information
     */
    void performSelection(xAOD::PhotonContainer photons,
                          xAOD::JetContainer &jets,
                          xAOD::JetContainer &jets_noJVT,
                          xAOD::MuonContainer &muons,
                          xAOD::ElectronContainer &electrons,
                          std::map<TString, float> &eventInfoFloats,
                          std::map<TString, int> &eventInfoInts);


    /*!
       \fn double weightBTagging(const xAOD::Jet *jet);

       \brief Fetch flavour tagging SF
     */
    double weightBTagging(const xAOD::Jet *jet);

    /*!
       \fn double weightJVT(HGamVLQTool::massCatEnum massCat, xAOD::JetContainer &jets_noJVT)
       \brief Fetch JVT (in)efficiency SF
       \param jets_noJVT The jet container for jets before JVT is applied.
     */
    double weightJVT(xAOD::JetContainer &jets_noJVT);

  };

}
#endif // HGAMANALYSISFRAMEWORK_HGAMVLQTOOL_H
