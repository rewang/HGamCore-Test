#ifndef HGamAnalysisFramework_HGamCategoryTool_H
#define HGamAnalysisFramework_HGamCategoryTool_H

#include "HGamAnalysisFramework/EventHandler.h"
#include "HGamAnalysisFramework/HgammaAnalysis.h"
#include <EventLoop/Worker.h>
#include "HGamAnalysisFramework/HgammaHandler.h"
#include "HGamAnalysisFramework/PhotonHandler.h"
#include "HGamAnalysisFramework/HgammaIncludes.h"

namespace TMVA {
  class Reader;
}

// Taken from xgboost/c_api.h, mut be identical!
typedef void *BoosterHandle;

namespace HG {

  class HGamCategoryTool {
  protected:
    xAOD::TEvent              *m_event;
    xAOD::TStore              *m_store;

    TMVA::Reader              *m_readerVBF_low; //!
    TMVA::Reader              *m_readerVBF_high; //!
    TMVA::Reader              *m_readerVH_had_Moriond2017; //!
    TMVA::Reader              *m_reader_ttHhad; //!
    TMVA::Reader              *m_reader_ttHlep; //!

    BoosterHandle             *m_xgboost_ttHhad; //!
    BoosterHandle             *m_xgboost_ttHlep; //!
    BoosterHandle             *m_xgboost_topreco; //!
    BoosterHandle             *m_xgboost_ttHhad_topReco; //!
    BoosterHandle             *m_xgboost_ttHlep_topReco; //!

    float t_pTt_yyGeV;
    float t_m_jjGeV;
    float t_pTt_yy;
    float t_m_jj;
    float t_dEta_jj;
    float t_dPhi_yy_jj;
    float t_Zepp;
    float t_Drmin_y_j;
    float t_Drmin_y_j2;
    float t_Dy_yy_jj;
    float t_cosTS_yy_jj;
    float t_eta_y1;
    float t_eta_y2;
    float t_m_alljet_30;
    float t_HT_30;
    float t_METsig;
    float t_N_j30;
    float t_N_j_central30;
    float t_N_bjet30;
    float t_pTlepMET;
    float t_massT;

    double m_weight;
    double m_score;

  protected:
    double getLeptonSFs(const xAOD::ElectronContainer *electrons,
                        const xAOD::MuonContainer     *muons);

    int  Passes_VBF_Moriond2017(const xAOD::JetContainer *jets);

    bool Passes_VH_hadronic_Moriond2017(const xAOD::JetContainer *jetsJVT);

    int Passes_VH_MET_Moriond2017(const xAOD::PhotonContainer    *photons,
                                  const xAOD::MissingETContainer *met);

    int  Passes_VH_leptonic_Moriond2017(const xAOD::PhotonContainer    *photons,
                                        const xAOD::ElectronContainer  *electrons,
                                        const xAOD::MuonContainer      *muons,
                                        const xAOD::MissingETContainer *met);

    int  Passes_VH_dileptons_Moriond2017(const xAOD::PhotonContainer    *photons,
                                         const xAOD::ElectronContainer  *electrons,
                                         const xAOD::MuonContainer      *muons);


    int split_ggH_STXS(const xAOD::PhotonContainer *photons, const xAOD::JetContainer *jetsJVT);

    int Passes_ttH_Moriond(const xAOD::PhotonContainer     *photons,
                           const xAOD::ElectronContainer   *electrons,
                           const xAOD::MuonContainer       *muons,
                           const xAOD::JetContainer        *jets,
                           const xAOD::JetContainer        *jetsJVT,
                           const xAOD::MissingETContainer  *met);

    int Passes_ttHBDT_Moriond(const xAOD::PhotonContainer     *photons,
                              const xAOD::ElectronContainer   *electrons,
                              const xAOD::MuonContainer       *muons,
                              const xAOD::JetContainer        *jets,
                              const xAOD::JetContainer        *jetsJVT,
                              const xAOD::MissingETContainer  *met);

    int Passes_ttHBDTlep_Moriond(const xAOD::PhotonContainer     *photons,
                                 const xAOD::ElectronContainer   *electrons,
                                 const xAOD::MuonContainer       *muons,
                                 const xAOD::JetContainer        *jets,
                                 const xAOD::JetContainer        *jetsJVT,
                                 const xAOD::MissingETContainer  *met);

    int Passes_XGBoost_ttH(const xAOD::PhotonContainer     *photons,
                           const xAOD::ElectronContainer   *electrons,
                           const xAOD::MuonContainer       *muons,
                           const xAOD::JetContainer        *jets,
                           const xAOD::JetContainer        *jetsJVT,
                           const xAOD::MissingETContainer  *met);


    int Passes_XGBoost_ttH_topReco(const xAOD::PhotonContainer    *photons,
                                   const xAOD::ElectronContainer  *electrons,
                                   const xAOD::MuonContainer      *muons,
                                   const xAOD::JetContainer       *jets,
                                   const xAOD::JetContainer       *jetsJVT,
                                   const xAOD::MissingETContainer *met);

    float getMVAWeight(TMVA::Reader *XReader);
    void resetReader();

    bool Passes_qqH_BSM(const xAOD::JetContainer  *jetsJVT);
    bool Passes_qqH_BSM_2jet(const xAOD::JetContainer  *jetsJVT);

    float *make_XGBoost_DMatrix_ttHhad(const xAOD::PhotonContainer    *photons,
                                       const xAOD::JetContainer       *jets,
                                       const xAOD::MissingETContainer *met);
    float *make_XGBoost_DMatrix_ttHlep(const xAOD::PhotonContainer    *photons,
                                       const xAOD::ElectronContainer  *electrons,
                                       const xAOD::MuonContainer      *muons,
                                       const xAOD::JetContainer       *jets,
                                       const xAOD::MissingETContainer *met);
    float *make_XGBoost_DMatrix_ttHhad_topReco(const xAOD::PhotonContainer *photons,
                                               const xAOD::JetContainer *jets,
                                               const xAOD::MissingETContainer *met);
    float *make_XGBoost_DMatrix_ttHlep_topReco(const xAOD::PhotonContainer    *photons,
                                               const xAOD::ElectronContainer  *electrons,
                                               const xAOD::MuonContainer      *muons,
                                               const xAOD::JetContainer       *jets,
                                               const xAOD::MissingETContainer *met);
    float get_XGBoost_Weight(BoosterHandle *boost, float *vars, int len);
    float get_pseudoCont_score(double btagscore);

    void decorateTopCandidate(const xAOD::JetContainer *jets);

  public:
    HGamCategoryTool(xAOD::TEvent *event, xAOD::TStore *store);
    virtual ~HGamCategoryTool();

    virtual EL::StatusCode initialize(Config &config);

    // Use below for splitting ggH categories
    //enum HGamCatIndex { FailDiphoton=-1, ggH_CenLow=1, ggH_CenHigh=2, ggH_FwdLow=3, ggH_FwdHigh=4, VBFloose=5, VBFtight=6, VHhad_loose=7, VHhad_tight=8, VHMET=9, VHlep=10, VHdilep=11, ttHhad=12, ttHlep=13 };

    enum HGamM17Index { M17_FailDiphoton = -1, M17_ggH_0J_Cen = 1, M17_ggH_0J_Fwd = 2, M17_ggH_1J_LOW = 3, M17_ggH_1J_MED = 4, M17_ggH_1J_HIGH = 5, M17_ggH_1J_BSM = 6, M17_ggH_2J_LOW = 7, M17_ggH_2J_MED = 8, M17_ggH_2J_HIGH = 9, M17_ggH_2J_BSM = 10, M17_VBF_HjjLOW_loose = 11, M17_VBF_HjjLOW_tight = 12, M17_VBF_HjjHIGH_loose = 13, M17_VBF_HjjHIGH_tight = 14, M17_VHhad_loose = 15, M17_VHhad_tight = 16, M17_qqH_BSM = 17, M17_VHMET_LOW = 18, M17_VHMET_HIGH = 19, M17_VHMET_BSM = 20, M17_VHlep_LOW = 21, M17_VHlep_HIGH = 22, M17_VHdilep_LOW = 23, M17_VHdilep_HIGH = 24, M17_ttH = 25 };

    std::pair<int, float> getCategoryAndWeightMoriond2017_ttHBDT_qqH2jet(
      const xAOD::PhotonContainer    *photons,
      const xAOD::ElectronContainer  *electrons,
      const xAOD::MuonContainer      *muons,
      const xAOD::JetContainer       *jets,
      const xAOD::JetContainer       *jetsJVT,
      const xAOD::MissingETContainer *met);

    std::pair<int, float> getCategoryAndWeightMoriond2017_ttHBDT(
      const xAOD::PhotonContainer    *photons,
      const xAOD::ElectronContainer  *electrons,
      const xAOD::MuonContainer      *muons,
      const xAOD::JetContainer       *jets,
      const xAOD::JetContainer       *jetsJVT,
      const xAOD::MissingETContainer *met);

    std::pair<int, float> getCategoryAndWeightMoriond2017_ttHBDTlep(
      const xAOD::PhotonContainer    *photons,
      const xAOD::ElectronContainer  *electrons,
      const xAOD::MuonContainer      *muons,
      const xAOD::JetContainer       *jets,
      const xAOD::JetContainer       *jetsJVT,
      const xAOD::MissingETContainer *met);

    std::pair<int, float> getCategoryAndWeightMoriond2017(const xAOD::PhotonContainer    *photons,
                                                          const xAOD::ElectronContainer  *electrons,
                                                          const xAOD::MuonContainer      *muons,
                                                          const xAOD::JetContainer       *jets,
                                                          const xAOD::JetContainer       *jetsJVT,
                                                          const xAOD::MissingETContainer *met);

    float *getCategoryAndWeightXGBoost_ttH(const xAOD::PhotonContainer    *photons,
                                           const xAOD::ElectronContainer  *electrons,
                                           const xAOD::MuonContainer      *muons,
                                           const xAOD::JetContainer       *jets,
                                           const xAOD::JetContainer       *jetsJVT,
                                           const xAOD::MissingETContainer *met);

    float *getCategoryAndWeightXGBoost_ttH_topReco(const xAOD::PhotonContainer    *photons,
                                                   const xAOD::ElectronContainer  *electrons,
                                                   const xAOD::MuonContainer      *muons,
                                                   const xAOD::JetContainer       *jets,
                                                   const xAOD::JetContainer       *jetsJVT,
                                                   const xAOD::MissingETContainer *met);

    int getMassCategoryRun1(const xAOD::PhotonContainer * /*photons*/);
    int getEtaMassCategory(const xAOD::PhotonContainer * /*photons*/);
    int getConvMassCategory(const xAOD::PhotonContainer * /*photons*/);
    int getPtMassCategory(const xAOD::PhotonContainer * /*photons*/);
    int getMuMassCategory(float mu);

  };

}
#endif // HGamAnalysisFramework_HGamCategoryTool_H
