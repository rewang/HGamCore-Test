#include "HGamAnalysisFramework/TruthStudy.h"

#include "MCUtils/PIDUtils.h"
// FastJet
#include "fastjet/ClusterSequenceArea.hh"
#include "fastjet/Selector.hh"
#include "fastjet/AreaDefinition.hh"
#include "fastjet/JetDefinition.hh"
#include "JetRec/PseudoJetGetter.h"

void TruthStudy::Init(const xAOD::TruthParticleContainer *tpc, unsigned long long evt)
{
  m_Done[0] = false;
  m_Done[1] = false;
  m_Done[2] = true;
  m_truthParticles = tpc;
  m_evt = evt;
  //m_EvtToDebug.insert(1727639);
  //m_EvtToDebug.insert(685083);
  //m_EvtToDebug.insert(689027);
}


void TruthStudy::GetTrueDensities()
{

  if (m_Done[1])
  { return; }

  m_Done[1] = true;

  m_partonDensityVec.clear();
  m_particleDensityVec.clear();

  std::vector<fastjet::PseudoJet> pseudoJPartons;
  std::vector<fastjet::PseudoJet> pseudoJParticles;

  for (auto ttt : m_allPartons)
  { pseudoJPartons.push_back(fastjet::PseudoJet(ttt->px(), ttt->py(), ttt->pz(), ttt->e())); }

  for (auto ttt : m_allParticles)
  { pseudoJParticles.push_back(fastjet::PseudoJet(ttt->px(), ttt->py(), ttt->pz(), ttt->e())); }

  double area, sigma, rho;
  double rmi, rma;
  // fastjet::ClusterSequenceArea seqPartons  (pseudoJPartons,  fastjet::JetDefinition(fastjet::kt_algorithm,0.5,fastjet::Best),fastjet::VoronoiAreaSpec(0.9));
  // fastjet::ClusterSequenceArea seqParticles(pseudoJParticles,fastjet::JetDefinition(fastjet::kt_algorithm,0.5,fastjet::Best),fastjet::VoronoiAreaSpec(0.9));
  fastjet::ClusterSequenceArea seqPartons(pseudoJPartons,  fastjet::JetDefinition(fastjet::kt_algorithm, 0.5, fastjet::E_scheme, fastjet::Best), fastjet::VoronoiAreaSpec(0.9));
  fastjet::ClusterSequenceArea seqParticles(pseudoJParticles, fastjet::JetDefinition(fastjet::kt_algorithm, 0.5, fastjet::E_scheme, fastjet::Best), fastjet::VoronoiAreaSpec(0.9));

  std::vector<fastjet::PseudoJet> allPartonJets   = seqPartons.inclusive_jets();
  std::vector<fastjet::PseudoJet> allParticleJets = seqParticles.inclusive_jets();

  Dens aDens, bDens;

  for (int ieta = 0; ieta <= 52; ieta++) {
    rmi = -4.0 + ieta * 0.1;
    rma = rmi + 3.0;

    if (ieta == 51) {
      rmi = 0.0;
      rma = 1.5;
    } else if (ieta == 52) {
      rmi = 1.5;
      rma = 3.0;
    }

    fastjet::Selector selF(ieta > 50 ? fastjet::SelectorAbsRapRange(rmi, rma) : fastjet::SelectorRapRange(rmi, rma));

    seqPartons.get_median_rho_and_sigma(selF, true, rho, sigma, area);
    aDens.eta   = float(ieta) * 0.1 - 2.5;
    aDens.ieta  = ieta;
    aDens.rho   = 1e-3 * rho;
    aDens.sig   = 1e-3 * sigma;
    aDens.area  = area;
    aDens.njet  = selF.count(allPartonJets);
    aDens.nejet = seqPartons.n_empty_jets(selF);

    seqParticles.get_median_rho_and_sigma(selF, true, rho, sigma, area);
    bDens.eta   = float(ieta) * 0.1 - 2.5;
    bDens.ieta  = ieta;
    bDens.rho   = 1e-3 * rho;
    bDens.sig   = 1e-3 * sigma;
    bDens.area  = area;
    bDens.njet  = selF.count(allParticleJets);
    bDens.nejet = seqParticles.n_empty_jets(selF);

    m_partonDensityVec.push_back(aDens);
    m_particleDensityVec.push_back(bDens);

  }

  return;
}

void TruthStudy::GetTrueParticles()
{

  if (m_Done[0]) { return; }

  m_Done[0] = true;

  m_allPartons.clear();
  m_allParticles.clear();
  m_allHardPartons.clear();

  TLorentzVector all(0, 0, 0, 0);
  TLorentzVector allP(0, 0, 0, 0);

  int nWarn = 0;

  for (auto truep : *m_truthParticles) {

    // no G4 particles
    if (truep->barcode() > 200000) { continue; }

    int sta = truep->status();

    // Get all partons from hard process
    if (sta == 3 || (sta >= 20 && sta <= 29))
    { m_allHardPartons.insert(truep); }

    // Finding partons
    int apdg = truep->absPdgId();

    if (apdg < 6 || apdg == 21 || MCUtils::PID::isDiQuark(apdg)) {
      if (truep->nChildren() > 0 && truep->child(0) && (truep->child(0)->absPdgId() > 100 && !MCUtils::PID::isDiQuark(truep->child(0)->absPdgId()))) {
        m_allPartons.insert(truep);
        all += truep->p4();

        if (m_EvtToDebug.find(m_evt) != m_EvtToDebug.end()) {
          Info("TruthStudy::GetTrueParticles", "A good hard parton %p %i %i %i %3.2f %3.2f %3.2f %3.2f", (void *)truep, truep->barcode(), truep->status(), truep->pdgId(),
               truep->p4().Px() * 1e-3, truep->p4().Py() * 1e-3, truep->p4().Pz() * 1e-3, truep->p4().E() * 1e-3);
        }
      } else if (truep->nChildren() == 0 || (truep->nChildren() > 0 && truep->child(0) == nullptr)) {
        // ?
        nWarn++;

        for (unsigned int id = 0; id < truep->nChildren(); id++) {
          int pdg = 0;

          if (truep->child(id))
          { pdg = truep->child(id)->absPdgId(); }

          if (abs(pdg) > 100 && !MCUtils::PID::isDiQuark(pdg)) {
            m_allPartons.insert(truep);
            all += truep->p4();
            break;
          }

          if (m_EvtToDebug.find(m_evt) != m_EvtToDebug.end()) {
            Info("TruthStudy::GetTrueParticles", "Burp parton %p %i %i %i %3.2f %3.2f %3.2f %3.2f without daughter or not stored daughter nDau = %lu",
                 (void *)truep, truep->barcode(), truep->status(), truep->pdgId(),
                 truep->p4().Px() * 1e-3, truep->p4().Py() * 1e-3, truep->p4().Pz() * 1e-3, truep->p4().E() * 1e-3, truep->nChildren());
          }
        }
      }

      // Finding the photons from the ME...
    } else if (apdg == 22 && truep->status() == 1) {
      bool toKeep = false;

      for (unsigned int ipa = 0; ipa < truep->nParents(); ipa++) {
        const xAOD::TruthParticle *parent = truep->parent(ipa);

        if (parent) {
          int pdgpa = parent->pdgId();

          //Info("TruthStudy::GetTrueParticles","a stable photon, bar = %i, nParents = %lu, %i, pdg = %i bar = %i, status = %i",
          //truep->barcode(),truep->nParents(),ipa,pdgpa,parent->barcode(),parent->status());
          if (pdgpa == 22) { toKeep = true; }
        }
      }

      if (toKeep) {
        m_allPartons.insert(truep);
        all += truep->p4();

        if (m_EvtToDebug.find(m_evt) != m_EvtToDebug.end()) {
          Info("TruthStudy::GetTrueParticles", "A good hard parton %p %i %i %i %3.2f %3.2f %3.2f %3.2f", (void *)truep, truep->barcode(), truep->status(), truep->pdgId(),
               truep->p4().Px() * 1e-3, truep->p4().Py() * 1e-3, truep->p4().Pz() * 1e-3, truep->p4().E() * 1e-3);
        }
      }
    }

    // Fiding stable particles
    if (truep->status() == 1) {
      m_allParticles.insert(truep);
      allP += truep->p4();
      //Info("TruthStudy::GetTrueParticles","A stable part %i %i %i %i %3.2f %3.2f %3.2f %3.2f %i",truep,truep->barcode(),truep->status(),truep->pdgId(),
      //   truep->p4().Px()*1e-3,truep->p4().Py()*1e-3,truep->p4().Pz()*1e-3,truep->p4().E()*1e-3);
    }
  }

  //if (fabs(all.E()-13e6) > 1e3 || fabs(allP.E()-13e6) > 1e3) {
  //  Info("TruthStudy::GetTrueParticles","In event %llu, more than one GeV deviation from partons or particles, Eparton %f, Eparticle = %f",m_evt,all.E(),allP.E());
  //  all.Print();
  //  allP.Print();
  //}
  //if (nWarn > 0)
  //  Info("TruthStudy::GetTrueParticles","In event %llu, recovery of partons for which not all daughters were stored",m_evt);

  //Info("TruthStudy::GetTrueParticles","NHardPartons = %lu, NPartons = %lu, NParticles = %lu",m_allHardPartons.size(),m_allPartons.size(),m_allParticles.size());

  return;
}

std::pair<int, std::vector<double> > TruthStudy::SherpaClass()
{

  if (!m_Done[0])
  { GetTrueParticles(); }

  m_Done[0] = true;

  int nq = 0, ng = 0;
  std::set<const xAOD::TruthParticle *> truePh;
  std::vector<const xAOD::TruthParticle *> fsPart;

  // Here this assumes that initial partons are not stored ! (as was done for 2015 mc...)
  // Need to check for 2016 Sherpa 2.2 mc...
  for (auto truep : m_allHardPartons) {
    int sta = truep->status();

    if (sta != 3) { continue; }

    if (truep->pdgId() == 22)
    { truePh.insert(truep); }
    else
    { fsPart.push_back(truep); }

    if (truep->absPdgId() < 7)
    { nq++; }
    else if (truep->absPdgId() == 21)
    { ng++; }
  }

  int sclass = 10 * nq + ng;
  std::vector<double> drmin(truePh.size(), 9e9);

  if (truePh.size() == 0) {
    sclass = 0;
    return make_pair(sclass, drmin);
  }

  if (nq + ng != (int)fsPart.size())
  { Info("TruthStudy::SherpaClass", "Strange event nquark = %i, ngluon = %i, ntot = %lu !", nq, ng, fsPart.size()); }

  int iph = 0;

  for (auto tph : truePh) {
    for (auto truep : fsPart) {
      double dr = tph->p4().DeltaR(truep->p4());

      if (drmin[iph] > dr)
      { drmin[iph] = dr; }
    }

    iph++;
  }

  return make_pair(sclass, drmin);
}

TruthStudy::Iso TruthStudy::TruthIso(const xAOD::TruthParticle *truthPh)
{

  Iso iso;

  for (int i = 0; i < 3; i++) {
    iso.ptcone[i]       = 0;
    iso.etcone[i]       = 0;
    iso.partonetcone[i] = 0;
  }

  if (truthPh != nullptr)  {

    if (!m_Done[0])
    { GetTrueParticles(); }

    // Parton isolation
    for (auto truep : m_allPartons) {
      double pt = truep->pt();

      if (pt <= 0.) { continue; }

      // For this logic, see "transvers momentum = 0! return +/- 10e10" warning in TVector3.cxx :
      if (truep->p4().Vect().CosTheta()*truep->p4().Vect().CosTheta() == 1 && truep->p4().Vect().Z() != 0) { continue; }

      double dr = truthPh->p4().DeltaR(truep->p4());

      if (truep->barcode() != truthPh->barcode() && dr < 0.4) {
        iso.partonetcone[2] += pt;

        if (dr < 0.3) {
          iso.partonetcone[1] += pt;

          if (dr < 0.2)
          { iso.partonetcone[0] += pt; }
        }
      }
    }

    // Particle isolation
    for (auto truep : m_allParticles) {
      if (truep->barcode() == truthPh->barcode()) { continue; }

      double pt = truep->pt();

      if (pt <= 0.) { continue; }

      int apdg = truep->absPdgId();

      if (apdg == 12 || apdg == 14 || apdg == 16) { continue; }

      //
      double dr = truthPh->p4().DeltaR(truep->p4());

      if (dr > 0.4) { continue; }

      int q     = truep->threeCharge();

      if (apdg != 13)                    { iso.etcone[2] += pt; }

      if (q != 0 && pt > m_trkPtCut)     { iso.ptcone[2] += pt; }

      if (dr < 0.3) {
        if (apdg != 13)                  { iso.etcone[1] += pt; }

        if (q != 0 && pt > m_trkPtCut)   { iso.ptcone[1] += pt; }

        if (dr < 0.2) {
          if (apdg != 13)                { iso.etcone[0] += pt; }

          if (q != 0 && pt > m_trkPtCut) { iso.ptcone[0] += pt; }
        }
      }
    }
  }

  return iso;
}

TruthStudy::Dens TruthStudy::GetTrueDensity(double eta, int p, int e)
{

  if (!m_Done[1])
  { GetTrueDensities(); }

  int bin = 51;

  if (e == 0 && fabs(eta) > 1.5)
  { bin = 52; }
  else if (e == 1)
  { bin = int((eta + 2.55) / 0.1); }

  if (p == 0)
  { return m_partonDensityVec.at(bin); }

  // by default return particle density
  return m_particleDensityVec.at(bin);
}
