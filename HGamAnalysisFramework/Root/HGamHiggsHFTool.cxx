///////////////////////////////////////////
// Higgs+HF Tool
// Perform preselection and calculate NN input variables
// All info decorated to MxAODs
// @author: isabel.nitsche@cern.ch
///////////////////////////////////////////

// STL includes
#include <algorithm>
// ATLAS/ROOT includes
#include <AsgTools/MsgStream.h>
#include <AsgTools/MsgStreamMacros.h>
#include <TMVA/Reader.h>
// Local includes
#include "HGamAnalysisFramework/EventHandler.h"
#include "HGamAnalysisFramework/HgammaIncludes.h"
#include "HGamAnalysisFramework/HGamHiggsHFTool.h"
#include "HGamAnalysisFramework/JetHandler.h"
#include "HGamAnalysisFramework/ElectronHandler.h"
#include "HGamAnalysisFramework/MuonHandler.h"
#include "HGamAnalysisFramework/TruthHandler.h"


namespace HG {
  // _______________________________________________________________________________________
  HGamHiggsHFTool::HGamHiggsHFTool(const char *name, HG::EventHandler *eventHandler, HG::TruthHandler *truthHandler, const bool & /*isMC*/)
    : asg::AsgMessaging(name)
    , m_eventHandler(eventHandler)
    , m_truthHandler(truthHandler)
    , m_minMuonPT(10)
    , m_minElectronPT(10)
    , m_centraljet_eta_max(2.5)
    , m_truthjet_rapidity_max(2.5)
    , m_centraljet_pt_min(25)
  {}

  // _______________________________________________________________________________________
  HGamHiggsHFTool::~HGamHiggsHFTool()
  {
  }

  // _______________________________________________________________________________________
  HG::EventHandler *HGamHiggsHFTool::eventHandler()
  {
    return m_eventHandler;
  }

  // _______________________________________________________________________________________
  HG::TruthHandler *HGamHiggsHFTool::truthHandler()
  {
    return m_truthHandler;
  }

  // _______________________________________________________________________________________
  EL::StatusCode HGamHiggsHFTool::initialize(Config &config)
  {
    // Set default cut values etc here...
    // Almost all of these are set hard-coded because this will be used by the nominal MxAOD production

    m_minMuonPT = config.getNum("HGamHiggsHFTool.MinMuonPt", 10.0); //! minimum muon pt (in GeV) for lepton veto
    ATH_MSG_INFO("MinMuonPt.................................. " << m_minMuonPT);

    m_minElectronPT = config.getNum("HGamHiggsHFTool.MinElectronPt", 10.0); //! minimum electron pt (in GeV) for lepton veto
    ATH_MSG_INFO("MinElectronPt.............................. " << m_minElectronPT);

    m_centraljet_eta_max = config.getNum("HGamHiggsHFTool.MaxCentralJetEta", 2.5); //! Max central jet abs eta
    ATH_MSG_INFO("MaxCentralJetEta........................... " << m_centraljet_eta_max);

    m_centraljet_pt_min = config.getNum("HGamHiggsHFTool.MinCentralJetPt", 25); //! Min central jet abs pt
    ATH_MSG_INFO("MinCentralJetPt........................... " << m_centraljet_pt_min);


    m_TMVAreader_HiggsHF = new TMVA::Reader("!Color:!Silent");
    //m_TMVAreader_HiggsHF->AddVariable("lead_mv2_jet_bin", &t_lead_mv2_jet_bin);
    m_TMVAreader_HiggsHF->AddVariable("deltaR_y2_j1", &t_deltaR_y2_j1);
    m_TMVAreader_HiggsHF->AddVariable("deltaR_y1_j1", &t_deltaR_y1_j1);
    m_TMVAreader_HiggsHF->AddVariable("deltaR_y2_j2", &t_deltaR_y2_j2);
    m_TMVAreader_HiggsHF->AddVariable("deltaR_y1_j2", &t_deltaR_y1_j2);
    m_TMVAreader_HiggsHF->AddVariable("deltaR_yy", &t_deltaR_yy);
    m_TMVAreader_HiggsHF->AddVariable("deltaEta_yy", &t_deltaEta_yy);
    m_TMVAreader_HiggsHF->AddVariable("effmass_all", &t_effmass_all);
    m_TMVAreader_HiggsHF->AddVariable("foxwolfram1_all", &t_foxwolfram1_all);
    TString readerPath_HiggsHF = PathResolverFindCalibFile("HGamAnalysisFramework/BDT/BDT_HiggsHF.weights.xml");
    ATH_MSG_INFO("Reading HiggsHF BDT XML file:  " << readerPath_HiggsHF);
    m_TMVAreader_HiggsHF->BookMVA("BDTG", readerPath_HiggsHF);

    return EL::StatusCode::SUCCESS;
  }


  //___________________________________________________________________________________________________________
  float HGamHiggsHFTool::getMVAWeight(TMVA::Reader *xReader)
  {
    float BDTG_weight = xReader->EvaluateMVA("BDTG") ;
    return BDTG_weight;
  }

  // _______________________________________________________________________________________
  void HGamHiggsHFTool::saveHGamHiggsHFInfo(const xAOD::PhotonContainer &photons,
                                            const xAOD::JetContainer   &jets,
                                            xAOD::JetContainer   &jetsJVT,
                                            xAOD::MuonContainer &muons,
                                            xAOD::ElectronContainer &electrons)
  {
    // --------
    // Maps containing all event info to decorate to MxAOD
    // Floats for all decimal values
    // Chars for all bools
    // Ints for all counters (Category and multiplicities)
    std::map<TString, float> eventInfoFloats;
    std::map<TString, int>   eventInfoInts;


    // Perform selection
    performSelection(photons, jets, jetsJVT, muons, electrons,
                     eventInfoFloats, eventInfoInts);


    // Save all the eventInfoMaps to the eventHandler()
    // Should always be called last, once all of these have been filled
    saveMapsToEventInfo(eventInfoFloats, eventInfoInts);
  }


  // _______________________________________________________________________________________
  void HGamHiggsHFTool::saveHGamHiggsHFTruthInfo(const xAOD::TruthParticleContainer &photons,
                                                 const xAOD::JetContainer   &jets,
                                                 const xAOD::JetContainer   &bjets,
                                                 const xAOD::JetContainer   &cjets,
                                                 xAOD::TruthParticleContainer &muons,
                                                 xAOD::TruthParticleContainer &electrons)
  {
    // --------
    // Maps containing all event info to decorate to MxAOD
    // Floats for all decimal values
    // Chars for all bools
    // Ints for all counters (Category and multiplicities)
    //std::map<TString, float> trutheventInfoFloats;
    std::map<TString, int>   trutheventInfoInts;


    // Perform truth selection
    performTruthSelection(photons, jets, bjets, cjets, muons, electrons, trutheventInfoInts);


    // Save all the eventInfoMaps to the eventHandler()
    // Should always be called last, once all of these have been filled
    saveMapsToTruthEventInfo(trutheventInfoInts);
  }

  // _______________________________________________________________________________________
  // Function which performs full HiggsHF truth selection and saves info to info maps (nominal by default)
  void HGamHiggsHFTool::performTruthSelection(const xAOD::TruthParticleContainer &photons,
                                              const xAOD::JetContainer   &jets,
                                              const xAOD::JetContainer   &bjets,
                                              const xAOD::JetContainer   &cjets,
                                              xAOD::TruthParticleContainer &muons,
                                              xAOD::TruthParticleContainer &electrons,
                                              std::map<TString, int> &eventInfoInts)
  {

    // First apply photon selection
    //if (photons.size() < 2) {
    if (!truthHandler()->passFiducial(&photons)) {
      eventInfoInts["HiggsHF_cutFlow"] = PHOTONS;
      return;
    }

    // Next apply lepton veto
    xAOD::TruthParticleContainer muonsPassing(SG::VIEW_ELEMENTS);

    // Apply muon requirements
    std::copy_if(muons.begin(), muons.end(), std::back_inserter(muonsPassing), [this](const xAOD::TruthParticle * muon) {
      return (muon->pt() >= m_minMuonPT * HG::GeV);
    });


    xAOD::TruthParticleContainer electronsPassing(SG::VIEW_ELEMENTS);

    // Apply electron requirements
    std::copy_if(electrons.begin(), electrons.end(), std::back_inserter(electronsPassing), [this](const xAOD::TruthParticle * electron) {
      return (electron->pt() >= m_minElectronPT * HG::GeV);
    });


    // Apply lepton veto
    if (muonsPassing.size() > 0) { eventInfoInts["HiggsHF_cutFlow"] = LEPVETO; return;}

    if (electronsPassing.size() > 0) { eventInfoInts["HiggsHF_cutFlow"] = LEPVETO; return;}

    // Number of central jets
    int Njet_central = 0;
    int Njet_b = 0;
    int Njet_c = 0;
    int Njet_HF = 0;


    //Save TLorentzVectors
    std::vector<TLorentzVector> central_jets;
    std::vector<TLorentzVector> sel_bjets;
    std::vector<TLorentzVector> sel_cjets;

    //Loop over jets
    for (auto jet : jets) {
      if (fabs(jet->eta()) > m_truthjet_rapidity_max || jet->pt() < m_centraljet_pt_min * HG::GeV) { continue; }

      Njet_central++;
      TLorentzVector jet_tmp = jet->p4();
      central_jets.push_back(jet_tmp);
    }

    //Loop over bjets
    for (auto bjet : bjets) {
      if (fabs(bjet->eta()) > m_truthjet_rapidity_max || bjet->pt() < m_centraljet_pt_min * HG::GeV) { continue; }

      Njet_b++;
      TLorentzVector bjet_tmp = bjet->p4();
      sel_bjets.push_back(bjet_tmp);
    }

    //Loop over cjets, reject jets that are already matched to a b-jet
    for (auto cjet : cjets) {
      if (fabs(cjet->eta()) > m_truthjet_rapidity_max || cjet->pt() < m_centraljet_pt_min * HG::GeV) { continue; }

      TLorentzVector cjet_tmp = cjet->p4();

      for (unsigned int i_jet = 0; i_jet < sel_bjets.size(); i_jet++) {
        if (cjet_tmp == sel_bjets[i_jet]) { continue; }
      }

      sel_cjets.push_back(cjet_tmp);
      Njet_c++;
    }

    //Number of heavy flavour jets per events
    Njet_HF = Njet_c + Njet_b;


    // Minimum requirement on the number of truth jets
    if (Njet_central == 0) { eventInfoInts["HiggsHF_cutFlow"] = CENJET_MIN; return;}

    // Maximum requirement on the number of truth jets
    if (Njet_central > 3) { eventInfoInts["HiggsHF_cutFlow"] = CENJET_MAX; return;}

    // Require at least one jet matched to a b- or c-hadron
    if (Njet_HF == 0) { eventInfoInts["HiggsHF_cutFlow"] = BTAG; return;}

    // Save events that pass all requirements
    eventInfoInts["HiggsHF_cutFlow"] = PASS;
    eventInfoInts["HiggsHF_N_bjets"] = Njet_b;
    eventInfoInts["HiggsHF_N_cjets"] = Njet_c;
  }

  // _______________________________________________________________________________________
  // Function which performs full HiggsHF selection and saves info to info maps (nominal by default)
  void HGamHiggsHFTool::performSelection(const xAOD::PhotonContainer &photons,
                                         const xAOD::JetContainer &jets,
                                         xAOD::JetContainer &jets_noJVT,
                                         xAOD::MuonContainer &muons,
                                         xAOD::ElectronContainer &electrons,
                                         std::map<TString, float> &eventInfoFloats,
                                         std::map<TString, int> &eventInfoInts)
  {

    // First apply photon selection
    if (photons.size() < 2) {
      eventInfoInts["HiggsHF_cutFlow"] = PHOTONS;
      return;
    }


    // Next apply lepton veto
    xAOD::MuonContainer muonsPassing(SG::VIEW_ELEMENTS);

    // Apply muon requirements
    std::copy_if(muons.begin(), muons.end(), std::back_inserter(muonsPassing), [this](const xAOD::Muon * muon) {
      return (muon->pt() >= m_minMuonPT * HG::GeV);
    });


    xAOD::ElectronContainer electronsPassing(SG::VIEW_ELEMENTS);

    // Apply electron requirements
    std::copy_if(electrons.begin(), electrons.end(), std::back_inserter(electronsPassing), [this](const xAOD::Electron * electron) {
      return (electron->pt() >= m_minElectronPT * HG::GeV);
    });


    // Apply lepton veto
    if (muonsPassing.size() > 0) { eventInfoInts["HiggsHF_cutFlow"] = LEPVETO; return;}

    if (electronsPassing.size() > 0) { eventInfoInts["HiggsHF_cutFlow"] = LEPVETO; return;}


    // Number of central jets
    int Njet_central = 0;

    // Save four-vectors of leading & subleading MV2 jet
    TLorentzVector LeadMV2Jet;
    LeadMV2Jet.SetPtEtaPhiM(0, 0, 0, 0);
    TLorentzVector SubleadMV2Jet;
    SubleadMV2Jet.SetPtEtaPhiM(0, 0, 0, 0);

    // Save b-tagging weights for leading & subleading MV2 jet
    double LeadMV2Weight = -999;
    double SubleadMV2Weight = -999;

    // Save b-tagging bin for leading & subleading MV2 jet
    int LeadMV2Jet_btagbin = -999;
    //int SubleadMV2Jet_btagbin = -999;

    // Save truth label of leading & subleading MV2 jet
    int LeadMV2Jet_truthlabel = -999;
    //int SubleadMV2Jet_truthlabel = -999;

    // Count b-tags using 85% WP for NN preselection
    int ntag85_25 = 0;

    // Store weight for pseudocontinuous b-tagging => Changed to 85% WP for now
    double weight_b_tagging = 1.0;

    //Save TLorentzVectors for calculating event shape variables
    std::vector<TLorentzVector> central_jets;

    //Loop over jets
    for (auto jet : jets) {
      if (fabs(jet->eta()) > m_centraljet_eta_max || jet->pt() < m_centraljet_pt_min * HG::GeV) { continue; }

      Njet_central++;

      TLorentzVector jet_tmp = jet->p4();
      central_jets.push_back(jet_tmp);

      double mv2_disc = jet->auxdata<double>("MV2c10_discriminant");
      int truthLabel = HG::isMC() ? jet->auxdata<int>("HadronConeExclTruthLabelID") : -99;
      int btagbin = -999;

      if (jet->auxdata<char>("MV2c10_FixedCutBEff_85")) { ntag85_25++; }

      if (jet->auxdata<char>("MV2c10_FixedCutBEff_60")) { btagbin = 4; }
      else if (jet->auxdata<char>("MV2c10_FixedCutBEff_70")) { btagbin = 3; }
      else if (jet->auxdata<char>("MV2c10_FixedCutBEff_77")) { btagbin = 2; }
      else if (jet->auxdata<char>("MV2c10_FixedCutBEff_85")) { btagbin = 1; }
      else { btagbin = 0; }


      //weight_b_tagging *= jet->auxdata<float>("SF_MV2c10_Continuous");
      weight_b_tagging *= jet->auxdata<float>("SF_MV2c10_FixedCutBEff_85");

      if (mv2_disc > LeadMV2Weight) {
        if (Njet_central > 1) {
          SubleadMV2Jet = LeadMV2Jet;
          SubleadMV2Weight = LeadMV2Weight;
          //SubleadMV2Jet_btagbin = LeadMV2Jet_btagbin;
          //SubleadMV2Jet_truthlabel = LeadMV2Jet_truthlabel;
        }

        LeadMV2Jet.SetPtEtaPhiM(jet->pt(), jet->eta(), jet->phi(), jet->m());
        LeadMV2Weight = mv2_disc;
        LeadMV2Jet_btagbin = btagbin;
        LeadMV2Jet_truthlabel = truthLabel;
      } else if ((mv2_disc > SubleadMV2Weight) && (Njet_central > 1)) {
        SubleadMV2Jet.SetPtEtaPhiM(jet->pt(), jet->eta(), jet->phi(), jet->m());
        SubleadMV2Weight = mv2_disc;
        //SubleadMV2Jet_btagbin = btagbin;
        //SubleadMV2Jet_truthlabel = truthLabel;
      }

    }

    // Minimum requirement on the number of central jets
    if (Njet_central == 0) { eventInfoInts["HiggsHF_cutFlow"] = CENJET_MIN; return;}

    // Maximum requirement on the number of central jets
    if (Njet_central > 3) { eventInfoInts["HiggsHF_cutFlow"] = CENJET_MAX; return;}

    // Loose b-tag requirement for the NN preselection
    if (ntag85_25 == 0) { eventInfoInts["HiggsHF_cutFlow"] = BTAG; return;}

    // Save events that pass all requirements
    eventInfoInts["HiggsHF_cutFlow"] = PASS;

    ///////////////////////////
    // Save NN input variables
    ///////////////////////////

    TLorentzVector higgs = (photons[0]->p4() + photons[1]->p4());
    TLorentzVector y1 = photons[0]->p4();
    TLorentzVector y2 = photons[1]->p4();

    std::vector<TLorentzVector> photons_central_jets = central_jets;
    photons_central_jets.push_back(y1);
    photons_central_jets.push_back(y2);

    // Variables related to the MV2 leading jet
    eventInfoFloats["HiggsHF_dR_y1_leadMV2Jet"] = fabs(y1.DeltaR(LeadMV2Jet));
    eventInfoFloats["HiggsHF_dR_y2_leadMV2Jet"] = fabs(y2.DeltaR(LeadMV2Jet));
    eventInfoFloats["HiggsHF_dPhi_yy_leadMV2jet"] = fabs(higgs.DeltaPhi(LeadMV2Jet));
    eventInfoInts["HiggsHF_leadMV2Jet_btagbin"] = LeadMV2Jet_btagbin;
    eventInfoFloats["HiggsHF_leadMV2jet_eta"] = LeadMV2Jet.Eta();
    eventInfoFloats["HiggsHF_leadMV2jet_pt"] = LeadMV2Jet.Pt() * HG::invGeV;

    // Variables related to the MV2 subleading jet
    eventInfoFloats["HiggsHF_dR_y1_subleadMV2Jet"] = (Njet_central > 1) ? fabs(y1.DeltaR(SubleadMV2Jet)) : -999 ;
    eventInfoFloats["HiggsHF_dR_y2_subleadMV2Jet"] = (Njet_central > 1) ? fabs(y2.DeltaR(SubleadMV2Jet)) : -999 ;

    // Photon related variables
    eventInfoFloats["HiggsHF_dEta_yy"] = fabs(y2.Eta() - y1.Eta());
    eventInfoFloats["HiggsHF_dR_yy"] = fabs(y1.DeltaR(y2));

    // Event shape variables
    eventInfoFloats["HiggsHF_effmass_yyjets"] = GetEffectiveMass(photons_central_jets);
    eventInfoFloats["HiggsHF_foxw1_yyjets"] = GetFoxWolframMoment(photons_central_jets, 1);


    //t_lead_mv2_jet_bin = LeadMV2Jet_btagbin;
    t_deltaR_y2_j1 = fabs(y2.DeltaR(LeadMV2Jet));
    t_deltaR_y1_j1 = fabs(y1.DeltaR(LeadMV2Jet));
    t_deltaR_y2_j2 = (Njet_central > 1) ? fabs(y1.DeltaR(SubleadMV2Jet)) : -999 ;
    t_deltaR_y1_j2 = (Njet_central > 1) ? fabs(y2.DeltaR(SubleadMV2Jet)) : -999 ;
    t_deltaR_yy = fabs(y1.DeltaR(y2));
    t_deltaEta_yy = fabs(y2.Eta() - y1.Eta());
    t_effmass_all = GetEffectiveMass(photons_central_jets);
    t_foxwolfram1_all = GetFoxWolframMoment(photons_central_jets, 1);

    float BDT_weight = getMVAWeight(m_TMVAreader_HiggsHF);
    eventInfoFloats["HiggsHF_BDT"] = BDT_weight;

    if (BDT_weight > 0.46) {eventInfoInts["HiggsHF_BDT_cat"] = 1;}
    else if (BDT_weight > -0.26) {eventInfoInts["HiggsHF_BDT_cat"] = 2;}
    else {eventInfoInts["HiggsHF_BDT_cat"] = 3;}

    // Save truth information about leading & subleading MV2 jet
    if (HG::isMC()) {
      eventInfoInts["HiggsHF_truth_label_LeadMV2Jet"] =  LeadMV2Jet_truthlabel;
    } else {
      eventInfoInts["HiggsHF_truth_label_LeadMV2Jet"] =  -999.;
    }

    // Calculate the JVT scale-factor from uncorrected, no-JVT jets
    double weight_JVT = weightJVT(jets_noJVT);

    // Get HGamHiggsHF weight from JVT and b-tagging
    eventInfoFloats["HiggsHF_weight"] = weight_JVT * weight_b_tagging;

  }


  // ___________________________________________________________________________________________
  // Multiply all the JVT (in)efficiencies for all jets passing the selection before the JVT cut
  double HGamHiggsHFTool::weightJVT(xAOD::JetContainer &jets_noJVT)
  {

    // Construct output jet container
    xAOD::JetContainer jets_noJVT_passing_cuts(SG::VIEW_ELEMENTS);

    // Apply central jet requirement (for b-tagging reasons) and appropriate pT cuts
    for (auto jet : jets_noJVT) {
      if (fabs(jet->eta()) > m_centraljet_eta_max) { continue; }

      if (jet->pt() < m_centraljet_pt_min * HG::invGeV) { continue; }

      jets_noJVT_passing_cuts.push_back(jet);
    }

    // Return multiplicative combination of JVT efficiencies
    if (jets_noJVT_passing_cuts.size() > 0) {
      return HG::JetHandler::multiplyJvtWeights(&jets_noJVT_passing_cuts);
    }

    return 1.0;
  }


  // _______________________________________________________________________________________
  // Save event info from maps to eventInfo
  void HGamHiggsHFTool::saveMapsToEventInfo(std::map<TString, float> &eventInfoFloats, std::map<TString, int> &eventInfoInts)
  {
    // Floats
    for (auto element : eventInfoFloats) {
      eventHandler()->storeVar<float>(element.first.Data(), element.second);
    }

    // Ints
    for (auto element : eventInfoInts) {
      eventHandler()->storeVar<int>(element.first.Data(), element.second);
    }
  }

  // _______________________________________________________________________________________
  // Save event info from maps to eventInfo
  void HGamHiggsHFTool::saveMapsToTruthEventInfo(std::map<TString, int> &eventInfoInts)
  {
    // Floats
    //for (auto element : eventInfoFloats) {
    // eventHandler()->storeTruthVar<float>(element.first.Data(), element.second);
    //}

    // Ints
    for (auto element : eventInfoInts) {
      eventHandler()->storeTruthVar<int>(element.first.Data(), element.second);
    }
  }


  //////////////////////
  // Event shape code
  //////////////////////

  float HGamHiggsHFTool::GetEffectiveMass(std::vector<TLorentzVector> fPart)
  {
    int size = fPart.size();

    if (size < 2) { return -999.; }

    TLorentzVector fEvent;
    fEvent.SetPtEtaPhiE(0, 0, 0, 0);

    for (int i = 0; i < size; i++) {
      fEvent += fPart[i];
    }

    return fEvent.M() * HG::invGeV;
  }

  float HGamHiggsHFTool::GetFoxWolframMoment(std::vector<TLorentzVector> fPart, int order)
  {
    int size = fPart.size();

    if (size < 2) { return -999.; }

    float fSumE = 0.;
    float fH[5];

    for (int idx = 0; idx < 5; idx++) { fH[idx] = 0; }

    float x;
    float P[5];

    for (int i = 0; i < size; i++) {
      fSumE += fPart[i].E();

      for (int j = 0; j < size; j++) {
        x = cos(fPart[i].Angle(fPart[j].Vect()));
        P[0] = 1.;
        P[1] = x;
        P[2] = 0.5 * (3 * x * x - 1);
        P[3] = 0.5 * (5 * x * x * x - 3 * x);
        P[4] = 0.125 * (35 * x * x * x * x - 30 * x * x + 3);

        for (int idx = 0; idx < 3; idx++) { fH[idx] += fabs(fPart[i].P()) * fabs(fPart[j].P()) * P[idx]; }
      }

      for (int j = i + 1; j < size; j++) {
        x = cos(fPart[i].Angle(fPart[j].Vect()));
        P[0] = 1.;
        P[1] = x;

        for (int idx = 0; idx < 2; idx++) { fH[idx + 3] += fabs(fPart[i].P()) * fabs(fPart[j].P()) * P[idx]; }
      }
    }

    if (order > 4) { return -999; }

    if (fSumE * fSumE == 0) { return 0; }

    return fH[order] / (fSumE * fSumE);
  }

}
