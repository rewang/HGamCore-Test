#include <cmath>
#include <algorithm>
#include <iostream>
#include <fstream>

#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>

#include "PATInterfaces/SystematicVariation.h"

#include <HGamAnalysisFramework/HgammaAnalysis.h>
#include <HGamAnalysisFramework/HgammaUtils.h>
#include <HGamAnalysisFramework/HGamVariables.h>
#include <HGamAnalysisFramework/HGamCategoryTool.h>

#include "PhotonVertexSelection/PhotonVertexHelpers.h"
#include "PhotonVertexSelection/PhotonPointingTool.h"

#include "HGamAnalysisFramework/VarHandler.h"

#include "TMVA/Factory.h"
#include "TMVA/Tools.h"
#include "TMVA/Reader.h"
#include "TMVA/MethodCuts.h"

#include <xgboost/c_api.h>

namespace HG {

  //______________________________________________________________________________
  HGamCategoryTool::HGamCategoryTool(xAOD::TEvent *event, xAOD::TStore *store)
    : m_event(event)
    , m_store(store)
    , m_readerVBF_low(nullptr)
    , m_readerVBF_high(nullptr)
    , m_readerVH_had_Moriond2017(nullptr)
    , m_reader_ttHhad(nullptr)
    , m_reader_ttHlep(nullptr)
    , m_xgboost_ttHhad(nullptr)
    , m_xgboost_ttHlep(nullptr)
    , m_xgboost_topreco(nullptr)
    , m_xgboost_ttHhad_topReco(nullptr)
    , m_xgboost_ttHlep_topReco(nullptr)
  { }

  //______________________________________________________________________________
  HGamCategoryTool::~HGamCategoryTool()
  {
    SafeDelete(m_readerVBF_high);
    SafeDelete(m_readerVBF_low);
    SafeDelete(m_readerVH_had_Moriond2017);
    SafeDelete(m_reader_ttHhad);
    SafeDelete(m_reader_ttHlep);
    SafeDelete(m_xgboost_ttHhad);
    SafeDelete(m_xgboost_ttHlep);
    SafeDelete(m_xgboost_topreco);
    SafeDelete(m_xgboost_ttHhad_topReco);
    SafeDelete(m_xgboost_ttHlep_topReco);
  }

  //______________________________________________________________________________
  EL::StatusCode HGamCategoryTool::initialize(Config &/*config*/)
  {
    // Setup MVA reader

    // VBF_high
    m_readerVBF_high = new TMVA::Reader("!Color:!Silent");
    m_readerVBF_high->AddVariable("pTt_yy",      &t_pTt_yy);
    m_readerVBF_high->AddVariable("m_jj",        &t_m_jj);
    m_readerVBF_high->AddVariable("jj_DeltaEta", &t_dEta_jj);
    m_readerVBF_high->AddVariable("Dphi_yy_jj",  &t_dPhi_yy_jj);
    m_readerVBF_high->AddVariable("abs(Zepp)",   &t_Zepp);
    m_readerVBF_high->AddVariable("DRmin_y_j",   &t_Drmin_y_j2);
    TString readerPathVBF_high = PathResolverFindCalibFile("HGamAnalysisFramework/BDT/MVA_VBF_high.weights.xml");
    m_readerVBF_high->BookMVA("BDTG", readerPathVBF_high);

    // VBF low
    m_readerVBF_low = new TMVA::Reader("!Color:!Silent");
    m_readerVBF_low->AddVariable("pTt_yy",      &t_pTt_yy);
    m_readerVBF_low->AddVariable("m_jj",        &t_m_jj);
    m_readerVBF_low->AddVariable("jj_DeltaEta", &t_dEta_jj);
    m_readerVBF_low->AddVariable("Dphi_yy_jj",  &t_dPhi_yy_jj);
    m_readerVBF_low->AddVariable("abs(Zepp)",   &t_Zepp);
    m_readerVBF_low->AddVariable("DRmin_y_j",   &t_Drmin_y_j2);
    TString readerPathVBF_low = PathResolverFindCalibFile("HGamAnalysisFramework/BDT/MVA_VBF_low.weights.xml");
    m_readerVBF_low->BookMVA("BDTG", readerPathVBF_low);

    // Setup VH_had MVA reader
    m_readerVH_had_Moriond2017 = new TMVA::Reader("!Color:!Silent");
    m_readerVH_had_Moriond2017->AddVariable("m_jj",       &t_m_jjGeV);
    m_readerVH_had_Moriond2017->AddVariable("pTt_yy",     &t_pTt_yyGeV);
    m_readerVH_had_Moriond2017->AddVariable("Dy_yy_jj",   &t_Dy_yy_jj);
    m_readerVH_had_Moriond2017->AddVariable("cosTS_yyjj", &t_cosTS_yy_jj);
    TString readerPathVH = PathResolverFindCalibFile("HGamAnalysisFramework/BDT/MVA_config_VH_had_Moriond2017.xml");
    m_readerVH_had_Moriond2017->BookMVA("BDTG", readerPathVH);

    // ttH hadronic BDT
    m_reader_ttHhad = new TMVA::Reader("!Color:!Silent");
    m_reader_ttHhad->AddVariable("m_alljet",      &t_m_alljet_30);
    m_reader_ttHhad->AddVariable("HT_30",         &t_HT_30);
    m_reader_ttHhad->AddVariable("N_j_30",        &t_N_j30);
    m_reader_ttHhad->AddVariable("N_j_btag30",    &t_N_bjet30);
    m_reader_ttHhad->AddVariable("N_j_central30", &t_N_j_central30);
    m_reader_ttHhad->BookMVA("BDTG", PathResolverFindCalibFile("HGamAnalysisFramework/BDT/MVA_config_ttHhad_Moriond2017.xml").c_str());

    // ttH leptonic BDT
    m_reader_ttHlep = new TMVA::Reader("!Color:!Silent");
    m_reader_ttHlep->AddVariable("N_j_central30",      &t_N_j_central30);
    m_reader_ttHlep->AddVariable("HT_30",              &t_HT_30);
    m_reader_ttHlep->AddVariable("m_pTlepEtmiss",      &t_pTlepMET);
    m_reader_ttHlep->AddVariable("m_mT",               &t_massT);
    m_reader_ttHlep->BookMVA("BDTG", PathResolverFindCalibFile("HGamAnalysisFramework/BDT/MVA_config_ttHlep_Moriond2017.xml").c_str());

    // ttH hadronic XGBoost BDT
    m_xgboost_ttHhad = new BoosterHandle();
    XGBoosterCreate(0, 0, * &m_xgboost_ttHhad);
    XGBoosterLoadModel(*m_xgboost_ttHhad, PathResolverFindCalibFile("HGamAnalysisFramework/model_hadronic_bdt.h5").c_str());

    // ttH leptonic XGBoost BDT
    m_xgboost_ttHlep = new BoosterHandle();
    XGBoosterCreate(0, 0, * &m_xgboost_ttHlep);
    XGBoosterLoadModel(*m_xgboost_ttHlep, PathResolverFindCalibFile("HGamAnalysisFramework/model_leptonic_bdt.h5").c_str());

    // ttH top recoXGBoost BDT
    m_xgboost_topreco = new BoosterHandle();
    XGBoosterCreate(0, 0, * &m_xgboost_topreco);
    XGBoosterLoadModel(*m_xgboost_topreco, PathResolverFindCalibFile("HGamAnalysisFramework/model_topRecoBDT.h5").c_str());

    // ttH hadronic XGBoost BDT
    m_xgboost_ttHhad_topReco = new BoosterHandle();
    XGBoosterCreate(0, 0, * &m_xgboost_ttHhad_topReco);
    XGBoosterLoadModel(*m_xgboost_ttHhad_topReco, PathResolverFindCalibFile("HGamAnalysisFramework/model_hadronic_bdt_plusTopReco.h5").c_str());

    // ttH leptonic XGBoost BDT
    m_xgboost_ttHlep_topReco = new BoosterHandle();
    XGBoosterCreate(0, 0, * &m_xgboost_ttHlep_topReco);
    XGBoosterLoadModel(*m_xgboost_ttHlep_topReco, PathResolverFindCalibFile("HGamAnalysisFramework/model_leptonic_bdt_plusTopReco.h5").c_str());

    return EL::StatusCode::SUCCESS;
  }

  //___________________________________________________________________________________________________________
  float HGamCategoryTool::getMVAWeight(TMVA::Reader *xReader)
  {
    //resetReader();  // redundant to call so many times, only done once now.
    float BDTG_weight = xReader->EvaluateMVA("BDTG") ;
    return BDTG_weight;
  }


  //___________________________________________________________________________________________________________
  int HGamCategoryTool::Passes_VBF_Moriond2017(const xAOD::JetContainer  *jetsJVT)
  {
    m_weight = 1.0;

    // Check Basic selection
    if (var::N_j_30() < 2) { return 0 ; }

    if (var::Deta_j_j() < 2) { return 0; }

    if (fabs(var::Zepp()) > 5) { return 0; }

    // Get JVT weight and return category
    for (auto jet : *jetsJVT)
      if (jet->pt() > 30 * HG::GeV) { m_weight *= jet->auxdata<float>("SF_jvt"); } // JVT SFs

    if (var::pT_yyjj() > 25 * HG::GeV) { return 2; }
    else { return 1; }
  }


  //___________________________________________________________________________________________________________
  bool HGamCategoryTool::Passes_VH_hadronic_Moriond2017(const xAOD::JetContainer *jetsJVT)
  {
    m_weight = 1.0;

    // Check basic selection
    if (var::N_j_30() < 2) { return 0; }

    if (var::m_jj() <  60 * HG::GeV) { return 0; }

    if (var::m_jj() > 120 * HG::GeV) { return 0; }

    // Get JVT weight and return passed
    for (auto jet : *jetsJVT)
      if (jet->pt() > 30 * HG::GeV) { m_weight *= jet->auxdata<float>("SF_jvt"); } // JVT SFs

    return 1;
  }


  //___________________________________________________________________________________________________________
  int HGamCategoryTool::Passes_VH_MET_Moriond2017(const xAOD::PhotonContainer *   /*photons*/,
                                                  const xAOD::MissingETContainer *met)
  {
    m_weight = 1.0;
    float TST_met = -99, TST_sumet = -99;

    if ((*met)["TST"] != nullptr) {
      TST_met   = (*met)["TST"]->met();
      TST_sumet = (*met)["TST"]->sumet();
    }

    float m_MET_signi = -99999;

    if (TST_met != 0) { m_MET_signi = TST_met * 0.001 / sqrt(TST_sumet * 0.001); }

    if (TST_met < 150 * HG::GeV && TST_met >= 80 * HG::GeV && m_MET_signi > 8) { return 1; }
    else if (TST_met < 250 * HG::GeV && TST_met >= 150 * HG::GeV && m_MET_signi > 9) { return 2; }
    else if (TST_met >= 250 * HG::GeV) { return 3; }
    else { return 0; }
  }


  //___________________________________________________________________________________________________________
  int HGamCategoryTool::Passes_VH_leptonic_Moriond2017(
    const xAOD::PhotonContainer    *photons,
    const xAOD::ElectronContainer  *electrons,
    const xAOD::MuonContainer      *muons,
    const xAOD::MissingETContainer *met)
  {

    m_weight = 1.0;
    int m_Nelectrons = electrons->size();
    int m_Nmuons = muons->size();
    m_weight *= getLeptonSFs(electrons, muons);   // lepton SFs

    if ((m_Nelectrons + m_Nmuons) != 1) { return 0; }

    bool passZey(true);

    for (auto el : *electrons) {
      double mey1 = (el->p4() + photons->at(0)->p4()).M() * HG::invGeV;
      double mey2 = (el->p4() + photons->at(1)->p4()).M() * HG::invGeV;

      if ((fabs(mey1 - 89) < 5) || (fabs(mey2 - 89) < 5)) { passZey = false; }
    }

    if (!passZey) { return 0; }

    float TST_met = -99, TST_sumet = -99;

    if ((*met)["TST"] != nullptr) {
      TST_met   = (*met)["TST"]->met();
      TST_sumet = (*met)["TST"]->sumet();
    }

    float m_MET_signi = -99999;

    if (TST_met != 0) { m_MET_signi = TST_met * HG::invGeV / sqrt(TST_sumet * HG::invGeV); }

    TLorentzVector metVec;
    metVec.SetPtEtaPhiM((*met)["TST"]->met(), 0, (*met)["TST"]->phi(), 0);

    bool pTVlt150 = true;

    if (((m_Nelectrons == 1) && ((electrons->at(0)->p4() + metVec).Pt() > 150 * HG::GeV)) ||
        ((m_Nmuons == 1) && ((muons    ->at(0)->p4() + metVec).Pt() > 150 * HG::GeV)))
    { pTVlt150 = false; }

    if (pTVlt150 && m_MET_signi > 1) { return 1; }
    else if (!pTVlt150) { return 2; }
    else { return 0; }
  }


  //___________________________________________________________________________________________________________
  int HGamCategoryTool::Passes_VH_dileptons_Moriond2017(const xAOD::PhotonContainer *    /*photons*/,
                                                        const xAOD::ElectronContainer  *electrons,
                                                        const xAOD::MuonContainer      *muons)
  {
    m_weight = 1.0;
    int m_Nelectrons = electrons->size();
    int m_Nmuons = muons->size();
    m_weight *= getLeptonSFs(electrons, muons);   // lepton SFs
    float m_m_mumu = 0, m_m_ee = 0, pT_m_ee = 0, pT_m_mumu = 0;

    if (m_Nelectrons > 1) {
      m_m_ee = (electrons->at(0)->p4() + electrons->at(1)->p4()).M();
      pT_m_ee = (electrons->at(0)->p4() + electrons->at(1)->p4()).Pt();
    }

    if (m_Nmuons > 1) {
      m_m_mumu = (muons->at(0)->p4() + muons->at(1)->p4()).M();
      pT_m_mumu = (muons->at(0)->p4() + muons->at(1)->p4()).Pt();
    }

    bool passMuSel = (m_Nmuons >= 2 && m_m_mumu > 70 * HG::GeV && m_m_mumu < 110 * HG::GeV);
    bool passElSel = (m_Nelectrons >= 2 && m_m_ee > 70 * HG::GeV && m_m_ee < 110 * HG::GeV);
    bool pTVlt150 = ((passMuSel && pT_m_mumu < 150 * HG::GeV) || (passElSel && pT_m_ee < 150 * HG::GeV));
    bool pTVgt150 = ((passMuSel && pT_m_mumu >= 150 * HG::GeV) || (passElSel && pT_m_ee >= 150 * HG::GeV));


    if (pTVlt150) { return 1; }
    else if (pTVgt150) { return 2; }
    else { return 0; }
  }

  //___________________________________________________________________________________________________________
  bool HGamCategoryTool::Passes_qqH_BSM_2jet(const xAOD::JetContainer *jetsJVT)
  {
    m_weight = 1.0;

    if (var::pT_j1() <= 200 * HG::GeV || var::N_j_30() < 2) { return 0; }

    // get JVT weight and return passed
    for (auto jet : *jetsJVT) { m_weight *= jet->auxdata<float>("SF_jvt"); } // JVT SFs

    return 1;
  }


  //___________________________________________________________________________________________________________
  bool HGamCategoryTool::Passes_qqH_BSM(const xAOD::JetContainer *jetsJVT)
  {
    m_weight = 1.0;

    if (var::pT_j1() <= 200 * HG::GeV) { return 0; }

    // get JVT weight and return passed
    for (auto jet : *jetsJVT) { m_weight *= jet->auxdata<float>("SF_jvt"); } // JVT SFs

    return 1;
  }


  //___________________________________________________________________________________________________________
  // ----------------------------------------------------------------------
  //  STXS ggF Categories (jared.vasquez@yale.edu)
  //  Updated splitting for Moriond analysis (STXS measurements)
  // ----------------------------------------------------------------------
  int HGamCategoryTool::split_ggH_STXS(const xAOD::PhotonContainer *photons,
                                       const xAOD::JetContainer    *jetsJVT)
  {
    m_weight = 1.0;

    // Get JVT weight from collection of 30 GeV jets w/ out JVT cut
    for (auto jet : *jetsJVT)
      if (jet->pt() > 30 * HG::GeV) { m_weight *= jet->auxdata<float>("SF_jvt"); }

    // Are photons forward or central?
    bool isFwd = (fabs(photons->at(0)->eta()) > 0.95 || fabs(photons->at(1)->eta()) > 0.95);

    // ggH+0j, split Cen/Fwd regions
    if (var::N_j_30() == 0 &&  isFwd) { return M17_ggH_0J_Fwd; }

    if (var::N_j_30() == 0 && !isFwd) { return M17_ggH_0J_Cen; }

    int pTbin = -999;

    if (var::pT_yy() <=  60 * HG::GeV) { pTbin = 0; }
    else if (var::pT_yy() <= 120 * HG::GeV) { pTbin = 1; }
    else if (var::pT_yy() <= 200 * HG::GeV) { pTbin = 2; }
    else                                    { pTbin = 3; }

    // >= 1 Jets
    if ((var::N_j_30() == 1) && (pTbin == 3)) { return M17_ggH_1J_BSM; }

    if ((var::N_j_30() == 1) && (pTbin == 2)) { return M17_ggH_1J_HIGH; }

    if ((var::N_j_30() == 1) && (pTbin == 1)) { return M17_ggH_1J_MED; }

    if ((var::N_j_30() == 1) && (pTbin == 0)) { return M17_ggH_1J_LOW; }

    // >= 2 Jets
    if ((var::N_j_30() >= 2) && (pTbin == 3)) { return M17_ggH_2J_BSM; }

    if ((var::N_j_30() >= 2) && (pTbin == 2)) { return M17_ggH_2J_HIGH; }

    if ((var::N_j_30() >= 2) && (pTbin == 1)) { return M17_ggH_2J_MED; }

    if ((var::N_j_30() >= 2) && (pTbin == 0)) { return M17_ggH_2J_LOW; }

    Error("HGamCategoryTool::split_ggH_STXS()", "Please Investigate. Could not find proper inclusive category!");
    return 0; // Should never get here.
  }


  //___________________________________________________________________________________________________________
  // ---------------------------------------------------------------------------------
  //   ttH Category Moriond 2017, Cut Based Selection  (jared.vasquez@yale.edu)
  // ---------------------------------------------------------------------------------
  int HGamCategoryTool::Passes_ttH_Moriond(
    const xAOD::PhotonContainer *   /*photons*/,
    const xAOD::ElectronContainer  *electrons,
    const xAOD::MuonContainer      *muons,
    const xAOD::JetContainer       *jets,
    const xAOD::JetContainer       *jetsJVT,
    const xAOD::MissingETContainer * /*met*/)
  {
    m_weight = 1.0;
    int n_electrons = electrons->size();
    int n_muons = muons->size();
    int n_leptons = (n_electrons + n_muons);

    int n_jetsCen25(0), n_jetsCen35(0), n_jetsFwd25(0);
    int n_tags25_70(0), n_tags35_70(0);

    double bweight25_70(1.0), bweight35_70(1.0);

    // count only central jets s.t. |eta| < 2.5
    for (auto jet : *jets) {
      if (fabs(jet->eta()) > 2.5) { n_jetsFwd25++; continue; }

      // Count 25 GeV Jets and Tags
      n_jetsCen25++;
      bweight25_70 *= jet->auxdata<float>("SF_MV2c10_FixedCutBEff_70");

      if (jet->auxdata<char>("MV2c10_FixedCutBEff_70")) { n_tags25_70++; }

      // Count 35 GeV Jets and Tags
      if (jet->pt() >= 35.e3) {
        n_jetsCen35++;
        bweight35_70 *= jet->auxdata<float>("SF_MV2c10_FixedCutBEff_70");

        if (jet->auxdata<char>("MV2c10_FixedCutBEff_70")) { n_tags35_70++; }
      }
    }

    // use other jet collection for JVT weights
    double alljetWeight25(1.0), jetWeight25(1.0), jetWeight35(1.0);

    for (auto jet : *jetsJVT) {
      alljetWeight25  *= jet->auxdata<float>("SF_jvt");

      if (fabs(jet->eta()) > 2.5) { continue; }

      jetWeight25  *= jet->auxdata<float>("SF_jvt");

      if (jet->pt() >= 35 * HG::GeV)
      { jetWeight35  *= jet->auxdata<float>("SF_jvt"); }
    }

    // Leptonic Channel Selection
    if (n_leptons >= 1) {

      double mll = -999.; // require OS dileptons in future?

      if (n_muons >= 2) { mll = (muons->at(0)->p4() + muons->at(1)->p4()).M() * HG::invGeV; }

      if (n_electrons >= 2) { mll = (electrons->at(0)->p4() + electrons->at(1)->p4()).M() * HG::invGeV; }

      if (fabs(mll - 91) < 10) { return 0; } // fail selection

      m_weight *= getLeptonSFs(electrons, muons);   // lepton SFs
      m_weight *= (alljetWeight25 * bweight25_70); // jvt & b-tag SFs

      bool passPreselTH = ((n_leptons == 1) && (n_tags25_70 >= 1));

      if (passPreselTH && (n_jetsCen25 <= 3) && (n_jetsFwd25 == 0))  { return 9; }
      else if (passPreselTH && (n_jetsCen25 <= 4) && (n_jetsFwd25 >= 1))  { return 8; }
      else if ((n_jetsCen25 >= 2) && (n_tags25_70 >= 1))  { return 7; }

      // Hadronic Channel Selection
    } else {

      if ((n_jetsCen35 == 4) && (n_tags35_70 == 1)) {
        m_weight *= (jetWeight35 * bweight35_70);
        return 6;

      } else if ((n_jetsCen35 == 4) && (n_tags35_70 >= 2)) {
        m_weight *= (jetWeight35 * bweight35_70);
        return 5;

      } else if ((n_jetsCen25 == 5) && (n_tags25_70 == 1)) {
        m_weight *= (jetWeight25 * bweight25_70);
        return 4;

      } else if ((n_jetsCen25 == 5) && (n_tags25_70 >= 2)) {
        m_weight *= (jetWeight25 * bweight25_70);
        return 3;

      } else if ((n_jetsCen25 >= 6) && (n_tags25_70 == 1)) {
        m_weight *= (jetWeight25 * bweight25_70);
        return 2;

      } else if ((n_jetsCen25 >= 6) && (n_tags25_70 >= 2)) {
        m_weight *= (jetWeight25 * bweight25_70);
        return 1;
      }
    }

    // should only reach here if failed selection
    return 0;
  }


  //___________________________________________________________________________________________________________
  // ---------------------------------------------------------------------------------
  //   ttH Category Moriond 2017, BDT Based Selection  (jared.vasquez@yale.edu)
  // ---------------------------------------------------------------------------------
  int HGamCategoryTool::Passes_ttHBDT_Moriond(
    const xAOD::PhotonContainer *    /*photons*/,
    const xAOD::ElectronContainer  *electrons,
    const xAOD::MuonContainer      *muons,
    const xAOD::JetContainer       *jets,
    const xAOD::JetContainer       *jetsJVT,
    const xAOD::MissingETContainer * /*met*/)
  {
    m_weight = 1.0;
    int n_electrons = electrons->size();
    int n_muons = muons->size();
    int n_leptons = (n_electrons + n_muons);

    int n_jetsCen25(0), n_jetsCen30(0), n_jetsFwd25(0);
    int n_tags25_70(0), n_tags30_70(0);

    double bweight25_70(1.0), bweight30_70(1.0);

    // count only central jets s.t. |eta| < 2.5
    for (auto jet : *jets) {
      if (fabs(jet->eta()) > 2.5) { n_jetsFwd25++; continue; }

      n_jetsCen25++;
      bweight25_70 *= jet->auxdata<float>("SF_MV2c10_FixedCutBEff_70");

      if (jet->auxdata<char>("MV2c10_FixedCutBEff_70")) { n_tags25_70++; }

      // Count 30 GeV Jets and Tags
      if (jet->pt() >= 30 * HG::GeV) {
        n_jetsCen30++;
        bweight30_70 *= jet->auxdata<float>("SF_MV2c10_FixedCutBEff_70");

        if (jet->auxdata<char>("MV2c10_FixedCutBEff_70")) { n_tags30_70++; }
      }
    }

    // use other jet collection for JVT weights
    double jetWeight25(1.0), jetWeight30(1.0);

    for (auto jet : *jetsJVT) {
      jetWeight25   *= jet->auxdata<float>("SF_jvt");

      if (jet->pt() >= 30 * HG::GeV)
      { jetWeight30 *= jet->auxdata<float>("SF_jvt"); }
    }

    // Leptonic Channel Selection
    if (n_leptons >= 1) {

      double mll = -999.; // require OS dileptons in future?

      if (n_muons >= 2) { mll = (muons->at(0)->p4() + muons->at(1)->p4()).M() * HG::invGeV; }

      if (n_electrons >= 2) { mll = (electrons->at(0)->p4() + electrons->at(1)->p4()).M() * HG::invGeV; }

      if (fabs(mll - 91) < 10) { return 0; } // fail selection

      m_weight *= getLeptonSFs(electrons, muons);   // lepton SFs
      m_weight *= (jetWeight25 * bweight25_70); // jvt & b-tag SFs

      bool passPreselTH = ((n_leptons == 1) && (n_tags25_70 >= 1));

      if (passPreselTH && (n_jetsCen25 <= 3) && (n_jetsFwd25 == 0))  { return 9; }
      else if (passPreselTH && (n_jetsCen25 <= 4) && (n_jetsFwd25 >= 1))  { return 8; }
      else if ((n_jetsCen25 >= 2) && (n_tags25_70 >= 1))  { return 7; }

      // Hadronic Channel Selection
    } else {

      if (n_leptons > 0) { return 0; } // dumb check

      // Get some features not in HGamVariables
      t_N_bjet30    = n_tags30_70;

      bool preselBDT = ((n_tags30_70 > 0) && (var::N_j_30() >= 3));
      float MVA_weight_ttHhad = (!preselBDT) ? -1.0 : getMVAWeight(m_reader_ttHhad);

      // BDT hadronic selection
      if (preselBDT && (MVA_weight_ttHhad > 0.92)) {
        m_weight *= (jetWeight30 * bweight30_70);
        return 6;
      } else if (preselBDT && (MVA_weight_ttHhad > 0.83)) {
        m_weight *= (jetWeight30 * bweight30_70);
        return 5;
      } else if (preselBDT && (MVA_weight_ttHhad > 0.79)) {
        m_weight *= (jetWeight30 * bweight30_70);
        return 4;
      } else if (preselBDT && (MVA_weight_ttHhad > 0.52)) {
        m_weight *= (jetWeight30 * bweight30_70);
        return 3;

        // tH hadronic selection
      } else if ((n_jetsCen25 == 4) && (n_tags25_70 == 1)) {
        m_weight *= (jetWeight25 * bweight25_70);
        return 2;
      } else if ((n_jetsCen25 == 4) && (n_tags25_70 >= 2)) {
        m_weight *= (jetWeight25 * bweight25_70);
        return 1;
      }

    }

    // should only reach here if failed selection
    return 0;
  }


  //___________________________________________________________________________________________________________
  // ---------------------------------------------------------------------------------
  //   ttH Category Moriond 2017, BDT Based Selection  (jared.vasquez@yale.edu)
  // ---------------------------------------------------------------------------------
  int HGamCategoryTool::Passes_ttHBDTlep_Moriond(
    const xAOD::PhotonContainer *    /*photons*/,
    const xAOD::ElectronContainer  *electrons,
    const xAOD::MuonContainer      *muons,
    const xAOD::JetContainer       *jets,
    const xAOD::JetContainer       *jetsJVT,
    const xAOD::MissingETContainer * /*met*/)
  {
    m_weight = 1.0;
    int n_electrons = electrons->size();
    int n_muons = muons->size();
    int n_leptons = (n_electrons + n_muons);

    int n_jetsCen25(0), n_jetsCen30(0), n_jetsFwd25(0);
    int n_tags25_70(0), n_tags30_70(0);

    double bweight25_70(1.0), bweight30_70(1.0);

    // count only central jets s.t. |eta| < 2.5
    for (auto jet : *jets) {
      if (fabs(jet->eta()) > 2.5) { n_jetsFwd25++; continue; }

      n_jetsCen25++;
      bweight25_70 *= jet->auxdata<float>("SF_MV2c10_FixedCutBEff_70");

      if (jet->auxdata<char>("MV2c10_FixedCutBEff_70")) { n_tags25_70++; }

      // Count 30 GeV Jets and Tags
      if (jet->pt() >= 30 * HG::GeV) {
        n_jetsCen30++;
        bweight30_70 *= jet->auxdata<float>("SF_MV2c10_FixedCutBEff_70");

        if (jet->auxdata<char>("MV2c10_FixedCutBEff_70")) { n_tags30_70++; }
      }
    }

    // use other jet collection for JVT weights
    double jetWeight25(1.0), jetWeight30(1.0);

    for (auto jet : *jetsJVT) {
      jetWeight25   *= jet->auxdata<float>("SF_jvt");

      if (jet->pt() >= 30 * HG::GeV)
      { jetWeight30 *= jet->auxdata<float>("SF_jvt"); }
    }

    // Leptonic Channel Selection
    if (n_leptons >= 1) {

      m_weight *= getLeptonSFs(electrons, muons);   // lepton SFs

      // Get some features not in HGamVariables
      t_N_bjet30    = n_tags30_70;
      t_pTlepMET    = var::pTlepMET();
      t_massT       = var::massTrans();

      bool preselBDT = (n_tags30_70 >= 1);
      float MVA_weight_ttHlep = (!preselBDT) ? -1.0 : getMVAWeight(m_reader_ttHlep);

      if (preselBDT && (MVA_weight_ttHlep > 0.83)) {
        m_weight *= (jetWeight30 * bweight30_70);
        return 9;
      } else if (preselBDT && (MVA_weight_ttHlep > 0.30)) {
        m_weight *= (jetWeight30 * bweight30_70);
        return 8;
      }

      bool passPreselTH = ((n_leptons == 1) && (n_tags25_70 >= 1));

      if (passPreselTH && (n_jetsCen25 <= 3) && (n_jetsFwd25 == 0)) {
        m_weight *= (jetWeight25 * bweight25_70);
        return 7;
      } else if (passPreselTH && (n_jetsCen25 <= 4) && (n_jetsFwd25 >= 1)) {
        m_weight *= (jetWeight25 * bweight25_70);
        return 6;
      }

      // Hadronic Channel Selection
    } else {

      if (n_leptons > 0) { return 0; } // dumb check

      // Get some features not in HGamVariables
      t_N_bjet30    = n_tags30_70;

      bool preselBDT = ((n_tags30_70 > 0) && (var::N_j_30() >= 3));
      float MVA_weight_ttHhad = (!preselBDT) ? -1.0 : getMVAWeight(m_reader_ttHhad);

      // BDT hadronic selection
      if (preselBDT && (MVA_weight_ttHhad > 0.92)) {
        m_weight *= (jetWeight30 * bweight30_70);
        return 5;
      } else if (preselBDT && (MVA_weight_ttHhad > 0.83)) {
        m_weight *= (jetWeight30 * bweight30_70);
        return 4;
      } else if (preselBDT && (MVA_weight_ttHhad > 0.79)) {
        m_weight *= (jetWeight30 * bweight30_70);
        return 3;
      } else if (preselBDT && (MVA_weight_ttHhad > 0.52)) {
        m_weight *= (jetWeight30 * bweight30_70);
        return 2;

        // tH hadronic selection
      } else if ((n_jetsCen25 == 4) && (n_tags25_70 >= 1)) {
        m_weight *= (jetWeight25 * bweight25_70);
        return 1;
      }

    }

    // should only reach here if failed selection
    return 0;
  }


  //___________________________________________________________________________________________________________
  // ---------------------------------------------------------------------------------
  //   XGBoost ttH
  // ---------------------------------------------------------------------------------
  int HGamCategoryTool::Passes_XGBoost_ttH(
    const xAOD::PhotonContainer    *photons,
    const xAOD::ElectronContainer  *electrons,
    const xAOD::MuonContainer      *muons,
    const xAOD::JetContainer       *jets,
    const xAOD::JetContainer       *jetsJVT,
    const xAOD::MissingETContainer *met)
  {
    m_weight = 1.0;
    m_score = -1.0;
    int n_electrons = electrons->size();
    int n_muons = muons->size();
    int n_leptons = (n_electrons + n_muons);

    int n_jets25(0);
    int n_tags25_77(0);

    double bweight(1.0);

    for (auto jet : *jets) {
      n_jets25++;

      if (jet->auxdata<char>("MV2c10_FixedCutBEff_77")) { n_tags25_77++; }

      /*
      int pscore = (int) get_pseudoCont_score(jet->auxdata<double>("MV2c10_discriminant"));
      std::string bsf = "SF_MV2c10_FixedCutBEff_";

      if (pscore == 5) { bweight *= jet->auxdata<float>(bsf + "60"); }
      else if (pscore == 4) { bweight *= jet->auxdata<float>(bsf + "70"); }
      else if (pscore == 3) { bweight *= jet->auxdata<float>(bsf + "77"); }
      else if (pscore == 2) { bweight *= jet->auxdata<float>(bsf + "85"); }
      */

      bweight *= jet->isAvailable<float>("SF_MV2c10_Continuous") ? jet->auxdata<float>("SF_MV2c10_Continuous") : 1.0;
    }

    // use other jet collection for JVT weights
    double jetWeight25(1.0);

    for (auto jet : *jetsJVT) {
      jetWeight25   *= jet->auxdata<float>("SF_jvt");
    }

    // Leptonic Channel Selection
    if (n_leptons >= 1) {
      if (!(n_tags25_77 > 0)) { return 0; }

      m_weight *= getLeptonSFs(electrons, muons);   // lepton SFs
      m_weight *= (jetWeight25 * bweight); // jvt & b-tag SFs

      float *vars = make_XGBoost_DMatrix_ttHlep(photons, electrons, muons, jets, met); // build DMatrix
      m_score = get_XGBoost_Weight(m_xgboost_ttHlep, vars, 45); // get score

      if (m_score < 0.013)  { return 9; }
      else if (m_score < 0.058)  { return 8; }
      else if (m_score < 0.295)  { return 7; }

      // Hadronic Channel Selection
    } else {
      if (!((n_tags25_77 > 0) && (n_jets25 >= 3))) { return 0; }   // preselection

      float *vars = make_XGBoost_DMatrix_ttHhad(photons, jets, met); // build DMatrix
      m_score = get_XGBoost_Weight(m_xgboost_ttHhad, vars, 45); // get score

      if (m_score < 0.004) {
        m_weight *= (jetWeight25 * bweight);
        return 6;
      } else if (m_score < 0.009) {
        m_weight *= (jetWeight25 * bweight);
        return 5;
      } else if (m_score < 0.029) {
        m_weight *= (jetWeight25 * bweight);
        return 4;
      } else if (m_score < 0.089) {
        m_weight *= (jetWeight25 * bweight);
        return 3;
      }
    }

    // should only reach here if failed selection
    return 0;
  }

  //___________________________________________________________________________________________________________
  // ---------------------------------------------------------------------------------
  //   XGBoost ttH + top reco
  // ---------------------------------------------------------------------------------
  int HGamCategoryTool::Passes_XGBoost_ttH_topReco(
    const xAOD::PhotonContainer    *photons,
    const xAOD::ElectronContainer  *electrons,
    const xAOD::MuonContainer      *muons,
    const xAOD::JetContainer       *jets,
    const xAOD::JetContainer       *jetsJVT,
    const xAOD::MissingETContainer *met)
  {
    m_weight = 1.0;
    m_score = -1.0;

    int n_electrons = electrons->size();
    int n_muons = muons->size();
    int n_leptons = (n_electrons + n_muons);

    int n_jets25 = 0;
    int n_tags25_77 = 0;
    int n_tags25_85 = 0;

    double bweight(1.0);

    for (auto jet : *jets) {
      n_jets25++;

      if (jet->auxdata<char>("MV2c10_FixedCutBEff_77")) { n_tags25_77++; }

      if (jet->auxdata<char>("MV2c10_FixedCutBEff_85")) { n_tags25_85++; }

      bweight *= jet->isAvailable<float>("SF_MV2c10_Continuous") ? jet->auxdata<float>("SF_MV2c10_Continuous") : 1.0;
    }

    // use other jet collection for JVT weights
    double jetWeight25 = 1.0;

    for (auto jet : *jetsJVT) {
      jetWeight25   *= jet->auxdata<float>("SF_jvt");
    }

    // Leptonic Channel Selection
    if (var::N_lep() > 0 && n_tags25_85 > 0) {
      float *vars = make_XGBoost_DMatrix_ttHlep_topReco(photons, electrons, muons, jets, met);

      m_weight *= getLeptonSFs(electrons, muons);   // lepton SFs
      m_weight *= (jetWeight25 * bweight); // jvt & b-tag SFs

      m_score = get_XGBoost_Weight(m_xgboost_ttHlep_topReco, vars, 17); // get score

      if (m_score > 0.761)  { return 9; }
      else if (m_score > 0.550)  { return 8; }
      else if (m_score > 0.210)  { return 7; }
    }

    // Calculate hadronic top variables for the loosest possible pre-selection
    if (var::N_lep() == 0 && n_jets25 > 2 && n_tags25_77 > 0) {
      decorateTopCandidate(jets);
    }

    // Hadronic Channel Selection
    if (var::N_lep() == 0 && var::N_j_30() > 2 && var::N_j_btag30() > 0) {
      float *vars = make_XGBoost_DMatrix_ttHhad_topReco(photons, jets, met);
      m_score = get_XGBoost_Weight(m_xgboost_ttHhad_topReco, vars, 20);

      if (m_score > 0.973) {
        m_weight *= (jetWeight25 * bweight);
        return 6;
      } else if (m_score > 0.960) {
        m_weight *= (jetWeight25 * bweight);
        return 5;
      } else if (m_score > 0.900) {
        m_weight *= (jetWeight25 * bweight);
        return 4;
      } else if (m_score > 0.700) {
        m_weight *= (jetWeight25 * bweight);
        return 3;
      }
    }

    // should only reach here if failed selection
    return 0;
  }

  //___________________________________________________________________________________________________________
  double HGamCategoryTool::getLeptonSFs(const xAOD::ElectronContainer *electrons, const xAOD::MuonContainer *muons)
  {
    double lepSF = 1.0;

    for (auto el : *electrons) { lepSF *= el->auxdata< float >("scaleFactor"); }

    for (auto mu : *muons)     { lepSF *= mu->auxdata< float >("scaleFactor"); }

    return lepSF;
  }


  //___________________________________________________________________________________________________________
  void HGamCategoryTool::resetReader()
  {
    t_pTt_yyGeV     = var::pTt_yy() * HG::invGeV; // VH had uses GeV
    t_m_jjGeV       = var::m_jj() * HG::invGeV; // VH had uses GeV
    t_pTt_yy        = var::pTt_yy();
    t_m_jj          = var::m_jj();
    t_cosTS_yy_jj   = var::cosTS_yyjj();
    t_dEta_jj       = var::Deta_j_j();
    t_dPhi_yy_jj    = var::Dphi_yy_jj();
    t_Zepp          = fabs(var::Zepp());
    //t_Drmin_y_j     = var::DRmin_y_j();
    t_Drmin_y_j2    = var::DRmin_y_j_2();
    t_Dy_yy_jj      = var::Dy_yy_jj();
    t_N_j30         = var::N_j_30();
    t_N_j_central30 = var::N_j_central30();
    t_m_alljet_30   = var::m_alljet_30();
    t_HT_30         = var::HT_30();
  }


  //___________________________________________________________________________________________________________
  // -----------------------------------------------
  //  Moriond 2017 Categories ( Cut Based ttH )
  // -----------------------------------------------
  std::pair<int, float> HGamCategoryTool::getCategoryAndWeightMoriond2017(
    const xAOD::PhotonContainer     *photons,
    const xAOD::ElectronContainer   *electrons,
    const xAOD::MuonContainer       *muons,
    const xAOD::JetContainer        *jets,
    const xAOD::JetContainer        *jetsJVT,
    const xAOD::MissingETContainer  *met)
  {
    if (not photons || not electrons || not muons || not jets || not jetsJVT || not met) { return std::make_pair(-1, 0.0); }

    if (photons->size() < 2)     { return std::make_pair(M17_FailDiphoton, 0.0); }

    resetReader(); // Preps variables for TMVA reader

    //    ttH Categories
    // --------------------------------
    int CtthCat = Passes_ttH_Moriond(photons, electrons, muons, jets, jetsJVT, met);

    if (CtthCat > 0)               { return std::make_pair(M17_ttH + CtthCat - 1, m_weight); }

    //   qq->VH Categories  3+2+2
    // --------------------------------
    int CVH2l  = Passes_VH_dileptons_Moriond2017(photons, electrons, muons);

    if (CVH2l >= 2)                { return std::make_pair(M17_VHdilep_HIGH, m_weight); }

    if (CVH2l == 1)                { return std::make_pair(M17_VHdilep_LOW,  m_weight); }

    int CVHlep = Passes_VH_leptonic_Moriond2017(photons, electrons, muons, met);

    if (CVHlep >= 2)               { return std::make_pair(M17_VHlep_HIGH,   m_weight); }

    if (CVHlep == 1)               { return std::make_pair(M17_VHlep_LOW,    m_weight); }

    int CVHMET = Passes_VH_MET_Moriond2017(photons, met);

    if (CVHMET >= 3)               { return std::make_pair(M17_VHMET_BSM,    m_weight); }

    if (CVHMET == 2)               { return std::make_pair(M17_VHMET_HIGH,   m_weight); }

    if (CVHMET == 1)               { return std::make_pair(M17_VHMET_LOW,    m_weight); }

    //   qq->H 1 category
    // --------------------------------
    if (Passes_qqH_BSM(jetsJVT)) { return std::make_pair(M17_qqH_BSM,      m_weight); }

    //   VH hadronic 2 categories
    // --------------------------------
    if (Passes_VH_hadronic_Moriond2017(jetsJVT)) {
      float wMVA_VH = getMVAWeight(m_readerVH_had_Moriond2017);

      if (wMVA_VH >  0.73)    { return std::make_pair(M17_VHhad_tight,  m_weight); }

      if (wMVA_VH >  0.18)    { return std::make_pair(M17_VHhad_loose,  m_weight); }
    }

    //   VBF categories 2+2
    // --------------------------------
    int CVBF = Passes_VBF_Moriond2017(jetsJVT);

    if (CVBF == 2)  { // VBF, High pT Hjj
      float wMVA_VBF = getMVAWeight(m_readerVBF_high);

      if (wMVA_VBF >  0.48)    { return std::make_pair(M17_VBF_HjjHIGH_tight, m_weight); }

      if (wMVA_VBF > -0.50)    { return std::make_pair(M17_VBF_HjjHIGH_loose, m_weight); }
    }

    if (CVBF == 1)  { // VBF, Low pT Hjj
      float wMVA_VBF = getMVAWeight(m_readerVBF_low);

      if (wMVA_VBF >  0.87)    { return std::make_pair(M17_VBF_HjjLOW_tight,  m_weight); }

      if (wMVA_VBF > -0.39)    { return std::make_pair(M17_VBF_HjjLOW_loose,  m_weight); }
    }

    //   gg->H Categories
    // --------------------------------
    return std::make_pair(split_ggH_STXS(photons, jetsJVT), m_weight);
  }



  //___________________________________________________________________________________________________________
  // -----------------------------------------------
  //  Moriond 2017 Categories ( BDT Based ttH )
  // -----------------------------------------------
  std::pair<int, float> HGamCategoryTool::getCategoryAndWeightMoriond2017_ttHBDT_qqH2jet(
    const xAOD::PhotonContainer     *photons,
    const xAOD::ElectronContainer   *electrons,
    const xAOD::MuonContainer       *muons,
    const xAOD::JetContainer        *jets,
    const xAOD::JetContainer        *jetsJVT,
    const xAOD::MissingETContainer  *met)
  {
    if (not photons || not electrons || not muons || not jets || not jetsJVT || not met) { return std::make_pair(-1, 0.0); }

    if (photons->size() < 2)     { return std::make_pair(M17_FailDiphoton, 0.0); }

    resetReader(); // Preps variables for TMVA reader

    //    ttH Categories
    // --------------------------------
    int CtthCat = Passes_ttHBDT_Moriond(photons, electrons, muons, jets, jetsJVT, met);

    if (CtthCat > 0)               { return std::make_pair(M17_ttH + CtthCat - 1, m_weight); }

    //   qq->VH Categories  3+2+2
    // --------------------------------
    int CVH2l  = Passes_VH_dileptons_Moriond2017(photons, electrons, muons);

    if (CVH2l >= 2)                { return std::make_pair(M17_VHdilep_HIGH, m_weight); }

    if (CVH2l == 1)                { return std::make_pair(M17_VHdilep_LOW,  m_weight); }

    int CVHlep = Passes_VH_leptonic_Moriond2017(photons, electrons, muons, met);

    if (CVHlep >= 2)               { return std::make_pair(M17_VHlep_HIGH,   m_weight); }

    if (CVHlep == 1)               { return std::make_pair(M17_VHlep_LOW,    m_weight); }

    int CVHMET = Passes_VH_MET_Moriond2017(photons, met);

    if (CVHMET >= 3)               { return std::make_pair(M17_VHMET_BSM,    m_weight); }

    if (CVHMET == 2)               { return std::make_pair(M17_VHMET_HIGH,   m_weight); }

    if (CVHMET == 1)               { return std::make_pair(M17_VHMET_LOW,    m_weight); }

    //   qq->H 1 category
    // --------------------------------
    if (Passes_qqH_BSM_2jet(jetsJVT)) { return std::make_pair(M17_qqH_BSM,      m_weight); }

    //   VH hadronic 2 categories
    // --------------------------------
    if (Passes_VH_hadronic_Moriond2017(jetsJVT)) {
      float wMVA_VH = getMVAWeight(m_readerVH_had_Moriond2017);

      if (wMVA_VH >  0.73)    { return std::make_pair(M17_VHhad_tight,  m_weight); }

      if (wMVA_VH >  0.18)    { return std::make_pair(M17_VHhad_loose,  m_weight); }
    }

    //   VBF categories 2+2
    // --------------------------------
    int CVBF = Passes_VBF_Moriond2017(jetsJVT);

    if (CVBF == 2)  { // VBF, High pT Hjj
      float wMVA_VBF = getMVAWeight(m_readerVBF_high);

      if (wMVA_VBF >  0.48)    { return std::make_pair(M17_VBF_HjjHIGH_tight, m_weight); }

      if (wMVA_VBF > -0.50)    { return std::make_pair(M17_VBF_HjjHIGH_loose, m_weight); }
    }

    if (CVBF == 1)  { // VBF, Low pT Hjj
      float wMVA_VBF = getMVAWeight(m_readerVBF_low);

      if (wMVA_VBF >  0.87)    { return std::make_pair(M17_VBF_HjjLOW_tight,  m_weight); }

      if (wMVA_VBF > -0.39)    { return std::make_pair(M17_VBF_HjjLOW_loose,  m_weight); }
    }


    //   gg->H Categories
    // --------------------------------
    return std::make_pair(split_ggH_STXS(photons, jetsJVT), m_weight);
  }



  //___________________________________________________________________________________________________________
  // -----------------------------------------------
  //  Moriond 2017 Categories ( BDT Based ttH )
  // -----------------------------------------------
  std::pair<int, float> HGamCategoryTool::getCategoryAndWeightMoriond2017_ttHBDT(
    const xAOD::PhotonContainer     *photons,
    const xAOD::ElectronContainer   *electrons,
    const xAOD::MuonContainer       *muons,
    const xAOD::JetContainer        *jets,
    const xAOD::JetContainer        *jetsJVT,
    const xAOD::MissingETContainer  *met)
  {
    if (not photons || not electrons || not muons || not jets || not jetsJVT || not met) { return std::make_pair(-1, 0.0); }

    if (photons->size() < 2)     { return std::make_pair(M17_FailDiphoton, 0.0); }

    resetReader(); // Preps variables for TMVA reader

    //    ttH Categories
    // --------------------------------
    int CtthCat = Passes_ttHBDT_Moriond(photons, electrons, muons, jets, jetsJVT, met);

    if (CtthCat > 0)               { return std::make_pair(M17_ttH + CtthCat - 1, m_weight); }

    //   qq->VH Categories  3+2+2
    // --------------------------------
    int CVH2l  = Passes_VH_dileptons_Moriond2017(photons, electrons, muons);

    if (CVH2l >= 2)                { return std::make_pair(M17_VHdilep_HIGH, m_weight); }

    if (CVH2l == 1)                { return std::make_pair(M17_VHdilep_LOW,  m_weight); }

    int CVHlep = Passes_VH_leptonic_Moriond2017(photons, electrons, muons, met);

    if (CVHlep >= 2)               { return std::make_pair(M17_VHlep_HIGH,   m_weight); }

    if (CVHlep == 1)               { return std::make_pair(M17_VHlep_LOW,    m_weight); }

    int CVHMET = Passes_VH_MET_Moriond2017(photons, met);

    if (CVHMET >= 3)               { return std::make_pair(M17_VHMET_BSM,    m_weight); }

    if (CVHMET == 2)               { return std::make_pair(M17_VHMET_HIGH,   m_weight); }

    if (CVHMET == 1)               { return std::make_pair(M17_VHMET_LOW,    m_weight); }

    //   qq->H 1 category
    // --------------------------------
    if (Passes_qqH_BSM(jetsJVT)) { return std::make_pair(M17_qqH_BSM,      m_weight); }

    //   VH hadronic 2 categories
    // --------------------------------
    if (Passes_VH_hadronic_Moriond2017(jetsJVT)) {
      float wMVA_VH = getMVAWeight(m_readerVH_had_Moriond2017);

      if (wMVA_VH >  0.73)    { return std::make_pair(M17_VHhad_tight,  m_weight); }

      if (wMVA_VH >  0.18)    { return std::make_pair(M17_VHhad_loose,  m_weight); }
    }

    //   VBF categories 2+2
    // --------------------------------
    int CVBF = Passes_VBF_Moriond2017(jetsJVT);

    if (CVBF == 2)  { // VBF, High pT Hjj
      float wMVA_VBF = getMVAWeight(m_readerVBF_high);

      if (wMVA_VBF >  0.48)    { return std::make_pair(M17_VBF_HjjHIGH_tight, m_weight); }

      if (wMVA_VBF > -0.50)    { return std::make_pair(M17_VBF_HjjHIGH_loose, m_weight); }
    }

    if (CVBF == 1)  { // VBF, Low pT Hjj
      float wMVA_VBF = getMVAWeight(m_readerVBF_low);

      if (wMVA_VBF >  0.87)    { return std::make_pair(M17_VBF_HjjLOW_tight,  m_weight); }

      if (wMVA_VBF > -0.39)    { return std::make_pair(M17_VBF_HjjLOW_loose,  m_weight); }
    }


    //   gg->H Categories
    // --------------------------------
    return std::make_pair(split_ggH_STXS(photons, jetsJVT), m_weight);
  }



  //___________________________________________________________________________________________________________
  // -----------------------------------------------
  //  Moriond 2017 Categories ( BDT Based ttH )
  // -----------------------------------------------
  std::pair<int, float> HGamCategoryTool::getCategoryAndWeightMoriond2017_ttHBDTlep(
    const xAOD::PhotonContainer     *photons,
    const xAOD::ElectronContainer   *electrons,
    const xAOD::MuonContainer       *muons,
    const xAOD::JetContainer        *jets,
    const xAOD::JetContainer        *jetsJVT,
    const xAOD::MissingETContainer  *met)
  {
    if (not photons || not electrons || not muons || not jets || not jetsJVT || not met) { return std::make_pair(-1, 0.0); }

    if (photons->size() < 2)     { return std::make_pair(M17_FailDiphoton, 0.0); }

    resetReader(); // Preps variables for TMVA reader

    //    ttH Categories
    // --------------------------------
    int CtthCat = Passes_ttHBDTlep_Moriond(photons, electrons, muons, jets, jetsJVT, met);

    if (CtthCat > 0)               { return std::make_pair(M17_ttH + CtthCat - 1, m_weight); }

    //   qq->VH Categories  3+2+2
    // --------------------------------
    int CVH2l  = Passes_VH_dileptons_Moriond2017(photons, electrons, muons);

    if (CVH2l >= 2)                { return std::make_pair(M17_VHdilep_HIGH, m_weight); }

    if (CVH2l == 1)                { return std::make_pair(M17_VHdilep_LOW,  m_weight); }

    int CVHlep = Passes_VH_leptonic_Moriond2017(photons, electrons, muons, met);

    if (CVHlep >= 2)               { return std::make_pair(M17_VHlep_HIGH,   m_weight); }

    if (CVHlep == 1)               { return std::make_pair(M17_VHlep_LOW,    m_weight); }

    int CVHMET = Passes_VH_MET_Moriond2017(photons, met);

    if (CVHMET >= 3)               { return std::make_pair(M17_VHMET_BSM,    m_weight); }

    if (CVHMET == 2)               { return std::make_pair(M17_VHMET_HIGH,   m_weight); }

    if (CVHMET == 1)               { return std::make_pair(M17_VHMET_LOW,    m_weight); }

    //   qq->H 1 category
    // --------------------------------
    if (Passes_qqH_BSM(jetsJVT)) { return std::make_pair(M17_qqH_BSM,      m_weight); }

    //   VH hadronic 2 categories
    // --------------------------------
    if (Passes_VH_hadronic_Moriond2017(jetsJVT)) {
      float wMVA_VH = getMVAWeight(m_readerVH_had_Moriond2017);

      if (wMVA_VH >  0.73)    { return std::make_pair(M17_VHhad_tight,  m_weight); }

      if (wMVA_VH >  0.18)    { return std::make_pair(M17_VHhad_loose,  m_weight); }
    }

    //   VBF categories 2+2
    // --------------------------------
    int CVBF = Passes_VBF_Moriond2017(jetsJVT);

    if (CVBF == 2)  { // VBF, High pT Hjj
      float wMVA_VBF = getMVAWeight(m_readerVBF_high);

      if (wMVA_VBF >  0.48)    { return std::make_pair(M17_VBF_HjjHIGH_tight, m_weight); }

      if (wMVA_VBF > -0.50)    { return std::make_pair(M17_VBF_HjjHIGH_loose, m_weight); }
    }

    if (CVBF == 1)  { // VBF, Low pT Hjj
      float wMVA_VBF = getMVAWeight(m_readerVBF_low);

      if (wMVA_VBF >  0.87)    { return std::make_pair(M17_VBF_HjjLOW_tight,  m_weight); }

      if (wMVA_VBF > -0.39)    { return std::make_pair(M17_VBF_HjjLOW_loose,  m_weight); }
    }


    //   gg->H Categories
    // --------------------------------
    return std::make_pair(split_ggH_STXS(photons, jetsJVT), m_weight);
  }

  //___________________________________________________________________________________________________________
  // -----------------------------------------------
  //  XGBoost ttH
  // -----------------------------------------------
  float *HGamCategoryTool::getCategoryAndWeightXGBoost_ttH(
    const xAOD::PhotonContainer     *photons,
    const xAOD::ElectronContainer   *electrons,
    const xAOD::MuonContainer       *muons,
    const xAOD::JetContainer        *jets,
    const xAOD::JetContainer        *jetsJVT,
    const xAOD::MissingETContainer  *met)
  {
    static float results[3] = {0, 0, 0};

    if (not photons || not electrons || not muons || not jets || not jetsJVT || not met) {results[0] = -1; return results;}

    if (photons->size() < 2)     {results[0] = M17_FailDiphoton; return results;}

    resetReader(); // Preps variables for TMVA reader

    //    ttH Categories
    // --------------------------------
    int CtthCat = Passes_XGBoost_ttH(photons, electrons, muons, jets, jetsJVT, met);

    //std::cout << m_score << std::endl;
    if (CtthCat > 0)               {results[0] = M17_ttH + CtthCat - 1; results[1] = m_weight; results[2] = m_score; return results;}

    //   qq->VH Categories  3+2+2
    // --------------------------------
    int CVH2l  = Passes_VH_dileptons_Moriond2017(photons, electrons, muons);

    if (CVH2l >= 2)                {results[0] = M17_VHdilep_HIGH; results[1] = m_weight; results[2] = m_score; return results;}

    if (CVH2l == 1)                {results[0] = M17_VHdilep_LOW;  results[1] = m_weight; results[2] = m_score; return results;}

    int CVHlep = Passes_VH_leptonic_Moriond2017(photons, electrons, muons, met);

    if (CVHlep >= 2)               {results[0] = M17_VHlep_HIGH; results[1] = m_weight; results[2] = m_score; return results;}

    if (CVHlep == 1)               {results[0] = M17_VHlep_LOW;  results[1] = m_weight; results[2] = m_score; return results;}

    int CVHMET = Passes_VH_MET_Moriond2017(photons, met);

    if (CVHMET >= 3)               {results[0] = M17_VHMET_BSM;  results[1] = m_weight; results[2] = m_score; return results;}

    if (CVHMET == 2)               {results[0] = M17_VHMET_HIGH; results[1] = m_weight; results[2] = m_score; return results;}

    if (CVHMET == 1)               {results[0] = M17_VHMET_LOW;  results[1] = m_weight; results[2] = m_score; return results;}

    //   qq->H 1 category
    // --------------------------------
    // if (Passes_qqH_BSM(jetsJVT)) {results[0] = M17_qqH_BSM; results[1] = m_weight; results[2] = m_score; return results;}
    if (Passes_qqH_BSM_2jet(jetsJVT)) {results[0] = M17_qqH_BSM; results[1] = m_weight; results[2] = m_score; return results;}

    //   VH hadronic 2 categories
    // --------------------------------
    if (Passes_VH_hadronic_Moriond2017(jetsJVT)) {
      float wMVA_VH = getMVAWeight(m_readerVH_had_Moriond2017);

      if (wMVA_VH >  0.73)    {results[0] = M17_VHhad_tight; results[1] = m_weight; results[2] = m_score; return results;}

      if (wMVA_VH >  0.18)    {results[0] = M17_VHhad_loose; results[1] = m_weight; results[2] = m_score; return results;}
    }

    //   VBF categories 2+2
    // --------------------------------
    int CVBF = Passes_VBF_Moriond2017(jetsJVT);

    if (CVBF == 2)  { // VBF, High pT Hjj
      float wMVA_VBF = getMVAWeight(m_readerVBF_high);

      if (wMVA_VBF >  0.48)    {results[0] = M17_VBF_HjjHIGH_tight; results[1] = m_weight; results[2] = m_score; return results;}

      if (wMVA_VBF > -0.50)    {results[0] = M17_VBF_HjjHIGH_loose; results[1] = m_weight; results[2] = m_score; return results;}
    }

    if (CVBF == 1)  { // VBF, Low pT Hjj
      float wMVA_VBF = getMVAWeight(m_readerVBF_low);

      if (wMVA_VBF >  0.87)    {results[0] = M17_VBF_HjjLOW_tight; results[1] = m_weight; results[2] = m_score; return results;}

      if (wMVA_VBF > -0.39)    {results[0] = M17_VBF_HjjLOW_loose; results[1] = m_weight; results[2] = m_score; return results;}
    }


    //   gg->H Categories
    // --------------------------------
    results[0] = split_ggH_STXS(photons, jetsJVT);
    results[1] = m_weight;
    results[2] = m_score;
    return results;
  }

  //___________________________________________________________________________________________________________
  // -----------------------------------------------
  //  XGBoost ttH + top reco
  // -----------------------------------------------
  float *HGamCategoryTool::getCategoryAndWeightXGBoost_ttH_topReco(
    const xAOD::PhotonContainer     *photons,
    const xAOD::ElectronContainer   *electrons,
    const xAOD::MuonContainer       *muons,
    const xAOD::JetContainer        *jets,
    const xAOD::JetContainer        *jetsJVT,
    const xAOD::MissingETContainer  *met)
  {
    static float results[3] = {0, 0, 0};

    if (not photons || not electrons || not muons || not jets || not jetsJVT || not met) {results[0] = -1; return results;}

    if (photons->size() < 2)     {results[0] = M17_FailDiphoton; return results;}

    resetReader(); // Preps variables for TMVA reader

    int CtthCat = Passes_XGBoost_ttH_topReco(photons, electrons, muons, jets, jetsJVT, met);

    //std::cout << m_score << std::endl;
    if (CtthCat > 0) {results[0] = M17_ttH + CtthCat - 1; results[1] = m_weight; results[2] = m_score; return results; }

    //   qq->VH Categories  3+2+2
    // --------------------------------
    int CVH2l  = Passes_VH_dileptons_Moriond2017(photons, electrons, muons);

    if (CVH2l >= 2)                {results[0] = M17_VHdilep_HIGH; results[1] = m_weight; results[2] = m_score; return results;}

    if (CVH2l == 1)                {results[0] = M17_VHdilep_LOW;  results[1] = m_weight; results[2] = m_score; return results;}

    int CVHlep = Passes_VH_leptonic_Moriond2017(photons, electrons, muons, met);

    if (CVHlep >= 2)               {results[0] = M17_VHlep_HIGH; results[1] = m_weight; results[2] = m_score; return results;}

    if (CVHlep == 1)               {results[0] = M17_VHlep_LOW;  results[1] = m_weight; results[2] = m_score; return results;}

    int CVHMET = Passes_VH_MET_Moriond2017(photons, met);

    if (CVHMET >= 3)               {results[0] = M17_VHMET_BSM;  results[1] = m_weight; results[2] = m_score; return results;}

    if (CVHMET == 2)               {results[0] = M17_VHMET_HIGH; results[1] = m_weight; results[2] = m_score; return results;}

    if (CVHMET == 1)               {results[0] = M17_VHMET_LOW;  results[1] = m_weight; results[2] = m_score; return results;}

    //   qq->H 1 category
    // --------------------------------

    if (Passes_qqH_BSM_2jet(jetsJVT)) { results[0] = M17_qqH_BSM; results[1] = m_weight; return results;}

    //   VH hadronic 2 categories
    // --------------------------------
    if (Passes_VH_hadronic_Moriond2017(jetsJVT)) {
      float wMVA_VH = getMVAWeight(m_readerVH_had_Moriond2017);

      if (wMVA_VH >  0.73)    {results[0] = M17_VHhad_tight; results[1] = m_weight; results[2] = m_score; return results;}

      if (wMVA_VH >  0.18)    {results[0] = M17_VHhad_loose; results[1] = m_weight; results[2] = m_score; return results;}
    }

    //   VBF categories 2+2
    // --------------------------------
    int CVBF = Passes_VBF_Moriond2017(jetsJVT);

    if (CVBF == 2)  { // VBF, High pT Hjj
      float wMVA_VBF = getMVAWeight(m_readerVBF_high);

      if (wMVA_VBF >  0.48)    {results[0] = M17_VBF_HjjHIGH_tight; results[1] = m_weight; results[2] = m_score; return results;}

      if (wMVA_VBF > -0.50)    {results[0] = M17_VBF_HjjHIGH_loose; results[1] = m_weight; results[2] = m_score; return results;}
    }

    if (CVBF == 1)  { // VBF, Low pT Hjj
      float wMVA_VBF = getMVAWeight(m_readerVBF_low);

      if (wMVA_VBF >  0.87)    {results[0] = M17_VBF_HjjLOW_tight; results[1] = m_weight; results[2] = m_score; return results;}

      if (wMVA_VBF > -0.39)    {results[0] = M17_VBF_HjjLOW_loose; results[1] = m_weight; results[2] = m_score; return results;}
    }

    //   gg->H Categories
    // --------------------------------
    results[0] = split_ggH_STXS(photons, jetsJVT);
    results[1] = m_weight;
    results[2] = m_score;
    return results;
  }

  // RUN1 Mass category: https://cds.cern.ch/record/1642851/files/ATL-COM-PHYS-2014-018.pdf, p.18
  int HGamCategoryTool::getMassCategoryRun1(const xAOD::PhotonContainer *photons)
  {
    double low_pTt = 70;

    if (photons != nullptr && photons->size() > 1) {
      static SG::AuxElement::ConstAccessor<float> eta_s2("eta_s2");

      double abs_etas2_y1 = fabs(eta_s2(*(*photons)[0]));
      double abs_etas2_y2 = fabs(eta_s2(*(*photons)[1]));

      int conv_y1 = (*photons)[0]->conversionType();
      int conv_y2 = (*photons)[1]->conversionType();

      double pTt_yy = var::pTt_yy() / 1000.0;

      const bool double_unconverted = (conv_y1 + conv_y2) == 0;
      const bool isGood = (abs_etas2_y1 < 0.75  && abs_etas2_y2 < 0.75);
      const bool isBad = ((abs_etas2_y1 >= 1.3 && abs_etas2_y1 <= 1.75) || (abs_etas2_y2 >= 1.3 && abs_etas2_y2 <= 1.75));

      if (double_unconverted) {
        if (isGood) {
          if (pTt_yy < low_pTt) { return 1; }
          else { return 2; }
        } else if (!isBad) {
          if (pTt_yy < low_pTt) { return 3; }
          else { return 4; }
        } else { return 5; }
      } else {
        if (isGood) {
          if (pTt_yy < low_pTt) { return 6; }
          else { return 7; }
        } else if (!isBad) {
          if (pTt_yy < low_pTt) { return 8; }
          else { return 9; }
        } else { return 10; }
      }
    } else { return -1; }
  }

  int HGamCategoryTool::getEtaMassCategory(const xAOD::PhotonContainer *photons)
  {
    if (photons->size() > 1) {
      static SG::AuxElement::ConstAccessor<float> eta_s2("eta_s2");
      double abs_etas2_y1 = fabs(eta_s2(*(*photons)[0]));
      double abs_etas2_y2 = fabs(eta_s2(*(*photons)[1]));
      const bool Barrel = (abs_etas2_y1 < 1.37 && abs_etas2_y2 < 1.37);
      const bool Endcap = ((abs_etas2_y1 > 1.52 && abs_etas2_y1 < 2.37) && (abs_etas2_y2 > 1.52 && abs_etas2_y2 < 2.37));

      if (Barrel) { return 1; }
      else if (Endcap) { return 3; }
      else { return 2; }

    } else { return -1; }
  }



  int HGamCategoryTool::getConvMassCategory(const xAOD::PhotonContainer *photons)
  {
    if (photons->size() > 1) {
      int conv_y1 = (*photons)[0]->conversionType() != 0;
      int conv_y2 = (*photons)[1]->conversionType() != 0;
      return conv_y1 + conv_y2 + 1;
    } else { return -1; }
  }



  int HGamCategoryTool::getPtMassCategory(const xAOD::PhotonContainer *photons)
  {
    double low_pT = 58.;

    if (photons->size() > 1) {
      double pT_y1 = (*photons)[0]->pt() / 1000.;
      double pT_y2 = (*photons)[1]->pt() / 1000.;

      if (pT_y1 < low_pT && pT_y2 < low_pT) { return 1; }
      else if (pT_y1 < low_pT || pT_y2 < low_pT) { return 2; }
      else { return 3; }
    } else { return -1; }
  }



  int HGamCategoryTool::getMuMassCategory(float mu)
  {
    if (mu >= 0 && mu < 16) { return 1; }
    else if (mu >= 16 && mu < 20) { return 2; }
    else if (mu >= 20 && mu < 24) { return 3; }
    else if (mu >= 24 && mu < 28) { return 4; }
    else if (mu >= 28) { return 5; }
    else { return -1; }
  }

  float HGamCategoryTool::get_pseudoCont_score(double btagscore)
  {
    if (btagscore > 0.94) { return 5.; }
    else if (btagscore > 0.83) { return 4.; }
    else if (btagscore > 0.64) { return 3.; }
    else if (btagscore > 0.11) { return 2.; }
    else if (btagscore > -1.000000) { return 1.; }
    else { return -9.; }
  }

  float *HGamCategoryTool::make_XGBoost_DMatrix_ttHhad(const xAOD::PhotonContainer    *photons,
                                                       const xAOD::JetContainer       *jets,
                                                       const xAOD::MissingETContainer *met)
  {
    static float vars[45] = {0};

    // photons
    float myy = (photons->at(0)->p4() + photons->at(1)->p4()).M();
    vars[0] = photons->at(0)->p4().Pt() / myy;
    vars[1] = photons->at(0)->p4().Eta();
    vars[2] = photons->at(0)->p4().Phi();
    vars[3] = photons->at(0)->p4().E() / myy;
    vars[4] = 22;
    vars[5] = photons->at(1)->p4().Pt() / myy;
    vars[6] = photons->at(1)->p4().Eta();
    vars[7] = photons->at(1)->p4().Phi();
    vars[8] = photons->at(1)->p4().E() / myy;
    vars[9] = 22;

    // met
    vars[10] = (*met)["TST"]->met();
    vars[11] = 0;
    vars[12] = (*met)["TST"]->phi();
    vars[13] = 0;
    vars[14] = 16;

    // jets
    std::vector< std::vector<float> > js;

    for (auto j : *jets) {
      std::vector<float> ij;
      ij.push_back((float) j->p4().Pt());
      ij.push_back((float) j->p4().Eta());
      ij.push_back((float) j->p4().Phi());
      ij.push_back((float) j->p4().E());

      if ((bool)j->auxdata<char>("MV2c10_FixedCutBEff_77")) { ij.push_back(1.); }
      else { ij.push_back(0.); }

      //ij.push_back(get_pseudoCont_score(j->auxdata<double>("MV2c10_discriminant")));

      js.push_back(ij);
    }

    std::sort(js.begin(), js.end(), [](std::vector<float> a, std::vector<float> b) {return a[0] > b[0];});

    while (js.size() < 6) {
      std::vector<float> ij;

      for (int i = 0; i < 5; i++) { ij.push_back(-999.); }

      js.push_back(ij);
    }

    int i = 15;

    for (auto j : js) {
      vars[i] = j[0];
      i++;
      vars[i] = j[1];
      i++;
      vars[i] = j[2];
      i++;
      vars[i] = j[3];
      i++;
      vars[i] = j[4];
      i++;

      if (i >= 45) { break; }
    }

    return vars;
  }

  float *HGamCategoryTool::make_XGBoost_DMatrix_ttHlep(const xAOD::PhotonContainer    *photons,
                                                       const xAOD::ElectronContainer  *electrons,
                                                       const xAOD::MuonContainer      *muons,
                                                       const xAOD::JetContainer       *jets,
                                                       const xAOD::MissingETContainer *met)
  {
    static float vars[45] = {0};

    // photons
    float myy = (photons->at(0)->p4() + photons->at(1)->p4()).M();
    vars[0] = photons->at(0)->p4().Pt() / myy;
    vars[1] = photons->at(0)->p4().Eta();
    vars[2] = photons->at(0)->p4().Phi();
    vars[3] = photons->at(0)->p4().E() / myy;
    vars[4] = 22;
    vars[5] = photons->at(1)->p4().Pt() / myy;
    vars[6] = photons->at(1)->p4().Eta();
    vars[7] = photons->at(1)->p4().Phi();
    vars[8] = photons->at(1)->p4().E() / myy;
    vars[9] = 22;

    // lepton
    std::vector< std::vector<float> > ls;

    for (auto e : *electrons) {
      std::vector<float> ie;
      ie.push_back((float) e->p4().Pt());
      ie.push_back((float) e->p4().Eta());
      ie.push_back((float) e->p4().Phi());
      ie.push_back((float) e->p4().E());
      ie.push_back(12.);
      ls.push_back(ie);
    }

    for (auto m : *muons) {
      std::vector<float> im;
      im.push_back((float) m->p4().Pt());
      im.push_back((float) m->p4().Eta());
      im.push_back((float) m->p4().Phi());
      im.push_back((float) m->p4().E());
      im.push_back(12.);
      ls.push_back(im);
    }

    std::sort(ls.begin(), ls.end(), [](std::vector<float> a, std::vector<float> b) {return a[0] > b[0];});
    vars[10] = ls[0][0];
    vars[11] = ls[0][1];
    vars[12] = ls[0][2];
    vars[13] = ls[0][3];
    vars[14] = ls[0][4];

    if (ls.size() > 1) {
      vars[15] = ls[1][0];
      vars[16] = ls[1][1];
      vars[17] = ls[1][2];
      vars[18] = ls[1][3];
      vars[19] = ls[1][4];
    } else {
      vars[15] = -999.;
      vars[16] = -999.;
      vars[17] = -999.;
      vars[18] = -999.;
      vars[19] = -999.;
    }

    // met
    vars[20] = (*met)["TST"]->met();
    vars[21] = 0;
    vars[22] = (*met)["TST"]->phi();
    vars[23] = 0;
    vars[24] = 16;

    // jets
    std::vector< std::vector<float> > js;

    for (auto j : *jets) {
      std::vector<float> ij;
      ij.push_back((float) j->p4().Pt());
      ij.push_back((float) j->p4().Eta());
      ij.push_back((float) j->p4().Phi());
      ij.push_back((float) j->p4().E());
      ij.push_back(1.);
      js.push_back(ij);
    }

    std::sort(js.begin(), js.end(), [](std::vector<float> a, std::vector<float> b) {return a[0] > b[0];});

    while (js.size() < 4) {
      std::vector<float> ij;

      for (int i = 0; i < 5; i++) { ij.push_back(-999.); }

      js.push_back(ij);
    }

    int i = 25;

    for (auto j : js) {
      vars[i] = j[0];
      i++;
      vars[i] = j[1];
      i++;
      vars[i] = j[2];
      i++;
      vars[i] = j[3];
      i++;
      vars[i] = j[4];
      i++;

      if (i >= 45) { break; }
    }

    return vars;
  }

  float *HGamCategoryTool::make_XGBoost_DMatrix_ttHhad_topReco(const xAOD::PhotonContainer *photons,
      const xAOD::JetContainer *jets,
      const xAOD::MissingETContainer *met)
  {
    static float vars[20] = {0};

    double HT = 0;
    int m_njet30_cen = 0;

    for (auto jet : *jets) {
      HT += jet->pt() * HG::invGeV;

      if (fabs(jet->rapidity()) < 2.5 && jet->pt() > 30 * HG::GeV) { m_njet30_cen++; }
    }

    // EPS variables
    vars[9] = var::N_j_btag30();
    vars[0] = var::N_j_30();
    vars[1] = m_njet30_cen;
    vars[2] = ((*met)["TST"]->met() * HG::invGeV) / sqrt(HT);

    // photons
    const xAOD::Photon *ph1 = photons->at(0);
    const xAOD::Photon *ph2 = photons->at(1);

    TLorentzVector v_yy = ph1->p4() + ph2->p4();
    float myy = v_yy.M();
    v_yy *= HG::invGeV;

    vars[3] = ph1->p4().Pt() / myy;
    vars[4] = ph1->p4().Eta();
    vars[5] = ph1->p4().E() / myy;
    vars[6] = ph2->p4().Pt() / myy;
    vars[7] = ph2->p4().Eta();
    vars[8] = ph2->p4().E() / myy;

    // top
    vars[10] = var::score_top1();
    vars[11] = var::pT_top1();
    vars[12] = var::eta_top1();
    vars[13] = var::m_top1();

    vars[14] = var::pT_leftover();
    vars[15] = var::eta_leftover();
    vars[16] = var::m_leftover();

    TLorentzVector v_top;
    v_top.SetPtEtaPhiM(var::pT_top1(), var::eta_top1(), var::phi_top1(), var::m_top1());

    vars[17] = v_top.DeltaR(v_yy);

    vars[18] = -1.0;
    vars[19] = -1.0;

    if (var::pT_leftover() > 0) {
      TLorentzVector v_EE;
      v_EE.SetPtEtaPhiM(var::pT_leftover(), var::eta_leftover(), var::phi_leftover(), var::m_leftover());

      vars[18] = v_yy.DeltaR(v_EE);
      vars[19] = v_top.DeltaR(v_EE);
    }

    return vars;
  }

  float *HGamCategoryTool::make_XGBoost_DMatrix_ttHlep_topReco(const xAOD::PhotonContainer    *photons,
      const xAOD::ElectronContainer  *electrons,
      const xAOD::MuonContainer      *muons,
      const xAOD::JetContainer       *jets,
      const xAOD::MissingETContainer *met)
  {

    static float vars[17] = {0};

    double HT = 0;
    int m_njet25_cen = 0;

    for (auto jet : *jets) {
      HT += jet->pt() * HG::invGeV;

      if (fabs(jet->rapidity()) < 2.5) { m_njet25_cen++; }
    }

    // EPS variables
    vars[0] = m_njet25_cen;
    vars[1] = HT;
    vars[2] = var::pTlepMET() * HG::invGeV;
    vars[3] = var::massTrans() * HG::invGeV;
    vars[4] = ((*met)["TST"]->met() * HG::invGeV) / sqrt(HT);

    // photons
    const xAOD::Photon *ph1 = photons->at(0);
    const xAOD::Photon *ph2 = photons->at(1);

    if (ph1->pt() < ph2->pt()) {
      ph1 = photons->at(1);
      ph2 = photons->at(0);
    }

    TLorentzVector v_yy = ph1->p4() + ph2->p4();
    float myy = v_yy.M();
    v_yy *= HG::invGeV;

    vars[5] = ph1->p4().Pt() / myy;
    vars[6] = ph1->p4().Eta();
    vars[7] = ph1->p4().E() / myy;
    vars[8] = ph2->p4().Pt() / myy;
    vars[9] = ph2->p4().Eta();
    vars[10] = ph2->p4().E() / myy;

    vars[11] = var::pT_lep_top1();
    vars[12] = var::eta_lep_top1();
    vars[13] = var::m_lep_top1();

    vars[14] = var::pT_lep_leftover();
    vars[15] = var::eta_lep_leftover();
    vars[16] = var::m_lep_leftover();

    return vars;
  }

  float HGamCategoryTool::get_XGBoost_Weight(BoosterHandle *boost, float *vars, int len)
  {
    DMatrixHandle dmat;
    XGDMatrixCreateFromMat(vars, 1, len, -1, &dmat);
    bst_ulong out_len;
    const float *f;
    XGBoosterPredict(*boost, dmat, 0, 0, &out_len, &f);
    XGDMatrixFree(dmat);
    return *f;
  }

  //___________________________________________________________________________________________________________
  // ---------------------------------------------------------------------------------
  //   XGBoost top reconstruction
  // ---------------------------------------------------------------------------------
  void HGamCategoryTool::decorateTopCandidate(const xAOD::JetContainer *jets)
  {

    static SG::AuxElement::Accessor<float> bdt_top1("bdt_top1");
    float best_score = -1;

    const xAOD::Jet *recojet1_t = 0;
    const xAOD::Jet *recojet2_t = 0;
    const xAOD::Jet *recojet3_t = 0;

    // Loop over all sets of 3 reco jets
    int indexx1 = -1;

    for (auto recojet1 : *jets) {
      bdt_top1(*const_cast<xAOD::Jet *>(recojet1)) = 0;
      indexx1++;
      double indexx2 = -1;

      for (auto recojet2 : *jets) {
        indexx2++;

        if (indexx1 >= indexx2) { continue; }

        // Don't repeat combinations

        double indexx3 = -1;

        for (auto recojet3 : *jets) {
          indexx3++;

          if (indexx2 >= indexx3) { continue; }

          // Don't repeat combinations

          double PCb1 = 1 ;

          if ((bool)recojet1->auxdata<char>("MV2c10_FixedCutBEff_60"))  { PCb1 = 5; }
          else if ((bool)recojet1->auxdata<char>("MV2c10_FixedCutBEff_70")) { PCb1 = 4; }
          else if ((bool)recojet1->auxdata<char>("MV2c10_FixedCutBEff_77")) { PCb1 = 3; }
          else if ((bool)recojet1->auxdata<char>("MV2c10_FixedCutBEff_85")) { PCb1 = 2; }

          double PCb2 = 1 ;

          if ((bool)recojet2->auxdata<char>("MV2c10_FixedCutBEff_60"))  { PCb2 = 5; }
          else if ((bool)recojet2->auxdata<char>("MV2c10_FixedCutBEff_70")) { PCb2 = 4; }
          else if ((bool)recojet2->auxdata<char>("MV2c10_FixedCutBEff_77")) { PCb2 = 3; }
          else if ((bool)recojet2->auxdata<char>("MV2c10_FixedCutBEff_85")) { PCb2 = 2; }

          double PCb3 = 1 ;

          if ((bool)recojet3->auxdata<char>("MV2c10_FixedCutBEff_60"))  { PCb3 = 5; }
          else if ((bool)recojet3->auxdata<char>("MV2c10_FixedCutBEff_70")) { PCb3 = 4; }
          else if ((bool)recojet3->auxdata<char>("MV2c10_FixedCutBEff_77")) { PCb3 = 3; }
          else if ((bool)recojet3->auxdata<char>("MV2c10_FixedCutBEff_85")) { PCb3 = 2; }

          std::vector<double> btags = {PCb1, PCb2, PCb3};
          std::vector<const xAOD::Jet *> this_trip;
          std::vector<const xAOD::Jet *> tmp_trip;

          tmp_trip.push_back(recojet1);
          tmp_trip.push_back(recojet2);
          tmp_trip.push_back(recojet3);

          // initialize original index locations
          std::vector<int> b_index(3);

          for (unsigned int i = 0; i < b_index.size(); i++) {
            b_index.at(i) = i;
          }

          // sort indexes based on comparing values in v
          sort(b_index.begin(), b_index.end(),
          [&btags](size_t i1, size_t i2) {return btags.at(i1) > btags.at(i2);});

          // Require that one jet in the triplet is b-tagged at 77% WP
          if (btags.at(b_index.at(0)) < 3) { continue; }

          this_trip.push_back(tmp_trip.at(b_index.at(0)));
          this_trip.push_back(tmp_trip.at(b_index.at(1)));
          this_trip.push_back(tmp_trip.at(b_index.at(2)));

          TLorentzVector vb = this_trip.at(0)->p4();
          vb *= HG::invGeV;

          TLorentzVector vj1 = this_trip.at(1)->p4();
          vj1 *= HG::invGeV;

          TLorentzVector vj2 = this_trip.at(2)->p4();
          vj2 *= HG::invGeV;

          TLorentzVector vW = vj1 + vj2;

          // Top reco BDT variables
          // vbl_names = ['W_pt','W_eta','W_phi','W_E']
          // vbl_names += ['b_pt','b_eta','b_phi','b_E']
          // vbl_names += ['Wjj_dR','tWb_dR','btag1']
          static float vars[11] = {0};
          vars[0] = vW.Pt();
          vars[1] = vW.Eta();
          vars[2] = vW.Phi();
          vars[3] = vW.E();
          vars[4] = vb.Pt();
          vars[5] = vb.Eta();
          vars[6] = vb.Phi();
          vars[7] = vb.E();
          vars[8] = vj1.DeltaR(vj2);
          vars[9] = vW.DeltaR(vb);
          vars[10] = btags.at(b_index.at(0));

          float m_score = get_XGBoost_Weight(m_xgboost_topreco, vars, 11);

          if (m_score > best_score) {
            best_score = m_score;
            recojet1_t = this_trip.at(0);
            recojet2_t = this_trip.at(1);
            recojet3_t = this_trip.at(2);
          }

        } // End loop over reco jets
      } // End loop over reco jets
    } // End loop over reco jets

    if (best_score > 0) {
      bdt_top1(*const_cast<xAOD::Jet *>(recojet1_t)) = best_score;
      bdt_top1(*const_cast<xAOD::Jet *>(recojet2_t)) = best_score;
      bdt_top1(*const_cast<xAOD::Jet *>(recojet3_t)) = best_score;
    }

    return;
  }

}
