#include <HGamAnalysisFramework/FCNCTool.h>

namespace HG {

  //_______________________________________________________________________________________
  FCNCTool::FCNCTool(HG::EventHandler *eventHandler)
    : m_eventHandler(eventHandler)
  { }

  //_______________________________________________________________________________________
  HG::EventHandler *FCNCTool::eventHandler()
  {
    return m_eventHandler;
  }

  //_______________________________________________________________________________________
  EL::StatusCode FCNCTool::initialize(Config &config)
  {

    m_debug = config.getInt("HGamFCNC.Debug", 0);

    // Up to Njets
    m_maxNjet    = config.getInt("HGamFCNC.maxNjet", 4);
    m_maxNjetLep = config.getInt("HGamFCNC.maxNjetLep", 2);

    // All the following could go to the config.
    // jet pT cuts
    m_jptCC = 30.;
    m_jptCF = 30.;
    m_doLooseJ = config.getBool("HGamFCNC.doLooseJ", false);

    if (m_doLooseJ) {
      m_jptCC = 25.;
      Info("", "Applying loose central jet pt cut");
    }

    // top mass windows
    m_mTop1HadLow  = config.getNum("HGamFCNC.mT1tightHadLo", 152.);
    m_mTop1HadHigh = config.getNum("HGamFCNC.mT1tightHadHi", 190.);
    m_mTop2HadLow  = config.getNum("HGamFCNC.mT2tightHadLo", 120.);
    m_mTop2HadHigh = config.getNum("HGamFCNC.mT2tightHadHi", 220.);
    m_mTop1LepLow  = config.getNum("HGamFCNC.mT1tightLepLo", 152.);
    m_mTop1LepHigh = config.getNum("HGamFCNC.mT1tightLepHi", 190.);
    m_mTop2LepLow  = config.getNum("HGamFCNC.mT2tightLepLo", 130.);
    m_mTop2LepHigh = config.getNum("HGamFCNC.mT2tightLepHi", 210.);
    //config.getNum("HGamFCNC.mT1looseHadLo", 152.);
    //config.getNum("HGamFCNC.mT1looseHadHi", 190.);
    //config.getNum("HGamFCNC.mT1looseLepLo", 152.);
    //config.getNum("HGamFCNC.mT1looseLepHi", 190.);

    // pT cut on electron (tighter than default in HGam a priori) and muon (default HGam a priori)
    m_ptelC = 15.;
    m_ptmuC = 10.;

    // mT cut
    m_mTC = 30.;

    // truthhandler is null by default
    m_truthHandler = nullptr;
    std::vector<double> tqHlist{410804, 410805, 410806, 410807, 410824, 410825, 410826, 410827};
    m_mcidForMatch = config.getNumV("HGamFCNC.tqHMCList", tqHlist);

    m_writeTruthInfo = false;
    int mcid = getChannelNumber();

    // aMcAtNlo+Pythia8
    if (mcid == 410752 or mcid == 410753 or mcid == 410754 or mcid == 410755 or // t-H FCNC
        mcid == 410756 or mcid == 410757 or mcid == 410758 or mcid == 410759 or // u/c-H FCNC
        mcid == 410804 or mcid == 410805 or mcid == 410806 or mcid == 410807 or // tt-cH-FCNC
        mcid == 410824 or mcid == 410825 or mcid == 410826 or mcid == 410827) { m_writeTruthInfo = true; } // tt-uH-FCNC

    // Powheg+Pythia8
    if (mcid == 410764 or mcid == 410765 or mcid == 410766 or mcid == 410767 or // tt-cH-FCNC
        mcid == 410768 or mcid == 410769 or mcid == 410770 or mcid == 410771) { m_writeTruthInfo = true; } // tt-uH-FCNC

    return EL::StatusCode::SUCCESS;
  }

  void FCNCTool::InitMap()
  {

    if (!m_declared) {
      m_declared = true;

      // Integer
      m_eventInfoInts.insert(std::make_pair("fcnc_cutFlow", 0));
      m_eventInfoInts.insert(std::make_pair("fcnc_njet", -1));
      m_eventInfoInts.insert(std::make_pair("fcnc_njet30", -1));
      m_eventInfoInts.insert(std::make_pair("fcnc_idLep", 0));
      m_eventInfoInts.insert(std::make_pair("fcnc_ntopc", -1));
      m_eventInfoInts.insert(std::make_pair("fcnc_cat", -1));
      m_eventInfoInts.insert(std::make_pair("fcnc_inl", -1));
      m_eventInfoInts.insert(std::make_pair("fcnc_ginl", -1));

      m_eventInfoInts.insert(std::make_pair("fcnc_T1qpid", 0));
      m_eventInfoInts.insert(std::make_pair("fcnc_T2qpid", 0));

      // Floats
      //m_eventInfoFloats.insert(std::make_pair("fcnc_myy",0)); // not needed, this is the standard m_yy in HGamEventInfo
      m_eventInfoFloats.insert(std::make_pair("fcnc_mT", -1.));
      m_eventInfoFloats.insert(std::make_pair("fcnc_etm", -1.));
      m_eventInfoFloats.insert(std::make_pair("fcnc_phim", 999.));
      m_eventInfoFloats.insert(std::make_pair("fcnc_weight", 0.));

      if (m_truthHandler != nullptr) {
        m_eventInfoFloats.insert(std::make_pair("fcnc_mTrueT1", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_pxTrueT1", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_pyTrueT1", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_pzTrueT1", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_ptTrueT1q", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_etaTrueT1q", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_phiTrueT1q", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_mTrueT1q", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_ptTrueT1qv", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_etaTrueT1qv", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_phiTrueT1qv", -99.));

        m_eventInfoFloats.insert(std::make_pair("fcnc_mTrueT2", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_pxTrueT2", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_pyTrueT2", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_pzTrueT2", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_ptTrueT2q", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_etaTrueT2q", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_phiTrueT2q", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_mTrueT2q", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_ptTrueT2qv", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_etaTrueT2qv", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_phiTrueT2qv", -99.));

        m_eventInfoFloats.insert(std::make_pair("fcnc_ptTrueGam0", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_phiTrueGam0", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_etaTrueGam0;", -99.));

        m_eventInfoFloats.insert(std::make_pair("fcnc_ptTrueGam1", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_phiTrueGam1", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_etaTrueGam1;", -99.));

        m_eventInfoInts.insert(std::make_pair("fcnc_qpidWc0", -99));
        m_eventInfoFloats.insert(std::make_pair("fcnc_ptTrueT2Wc0", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_phiTrueT2Wc0", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_etaTrueT2Wc0", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_mTrueT2Wc0", -99.));

        m_eventInfoInts.insert(std::make_pair("fcnc_qpidWc1", -99));
        m_eventInfoFloats.insert(std::make_pair("fcnc_ptTrueT2Wc1", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_phiTrueT2Wc1", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_etaTrueT2Wc1", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_mTrueT2Wc1", -99.));

      }

      // Vector of integer
      m_eventInfoVInts.insert(std::make_pair("fcnc_nbTop2", vint()));

      // Vector of floats
      m_eventInfoVFloats.insert(std::make_pair("fcnc_mTop1", vfloat()));
      m_eventInfoVFloats.insert(std::make_pair("fcnc_mTop2", vfloat()));

    } else {

      m_eventInfoInts["fcnc_cutFlow"] = 0;
      m_eventInfoInts["fcnc_njet"]    = -1;
      m_eventInfoInts["fcnc_njet30"]  = -1;
      m_eventInfoInts["fcnc_idLep"]   = 0;
      m_eventInfoInts["fcnc_ntopc"]   = -1;
      m_eventInfoInts["fcnc_cat"]     = -1;
      m_eventInfoInts["fcnc_inl"]     = -1;
      m_eventInfoInts["fcnc_ginl"]    = -1;

      m_eventInfoInts["fcnc_T1qpid"]     = 0;
      m_eventInfoInts["fcnc_T2qpid"]     = 0;

      // Floats
      //m_eventInfoFloats["fcnc_myy"] = 0.; // not needed, this is the standard m_yy in HGamEventInfo
      m_eventInfoFloats["fcnc_mT"]     = -1.;
      m_eventInfoFloats["fcnc_etm"]    = -1.;
      m_eventInfoFloats["fcnc_phim"]   = 999.;
      m_eventInfoFloats["fcnc_weight"] = 0.;

      if (m_truthHandler != nullptr) {
        m_eventInfoFloats["fcnc_mTrueT1"]     = -99.;
        m_eventInfoFloats["fcnc_pxTrueT1"]    = -99.;
        m_eventInfoFloats["fcnc_pyTrueT1"]    = -99.;
        m_eventInfoFloats["fcnc_pzTrueT1"]    = -99.;
        m_eventInfoFloats["fcnc_ptTrueT1q"]   = -99.;
        m_eventInfoFloats["fcnc_etaTrueT1q"]  = -99.;
        m_eventInfoFloats["fcnc_phiTrueT1q"]  = -99.;
        m_eventInfoFloats["fcnc_mTrueT1q"]    = -99.;
        m_eventInfoFloats["fcnc_ptTrueT1qv"]  = -99.;
        m_eventInfoFloats["fcnc_etaTrueT1qv"] = -99.;
        m_eventInfoFloats["fcnc_phiTrueT1qv"] = -99.;

        m_eventInfoFloats["fcnc_mTrueT2"]     = -99.;
        m_eventInfoFloats["fcnc_pxTrueT2"]    = -99.;
        m_eventInfoFloats["fcnc_pyTrueT2"]    = -99.;
        m_eventInfoFloats["fcnc_pzTrueT2"]    = -99.;
        m_eventInfoFloats["fcnc_ptTrueT2q"]   = -99.;
        m_eventInfoFloats["fcnc_etaTrueT2q"]  = -99.;
        m_eventInfoFloats["fcnc_phiTrueT2q"]  = -99.;
        m_eventInfoFloats["fcnc_mTrueT2q"]    = -99.;
        m_eventInfoFloats["fcnc_ptTrueT2qv"]  = -99.;
        m_eventInfoFloats["fcnc_etaTrueT2qv"] = -99.;
        m_eventInfoFloats["fcnc_phiTrueT2qv"] = -99.;

        m_eventInfoFloats["fcnc_ptTrueGam0"]  = -99.;
        m_eventInfoFloats["fcnc_phiTrueGam0"] = -99.;
        m_eventInfoFloats["fcnc_etaTrueGam0"] = -99.;

        m_eventInfoFloats["fcnc_ptTrueGam1"]  = -99.;
        m_eventInfoFloats["fcnc_phiTrueGam1"] = -99.;
        m_eventInfoFloats["fcnc_etaTrueGam1"] = -99.;

        m_eventInfoInts["fcnc_qpidWc0"]        = -99.;
        m_eventInfoFloats["fcnc_ptTrueT2Wc0"]  = -99.;
        m_eventInfoFloats["fcnc_phiTrueT2Wc0"] = -99.;
        m_eventInfoFloats["fcnc_etaTrueT2Wc0"] = -99.;
        m_eventInfoFloats["fcnc_mTrueT2Wc0"]   = -99.;

        m_eventInfoInts["fcnc_qpidWc1"]        = -99.;
        m_eventInfoFloats["fcnc_ptTrueT2Wc1"]  = -99.;
        m_eventInfoFloats["fcnc_phiTrueT2Wc1"] = -99.;
        m_eventInfoFloats["fcnc_etaTrueT2Wc1"] = -99.;
        m_eventInfoFloats["fcnc_mTrueT2Wc1"]   = -99.;

        m_mTrueT1 = -99;
        m_pxTrueT1 = -99;
        m_pyTrueT1 = -99;
        m_pzTrueT1 = -99;
        m_mTrueT2 = -99;
        m_pxTrueT2 = -99;
        m_pyTrueT2 = -99;
        m_pzTrueT2 = -99;
        m_T1qpid = -99;
        m_ptTrueT1q = -99;
        m_phiTrueT1q = -99;
        m_etaTrueT1q = -99,
        m_mTrueT1q = -99;
        m_T2qpid = -99;
        m_ptTrueT2q = -99;
        m_phiTrueT2q = -99;
        m_etaTrueT2q = -99,
        m_mTrueT2q = -99;
        m_ptTrueT1qv = -99;
        m_phiTrueT1qv = -99;
        m_etaTrueT1qv = -99;
        m_ptTrueT2qv = -99;
        m_phiTrueT2qv = -99;
        m_etaTrueT2qv = -99;

        m_ptTrueGam0 = -99;
        m_phiTrueGam0 = -99;
        m_etaTrueGam0 = -99;
        m_ptTrueGam1 = -99;
        m_phiTrueGam1 = -99;
        m_etaTrueGam1 = -99;
        m_qpidWc0 = -99;
        m_ptTrueT2Wc0 = -99;
        m_phiTrueT2Wc0 = -99;
        m_etaTrueT2Wc0 = -99;
        m_mTrueT2Wc0 = -99;
        m_qpidWc1 = -99;
        m_ptTrueT2Wc1 = -99;
        m_phiTrueT2Wc1 = -99;
        m_etaTrueT2Wc1 = -99;
        m_mTrueT2Wc1 = -99;

      }

      // Vector of integer
      m_eventInfoVInts["fcnc_nbTop2"].clear();

      // Vector of floats
      m_eventInfoVFloats["fcnc_mTop1"].clear();
      m_eventInfoVFloats["fcnc_mTop2"].clear();

    }

  }

  //_______________________________________________________________________________________
  void FCNCTool::saveFCNCInfo(xAOD::PhotonContainer    &photons,
                              xAOD::MuonContainer      &muons,
                              xAOD::ElectronContainer  &electrons,
                              xAOD::MissingETContainer &met,
                              xAOD::JetContainer       &jets)
  {

    // Initialisation
    InitMap();

    // Perform the analysis
    m_eventInfoInts["fcnc_cutFlow"] = performSelection(photons, muons, electrons, met, jets);

    if (m_truthHandler) {

      bool foundTrueTopEx = false;

      if (m_writeTruthInfo) { foundTrueTopEx = findTrueTopEx(); }

      if (foundTrueTopEx) {
        m_eventInfoFloats["fcnc_mTrueT1"]     = m_mTrueT1;
        m_eventInfoFloats["fcnc_pxTrueT1"]    = m_pxTrueT1;
        m_eventInfoFloats["fcnc_pyTrueT1"]    = m_pyTrueT1;
        m_eventInfoFloats["fcnc_pzTrueT1"]    = m_pzTrueT1;
        m_eventInfoFloats["fcnc_ptTrueT1q"]   = m_ptTrueT1q;
        m_eventInfoFloats["fcnc_etaTrueT1q"]  = m_etaTrueT1q;
        m_eventInfoFloats["fcnc_phiTrueT1q"]  = m_phiTrueT1q;
        m_eventInfoFloats["fcnc_mTrueT1q"]    = m_mTrueT1q;
        m_eventInfoInts["fcnc_T1qpid"]        = m_T1qpid;
        m_eventInfoFloats["fcnc_ptTrueT1qv"]  = m_ptTrueT1qv;
        m_eventInfoFloats["fcnc_etaTrueT1qv"] = m_etaTrueT1qv;
        m_eventInfoFloats["fcnc_phiTrueT1qv"] = m_phiTrueT1qv;
      } else if (m_debug > 0) {
        Info("", "did not find the true top decaying to Higgs ?");
      }

      bool foundTrueTopSM = false;

      if (m_writeTruthInfo) { foundTrueTopSM = findTrueTopSM(); }

      if (foundTrueTopSM) {
        m_eventInfoFloats["fcnc_mTrueT2"]     = m_mTrueT2;
        m_eventInfoFloats["fcnc_pxTrueT2"]    = m_pxTrueT2;
        m_eventInfoFloats["fcnc_pyTrueT2"]    = m_pyTrueT2;
        m_eventInfoFloats["fcnc_pzTrueT2"]    = m_pzTrueT2;
        m_eventInfoFloats["fcnc_ptTrueT2q"]   = m_ptTrueT2q;
        m_eventInfoFloats["fcnc_etaTrueT2q"]  = m_etaTrueT2q;
        m_eventInfoFloats["fcnc_phiTrueT2q"]  = m_phiTrueT2q;
        m_eventInfoFloats["fcnc_mTrueT2q"]    = m_mTrueT2q;
        m_eventInfoInts["fcnc_T2qpid"]        = m_T2qpid;
        m_eventInfoFloats["fcnc_ptTrueT2qv"]  = m_ptTrueT2qv;
        m_eventInfoFloats["fcnc_etaTrueT2qv"] = m_etaTrueT2qv;
        m_eventInfoFloats["fcnc_phiTrueT2qv"] = m_phiTrueT2qv;

        m_eventInfoFloats["fcnc_ptTrueGam0"]  = m_ptTrueGam0;
        m_eventInfoFloats["fcnc_phiTrueGam0"] = m_phiTrueGam0;
        m_eventInfoFloats["fcnc_etaTrueGam0"] = m_etaTrueGam0;

        m_eventInfoFloats["fcnc_ptTrueGam1"]  = m_ptTrueGam1;
        m_eventInfoFloats["fcnc_phiTrueGam1"] = m_phiTrueGam1;
        m_eventInfoFloats["fcnc_etaTrueGam1"] = m_etaTrueGam1;

        m_eventInfoInts["fcnc_qpidWc0"]      = m_qpidWc0;
        m_eventInfoFloats["fcnc_ptTrueT2Wc0"]  = m_ptTrueT2Wc0;
        m_eventInfoFloats["fcnc_phiTrueT2Wc0"] = m_phiTrueT2Wc0;
        m_eventInfoFloats["fcnc_etaTrueT2Wc0"] = m_etaTrueT2Wc0;
        m_eventInfoFloats["fcnc_mTrueT2Wc0"]   = m_mTrueT2Wc0;

        m_eventInfoInts["fcnc_qpidWc1"]      = m_qpidWc1;
        m_eventInfoFloats["fcnc_ptTrueT2Wc1"]  = m_ptTrueT2Wc1;
        m_eventInfoFloats["fcnc_phiTrueT2Wc1"] = m_phiTrueT2Wc1;
        m_eventInfoFloats["fcnc_etaTrueT2Wc1"] = m_etaTrueT2Wc1;
        m_eventInfoFloats["fcnc_mTrueT2Wc1"]   = m_mTrueT2Wc1;
      } else if (m_debug > 0) {
        Info("", "did not find the true top decaying to Higgs ?");
      }
    }

    //Save all the eventInfoMaps to the eventHandler()
    //Should always be called last
    saveMapsToEventInfo();

  }

  unsigned int FCNCTool::performSelection(xAOD::PhotonContainer    &photons,
                                          xAOD::MuonContainer      &mus,
                                          xAOD::ElectronContainer  &els,
                                          xAOD::MissingETContainer &met,
                                          xAOD::JetContainer       &jets)
  {
    static SG::AuxElement::ConstAccessor<float> emSF("scaleFactor");
    static SG::AuxElement::ConstAccessor<char> isoAcc("isIsoFixedCutLoose");
    static SG::AuxElement::ConstAccessor<char> idAcc("isTight");

    // Two good photons
    if ((photons.size() < 2) || (photons.size() >= 2 && (photons[0]->pt() < 40 * HG::GeV || photons[1]->pt() < 30 * HG::GeV)))
    { return 0; }

    //
    if (!idAcc.isAvailable(*photons[0]) || !idAcc.isAvailable(*photons[1]))
    { fatal("Cannot access the id information (Tight)"); }

    if (!(idAcc(*photons[0]) && idAcc(*photons[1])))
    { return 0; }

    if (!isoAcc.isAvailable(*photons[0]) || !isoAcc.isAvailable(*photons[1]))
    { fatal("Cannot access the isolation information (FixedCutLoose)"); }

    if (!(isoAcc(*photons[0]) && isoAcc(*photons[1])))
    { return 0; }

    // with an invariant mass within [100,160] GeV/c2
    m_p4H = photons[0]->p4() + photons[1]->p4();
    m_mgg = m_p4H.M() / HG::GeV;

    //m_eventInfoFloats["fcnc_myy"] = m_mgg; // not needed, this is the standard m_yy in HGamEventInfo
    if (m_mgg < 100 || m_mgg > 160)
    { return 1; }

    int Nbjet  = 0;
    int Njetxx = 0;
    int Njet30 = 0;

    for (auto jet : jets) {
      bool isB77 = jet->auxdata<char>("MV2c10_FixedCutBEff_77");

      if (isB77) { Nbjet++; }

      double jpt = jet->pt() / HG::GeV;

      if (isGoodJ(jet)) { Njetxx++; }

      if (jpt > 30.) { Njet30++; }
    }

    m_eventInfoInts["fcnc_njet"]   = Njetxx;
    m_eventInfoInts["fcnc_njet30"] = Njet30;

    // at least 4 jets or 2 jets and a lepton
    m_lep.p4 = TLorentzVector(0, 0, 0, 0);
    m_lep.sf = 0;
    m_lep.id = 0;
    unsigned int nMu = 0, nEl = 0;

    for (auto el : els) {
      if (el->pt() > m_ptelC * HG::GeV) {
        if (nEl == 0) {
          m_lep.p4 = el->p4();
          m_lep.sf = emSF(*el);
          m_lep.id = -11 * int(el->charge());
        }

        nEl++;
      }
    }

    for (auto mu : mus) {
      if (mu->pt() > m_ptmuC * HG::GeV) {
        if (nMu == 0) { // I can eventually overwrite, anyway I do not care since I want at most one lepton
          m_lep.p4 = mu->p4();
          m_lep.sf = emSF(*mu);
          m_lep.id = -13 * int(mu->charge());
        }

        nMu++;
      }
    }

    unsigned int nLep = nMu + nEl;

    if (!((Njetxx >= 4) || (Njetxx >= 2 && nLep >= 1)))
    { return 2; }

    if (Njetxx >= 2 && nLep == 1)
    { return LeptonAnaSel(met, jets); }
    else if (Njetxx >= 4 && nLep == 0)
    { return HadronAnaSel(jets); }
    else
    { return 3; }

    return 100;
  }


  //_______________________________________________________________________________________
  //Save event info from maps to eventInfo
  void FCNCTool::saveMapsToEventInfo()
  {
    //Floats
    for (auto element : m_eventInfoFloats)
    { eventHandler()->storeVar<float>(element.first.Data(), element.second); }

    //Ints
    for (auto element : m_eventInfoInts)
    { eventHandler()->storeVar<int>(element.first.Data(), element.second); }

    //Vectors
    for (auto element : m_eventInfoVInts)
    { eventHandler()->storeVar<std::vector<int>>(element.first.Data(), element.second); }

    for (auto element : m_eventInfoVFloats)
    { eventHandler()->storeVar<std::vector<float>>(element.first.Data(), element.second); }

  }

  std::vector<double> FCNCTool::getNuPz(TLorentzVector &lep, const xAOD::MissingET *met, double &mT)
  {

    mT = sqrt(2.*lep.Pt() * met->met() * (1 - cos(lep.Phi() - met->phi())));

    //
    bool useDaniel = true;
    double amW = 80.4e3;

    if (useDaniel && mT > amW)
    { amW = mT + 100; }

    double ci = amW * amW / 2. + lep.Px() * met->mpx() + lep.Py() * met->mpy();
    //
    double a = lep.Perp2();
    double b = -2.*lep.Pz() * ci;
    double c = lep.E() * lep.E() * met->met() * met->met() - ci * ci;
    //
    double delta = b * b - 4.*a * c;
    std::vector<double> pz; // if useDaniel, should always be two solutions

    if (delta > 0) {
      pz.resize(2);
      pz[0] = (-b - sqrt(delta)) / 2. / a;
      pz[1] = (-b + sqrt(delta)) / 2. / a;
    } else {
      pz.resize(1);
      pz[0] = -b / 2. / a;
    }

    if (m_debug > 0) {
      TLorentzVector p4nu1(met->mpx(), met->mpy(), pz[0], 0.);
      p4nu1.SetE(p4nu1.P());
      TLorentzVector p4nu2(met->mpx(), met->mpy(), pz[1], 0.);
      p4nu2.SetE(p4nu2.P());
      Info("", "the event, mgg = %f, ETm = %f, pTLep = %f, phiMET = %f, phiLep = %f, mT = %f, pznu1 = %f, pznu2 = %f, mW1 = %f, mW2 = %f",
           m_mgg, met->met() * 1e-3, lep.Pt() * 1e-3, met->phi(), lep.Phi(), mT * 1e-3, pz[0] * 1e-3, pz[1] * 1e-3, (lep + p4nu1).M() * 1e-3, (lep + p4nu2).M() * 1e-3);
    }

    return pz;
  }

  unsigned int FCNCTool::LeptonAnaSel(xAOD::MissingETContainer &met,
                                      xAOD::JetContainer       &jets)
  {

    static SG::AuxElement::Accessor<float> btSF("SF_MV2c10_FixedCutBEff_77");

    xAOD::MissingET *metFinal = *(met.find("TST"));
    double jetSF = 1;

    double mT = -1;
    std::vector<double> pznu = this->getNuPz(m_lep.p4, metFinal, mT);

    m_eventInfoFloats["fcnc_mT"]   = mT;
    m_eventInfoFloats["fcnc_etm"]  = metFinal->met() / HG::GeV;
    m_eventInfoFloats["fcnc_phim"] = metFinal->phi();
    m_eventInfoInts["fcnc_idLep"]  = m_lep.id;

    // mT > 30 GeV
    if (mT < m_mTC * HG::GeV)
    { return 11; }

    // Jets
    int in2l = -1, gin2l = -1;
    vfloat imTop1, imTop2;
    vint inbTop2;
    int nMaxComb = 2 * m_maxNjetLep * (m_maxNjetLep - 1);
    imTop1.resize(nMaxComb);
    imTop2.resize(nMaxComb);
    inbTop2.resize(nMaxComb);
    int combOK = 0;
    int ic = 0;

    for (unsigned int isol = 0; isol < pznu.size(); isol++) {
      TLorentzVector nup4(metFinal->mpx(), metFinal->mpy(), pznu[isol], 0.);
      nup4.SetE(nup4.P());
      TLorentzVector wp4 = m_lep.p4 + nup4;

      // The loop for exot top decay
      int kc = 0;

      for (auto jet : jets) {
        if (!isGoodJ(jet)) { continue; }

        if (kc > m_maxNjetLep - 1) { break; }

        if (isol == 0) { jetSF *= btSF(*jet); } // this is probably wrong if maxNjetLep > 2 or not ? I test all jets, so just multiply the SF... maybe OK

        kc++;
        bool isB77 = jet->auxdata<char>("MV2c10_FixedCutBEff_77");
        TLorentzVector tExot = jet->p4() + m_p4H;
        double mTop1 = tExot.M() / HG::GeV;

        // The loop for SM top decay
        int jc = 0;

        for (auto kjet : jets) {
          if (!isGoodJ(kjet)) { continue; }

          if (jc > m_maxNjetLep - 1) { break; }

          jc++;

          if (kjet == jet) { continue; }

          bool isB77k = kjet->auxdata<char>("MV2c10_FixedCutBEff_77");
          TLorentzVector tStan = wp4 + kjet->p4();
          double mTop2 = tStan.M() / HG::GeV;

          if (mTop1 >= m_mTop1LepLow && mTop1 <= m_mTop1LepHigh && (isB77 || isB77k)) {
            if (combOK == 0) { combOK = 1; } // if no good combi found already (otherwise combOK could be 2 and then 1 again...)

            if (kc < 3 && jc < 3) { in2l = 1; }

            if (mTop2 >= m_mTop2LepLow && mTop2 <= m_mTop2LepHigh) {
              combOK = 2;

              if (kc < 3 && jc < 3) { gin2l = 1; }
            }
          }

          imTop1[ic]  = mTop1;
          imTop2[ic]  = mTop2;
          inbTop2[ic] = isB77k ? 1 : 0;
          ic++;
        } // jet for sm top decay
      }   // jet for exot top decay
    }     // neutrino pz sol

    m_eventInfoInts["fcnc_ntopc"]    = ic;
    m_eventInfoVFloats["fcnc_mTop1"] = imTop1;
    m_eventInfoVFloats["fcnc_mTop2"] = imTop2;
    m_eventInfoVInts["fcnc_nbTop2"]  = inbTop2;
    m_eventInfoInts["fcnc_inl"]      = in2l;
    m_eventInfoInts["fcnc_ginl"]     = gin2l;

    if (combOK == 0)
    { return 12; }

    m_eventInfoFloats["fcnc_weight"] = m_lep.sf * jetSF;

    if (combOK == 1)
    { m_eventInfoInts["fcnc_cat"] = 1; }
    else
    { m_eventInfoInts["fcnc_cat"] = 2; }

    return 13;

  }

  unsigned int FCNCTool::HadronAnaSel(xAOD::JetContainer       &jets)
  {

    static SG::AuxElement::Accessor<float> btSF("SF_MV2c10_FixedCutBEff_77");

    int combOK = 0;

    // nComb = n! / 3! (n-4)!
    int nMaxComb = TMath::Factorial(m_maxNjet) / 6 / TMath::Factorial(m_maxNjet - 4); // 24 / 6 / 1 = 4
    vfloat imTop1, imTop2;
    vint inbTop2;
    vint nbComb; // was unsigned short [100]
    imTop1.resize(nMaxComb);
    imTop2.resize(nMaxComb);
    inbTop2.resize(nMaxComb);
    nbComb.resize(nMaxComb);

    // I used to store all these in a ntuple
    int goneok = -1, oneok = -1, ib = -1, in4l = -1, gin4l = -1;
    //
    int nc = 0;
    int ic = 0;
    double jetSF = 1;

    for (auto jet : jets) {

      // Top1 building : gamgamjet

      if (!isGoodJ(jet)) { continue; }

      if (ic > m_maxNjet - 1) { break; } // use only the maxNjet leading

      ic++;
      jetSF *= btSF(*jet); // This is probably wrong if maxNjet > 4 or not ? I test all jets, so just multiply the SF... maybe OK
      bool isB77 = jet->auxdata<char>("MV2c10_FixedCutBEff_77");

      if (isB77) {
        if (ib == -1) { ib = ic; } // position of the first b-jet
      }

      TLorentzVector tExot = jet->p4() + m_p4H;
      double mTop1 = tExot.M() * 1e-3;

      // Now Top2 building : jjj
      int kc = 0;

      for (auto kjet : jets) {

        if (!isGoodJ(kjet)) { continue; }

        if (kc > m_maxNjet - 1) { break; } // use only the maxNjet leading

        kc++;

        if (kjet == jet) { continue; }

        bool isB77k = kjet->auxdata<char>("MV2c10_FixedCutBEff_77");

        int lc = 0;

        for (auto ljet : jets) {
          if (!isGoodJ(ljet)) { continue; }

          if (lc > m_maxNjet - 1) { break; } // use only the maxNjet leading

          lc++;

          if (lc < kc) { continue; } // do not care about the order

          if (ljet == jet || ljet == kjet) { continue; }

          bool isB77l = ljet->auxdata<char>("MV2c10_FixedCutBEff_77");

          int jc = 0;

          for (auto jjet : jets) {
            if (!isGoodJ(jjet)) { continue; }

            if (jc > m_maxNjet - 1) { break; } // use only the maxNjet leading

            jc++;

            if (jc < lc) { continue; } // do not care about the order

            if (jjet == jet || jjet == kjet || jjet == ljet) { continue; }

            bool isB77j = jjet->auxdata<char>("MV2c10_FixedCutBEff_77");

            TLorentzVector tStan = kjet->p4() + ljet->p4() + jjet->p4();
            double mTop2 = tStan.M() * 1e-3;

            if (mTop1 >= m_mTop1HadLow && mTop1 <= m_mTop1HadHigh && (isB77 || isB77k || isB77l || isB77j)) {
              if (combOK == 0) { combOK = 1; }       // if no good combi found already (otherwise combOK could be 2 and the 1 again...)

              if (jc < 5 && ic < 5) { in4l = 1; }    // a good combi using only the 4 leading jets.

              if (oneok == -1) { oneok  = nc; }      // first good combination

              if (mTop2 >= m_mTop2HadLow && mTop2 <= m_mTop2HadHigh) {
                if (goneok == -1) { goneok = nc; }   // first good full combination with a b in it

                if (jc < 5 && ic < 5) { gin4l = 1; } // a full good combi using only the 4 leading jets.

                combOK = 2;
              }
            }

            imTop1[nc]  = mTop1;
            imTop2[nc]  = mTop2;
            nbComb[nc]  = (isB77 || isB77k || isB77l || isB77j) ? 1 : 0;
            inbTop2[nc] = (isB77k || isB77l || isB77j) ? 1 : 0;
            nc++;
          } // 3eme jet de Top2
        }   // 2eme jet de Top2
      }     // 1er  jet de Top2
    }       //      jet de Top1

    m_eventInfoInts["fcnc_ntopc"]    = nc;
    m_eventInfoVFloats["fcnc_mTop1"] = imTop1;
    m_eventInfoVFloats["fcnc_mTop2"] = imTop2;
    m_eventInfoVInts["fcnc_nbTop2"]  = inbTop2;
    m_eventInfoInts["fcnc_inl"]      = in4l;
    m_eventInfoInts["fcnc_ginl"]     = gin4l;

    if (combOK == 0)
    { return 22; }

    m_eventInfoFloats["fcnc_weight"] = jetSF;

    if (combOK == 1)
    { m_eventInfoInts["fcnc_cat"] = 3; }
    else
    { m_eventInfoInts["fcnc_cat"] = 4; }

    return 23;
  }

  bool FCNCTool::isGoodJ(const xAOD::Jet *jet)
  {
    if (!((jet->pt() > m_jptCC && fabs(jet->eta()) <= 2.5) || (jet->pt() > m_jptCF && fabs(jet->eta()) > 2.5))) { return false; }

    return true;
  }

  bool FCNCTool::findTrueTopEx()
  {

    const xAOD::TruthParticleContainer *truthParticles = m_truthHandler->getTruthParticles();
    const xAOD::TruthParticle *trueT1(nullptr);

    for (auto tp : *truthParticles) {
      int pdg    = tp->pdgId();

      if (abs(pdg) != 6)
      { continue; }

      int status = tp->status();

      if (status != 62) // Only Py8 for the time being
      { continue; }

      unsigned int nC = tp->nChildren();

      for (unsigned int ic = 0; ic < nC; ic++) {
        int cpdg = tp->child(ic)->pdgId();

        if (cpdg == 25) {
          trueT1  = tp;

          const xAOD::TruthParticle *trueHiggs = (xAOD::TruthParticle *)tp->child(ic);
          const xAOD::TruthParticle *lastHiggs = (xAOD::TruthParticle *)ThisParticleFinal(trueHiggs);

          if (lastHiggs->nChildren() == 2) {
            m_ptTrueGam0 =  lastHiggs->child(0)->pt() / HG::GeV;
            m_phiTrueGam0 = lastHiggs->child(0)->phi();
            m_etaTrueGam0 = lastHiggs->child(0)->eta();

            m_ptTrueGam1 =  lastHiggs->child(1)->pt() / HG::GeV;
            m_phiTrueGam1 = lastHiggs->child(1)->phi();
            m_etaTrueGam1 = lastHiggs->child(1)->eta();
          }

          break;
        }
      }

      if (trueT1)
      { break; }
    }

    if (trueT1) {
      m_mTrueT1  = trueT1->m() / HG::GeV;
      m_pxTrueT1 = trueT1->px() / HG::GeV;
      m_pyTrueT1 = trueT1->py() / HG::GeV;
      m_pzTrueT1 = trueT1->pz() / HG::GeV;

      unsigned int nC = trueT1->nChildren();

      for (unsigned int ic = 0; ic < nC; ic++) {
        const xAOD::TruthParticle *tpc = trueT1->child(ic);
        int cpdg = tpc->pdgId();

        if (abs(cpdg) < 5) { // up / charm quark
          m_ptTrueT1q  = tpc->pt() / HG::GeV;
          m_phiTrueT1q = tpc->phi();
          m_etaTrueT1q = tpc->eta();
          m_mTrueT1q = tpc->m() / HG::GeV;
          m_T1qpid       = cpdg;
          std::vector<const xAOD::TruthParticle *> dauV;
          GetDaughter(tpc, dauV);
          TLorentzVector hlvVis(0, 0, 0, 0);

          //std::cout << "The quark daughters, nD = " << dauV.size() << std::endl;
          for (auto dau : dauV) {
            //std::cout << "Daughter " << dau->barcode() << " " << dau->status() << " " << dau->pdgId() << std::endl;
            if (!(dau->absPdgId() == 13 || dau->isNeutrino())) // jet from top without muons and neutrinos
            { hlvVis += dau->p4(); }
          }

          m_ptTrueT1qv  = hlvVis.Pt() / HG::GeV;
          m_phiTrueT1qv = hlvVis.Phi();
          m_etaTrueT1qv = hlvVis.Eta();
        }
      }

      return true;
    }

    return false;
  }

  bool FCNCTool::findTrueTopSM()
  {

    const xAOD::TruthParticleContainer *truthParticles = m_truthHandler->getTruthParticles();
    const xAOD::TruthParticle *trueT2(nullptr);

    for (auto tp : *truthParticles) {
      int pdg    = tp->pdgId();

      if (abs(pdg) != 6)
      { continue; }

      int status = tp->status();

      if (status != 62) // Only Py8 for the time being
      { continue; }

      unsigned int nC = tp->nChildren();

      for (unsigned int ic = 0; ic < nC; ic++) {
        int cpdg = tp->child(ic)->pdgId();

        if (abs(cpdg) == 5) {
          trueT2  = tp;
          break;
        }
      }

      if (trueT2)
      { break; }
    }

    if (trueT2) {
      m_mTrueT2  = trueT2->m() / HG::GeV;
      m_pxTrueT2 = trueT2->px() / HG::GeV;
      m_pyTrueT2 = trueT2->py() / HG::GeV;
      m_pzTrueT2 = trueT2->pz() / HG::GeV;

      unsigned int nC = trueT2->nChildren();

      for (unsigned int ic = 0; ic < nC; ic++) {
        const xAOD::TruthParticle *tpc = trueT2->child(ic);
        int cpdg = tpc->pdgId();

        // retrieve the b-quark (or maybe d, s)
        if (abs(cpdg) == 1 or abs(cpdg) == 3 or abs(cpdg) == 5) {
          m_ptTrueT2q  = tpc->pt() / HG::GeV;
          m_phiTrueT2q = tpc->phi();
          m_etaTrueT2q = tpc->eta();
          m_mTrueT2q = tpc->m() / HG::GeV;
          m_T2qpid     = cpdg;
          std::vector<const xAOD::TruthParticle *> dauV;
          GetDaughter(tpc, dauV);
          TLorentzVector hlvVis(0, 0, 0, 0);

          for (auto dau : dauV) {
            if (!(dau->absPdgId() == 13 || dau->isNeutrino()))
            { hlvVis += dau->p4(); }
          }

          m_ptTrueT2qv  = hlvVis.Pt() / HG::GeV;
          m_phiTrueT2qv = hlvVis.Phi();
          m_etaTrueT2qv = hlvVis.Eta();
        }

        // retrieve the W boson and its decay products
        if (abs(cpdg) == 24) {

          const xAOD::TruthParticle *lastW = (xAOD::TruthParticle *)ThisParticleFinal(tpc);
          int nWC = lastW->nChildren();

          for (unsigned int jc = 0; jc < nWC; jc++) {
            const xAOD::TruthParticle *wc = lastW->child(jc);
            int Wc_pdg = wc->pdgId();

            if (abs(wc->pdgId()) < 20) {
              if (abs(wc->pdgId()) % 2 == 0) { // c-quark, or charged lepton
                m_ptTrueT2Wc0  = wc->pt() / HG::GeV;
                m_phiTrueT2Wc0 = wc->phi();
                m_etaTrueT2Wc0 = wc->eta();
                m_mTrueT2Wc0   = wc->m() / HG::GeV;
                m_qpidWc0      = Wc_pdg;
              } else {                     // s-quark, or neutrino
                m_ptTrueT2Wc1  = wc->pt() / HG::GeV;
                m_phiTrueT2Wc1 = wc->phi();
                m_etaTrueT2Wc1 = wc->eta();
                m_mTrueT2Wc1   = wc->m() / HG::GeV;
                m_qpidWc1      = Wc_pdg;
              }
            }
          }

        }
      }

      return true;
    }

    return false;
  }


  void FCNCTool::GetDaughter(const xAOD::TruthParticle *tp, std::vector<const xAOD::TruthParticle *> &vecDaughter)
  {
    const xAOD::TruthVertex *decVtx = tp->decayVtx();

    if (decVtx) {
      int nP = decVtx->nOutgoingParticles();

      for (int id = 0; id < nP; id++) {
        const xAOD::TruthParticle *tpd = decVtx->outgoingParticle(id);

        if (tpd) {
          bool oksta = tpd->barcode() < 200000;
          bool okpdg = (tp->pdgId() != 21 && tp->absPdgId() > 6);

          if (oksta && okpdg && std::find(vecDaughter.begin(), vecDaughter.end(), tpd) == vecDaughter.end())
          { vecDaughter.push_back(tpd); }

          this->GetDaughter(tpd, vecDaughter);
        }
      }
    }
  }

  const xAOD::TruthParticle *FCNCTool::ThisParticleFinal(const xAOD::TruthParticle *p)
  {
    if (p->nChildren() == 1) {
      const xAOD::TruthParticle *c = (const xAOD::TruthParticle *)p->child(0);

      if (c->pdgId() != p->pdgId()) { return p; }
      else { return ThisParticleFinal(c); }
    } else { return p; }
  }

}
