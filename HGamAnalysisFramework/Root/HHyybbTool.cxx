///////////////////////////////////////////
// HH2yybb Tool
// Tool performs all yybb selections
// All info decorated to MxAODs
// @author: robert.reed@cern.ch
// @author: leonor.cerda.alberich@cern.ch
// @author: james.robinson@cern.ch
// @author: michela.paganini@cern.ch
// @author: elizabeth.brost@cern.ch
///////////////////////////////////////////

// STL includes
#include <algorithm>
// ATLAS/ROOT includes
#include <AsgTools/MsgStream.h>
#include <AsgTools/MsgStreamMacros.h>
#include <TMVA/Reader.h>
// Local includes
#include "HGamAnalysisFramework/EventHandler.h"
#include "HGamAnalysisFramework/HgammaIncludes.h"
#include "HGamAnalysisFramework/HHyybbTool.h"
#include "HGamAnalysisFramework/JetHandler.h"
#include "HGamAnalysisFramework/TruthHandler.h"
#include "hhTruthWeightTools/hhWeightTool.h"
// #include <deque>
// #include "MCUtils/PIDUtils.h"

namespace HG {
  // _______________________________________________________________________________________
  HHyybbTool::HHyybbTool(const char *name, HG::EventHandler *eventHandler, HG::TruthHandler *truthHandler, const bool & /*isMC*/)
    : asg::AsgMessaging(name)
    , m_eventHandler(eventHandler)
    , m_truthHandler(truthHandler)
    , m_1BTagWP("MV2c10_FixedCutBEff_60")
    , m_2BTagWP("MV2c10_FixedCutBEff_70")
    , m_bJet1_pTMin_low_mass(40.0)
    , m_bJet2_pTMin_low_mass(25.0)
    , m_mbb_min_low_mass(80.0)
    , m_mbb_max_low_mass(140.0)
    , m_bJet1_pTMin_high_mass(100.0)
    , m_bJet2_pTMin_high_mass(30.0)
    , m_mbb_min_high_mass(90.0)
    , m_mbb_max_high_mass(140.0)
    , m_myy_min_low_mass(120.39)
    , m_myy_max_low_mass(129.79)
    , m_myy_min_high_mass(120.79)
    , m_myy_max_high_mass(129.39)
    , nEventsOneTagJetChanged(0)
    , nEventsOneTag(0)
  {}

  // _______________________________________________________________________________________
  HHyybbTool::~HHyybbTool()
  {
    if (m_reader_low_mass) {
      delete m_reader_low_mass;
      m_reader_low_mass = nullptr;
    }

    if (m_reader_high_mass) {
      delete m_reader_high_mass;
      m_reader_high_mass = nullptr;
    }

    if (m_hhWeightTool) {
      delete m_hhWeightTool;
      m_hhWeightTool = nullptr;
    }
  }

  // _______________________________________________________________________________________
  HG::EventHandler *HHyybbTool::eventHandler()
  {
    return m_eventHandler;
  }

  // _______________________________________________________________________________________
  HG::TruthHandler *HHyybbTool::truthHandler()
  {
    return m_truthHandler;
  }

  // _______________________________________________________________________________________
  EL::StatusCode HHyybbTool::initialize(Config &config)
  {
    // Set default cut values etc here...
    // Almost all of these are set hard-coded because this will be used by the nominal MxAOD production
    // Also safer :)

    // Check if we are doing a detailed run or not
    m_detailedHHyybb = config.getBool("HHyybb.DetailedInfo", false);
    ATH_MSG_INFO("Write out detailed information?............... " << (m_detailedHHyybb ? "YES" : "NO"));

    // Muon and jet configuration
    m_muCorrType = allMu;
    ATH_MSG_INFO("Muons-in-jets correction...................... " << getMuonCorrPrefix(m_muCorrType).ReplaceAll("Mu_", ""));
    m_minMuonPT = config.getNum("HHyybbTool.MinMuonPt", 4.0); //! all muons to be considered for muon correction must have pT > m_minMuonPT (in GeV)
    ATH_MSG_INFO("MinMuonPt..................................... " << m_minMuonPT << " GeV");
    m_maxMuonJetDR = config.getNum("HHyybbTool.MaxMuonJetDR", 0.4); //! all muons to be considered for muon correction must have dR(muon, jet) < m_maxMuonJetDR
    ATH_MSG_INFO("MaxMuonJetDR.................................. " << m_maxMuonJetDR);
    m_jet_eta_max = config.getNum("HHyybbTool.MaxJetEta", 2.5); //! only central jets are b-tagged
    ATH_MSG_INFO("MaxJetEta..................................... " << m_jet_eta_max);

    // Check if we are doing a detailed run or not
    bool useBtaggingInMVA = config.getBool("HHyybbTool.UseBtaggingInMVA", true);
    ATH_MSG_INFO("Use MVAs trained with b-tagging information?.. " << (useBtaggingInMVA ? "YES" : "NO"));

    // Low mass classifier
    m_MVA_xml_low_mass = PathResolverFindCalibFile((useBtaggingInMVA ? "HGamAnalysisFramework/BDT/MVA_config_hh2yybb_NLO_low_mass_with_booleans.xml" : "HGamAnalysisFramework/BDT/MVA_config_hh2yybb_NLO_low_mass_without_booleans.xml"));
    ATH_MSG_INFO("... reading low-mass XML file:  " << m_MVA_xml_low_mass);
    m_reader_low_mass = new TMVA::Reader();
    m_reader_low_mass->AddVariable("abs_eta_j",     &m_abs_eta_j);
    m_reader_low_mass->AddVariable("abs_eta_jb",    &m_abs_eta_jb);
    m_reader_low_mass->AddVariable("Delta_eta_jb",  &m_Delta_eta_jb);
    m_reader_low_mass->AddVariable("idx_by_mH",     &m_idx_by_mH);
    m_reader_low_mass->AddVariable("idx_by_pT",     &m_idx_by_pT);
    m_reader_low_mass->AddVariable("idx_by_pT_jb",  &m_idx_by_pT_jb);
    m_reader_low_mass->AddVariable("m_jb",          &m_m_jb);

    if (useBtaggingInMVA) {
      m_reader_low_mass->AddVariable("passes_WP77", &m_passes_WP77);
      m_reader_low_mass->AddVariable("passes_WP85", &m_passes_WP85);
    }

    m_reader_low_mass->AddVariable("pT_j",          &m_pT_j);
    m_reader_low_mass->AddVariable("pT_jb",         &m_pT_jb);
    m_reader_low_mass->BookMVA("yybb_BDT_low_mass",  m_MVA_xml_low_mass);

    // High mass classifier
    m_MVA_xml_high_mass = PathResolverFindCalibFile((useBtaggingInMVA ? "HGamAnalysisFramework/BDT/MVA_config_hh2yybb_NLO_high_mass_with_booleans.xml" : "HGamAnalysisFramework/BDT/MVA_config_hh2yybb_NLO_high_mass_without_booleans.xml"));
    ATH_MSG_INFO("... reading high-mass XML file: " << m_MVA_xml_high_mass);
    m_reader_high_mass = new TMVA::Reader();
    m_reader_high_mass->AddVariable("abs_eta_j",     &m_abs_eta_j);
    m_reader_high_mass->AddVariable("abs_eta_jb",    &m_abs_eta_jb);
    m_reader_high_mass->AddVariable("Delta_eta_jb",  &m_Delta_eta_jb);
    m_reader_high_mass->AddVariable("idx_by_mH",     &m_idx_by_mH);
    m_reader_high_mass->AddVariable("idx_by_pT",     &m_idx_by_pT);
    m_reader_high_mass->AddVariable("idx_by_pT_jb",  &m_idx_by_pT_jb);
    m_reader_high_mass->AddVariable("m_jb",          &m_m_jb);

    if (useBtaggingInMVA) {
      m_reader_high_mass->AddVariable("passes_WP77", &m_passes_WP77);
      m_reader_high_mass->AddVariable("passes_WP85", &m_passes_WP85);
    }

    m_reader_high_mass->AddVariable("pT_j",          &m_pT_j);
    m_reader_high_mass->AddVariable("pT_jb",         &m_pT_jb);
    m_reader_high_mass->BookMVA("yybb_BDT_high_mass", m_MVA_xml_high_mass);

    m_hhWeightTool = new xAOD::hhWeightTool("hhWeights");
    m_hhWeightTool->initialize();

    return EL::StatusCode::SUCCESS;
  }

  // _______________________________________________________________________________________
  void HHyybbTool::saveHHyybbInfo(xAOD::PhotonContainer photons,
                                  xAOD::MuonContainer   muons,
                                  xAOD::JetContainer   &jets,
                                  xAOD::JetContainer   &jetsJVT)
  {
    // --------
    // Maps containing all event info to decorate to MxAOD
    // Floats for all decimal values
    // Chars for all bools
    // Ints for all counters (Category and multiplicities)
    std::map<TString, float> eventInfoFloats;
    std::map<TString, char>  eventInfoChars;
    std::map<TString, int>   eventInfoInts;

    // --------
    // Decorate Muon Correction to all jets (For all methods)
    ATH_MSG_DEBUG("About to decorate muon corrections");
    decorateMuonCorrections(jets, muons);

    // Perform low-mass selection
    ATH_MSG_DEBUG("About to run low-mass selection");
    performSelection(photons, jets, jetsJVT, eventInfoFloats, eventInfoChars, eventInfoInts, HHyybbTool::lowMass);

    // Perform high-mass selection
    ATH_MSG_DEBUG("About to run high-mass selection");
    performSelection(photons, jets, jetsJVT, eventInfoFloats, eventInfoChars, eventInfoInts, HHyybbTool::highMass);

    // --------
    // Save all the eventInfoMaps to the eventHandler()
    // Should always be called last, once all of these have been filled
    saveMapsToEventInfo(eventInfoFloats, eventInfoChars, eventInfoInts);
  }

  // _______________________________________________________________________________________
  // Function which performs full yybb selection and saves info to info maps (nominal by default)
  void HHyybbTool::performSelection(xAOD::PhotonContainer photons,
                                    xAOD::JetContainer &jets,
                                    xAOD::JetContainer &jets_noJVT,
                                    std::map<TString, float> &eventInfoFloats,
                                    std::map<TString, char> & /*eventInfoChars*/,
                                    std::map<TString, int> &eventInfoInts,
                                    HHyybbTool::massCatEnum massCat)
  {
    // -------
    // Set output label
    TString label(massCat == HHyybbTool::lowMass ? "lowMass_" :
                  massCat == HHyybbTool::highMass ? "highMass_" :
                  "");

    // --------
    // Apply central jet requirement (for b-tagging reasons)
    xAOD::JetContainer jets_central(SG::VIEW_ELEMENTS);
    std::copy_if(jets.begin(), jets.end(), std::back_inserter(jets_central), [this](const xAOD::Jet * j) {
      return fabs(j->eta()) <= m_jet_eta_max;
    });

    // --------
    // Construct jet containers based on which b-tagging WPs they pass
    xAOD::JetContainer jets_passing_2tag_WP(SG::VIEW_ELEMENTS);
    xAOD::JetContainer jets_passing_1tag_WP(SG::VIEW_ELEMENTS);
    xAOD::JetContainer jets_failing_1tag_WP(SG::VIEW_ELEMENTS);

    // Now classify as passing 2-tag and passing/failing 1-tag
    for (auto jet : jets_central) {
      if (jet->auxdata<char>(m_2BTagWP.Data())) { jets_passing_2tag_WP.push_back(jet); }

      if (jet->auxdata<char>(m_1BTagWP.Data())) { jets_passing_1tag_WP.push_back(jet); }
      else { jets_failing_1tag_WP.push_back(jet); }
    }

    // --------
    // Get btag category
    bTagCatEnum bTagCat = getBTagCategory(jets_passing_2tag_WP, jets_passing_1tag_WP, jets_failing_1tag_WP);
    ATH_MSG_DEBUG("yybb_bTagCat is " << bTagCat);
    eventInfoInts["yybb_bTagCat"] = bTagCat;

    // --------
    // Get the candidate jets: best two jets from (un)tagged jets
    // Internally, this uses the muon-corrected values
    float MVA_score = -99;
    xAOD::JetContainer candidateJets = getCandidateJetsWithMVAScore(jets_passing_2tag_WP, jets_passing_1tag_WP,
                                       jets_failing_1tag_WP, jets_central,
                                       bTagCat, massCat, MVA_score);
    ATH_MSG_DEBUG("yybb_" << label << "MVA_score is " << MVA_score);
    eventInfoFloats["yybb_" + label + "MVA_score"] = MVA_score;

    // --------
    // Retrieve vectors of jet 4-momenta with and without muon corrections
    std::vector<TLorentzVector> corrJets4V   = getMuonCorrJet4Vs(candidateJets, m_muCorrType);
    std::vector<TLorentzVector> unCorrJets4V = getMuonCorrJet4Vs(candidateJets, none);

    // --------
    // Get cutflows with corr and uncorr candidateJets
    eventInfoInts["yybb_" + label + "cutFlow"] = getHHyybbCutflow(photons, corrJets4V, bTagCat, massCat);

    if (m_detailedHHyybb) {
      eventInfoInts["yybb_" + label + "cutFlow_uncorr"] = getHHyybbCutflow(photons, unCorrJets4V, bTagCat, massCat);
    }

    // --------
    // Save truth flavor info for selected jets
    eventInfoInts["yybb_" + label + "truth_label_j1"] = int((HG::isMC() && candidateJets.size() > 0) ? candidateJets[0]->auxdata<int>("HadronConeExclTruthLabelID") : -99);
    eventInfoInts["yybb_" + label + "truth_label_j2"] = int((HG::isMC() && candidateJets.size() > 1) ? candidateJets[1]->auxdata<int>("HadronConeExclTruthLabelID") : -99);

    // --------
    // Calculate the JVT scale-factor from uncorrected, no-JVT jets
    double weight_JVT = weightJVT(massCat, jets_noJVT);
    ATH_MSG_DEBUG("weight_JVT is " << weight_JVT);

    // --------
    // Calculate the b-tagging scale-factor from the candidate jets
    // Pass jets rather than corrJets4V because we need to access the decorations
    // BTagging is done before muon correction anyway.
    double weight_b_tagging = weightBTagging(candidateJets, bTagCat);
    ATH_MSG_DEBUG("weight_b_tagging is " << weight_b_tagging);
    double weight_b_tagging_discrete = weightBTagging(candidateJets, bTagCat, true);
    ATH_MSG_DEBUG("weight_b_tagging_discrete is " << weight_b_tagging_discrete);
    double weight_NLO = weightNLO();
    ATH_MSG_DEBUG("weight_NLO is " << weight_NLO);

    // --------
    // Get HHyybb weight from JVT, b-tagging and NLO scale factors
    eventInfoFloats["yybb_" + label + "weight"] = weight_JVT * weight_b_tagging * weight_NLO;
    eventInfoFloats["yybb_" + label + "weight_LO"] = weight_JVT * weight_b_tagging;
    eventInfoFloats["yybb_" + label + "weight_discreteSFs"] = weight_JVT * weight_b_tagging_discrete;

    // --------
    // Set the four object information
    evaluateOutputKinematicQuantities(photons, corrJets4V, unCorrJets4V, eventInfoFloats, label);
  }


  // _______________________________________________________________________________________
  // Decorate the muon corrections to the jets
  void HHyybbTool::decorateMuonCorrections(xAOD::JetContainer &jets, xAOD::MuonContainer muons)
  {
    // --------
    // Apply pT to the muons
    xAOD::MuonContainer muons_passing_pT_cut(SG::VIEW_ELEMENTS);

    for (auto muon : muons) {
      if (!muon || (muon->pt() < m_minMuonPT * HG::GeV)) { continue; } // pT cut

      muons_passing_pT_cut.push_back(muon);
    }

    // --------
    // Decorate each jet with the appropriate muon correction
    for (auto jet : jets) {
      // Filter only muons which pass the dR cut
      xAOD::MuonContainer muons_passing_dR_cut(SG::VIEW_ELEMENTS);

      for (auto muon : muons_passing_pT_cut) {
        if (jet->p4().DeltaR(muon->p4()) > m_maxMuonJetDR) { continue; } // dR cut

        muons_passing_dR_cut.push_back(muon);
      }

      decorateMuonCorrectionToJet(jet, muons_passing_dR_cut, m_muCorrType);
    }
  }


  // _______________________________________________________________________________________
  // Calls the selected muon correction strategy, saves results of correction in jet branches ending in `_muonsInJ`
  void HHyybbTool::decorateMuonCorrectionToJet(xAOD::Jet *jet, xAOD::MuonContainer muons, HHyybbTool::muCorrTypeEnum corrType)
  {
    // Do nothing if we disable the correction.
    if (corrType == none) { return; }

    TString corrPrefix = getMuonCorrPrefix(corrType);

    // Kinematics of muons to be added to the jet
    jet->auxdata<float>((corrPrefix + "pt_muonInJ").Data())  = -99;
    jet->auxdata<float>((corrPrefix + "eta_muonInJ").Data()) = -99;
    jet->auxdata<float>((corrPrefix + "phi_muonInJ").Data()) = -99;
    jet->auxdata<float>((corrPrefix + "m_muonInJ").Data())   = -99;
    jet->auxdata<int>((corrPrefix + "n_muonsInJ").Data())    = -99;

    if (muons.size() == 0) { return; } // If there are no muons then the default -99 is kept

    // Get result of applying appropriate muon correction
    TLorentzVector muonCorrV4(corrType == allMu ? muonCorrectionAll(muons) :
                              corrType == clMu ? muonCorrectionClosest(jet, muons) :
                              corrType == pTMu ? muonCorrectionHighestPt(muons) :
                              TLorentzVector(0.0, 0.0, 0.0, 0.0)
                             );

    // Decorate jet with corrected values
    jet->auxdata<float>((corrPrefix + "pt_muonInJ").Data())  = muonCorrV4.Pt();
    jet->auxdata<float>((corrPrefix + "eta_muonInJ").Data()) = muonCorrV4.Eta();
    jet->auxdata<float>((corrPrefix + "phi_muonInJ").Data()) = muonCorrV4.Phi();
    jet->auxdata<float>((corrPrefix + "m_muonInJ").Data())   = muonCorrV4.M();
    jet->auxdata<int>((corrPrefix + "n_muonsInJ").Data())    = muons.size();
  }


  // _______________________________________________________________________________________
  void HHyybbTool::decorateWithMVAInputs(xAOD::Jet *bJet, xAOD::JetContainer &untaggedJets)
  {
    // Add indices which are needed for 1-tag MVA calculation
    for (auto uJet : untaggedJets) {
      TLorentzVector uJetMuonCorrected = getMuonCorrJet4V(uJet, m_muCorrType);
      TLorentzVector jb_p4 = bJet->p4() + uJetMuonCorrected;
      uJet->auxdata<float>("m_jb")         = jb_p4.M() / HG::GeV;
      uJet->auxdata<float>("pT_jb")        = jb_p4.Pt() / HG::GeV;
      uJet->auxdata<float>("abs_eta_jb")   = fabs(jb_p4.Eta());
      uJet->auxdata<float>("Delta_eta_jb") = fabs(bJet->eta() - uJetMuonCorrected.Eta());
      uJet->auxdata<float>("Delta_phi_jb") = fabs(bJet->p4().DeltaPhi(uJetMuonCorrected));
      uJet->auxdata<float>("pT_j")         = uJetMuonCorrected.Pt() / HG::GeV;
      uJet->auxdata<float>("abs_eta_j")    = fabs(uJetMuonCorrected.Eta());
    }

    // Sort by pT_jb and add index
    SG::AuxElement::Accessor<int> accIdxBypTjb("idx_by_pT_jb");
    std::sort(untaggedJets.begin(), untaggedJets.end(), [](const xAOD::Jet * i, const xAOD::Jet * j) {
      return i->auxdata<float>("pT_jb") > j->auxdata<float>("pT_jb");
    });

    for (unsigned int idx = 0; idx < untaggedJets.size(); ++idx) { accIdxBypTjb(*untaggedJets.at(idx)) = idx; }

    // Sort by distance from mH and add index
    SG::AuxElement::Accessor<int> accIdxByMh("idx_by_mH");
    std::sort(untaggedJets.begin(), untaggedJets.end(), [](const xAOD::Jet * i, const xAOD::Jet * j) {
      return fabs(i->auxdata<float>("m_jb") - HHyybbTool::m_massHiggs) < fabs(j->auxdata<float>("m_jb") - HHyybbTool::m_massHiggs);
    });

    for (unsigned int idx = 0; idx < untaggedJets.size(); ++idx) { accIdxByMh(*untaggedJets.at(idx)) = idx; }

    // Sort by pT and add index
    SG::AuxElement::Accessor<int> accIdxByPt("idx_by_pT");
    std::sort(untaggedJets.begin(), untaggedJets.end(), [](const xAOD::Jet * i, const xAOD::Jet * j) {
      return i->pt() > j->pt();
    });

    for (unsigned int idx = 0; idx < untaggedJets.size(); ++idx) { accIdxByPt(*untaggedJets.at(idx)) = idx; }
  }


  // _______________________________________________________________________________________
  // Fetch the btagging SF weight
  double HHyybbTool::weightBTagging(xAOD::JetContainer &candidateJets, HHyybbTool::bTagCatEnum bTagCat, const bool &useDiscreteSFs)
  {
    // Our default is to use the scale-factor from continuous tagging
    TString SFName("SF_MV2c10_Continuous");

    // If we're using the discrete scale factors then set the name according to
    // which b-tagging category we're in - NB. this is definitely wrong!
    if (useDiscreteSFs) {
      SFName = TString("SF_") + (bTagCat == 2 ? m_2BTagWP :
                                 bTagCat == 1 ? m_1BTagWP :
                                 bTagCat == 0 ? m_1BTagWP :
                                 m_2BTagWP);
    }

    // Retrieve appropriate b-tagging scale-factors
    double jetSF  = 1.0;

    for (size_t i = 0; i < candidateJets.size() && i < 2; ++i) {
      jetSF *= candidateJets[i]->isAvailable<float>(SFName.Data()) ? candidateJets[i]->auxdata<float>(SFName.Data()) : -1;
    }

    return jetSF;
  }


  // ___________________________________________________________________________________________
  // Multiply all the JVT (in)efficiencies for all jets passing the selection before the JVT cut
  double HHyybbTool::weightJVT(HHyybbTool::massCatEnum massCat, xAOD::JetContainer &jets_noJVT)
  {
    // Use approprate pT cut
    double pTmin(massCat == HHyybbTool::lowMass ? m_bJet2_pTMin_low_mass :
                 massCat == HHyybbTool::highMass ? m_bJet2_pTMin_high_mass :
                 0.0);

    // Construct output jet container
    xAOD::JetContainer jets_noJVT_passing_cuts(SG::VIEW_ELEMENTS);

    // Apply central jet requirement (for b-tagging reasons) and appropriate pT cuts
    for (auto jet : jets_noJVT) {
      if (fabs(jet->eta()) > m_jet_eta_max) { continue; }

      if (jet->pt() < pTmin * HG::invGeV) { continue; }

      jets_noJVT_passing_cuts.push_back(jet);
    }

    // Return multiplicative combination of JVT efficiencies
    if (jets_noJVT_passing_cuts.size() > 0) {
      return HG::JetHandler::multiplyJvtWeights(&jets_noJVT_passing_cuts);
    }

    return 1.0;
  }


  // _______________________________________________________________________________________
  // Get hh truth weight to reweight signal hh MC samples to full NLO prediction
  double HHyybbTool::weightNLO()
  {
    if (HG::isData()) { return 1.0; }

    // Get truth Higgses and require that there are exactly two
    xAOD::TruthParticleContainer truthHiggses = truthHandler()->getHiggsBosons();

    if (truthHiggses.size() != 2) {
      if (truthHiggses.size() > 2) { ATH_MSG_ERROR("More than two final state Higgses in the event record."); }

      return 1.0;
    }

    ATH_MSG_DEBUG("Found " << truthHiggses.size() << " Higgses which may need an NLO weight.");

    // Hard-code the DSID for SM hh
    if (m_eventHandler->mcChannelNumber() != 342620) { return 1.0; }

    // // Check for a BSM particle in the truth record -- no longer doing this due to SM/BSM interference
    // const xAOD::TruthParticleContainer* truthParticles = truthHandler()->getTruthParticles();
    // for (auto ptcl : *truthParticles) {
    //   if (fabs(ptcl->pdgId()) == 35) {
    //     ATH_MSG_DEBUG("Found a BSM Higgs with PDG ID 35");
    //     return 1.0;
    //   }
    // }
    // ATH_MSG_DEBUG("Did not find a BSM Higgs.");

    // Pass mhh in MeV
    float mhh = (truthHiggses.at(0)->p4() + truthHiggses.at(1)->p4()).M();
    double weight = m_hhWeightTool->getWeight(mhh);
    ATH_MSG_DEBUG("Found a di-Higgs system with mass " << mhh / 1000. << " GeV => NLO weight: " <<  weight);
    return weight;
  }


  // _______________________________________________________________________________________
  // Get the btag category from number of bJets in container
  HHyybbTool::bTagCatEnum HHyybbTool::getBTagCategory(xAOD::JetContainer jets_passing_2tag_WP, xAOD::JetContainer jets_passing_1tag_WP, xAOD::JetContainer jets_failing_1tag_WP)
  {
    if (jets_passing_2tag_WP.size() > 2) { return HHyybbTool::noTag; }
    else if (jets_passing_2tag_WP.size() == 2) { return HHyybbTool::twoTag; }
    else if ((jets_passing_1tag_WP.size() == 1) && (jets_failing_1tag_WP.size() >= 1)) { return HHyybbTool::oneTag; }
    else if (jets_failing_1tag_WP.size() >= 2) { return HHyybbTool::zeroTag; }
    else { return HHyybbTool::noTag; } // Cannot make two jets. i.e. < 2 Jets
  }


  // _______________________________________________________________________________________
  // Get two jets from corresponding category
  xAOD::JetContainer HHyybbTool::getCandidateJetsWithMVAScore(xAOD::JetContainer jets_passing_2tag_WP, xAOD::JetContainer jets_passing_1tag_WP, xAOD::JetContainer jets_failing_1tag_WP, xAOD::JetContainer jets_central, HHyybbTool::bTagCatEnum bTagCat, HHyybbTool::massCatEnum massCat, float &MVA_score)
  {
    // Construct container to hold the output jets
    xAOD::JetContainer candidateJets(SG::VIEW_ELEMENTS);

    // 0-tag: return the two highest pT jets
    if (bTagCat == zeroTag) {
      // Find the central jet pairing which gives a mass closest to the Higgs mass
      int chosen_j1(-1), chosen_j2(-1);
      double distance_from_mH(1e99);
      std::vector<TLorentzVector> muon_corrected_jets = getMuonCorrJet4Vs(jets_central, m_muCorrType);

      for (unsigned int j1 = 0; j1 < muon_corrected_jets.size(); ++j1) {
        for (unsigned int j2 = 0; j2 < j1; ++j2) {
          double mjj = (muon_corrected_jets.at(j1) + muon_corrected_jets.at(j2)).M() / HG::GeV;

          if (fabs(mjj - HHyybbTool::m_massHiggs) < distance_from_mH) {
            distance_from_mH = fabs(mjj - HHyybbTool::m_massHiggs);
            chosen_j1 = j1;
            chosen_j2 = j2;
          }
        }
      }

      ATH_MSG_DEBUG("Best mH match among " << muon_corrected_jets.size() << " jets in 0-tag category was " << (muon_corrected_jets.at(chosen_j1) + muon_corrected_jets.at(chosen_j2)).M() / HG::GeV);
      candidateJets.push_back(jets_central.at(chosen_j1));
      candidateJets.push_back(jets_central.at(chosen_j2));


      // 1-tag: return the tagged jet and the best MVA match
    } else if (bTagCat == oneTag) {
      // Define which MVA score we are going to check
      std::string MVA_name(massCat == HHyybbTool::lowMass ? "yybb_BDT_low_mass" :
                           massCat == HHyybbTool::highMass ? "yybb_BDT_high_mass" :
                           "");

      // Evaluate the one-tag MVAs
      evaluateOneTagMVAs(jets_passing_1tag_WP.at(0), jets_failing_1tag_WP);

      // Sort by the appropriate MVA
      std::sort(jets_failing_1tag_WP.begin(), jets_failing_1tag_WP.end(), [MVA_name](const xAOD::Jet * i, const xAOD::Jet * j) {
        return i->auxdata<double>(MVA_name) > j->auxdata<double>(MVA_name);
      });
      MVA_score = jets_failing_1tag_WP.at(0)->auxdata<double>(MVA_name);

      // Use the best match with no threshold
      candidateJets.push_back(jets_passing_1tag_WP.at(0));
      candidateJets.push_back(jets_failing_1tag_WP.at(0));

      // 2-tag: return the two tagged jets
    } else if (bTagCat == twoTag) {
      candidateJets.push_back(jets_passing_2tag_WP.at(0));
      candidateJets.push_back(jets_passing_2tag_WP.at(1));
    }

    // Sort jets by pT before returning
    candidateJets.sort(HG::JetHandler::comparePt);
    return candidateJets;
  }


  // _______________________________________________________________________________________
  // Get the cutflow given the two selected jets and the category they belong too.
  HHyybbTool::yybbCutflowEnum HHyybbTool::getHHyybbCutflow(xAOD::PhotonContainer selPhotons, std::vector<TLorentzVector> jet4Vs, HHyybbTool::bTagCatEnum bTagCat, HHyybbTool::massCatEnum massCat)
  {
    // Cut 0 - CENJETS Cut
    // At least two jets (candidateJets should collection of jets eta < 2.5)
    if (jet4Vs.size() < 2) { return CENJETS; }

    TLorentzVector j1 =  jet4Vs[0] * HG::invGeV;
    TLorentzVector j2 =  jet4Vs[1] * HG::invGeV;

    // Cut 1 - TAGGING Cut
    if (bTagCat == noTag) { return TAGGING; }

    // Cut 2 - PT Cut
    if (massCat == HHyybbTool::lowMass) {
      if ((j1.Pt() <= m_bJet1_pTMin_low_mass) || (j2.Pt() <= m_bJet2_pTMin_low_mass)) { return BPT; }
    } else if (massCat == HHyybbTool::highMass) {
      if ((j1.Pt() <= m_bJet1_pTMin_high_mass) || (j2.Pt() <= m_bJet2_pTMin_high_mass)) { return BPT; }
    }

    // Cut 3 - BBMASS Cut
    TLorentzVector jj = j1 + j2;

    if (massCat == HHyybbTool::lowMass) {
      if ((jj.M() < m_mbb_min_low_mass) || (jj.M() > m_mbb_max_low_mass)) { return BBMASS; }
    } else if (massCat == HHyybbTool::highMass) {
      if ((jj.M() < m_mbb_min_high_mass) || (jj.M() > m_mbb_max_high_mass)) { return BBMASS; }
    }

    // Cut 4 - YYMASS Cut
    // Start by requiring two photons
    if (selPhotons.size() < 2) { return YYMASS; }

    TLorentzVector yy = (selPhotons[0]->p4() * HG::invGeV) + (selPhotons[1]->p4() * HG::invGeV);

    if (massCat == HHyybbTool::lowMass) {
      if ((yy.M() < m_myy_min_low_mass) || (yy.M() > m_myy_max_low_mass)) { return YYMASS; }
    } else if (massCat == HHyybbTool::highMass) {
      if ((yy.M() < m_myy_min_high_mass) || (yy.M() > m_myy_max_high_mass)) { return YYMASS; }
    }

    // If we pass everything then we have two jets that pass the pT and Mass cuts.
    return PASSYYBB;
  }


  // _______________________________________________________________________________________
  std::vector<TLorentzVector> HHyybbTool::getMuonCorrJet4Vs(xAOD::JetContainer &candidateJets, HHyybbTool::muCorrTypeEnum corrType)
  {
    // Get the prefix of the muon correction type
    TString corrPrefix = getMuonCorrPrefix(corrType);
    std::vector<TLorentzVector> correctedJets;

    // Fill vector with corrected jets
    for (auto jet : candidateJets) {
      correctedJets.push_back(getMuonCorrJet4V(jet, corrType));
    }

    // Sort by pT before returning
    std::sort(correctedJets.begin(), correctedJets.end(), [](const TLorentzVector & i, const TLorentzVector & j) { return i.Pt() > j.Pt(); });
    return correctedJets;
  }


  // _______________________________________________________________________________________
  TLorentzVector HHyybbTool::getMuonCorrJet4V(xAOD::Jet *jet, HHyybbTool::muCorrTypeEnum corrType)
  {
    // Get the prefix of the muon correction type
    if (corrType == none) { return jet->p4(); }

    TString corrPrefix = getMuonCorrPrefix(corrType);

    // If we have no muons in jet or we disable the feature then no correction applied.
    int nMuonsInJet = jet->auxdata<int>((corrPrefix + "n_muonsInJ").Data());

    if ((nMuonsInJet == -99) || (nMuonsInJet == 0)) { return jet->p4(); }

    // Get all info needed to build the 4V of the muon correction
    double muon_Pt  = jet->auxdata<float>((corrPrefix + "pt_muonInJ").Data());
    double muon_Eta = jet->auxdata<float>((corrPrefix + "eta_muonInJ").Data());
    double muon_Phi = jet->auxdata<float>((corrPrefix + "phi_muonInJ").Data());
    double muon_M   = jet->auxdata<float>((corrPrefix + "m_muonInJ").Data());

    // Set the 4V and add it to the raw calib jet.
    TLorentzVector muonJet4V;
    muonJet4V.SetPtEtaPhiM(muon_Pt, muon_Eta, muon_Phi, muon_M);
    muonJet4V += jet->p4();
    return muonJet4V;
  }


  // _______________________________________________________________________________________
  void HHyybbTool::evaluateOneTagMVAs(xAOD::Jet *bJet, xAOD::JetContainer untaggedJets)
  {
    // Decorate with event-level and jet-level variables
    decorateWithMVAInputs(bJet, untaggedJets);

    for (auto uJet : untaggedJets) {
      // Set branch variable values appropriately for this jet
      m_abs_eta_j    = uJet->auxdata<float>("abs_eta_j");
      m_abs_eta_jb   = uJet->auxdata<float>("abs_eta_jb");
      m_Delta_eta_jb = uJet->auxdata<float>("Delta_eta_jb");
      m_idx_by_mH    = float(uJet->auxdata<int>("idx_by_mH"));
      m_idx_by_pT    = float(uJet->auxdata<int>("idx_by_pT"));
      m_idx_by_pT_jb = float(uJet->auxdata<int>("idx_by_pT_jb"));
      m_jb           = uJet->auxdata<float>("m_jb");
      m_passes_WP77  = (uJet->auxdata<char>("MV2c10_FixedCutBEff_77") ? 1.0 : 0.0);
      m_passes_WP85  = (uJet->auxdata<char>("MV2c10_FixedCutBEff_85") ? 1.0 : 0.0);
      m_pT_j         = uJet->auxdata<float>("pT_j");
      m_pT_jb        = uJet->auxdata<float>("pT_jb");

      // Retrieve the corresponding MVA outputs and decorate the jets
      uJet->auxdata<double>("yybb_BDT_low_mass") = m_reader_low_mass->EvaluateMVA("yybb_BDT_low_mass");
      uJet->auxdata<double>("yybb_BDT_high_mass") = m_reader_high_mass->EvaluateMVA("yybb_BDT_high_mass");
    }
  }


  // _______________________________________________________________________________________
  // Set additional event info
  void HHyybbTool::evaluateOutputKinematicQuantities(xAOD::PhotonContainer selPhotons, std::vector<TLorentzVector> corrJets4V, std::vector<TLorentzVector> unCorrJets4V, std::map<TString, float> &eventInfoFloats, TString label)
  {
    // Default variable values
    eventInfoFloats["yybb_" + label + "pT_j1"]          = -99.0;
    eventInfoFloats["yybb_" + label + "pT_j2"]          = -99.0;
    eventInfoFloats["yybb_" + label + "m_yybb"]         = -99.0;
    eventInfoFloats["yybb_" + label + "m_bb_uncnstrnd"] = -99.0;

    // Detailed variables if requested
    if (m_detailedHHyybb) {
      eventInfoFloats["yybb_" + label + "m_bb_uncorr_uncnstrnd"]   = -99.0;
      eventInfoFloats["yybb_" + label + "m_yybb_uncorr"]           = -99.0;
      eventInfoFloats["yybb_" + label + "m_yybb_uncnstrnd"]        = -99.0;
      eventInfoFloats["yybb_" + label + "m_yybb_uncorr_uncnstrnd"] = -99.0;
    }

    // Require two photons and two jets
    if (selPhotons.size() < 2) { return; }

    if (corrJets4V.size() < 2) { return; }

    if (unCorrJets4V.size() < 2) { return; }

    // Set 4-vectors of combined quantities
    TLorentzVector yy = selPhotons[0]->p4() + selPhotons[1]->p4();

    // Dijets with/without muon correction and with/without constraining m_jj to the Higgs mass
    TLorentzVector jj_uncorr_uncnstrnd = unCorrJets4V[0] + unCorrJets4V[1];
    TLorentzVector jj_corr_uncnstrnd   = corrJets4V[0] + corrJets4V[1];
    TLorentzVector jj_uncorr_cnstrnd   = jj_uncorr_uncnstrnd * (HHyybbTool::m_massHiggs / (jj_uncorr_uncnstrnd.M() * HG::invGeV));
    TLorentzVector jj_corr_cnstrnd     = jj_corr_uncnstrnd * (HHyybbTool::m_massHiggs / (jj_corr_uncnstrnd.M() * HG::invGeV));

    // Four-body with/without muon correction and with/without constraining m_jj to the Higgs mass
    TLorentzVector yyjj_uncorr_uncnstrnd = yy + jj_uncorr_uncnstrnd;
    TLorentzVector yyjj_corr_uncnstrnd   = yy + jj_corr_uncnstrnd;
    TLorentzVector yyjj_uncorr_cnstrnd   = yy + jj_uncorr_cnstrnd;
    TLorentzVector yyjj_corr_cnstrnd     = yy + jj_corr_cnstrnd;

    // Write variables that we want to save
    eventInfoFloats["yybb_" + label + "pT_j1"]          = corrJets4V[0].Pt();
    eventInfoFloats["yybb_" + label + "pT_j2"]          = corrJets4V[1].Pt();
    eventInfoFloats["yybb_" + label + "m_yybb"]         = yyjj_corr_cnstrnd.M();
    eventInfoFloats["yybb_" + label + "m_bb_uncnstrnd"] = jj_corr_uncnstrnd.M();

    // ... and the ones that are only in detailed mode
    if (m_detailedHHyybb) {
      eventInfoFloats["yybb_" + label + "m_bb_uncorr_uncnstrnd"]   = jj_uncorr_uncnstrnd.M();
      eventInfoFloats["yybb_" + label + "m_yybb_uncorr_uncnstrnd"] = yyjj_uncorr_uncnstrnd.M();
      eventInfoFloats["yybb_" + label + "m_yybb_uncnstrnd"]        = yyjj_corr_uncnstrnd.M();
      eventInfoFloats["yybb_" + label + "m_yybb_uncorr"]           = yyjj_uncorr_cnstrnd.M();
    }
  }


  // _______________________________________________________________________________________
  // Save event info from maps to eventInfo
  void HHyybbTool::saveMapsToEventInfo(std::map<TString, float> &eventInfoFloats, std::map<TString, char> &eventInfoChars, std::map<TString, int> &eventInfoInts)
  {
    // Floats
    for (auto element : eventInfoFloats) {
      eventHandler()->storeVar<float>(element.first.Data(), element.second);
    }

    // Chars
    for (auto element : eventInfoChars) {
      eventHandler()->storeVar<char>(element.first.Data(), element.second);
    }

    // Ints
    for (auto element : eventInfoInts) {
      eventHandler()->storeVar<int>(element.first.Data(), element.second);
    }
  }


  // _______________________________________________________________________________________
  TString HHyybbTool::getMuonCorrPrefix(HHyybbTool::muCorrTypeEnum corrType)
  {
    if (corrType == HG::HHyybbTool::allMu) { return TString("allMu_"); }

    if (corrType == HG::HHyybbTool::clMu) { return TString("closeMu_"); }

    if (corrType == HG::HHyybbTool::pTMu) { return TString("pTMu_"); }

    return TString("");
  }


  // _______________________________________________________________________________________
  TLorentzVector HHyybbTool::muonCorrectionClosest(xAOD::Jet *jet, xAOD::MuonContainer muons)
  {
    TLorentzVector muonInJet(0, 0, 0, 0);

    // Sort muons by dR and return smallest (closest) muon
    if (muons.size() > 0) {
      std::sort(muons.begin(), muons.end(), [jet](const xAOD::Muon * i, const xAOD::Muon * j) {
        return jet->p4().DeltaR(i->p4()) < jet->p4().DeltaR(j->p4()); // sort by dR lowest first
      });
      muonInJet += muons.at(0)->p4();
    }

    return muonInJet;
  }


  // _______________________________________________________________________________________
  TLorentzVector HHyybbTool::muonCorrectionHighestPt(xAOD::MuonContainer muons)
  {
    TLorentzVector muonInJet(0, 0, 0, 0);

    // Sort muons by pT and return highest muon
    if (muons.size() > 0) {
      std::sort(muons.begin(), muons.end(), [](const xAOD::Muon * i, const xAOD::Muon * j) {
        return i->pt() > j->pt(); // sort by pT highest first
      });
      muonInJet += muons.at(0)->p4();
    }

    return muonInJet;
  }


  // _______________________________________________________________________________________
  TLorentzVector HHyybbTool::muonCorrectionAll(xAOD::MuonContainer muons)
  {
    TLorentzVector muonInJet(0, 0, 0, 0);

    for (auto muon : muons) { muonInJet += muon->p4(); }

    return muonInJet;
  }

}
