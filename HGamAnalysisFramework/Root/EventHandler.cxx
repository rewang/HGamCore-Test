#include "HGamAnalysisFramework/EventHandler.h"

#include "HGamAnalysisFramework/HGamVariables.h"
#include "HGamAnalysisFramework/JetHandler.h"

#include "PhotonVertexSelection/PhotonVertexHelpers.h"

#include "xAODEventShape/EventShape.h"
#include "GoodRunsLists/TGRLCollection.h"
#include "VertexPositionReweighting/VertexPositionReweightingTool.h"
#include "TrigBunchCrossingTool/WebBunchCrossingTool.h"
#include "TrigBunchCrossingTool/StaticBunchCrossingTool.h"

#include "TPRegexp.h"


namespace HG {

  SG::AuxElement::Decorator<float> EventHandler::trigSF("SF_trig");
  SG::AuxElement::Decorator<unsigned int> EventHandler::RandomRunNumber("RandomRunNumber");

  //______________________________________________________________________________
  EventHandler::EventHandler(const char *name, xAOD::TEvent *event, xAOD::TStore *store)
    : asg::AsgMessaging(name)
    , m_event(event)
    , m_store(store)
    , m_grl(nullptr)
    , m_vtxRW(nullptr)
    , m_configTool(nullptr)
    , m_trigGlobalTool("TrigGlobalEfficiencyCorrectionTool/TrigGlobal")
    , m_trigDecTool(nullptr)
    , m_trigMuonScaleFactors(nullptr)
    , m_trigElectronScaleFactors(nullptr)
    , m_trigElectronMCEfficiency(nullptr)
    , m_trigDiElectronScaleFactors(nullptr)
    , m_trigDiElectronMCEfficiency(nullptr)
    , m_forceWeights(false)
    , m_fixSherpa224(false)
    , m_checkGRL(false)
    , m_checkTile(false)
    , m_checkLAr(false)
    , m_checkCore(false)
    , m_checkBkg(false)
    , m_checkTrig(false)
    , m_checkSCT(false)
  { }

  //______________________________________________________________________________
  EventHandler::~EventHandler()
  {
    SafeDelete(m_grl);
    SafeDelete(m_vtxRW);
    SafeDelete(m_bunchTool);
    SafeDelete(m_configTool);
    SafeDelete(m_trigDecTool);
    SafeDelete(m_trigMuonScaleFactors);
    SafeDelete(m_trigElectronScaleFactors);
    SafeDelete(m_trigElectronMCEfficiency);
    SafeDelete(m_trigDiElectronScaleFactors);
    SafeDelete(m_trigDiElectronMCEfficiency);

    for (auto dec : m_trigDec) { SafeDelete(dec.second); }

    for (auto dec : m_trigAcc) { SafeDelete(dec.second); }
  }

  //______________________________________________________________________________
  EL::StatusCode EventHandler::initialize(Config &config)
  {
    const char *APP_NAME = "HG::EventHandler";

    // General options
    const xAOD::EventInfo *eventInfo = nullptr;

    if (m_event->retrieve(eventInfo, "EventInfo").isFailure()) {
      HG::fatal("Cannot access EventInfo");
    }

    m_jvt     = config.getNum("JetHandler.Selection.JVT", 0.941);

    m_is50ns = config.getBool("Is50ns", false);

    m_trigMatchPhotondR   = config.getNum("EventHandler.TriggerMatchdR.Photon", 0.1);
    m_trigMatchElectrondR = config.getNum("EventHandler.TriggerMatchdR.Electron", 0.1);
    m_trigMatchMuondR     = config.getNum("EventHandler.TriggerMatchdR.Muon", 0.1);

    if (HG::isMC()) {
      int mcid = eventInfo->mcChannelNumber();

      m_higgsWeightTool.setTypeAndName("xAOD::HiggsWeightTool/higgsWeightTool");

      std::map<TString, std::vector<int> > weightDSIDs;
      StrV higgsTypes = config.getStrV("EventHandler.HiggsWeights.Types", {});

      for (TString sample : higgsTypes) {
        StrV dsids = config.getStrV("EventHandler.HiggsWeights." + sample, {""});

        for (TString dsid : dsids) {
          weightDSIDs[sample].push_back(std::atoi(dsid.Data()));

          if (mcid == std::atoi(dsid.Data())) {
            CHECK(m_higgsWeightTool.setProperty(TString::Format("Force%s", sample.Data()).Data(), true));
            m_forceWeights = true;
          }
        }
      }

      CHECK(m_higgsWeightTool.setProperty("RequireFinite", config.getBool("EventHandler.HiggsWeights.RequireFinite", true)));

      TString option;

      if (config.isDefined("EventHandler.HiggsWeights.WeightCutOffFactor")) {
        option = TString::Format("CrossSection.%d", mcid);

        if (!config.isDefined(option))
        { fatal("HiggsWeightTool configured to used cut-off factor, but no cross-section available for this sample: " + option); }

        float xs = config.getNum(option);
        float factor = config.getNum("EventHandler.HiggsWeights.WeightCutOffFactor");
        float trunc = xs / 0.00227 * factor;
        Info("EventHandler", "Using a weight truncation of %f", trunc);
        CHECK(m_higgsWeightTool.setProperty("WeightCutOff", trunc));
      }

      if ((m_forceWeights || m_event->containsMeta<xAOD::TruthMetaData>("TruthMetaData")) && m_higgsWeightTool.retrieve().isFailure())
      { fatal("Failed to initialize HiggsWeightTool"); }

      option = TString::Format("NominalWeightIndex.%d", mcid);
      m_mcWeightIndex = config.getInt(option, -1);

      // Check for which DSIDs are affected by Sherpa 2.2.4 weight bug
      StrV dsids = config.getStrV("EventHandler.Sherpa224WeightBug", {""});

      for (TString dsid : dsids) {
        if (mcid == std::atoi(dsid.Data()))
        { m_fixSherpa224 = true; }
      }

    }

    // GRL selection
    std::vector<std::string> vecGRL;

    for (auto grl : config.getStrV("EventHandler.GRL"))
    { vecGRL.push_back(PathResolverFindCalibFile(grl.Data())); }

    m_grl = new GoodRunsListSelectionTool("GoodRunsListSelectionTool");
    CHECK(m_grl->setProperty("GoodRunsListVec", vecGRL));

    if (m_grl->initialize().isFailure())
    { fatal("Failed to initialize GRL tool"); }

    // Pileup weighting
    std::vector<std::string> confFiles;
    std::vector<std::string> lcalcFiles, lcalcFilesMC;

    if (m_is50ns) {
      for (TString val : config.getStrV("EventHandler.PRW.ConfigFiles50ns"))
      { confFiles.push_back(PathResolverFindCalibFile(val.Data())); }

      for (TString val : config.getStrV("EventHandler.PRW.LumiCalcFiles50ns"))
      { lcalcFiles.push_back(val.Data()); }
    } else {
      TString simType = (HG::isAFII() ? "FastSim" : "FullSim");

      if (HG::isMC()) {
        for (TString val : config.getStrV("EventHandler.PRW.ConfigFiles" + HG::mcType() + simType))
        { confFiles.push_back(val.Data()); }
      }

      for (TString val : config.getStrV("EventHandler.PRW.LumiCalcFiles"))
      { lcalcFiles.push_back(val.Data()); }

      if (HG::isMC()) {
        for (TString val : config.getStrV("EventHandler.PRW.LumiCalcFiles" + HG::mcType()))
        { lcalcFilesMC.push_back(val.Data()); }
      }
    }

    m_prwSF     = config.getNum("EventHandler.PRW.DataScaleFactor", 0.862069);
    // int defChan = config.getNum("EventHandler.PRW.DefaultChannel"+HG::mcType(), 341000);

    double prwSFup     = config.getNum("EventHandler.PRW.DataScaleFactorUP", 0.917431);
    double prwSFdown   = config.getNum("EventHandler.PRW.DataScaleFactorDOWN", 0.813008);

    m_pileupRW.setTypeAndName("CP::PileupReweightingTool/HGamPRW");
    CHECK(m_pileupRW.setProperty("ConfigFiles", confFiles));
    CHECK(m_pileupRW.setProperty("LumiCalcFiles", lcalcFilesMC));
    CHECK(m_pileupRW.setProperty("DataScaleFactor", m_prwSF));

    // Due to poor mu modelling, the prwSFup variation has unprepresented data, and breaks
    // the reweighting. Since MC16d is used for papers, MC16c uses the down component
    // twice to provide something ~reasonable.
    if (HG::isMC() && HG::mcType() == "MC16c")
    { CHECK(m_pileupRW.setProperty("DataScaleFactorUP", prwSFdown)); }
    else
    { CHECK(m_pileupRW.setProperty("DataScaleFactorUP", prwSFup)); }

    CHECK(m_pileupRW.setProperty("DataScaleFactorDOWN", prwSFdown));

    // CHECK(m_pileupRW.setProperty("DefaultChannel" , defChan   ));
    if (m_pileupRW.retrieve().isFailure())
    { fatal("Failed to retrieve PRW tool in EventHandler"); }

    m_pileupRWdata.setTypeAndName("CP::PileupReweightingTool/HGamPRWdata");
    CHECK(m_pileupRWdata.setProperty("ConfigFiles", confFiles));
    CHECK(m_pileupRWdata.setProperty("LumiCalcFiles", lcalcFiles));
    CHECK(m_pileupRWdata.setProperty("DataScaleFactor", m_prwSF));
    CHECK(m_pileupRWdata.setProperty("DataScaleFactorUP", prwSFup));
    CHECK(m_pileupRWdata.setProperty("DataScaleFactorDOWN", prwSFdown));

    if (m_pileupRWdata.retrieve().isFailure())
    { fatal("Failed to retrieve PRW tool in EventHandler"); }

    m_prwExtra = config.getStrV("EventHandler.PRW.ExtraWPs", {});

    for (TString point : m_prwExtra) {
      lcalcFilesMC.clear();

      for (TString val : config.getStrV("EventHandler.PRW." + point + ".LumiCalcFiles"))
      { lcalcFilesMC.push_back(val.Data()); }

      m_extraPileupRW.push_back(asg::AnaToolHandle<CP::IPileupReweightingTool>());

      TString name = "CP::PileupReweightingTool/HGamPRW" + point;
      m_extraPileupRW.back().setTypeAndName(name.Data());
      CHECK(m_extraPileupRW.back().setProperty("ConfigFiles", confFiles));
      CHECK(m_extraPileupRW.back().setProperty("LumiCalcFiles", lcalcFilesMC));
      CHECK(m_extraPileupRW.back().setProperty("DataScaleFactor", m_prwSF));
      CHECK(m_extraPileupRW.back().setProperty("DataScaleFactorUP", prwSFup));
      CHECK(m_extraPileupRW.back().setProperty("DataScaleFactorDOWN", prwSFdown));

      // CHECK(m_extraPileupRW.back().setProperty("DefaultChannel" , defChan   ));
      if (m_extraPileupRW.back().retrieve().isFailure())
      { fatal("Failed to retrieve extra PRW tool in EventHandler"); }
    }

    // Vertex weighting
    if (HG::isMC()) {
      m_vtxRW = new CP::VertexPositionReweightingTool("VertexPosition");
      CHECK(m_vtxRW->setProperty("DataMean",  config.getNum("EventHandler.VertexMeanZmm" + HG::mcType())));
      CHECK(m_vtxRW->setProperty("DataSigma",  config.getNum("EventHandler.VertexSigmaZmm" + HG::mcType())));

      if (m_vtxRW->initialize().isFailure())
      { fatal("Failed to initialize vertex RW tool"); }
    }

    // General options
    if (config.getBool("EventHandler.CheckDuplicate", true)) { m_checkDuplic = true; }

    if (config.getBool("EventHandler.CheckGRL", true)) { m_checkGRL    = true; }

    if (config.getBool("EventHandler.CheckTile", true)) { m_checkTile   = true; }

    if (config.getBool("EventHandler.CheckLAr", true)) { m_checkLAr    = true; }

    if (config.getBool("EventHandler.CheckCore", true)) { m_checkCore   = true; }

    if (config.getBool("EventHandler.CheckBackground", true)) { m_checkBkg    = true; }

    if (config.getBool("EventHandler.CheckVertex", true)) { m_checkVertex = true; }

    if (config.getBool("EventHandler.CheckTriggers", true)) { m_checkTrig   = true; }

    if (config.getBool("EventHandler.CheckSCT", true)) { m_checkSCT    = true; }

    m_truthPtclName = config.getStr("TruthParticles.ContainerName", "TruthParticle");

    bool doBunch = config.getBool("EventHandler.EnableBunchCrossingTool", true);
    m_bunchTool = nullptr;

    if (doBunch) {
      if (HG::isMC()) {
        // Bunch info for MC isn't accessible outside Athena, but luckily it's static
        Config bunchConf;
        bunchConf.addFile("HGamAnalysisFramework/MCBunchStructure.config");

        NumV bunchIntensity = bunchConf.getNumV(("BunchStructure." + HG::mcType()).Data());
        std::vector<float> bunchIntF;

        for (double bi : bunchIntensity) { bunchIntF.push_back(bi); }

        // Initialize static tool
        Trig::StaticBunchCrossingTool *bunch = new Trig::StaticBunchCrossingTool();
        CHECK(bunch->setProperty("OutputLevel", MSG::INFO));
        CHECK(bunch->setProperty("MaxBunchSpacing", 75));

        if (bunch->loadConfig(bunchIntF).isFailure())
        { fatal("Failed to loadConfig for StaticBunchCrossingTool"); }

        m_bunchTool = bunch;

      } else {
        // Bunch info for data is stored on the web, and changes run to trun
        Trig::WebBunchCrossingTool *bunch = new Trig::WebBunchCrossingTool("WebTool");
        CHECK(bunch->setProperty("OutputLevel", MSG::INFO));
        CHECK(bunch->setProperty("ServerAddress", "atlas-trigconf.cern.ch"));
        CHECK(bunch->setProperty("MaxBunchSpacing", 75));

        if (bunch->initialize().isFailure())
        { fatal("Failed to initialize WebBunchCrossingTool"); }

        m_bunchTool = bunch;
      }
    }

    if (HG::isMC()) { m_checkGRL = false; }

    m_requiredTriggers = config.getStrV("EventHandler.RequiredTriggers");

    // Trigger decision tool
    if (not HG::isMAOD()) {
      m_configTool = new TrigConf::xAODConfigTool("xAODConfigTool");
      ToolHandle<TrigConf::ITrigConfigTool> configHandle(m_configTool);

      if (configHandle->initialize().isFailure()) {
        fatal("Failed to initialize trigger config handle");
      }

      m_trigDecTool = new Trig::TrigDecisionTool("TrigDecisionTool");
      CHECK(m_trigDecTool->setProperty("ConfigTool", configHandle));
      CHECK(m_trigDecTool->setProperty("TrigDecisionKey", "xTrigDecision"));

      if (m_trigDecTool->initialize().isFailure()) {
        fatal("Failed to initialize trigger decision tool");
      }

      // Set up trigger matching map
      for (auto trig : m_requiredTriggers) {
        m_trigMatch[trig] = config.getStr("EventHandler.TriggerMatchType." + trig, "");

        // if it is an electron trigger then the thresholds are set manually in the config file
        if (TPRegexp("_e").MatchB(trig) || TPRegexp("_2e").MatchB(trig) || TPRegexp("_mu").MatchB(trig) || TPRegexp("_2mu").MatchB(trig)) {
          m_trigThresholds[trig] = config.getNumV("EventHandler.TriggerThresholds." + trig);
        }
      }

      m_trigMatching.setTypeAndName("Trig::MatchingTool/HGam");

      if (m_trigMatching.retrieve().isFailure())
      { fatal("Failed to initialize TriggerMatching tool"); }

      // Trigger matching for muons
      //      m_trigMuonMatchTool = new Trig::TrigMuonMatching("TrigMuonMatching");
      //      CHECK(m_trigMuonMatchTool->setProperty("TriggerTool", ToolHandle<Trig::TrigDecisionTool>(m_trigDecTool)));
      //      if (m_trigMuonMatchTool->initialize().isFailure()) {
      //        fatal("Failed to initialize TrigMuonMatchingTool");
      //      }
      //      m_trigElectronMatchTool = new Trig::TrigEgammaMatchingTool("TrigEgammaMatchingTool");

      // Determine if triggers only apply to certain runs
      for (TString trig : m_requiredTriggers) {
        TString first = "", last = "";
        int index = -1;
        StrV ranges;

        if (config.isDefined("EventHandler.RunNumbers." + trig))
        { ranges = config.getStrV("EventHandler.RunNumbers." + trig); }

        for (TString range : ranges) {
          if (range.Contains("-")) {
            // If it's a range, figure out which runs within the range are in GRL
            index  = range.Index("-");
            first  = range(0, index);
            last = range(index + 1, range.Length());

            for (int run = first.Atoi(); run <= last.Atoi(); ++run)
              if (m_grl->getGRLCollection().HasRun(run))
              { m_trigRunNumbers[trig].insert(run); }
          } else {
            // If it's just a single run, push it back
            m_trigRunNumbers[trig].insert(range.Atoi());
          }
        }
      }
    }

    StrV muonPids = config.getStrV("MuonHandler.Selection.PID", {"FRED"});
    TString muonPid = muonPids.size() > 0 ? muonPids[0] : "FRED";

    m_trigMuonScaleFactors = new CP::MuonTriggerScaleFactors("TrigMuonScaleFactors");
    CHECK(m_trigMuonScaleFactors->setProperty("MuonQuality", muonPid.Data()));

    if (HG::isMC() && ! m_trigMuonScaleFactors->initialize().isSuccess()) {
      fatal("Failed to properly initialize the MuonTriggerScaleFactors Tool. Exiting.");
    } else {
      std::cout << "initialised  AsgElectronEfficiencyTrigCorrectionTool" << std::endl;
    }

    // Electron trigger scale factor per electron
    m_trigElectronScaleFactors = new AsgElectronEfficiencyCorrectionTool("AsgElectronEfficiencyTrigCorrectionTool");
    TString bcid = m_is50ns ? "50ns" : "25ns";
    std::string file_trigSF   = PathResolverFindCalibFile(config.getStr("EventHandler.ScaleFactor.TrigCorrectionFileName" + bcid).Data());
    std::vector< std::string > correctionFilesTrigSF;
    correctionFilesTrigSF.push_back(file_trigSF);
    CHECK(m_trigElectronScaleFactors->setProperty("CorrectionFileNameList", correctionFilesTrigSF));
    CHECK(m_trigElectronScaleFactors->setProperty("ForceDataType", HG::isAFII() ? 3 : 1));
    CHECK(m_trigElectronScaleFactors->setProperty("CorrelationModel", config.getStr("ElectronHandler.Efficiency.CorrelationModel").Data()));


    if (m_trigElectronScaleFactors->initialize().isFailure()) {
      fatal("Failed to initialize AsgElectronEfficiencyTrigCorrectionTool");
    } else {
      std::cout << "initialised  AsgElectronEfficiencyTrigCorrectionTool" << std::endl;
    }


    m_trigElectronMCEfficiency = new AsgElectronEfficiencyCorrectionTool("AsgElectronEfficiencyTrigMCEffCorrectionTool");
    std::string file_trigMCEff   = PathResolverFindCalibFile(config.getStr("EventHandler.ScaleFactor.TrigMCEffCorrectionFileName" + bcid).Data());
    std::vector< std::string > correctionFilesTrigMCEff;
    correctionFilesTrigMCEff.push_back(file_trigMCEff);
    CHECK(m_trigElectronMCEfficiency->setProperty("CorrectionFileNameList", correctionFilesTrigMCEff));
    CHECK(m_trigElectronMCEfficiency->setProperty("ForceDataType", HG::isAFII() ? 3 : 1));
    CHECK(m_trigElectronMCEfficiency->setProperty("CorrelationModel", config.getStr("ElectronHandler.Efficiency.CorrelationModel").Data()));



    if (m_trigElectronMCEfficiency->initialize().isFailure()) {
      fatal("Failed to initialize AsgElectronEfficiencyTrigMCEffCorrectionTool");
    } else {
      std::cout << "initialised  AsgElectronEfficiencyTrigMCEffCorrectionTool" << std::endl;
    }

    // Electron trigger scale factor per electron
    m_trigDiElectronScaleFactors = 0;

    if (config.getStr("EventHandler.ScaleFactor.TrigDiElectronCorrectionFileName" + bcid) != "") {

      m_trigDiElectronScaleFactors = new AsgElectronEfficiencyCorrectionTool("AsgDiElectronEfficiencyTrigCorrectionTool");
      std::string file_trigDiSF   = PathResolverFindCalibFile(config.getStr("EventHandler.ScaleFactor.TrigDiElectronCorrectionFileName" + bcid).Data());
      correctionFilesTrigSF.clear();
      correctionFilesTrigSF.push_back(file_trigDiSF);
      CHECK(m_trigDiElectronScaleFactors->setProperty("CorrectionFileNameList", correctionFilesTrigSF));
      CHECK(m_trigDiElectronScaleFactors->setProperty("ForceDataType", HG::isAFII() ? 3 : 1));
      CHECK(m_trigDiElectronScaleFactors->setProperty("CorrelationModel", config.getStr("ElectronHandler.Efficiency.CorrelationModel").Data()));


      if (m_trigDiElectronScaleFactors->initialize().isFailure()) {
        fatal("Failed to initialize AsgDiElectronEfficiencyTrigCorrectionTool");
      } else {
        std::cout << "initialised  AsgDiElectronEfficiencyTrigCorrectionTool" << std::endl;
      }
    }


    m_trigDiElectronMCEfficiency = 0;

    if (config.getStr("EventHandler.ScaleFactor.TrigDiElectronMCEffCorrectionFileName" + bcid) != "") {
      m_trigDiElectronMCEfficiency = new AsgElectronEfficiencyCorrectionTool("AsgDiElectronEfficiencyTrigMCEffCorrectionTool");
      std::string file_trigDiMCEff   = PathResolverFindCalibFile(config.getStr("EventHandler.ScaleFactor.TrigDiElectronMCEffCorrectionFileName" + bcid).Data());
      correctionFilesTrigMCEff.clear();
      correctionFilesTrigMCEff.push_back(file_trigDiMCEff);
      CHECK(m_trigDiElectronMCEfficiency->setProperty("CorrectionFileNameList", correctionFilesTrigMCEff));
      CHECK(m_trigDiElectronMCEfficiency->setProperty("ForceDataType", HG::isAFII() ? 3 : 1));
      CHECK(m_trigDiElectronMCEfficiency->setProperty("CorrelationModel", config.getStr("ElectronHandler.Efficiency.CorrelationModel").Data()));


      if (m_trigDiElectronMCEfficiency->initialize().isFailure()) {
        fatal("Failed to initialize AsgDiElectronEfficiencyTrigMCEffCorrectionTool");
      } else {
        std::cout << "initialised  AsgDiElectronEfficiencyTrigMCEffCorrectionTool" << std::endl;
      }
    }

    // EGam global trigger SFs

    Info("EventHandler", "Configuring the photon CP tools");
    // /// For property 'PhotonEfficiencyTools':
    ToolHandleArray<IAsgPhotonEfficiencyCorrectionTool> photonEffTools;
    // /// For property 'PhotonScaleFactorTools':
    ToolHandleArray<IAsgPhotonEfficiencyCorrectionTool> photonSFTools;
    /// For property 'ListOfLegsPerTool':
    std::map<std::string, std::string> legsPerTool;

    enum { cLEGS, cKEY };
    std::vector<std::array<std::string, 2>> toolConfigs = {
      /// {<list of trigger legs>, <key in map file>}
      {"g35_medium_L1EM20VH", "HLT_g35_medium_L1EM20VH"},
      {"g25_medium_L1EM20VH", "HLT_g25_medium_L1EM20VH"}
    };

    const char *mapPath = "PhotonEfficiencyCorrection/2015_2017/"
                          "rel21.2/Winter2018_Prerec_v1/map1.txt";

    /// one instance per trigger leg x working point
    for (auto &cfg : toolConfigs) {
      for (int j = 0; j < 2; ++j) { /// two instances: 0 -> MC efficiencies, 1 -> SFs
        std::string name = "AsgPhotonEfficiencyCorrectionTool/"
                           + ((j ? "PhTrigEff_" : "PhTrigSF_")
                              + std::to_string(m_trigFactory.size() / 2));
        auto t = m_trigFactory.emplace(m_trigFactory.end(), name);
        CHECK(t->setProperty("MapFilePath", mapPath));
        CHECK(t->setProperty("TriggerKey", std::string(j ? "" : "Eff_") + cfg[cKEY]));
        CHECK(t->setProperty("IsoKey", "Loose"));
        CHECK(t->setProperty("ForceDataType", 1)); // FIXME when AFII SFs available

        if (t->initialize() != StatusCode::SUCCESS) {
          Error("EventHandler", "Unable to initialize the photon CP tool <%s>!",
                t->name().c_str());
          return EL::StatusCode::FAILURE;
        }

        auto &handles = (j ? photonSFTools : photonEffTools);
        handles.push_back(t->getHandle());
        /// Safer to retrieve the name from the final ToolHandle, it might be
        /// prefixed (by the parent tool name) when the handle is copied
        name = handles[handles.size() - 1].name();
        legsPerTool[name] = cfg[cLEGS];
      }
    }

    /* ********************************************************************** */

    Info("EventHandler", "Configuring the global trigger SF tool");
    CHECK(m_trigGlobalTool.setProperty("PhotonEfficiencyTools", photonEffTools));
    CHECK(m_trigGlobalTool.setProperty("PhotonScaleFactorTools", photonSFTools));
    CHECK(m_trigGlobalTool.setProperty("TriggerCombination2017", "g35_medium_g25_medium_L12EM20VH"));
    CHECK(m_trigGlobalTool.setProperty("ListOfLegsPerTool", legsPerTool));
    CHECK(m_trigGlobalTool.setProperty("OutputLevel", MSG::ERROR));

    if (m_trigGlobalTool.initialize() != StatusCode::SUCCESS) {
      Error("EventHandler", "Unable to initialize the TrigGlob tool!");
      return EL::StatusCode::FAILURE;
    }


    return EL::StatusCode::SUCCESS;
  }

  //______________________________________________________________________________
  xAOD::HiggsWeights EventHandler::higgsWeights()
  {
    static SG::AuxElement::ConstAccessor<int> STXSbin("HTXS_Stage1_Category_pTjet30");
    static SG::AuxElement::ConstAccessor<int> Njets("HTXS_Njets_pTjet30");
    static SG::AuxElement::ConstAccessor<float> pTH("HTXS_Higgs_pt");

    const xAOD::EventInfo *eventInfo = nullptr;

    if (m_event->retrieve(eventInfo, "EventInfo").isFailure()) {
      HG::fatal("Cannot access EventInfo");
    }

    if (!Njets.isAvailable(*eventInfo) || !STXSbin.isAvailable(*eventInfo))
    { return m_higgsWeightTool->getHiggsWeights(); }

    float pT_H = pTH.isAvailable(*eventInfo) ? pTH(*eventInfo) : var::pT_h1.truth();
    return m_higgsWeightTool->getHiggsWeights(Njets(*eventInfo), pT_H, STXSbin(*eventInfo));
  }

  //______________________________________________________________________________
  double EventHandler::mcWeight()
  {
    const xAOD::EventInfo *eventInfo = 0;

    if (m_event->retrieve(eventInfo, "EventInfo").isFailure()) {
      HG::fatal("Cannot access EventInfo");
    }

    if (HG::isMC()) {
      double mcweight = 1.0;

      // First check if we expect multiple weights
      if (m_mcWeightIndex >= 0) {
        if (m_forceWeights) {
          xAOD::HiggsWeights weights = higgsWeights();
          return weights.nominal;
        }

        const std::vector<float> weights = eventInfo->mcEventWeights();
        int size = weights.size();

        if (size > m_mcWeightIndex) {
          return weights[m_mcWeightIndex];
        }

        fatal(TString::Format("Index %d is bigger than mcEventWeights size (%d)?", m_mcWeightIndex, size));
      }

      // Check for Sherpa 2.2.4 multi-weight bug and fix
      if (m_fixSherpa224) {
        const std::vector<float> weights = eventInfo->mcEventWeights();

        if (weights.size() < 8)
        { fatal("Fixing a Sherpa 2.2.4 sample specified in config, which doesn't have weight index 7?"); }

        if (weights[0]*weights[7] < 0) {
          return -1.*weights[0];
        } else {
          return weights[0];
        }
      }

      // Next use TruthEvents if multiple weights found (bug in older files),
      // otherwise normal mcEventWeights
      const xAOD::TruthEventContainer *truthEvents = 0;

      if (m_event->contains<xAOD::TruthEventContainer>("TruthEvents") &&
          m_event->retrieve(truthEvents, "TruthEvents").isFailure())
      { HG::fatal("Can't access TruthEvents"); }

      const std::vector<float> weights = eventInfo->mcEventWeights();
      static SG::AuxElement::ConstAccessor<std::vector<float> > acc_weights("weights");

      if (truthEvents != nullptr                      &&
          acc_weights.isAvailable(*truthEvents->at(0)) &&
          truthEvents->at(0)->weights().size() > 1)   {
        mcweight = truthEvents->at(0)->weights()[0];
      } else {
        if (weights.size() > 0) { mcweight = weights[0]; }
      }

      return mcweight;
    }

    return 1.0;
  }

  //______________________________________________________________________________
  double EventHandler::vertexWeight()
  {
    if (!HG::isMC())
    { fatal("You shouldn't be calling vertexWeight on data, MC only."); }

    if (var::vertexWeight.exists())
    { return var::vertexWeight(); }

    var::vertexWeight.setValue(1.0);

    if (HG::isData())
    { return var::vertexWeight(); }

    double vtxWeight = 0.0;

    switch (m_vtxRW->getWeight(vtxWeight)) {
      case CP::CorrectionCode::Error:
        Error("vertexWeight()", "m_vtxWeight->getWeight returned error, returning 1.0");
        var::vertexWeight.setValue(1.0);
        break;

      case CP::CorrectionCode::OutOfValidityRange:
        var::vertexWeight.setValue(1.0);
        Error("vertexWeight()", "m_vtxWeight->getWeight out of validity range, returning 1.0");
        break;

      case CP::CorrectionCode::Ok:
        var::vertexWeight.setValue(vtxWeight);
    }

    return var::vertexWeight();
  }

  //______________________________________________________________________________
  double EventHandler::pileupWeight()
  {
    const xAOD::EventInfo *eventInfo = 0;

    if (m_event->retrieve(eventInfo, "EventInfo").isFailure()) {
      HG::fatal("Cannot access EventInfo");
    }

    var::pileupWeight.setValue(1.0);

    if (HG::isMC()) {
      var::pileupWeight.setValue(m_pileupRW->getCombinedWeight(*eventInfo));

      for (size_t i = 0; i < m_prwExtra.size(); ++i) {
        TString name = "pileupWeight" + m_prwExtra[i];
        storeVar<float>(name.Data(), m_extraPileupRW[i]->getCombinedWeight(*eventInfo));
      }
    }

    return var::pileupWeight();
  }

  //______________________________________________________________________________
  unsigned long long EventHandler::pileupHash()
  {
    const xAOD::EventInfo *eventInfo = 0;

    if (m_event->retrieve(eventInfo, "EventInfo").isFailure()) {
      HG::fatal("Cannot access EventInfo");
    }

    if (HG::isMC())
    { return m_pileupRW->getPRWHash(*eventInfo); }
    else
    { return 1.; }
  }

  //______________________________________________________________________________
  Trig::IBunchCrossingTool *EventHandler::bunchTool()
  { return m_bunchTool; }

  //______________________________________________________________________________
  int EventHandler::bunchDistanceFromFront()
  {
    const xAOD::EventInfo *eventInfo = nullptr;

    if (m_event->retrieve(eventInfo, "EventInfo").isFailure())
    { HG::fatal("EventHandler::bunchDistanceFromFront() : Cannot access EventInfo"); }

    static SG::AuxElement::ConstAccessor<int> acc_bunch("bunchDistanceFromFront");

    if (acc_bunch.isAvailable(*eventInfo))
    { return acc_bunch(*eventInfo); }

    if (m_bunchTool == nullptr)
    { return -99; }

    static SG::AuxElement::Decorator<int> dec_bunch("bunchDistanceFromFront");
    dec_bunch(*eventInfo) = m_bunchTool->distanceFromFront(eventInfo->bcid());
    return dec_bunch(*eventInfo);
  }

  //______________________________________________________________________________
  int EventHandler::bunchGapBeforeTrain()
  {
    const xAOD::EventInfo *eventInfo = nullptr;

    if (m_event->retrieve(eventInfo, "EventInfo").isFailure())
    { HG::fatal("EventHandler::bunchGapBeforeTrain() : Cannot access EventInfo"); }

    static SG::AuxElement::ConstAccessor<int> acc_bunch("bunchGapBeforeTrain");

    if (acc_bunch.isAvailable(*eventInfo))
    { return acc_bunch(*eventInfo); }

    if (m_bunchTool == nullptr)
    { return -99; }

    static SG::AuxElement::Decorator<int> dec_bunch("bunchGapBeforeTrain");
    dec_bunch(*eventInfo) = m_bunchTool->gapBeforeTrain(eventInfo->bcid());
    return dec_bunch(*eventInfo);
  }

  //______________________________________________________________________________
  float EventHandler::centralEventShapeDensity()
  {
    const xAOD::EventInfo *eventInfo = nullptr;

    if (m_event->retrieve(eventInfo, "EventInfo").isFailure())
    { HG::fatal("EventHandler::centralEventShapeDensity() : Cannot access EventInfo"); }

    static SG::AuxElement::ConstAccessor<float> acc_density("centralEventShapeDensity");

    if (acc_density.isAvailable(*eventInfo))
    { return acc_density(*eventInfo); }

    const xAOD::EventShape *eventShape = nullptr;

    if (m_event->retrieve(eventShape, "TopoClusterIsoCentralEventShape").isFailure())
    { HG::fatal("EventHandler::centralEventShapeDensity() : Cannot access EventInfo"); }

    double cED = 0.0;
    eventShape->getDensity(xAOD::EventShape::Density, cED);

    static SG::AuxElement::Decorator<float> dec_density("centralEventShapeDensity");
    dec_density(*eventInfo) = cED;

    return cED;
  }

  //______________________________________________________________________________
  float EventHandler::forwardEventShapeDensity()
  {
    const xAOD::EventInfo *eventInfo = nullptr;

    if (m_event->retrieve(eventInfo, "EventInfo").isFailure())
    { HG::fatal("EventHandler::forwardEventShapeDensity() : Cannot access EventInfo"); }

    static SG::AuxElement::ConstAccessor<float> acc_density("forwardEventShapeDensity");

    if (acc_density.isAvailable(*eventInfo))
    { return acc_density(*eventInfo); }

    const xAOD::EventShape *eventShape = nullptr;

    if (m_event->retrieve(eventShape, "TopoClusterIsoForwardEventShape").isFailure())
    { HG::fatal("EventHandler::forwardEventShapeDensity() : Cannot access EventInfo"); }

    double fED = 0.0;
    eventShape->getDensity(xAOD::EventShape::Density, fED);

    static SG::AuxElement::Decorator<float> dec_density("forwardEventShapeDensity");
    dec_density(*eventInfo) = fED;

    return fED;
  }

  //______________________________________________________________________________
  double EventHandler::triggerPrescaleWeight(TString triggerList, bool muDependent)
  {
    const xAOD::EventInfo *eventInfo = 0;

    if (m_event->retrieve(eventInfo, "EventInfo").isFailure()) {
      HG::fatal("Cannot access EventInfo");
    }

    if (HG::isMC())
    { return 1.0; }
    else
    { return m_pileupRWdata->getDataWeight(*eventInfo, triggerList, muDependent); }
  }

  //______________________________________________________________________________
  double EventHandler::triggerPrescale(TString trigger)
  {
    const xAOD::EventInfo *eventInfo = 0;

    if (m_event->retrieve(eventInfo, "EventInfo").isFailure()) {
      HG::fatal("Cannot access EventInfo");
    }

    if (HG::isMC())
    { return 1.0; }
    else {
      auto cg = m_trigDecTool->getChainGroup(std::string(trigger.Data()));
      return cg->getPrescale();
    }
  }

  //______________________________________________________________________________
  double EventHandler::integratedLumi()
  {
    return m_pileupRW->GetIntegratedLumi();
  }

  //______________________________________________________________________________
  bool EventHandler::pass()
  {
    if (var::isPassedBasic.exists())
    { return var::isPassedBasic(); }

    if (m_checkDuplic && isDuplicate()) { return false; }

    if (m_checkTrig   && !passTriggers()) { return false; }

    return passDQ();
  }

  bool EventHandler::passDQ()
  {
    const xAOD::EventInfo *eventInfo = 0;

    if (m_event->retrieve(eventInfo, "EventInfo").isFailure())
    { HG::fatal("Cannot access EventInfo"); }

    if (m_checkGRL    && !passGRL(eventInfo)) { return false; }

    if (m_checkLAr    && !passLAr(eventInfo)) { return false; }

    if (m_checkTile   && !passTile(eventInfo)) { return false; }

    if (m_checkCore   && !passCore(eventInfo)) { return false; }

    if (m_checkBkg    && !passBackground(eventInfo)) { return false; }

    if (m_checkSCT    && !passSCT(eventInfo)) { return false; }

    if (m_checkVertex && !passVertex(eventInfo)) { return false; }

    return true;
  }

  //______________________________________________________________________________
  bool EventHandler::isDuplicate()
  {
    const xAOD::EventInfo *eventInfo = 0;

    if (m_event->retrieve(eventInfo, "EventInfo").isFailure())
    { HG::fatal("Cannot access EventInfo"); }

    static SG::AuxElement::Decorator<char> acc_isDuplicate("isDuplicate");

    if (acc_isDuplicate.isAvailable(*eventInfo))
    { return acc_isDuplicate(*eventInfo); }

    unsigned int runNumber = eventInfo->runNumber();

    if (HG::isMC())  { runNumber = eventInfo->mcChannelNumber(); }

    acc_isDuplicate(*eventInfo) = not m_eventNumberSet[runNumber].insert(eventInfo->eventNumber()).second;
    return acc_isDuplicate(*eventInfo);
  }

  //______________________________________________________________________________
  bool EventHandler::isDalitz()
  {
    if (HG::isMC()) {
      if (var::isDalitzEvent.exists())
      { return var::isDalitzEvent(); }

      const xAOD::TruthParticleContainer *truthParticles = nullptr;

      if (m_event->retrieve(truthParticles, m_truthPtclName.Data()).isFailure())
      { HG::fatal("Can't access TruthParticleContainer"); }

      return HG::isDalitz(truthParticles);
    }

    // By default (for data) return false
    return false;
  }

  //______________________________________________________________________________
  double EventHandler::triggerScaleFactor(xAOD::ElectronContainer *Electrons, xAOD::MuonContainer *Muons)
  {
    if (HG::isData()) { return 1.; }

    const xAOD::EventInfo *eventInfo = 0;

    if (m_event->retrieve(eventInfo, "EventInfo").isFailure()) {
      HG::fatal("Cannot access EventInfo");
    }

    if (Electrons  && Electrons->size() > 0) {
      double _effElectronTrigSF = 0.0;
      getElectronTriggerScaleFactorTwoLeptons(*Electrons, _effElectronTrigSF);
      trigSF(*eventInfo) = _effElectronTrigSF;
    } else if (Muons && Muons->size() > 0) {
      double _effMuonTrigSF = 0.0;

      //Dont calculate reweighting for muons is the run number is 0

      if (runNumber() == 0) {
        trigSF(*eventInfo) = 1.0;
      } else {
        getMuonTriggerScaleFactor(*Muons, _effMuonTrigSF);
        trigSF(*eventInfo) = _effMuonTrigSF;
      }
    } else {
      HG::fatal("Unrecognised particle type for trigger scale factor, returning 1.0");
      trigSF(*eventInfo) = 1.0;
    }

    return trigSF(*eventInfo);

  }

  //______________________________________________________________________________
  bool EventHandler::passTriggers()
  {
    // Require at least one trigger to be passed
    for (auto trig : m_requiredTriggers) {
      if (passTrigger(trig.Data())) { return true; }
    }

    return false;
  }

  //______________________________________________________________________________
  StrV EventHandler::getPassedTriggers()
  {
    StrV passedTrigs;

    for (auto trig : m_requiredTriggers) {
      if (passTrigger(trig))
      { passedTrigs.push_back(trig); }
    }

    return passedTrigs;
  }

  //______________________________________________________________________________
  StrV EventHandler::getRequiredTriggers()
  {
    return m_requiredTriggers;
  }

  //______________________________________________________________________________
  bool EventHandler::passTriggerMatch_SinglePhoton(const TString &trig,
                                                   const xAOD::Photon &ph)
  {
    //    std::string str = trig.Data();
    //    return m_trigElectronMatchTool->matchHLT(&ph, str);
    return m_trigMatching->match(ph, trig.Data(), m_trigMatchPhotondR);
  }

  //______________________________________________________________________________
  bool EventHandler::passTriggerMatch_DiPhoton(const TString &trig,
                                               const xAOD::Photon &photon1,
                                               const xAOD::Photon &photon2)
  {
    //    if (passTriggerMatch_SinglePhoton(trig, photon1) &&
    //        passTriggerMatch_SinglePhoton(trig, photon2))
    //      return true;
    // Return false if both photons weren't matched to a trigger object
    //    return false;
    std::vector<const xAOD::IParticle *> myParticles;
    myParticles.clear();
    myParticles.push_back(&photon1);
    myParticles.push_back(&photon2);
    return  m_trigMatching->match(myParticles, trig.Data(), m_trigMatchPhotondR);
  }

  //______________________________________________________________________________
  bool EventHandler::passTriggerMatch_DiMuon(const TString &trig,
                                             const xAOD::Muon &muon1,
                                             const xAOD::Muon &muon2)
  {
    //    std::pair<bool, bool> isMatchedMuon1, isMatchedMuon2;
    //    isMatchedMuon1 = std::make_pair(false, false);
    //    isMatchedMuon2 = std::make_pair(false, false);
    //    m_trigMuonMatchTool->matchDimuon(&muon1, &muon2, trig.Data(), isMatchedMuon1, isMatchedMuon2);
    if (m_trigThresholds[trig].size() != 2)
    { HG::fatal("One of the di-muon triggers does not have thresholds set."); }

    double t1 = m_trigThresholds[trig].at(0);
    double t2 = m_trigThresholds[trig].at(1);

    bool passthresh = false;

    if (muon1.pt() / HG::GeV > t1 && muon2.pt() / HG::GeV > t2) { passthresh = true; }

    if (muon2.pt() / HG::GeV > t1 && muon1.pt() / HG::GeV > t2) { passthresh = true; }

    if (!passthresh) { return false; }

    //    return isMatchedMuon1.first && isMatchedMuon2.first;
    std::vector<const xAOD::IParticle *> myParticles;
    myParticles.clear();
    myParticles.push_back(&muon1);
    myParticles.push_back(&muon2);
    return m_trigMatching->match(myParticles, trig.Data(), m_trigMatchMuondR);
  }

  //______________________________________________________________________________
  bool EventHandler::passTriggerMatch_SingleMuon(const TString &trig,
                                                 const xAOD::Muon &muon)
  {
    //    return m_trigMuonMatchTool->match(&muon, trig.Data());
    if (m_trigThresholds[trig].size() != 1)
    { HG::fatal("One of the single-muon triggers does not have thresholds set."); }

    if (muon.pt() / HG::GeV < m_trigThresholds[trig].at(0)) { return false; }

    return m_trigMatching->match(muon, trig.Data(), m_trigMatchMuondR);
  }

  //______________________________________________________________________________
  bool EventHandler::passTriggerMatch_SingleElectron(const TString &trig,
                                                     const xAOD::Electron &el)
  {
    //    std::string str = trig.Data();
    //    return m_trigElectronMatchTool->matchHLT(&el, str);

    if (m_trigThresholds[trig].size() != 1)
    { HG::fatal("One of the single-electron triggers does not have thresholds set."); }

    if (el.pt() / HG::GeV < m_trigThresholds[trig].at(0)) { return false; }

    return m_trigMatching->match(el, trig.Data(), m_trigMatchElectrondR);
  }

  //______________________________________________________________________________
  bool EventHandler::passTriggerMatch_DiElectron(const TString &trig,
                                                 const xAOD::Electron &el1,
                                                 const xAOD::Electron &el2)
  {
    if (m_trigThresholds[trig].size() != 2)
    { HG::fatal("One of the di-electron triggers does not have thresholds set."); }

    double t1 = m_trigThresholds[trig].at(0);
    double t2 = m_trigThresholds[trig].at(1);

    bool passthresh = false;

    if (el1.pt() / HG::GeV > t1 && el2.pt() / HG::GeV > t2) { passthresh = true; }

    if (el2.pt() / HG::GeV > t1 && el1.pt() / HG::GeV > t2) { passthresh = true; }

    if (!passthresh) { return false; }

    std::vector<const xAOD::IParticle *> myParticles;
    myParticles.clear();
    myParticles.push_back(&el1);
    myParticles.push_back(&el2);
    return  m_trigMatching->match(myParticles, trig.Data(), m_trigMatchElectrondR);
  }

  //______________________________________________________________________________
  double EventHandler::selectedVertexSumPt2()
  {
    if (var::selectedVertexSumPt2.exists())
    { return var::selectedVertexSumPt2(); }

    if (!m_store->contains<ConstDataVector<xAOD::VertexContainer> >("HGamVertices"))
    { return -999; }

    ConstDataVector<xAOD::VertexContainer> *hgamvertices = nullptr;

    if (m_store->retrieve(hgamvertices, "HGamVertices").isFailure())
    { return -999; }

    if (hgamvertices && hgamvertices->size() > 0) {
      var::selectedVertexSumPt2.setValue(xAOD::PVHelpers::getVertexSumPt((*hgamvertices)[0], 2));
      return var::selectedVertexSumPt2();
    }

    return -999;
  }

  //______________________________________________________________________________
  double EventHandler::selectedVertexZ()
  {
    if (var::selectedVertexZ.exists())
    { return var::selectedVertexZ(); }

    if (!m_store->contains<ConstDataVector<xAOD::VertexContainer> >("HGamVertices"))
    { return -999; }

    ConstDataVector<xAOD::VertexContainer> *hgamvertices = nullptr;

    if (m_store->retrieve(hgamvertices, "HGamVertices").isFailure())
    { return -999; }

    if (hgamvertices && hgamvertices->size() > 0) {
      var::selectedVertexZ.setValue((*hgamvertices)[0]->z());
      return var::selectedVertexZ();
    }

    return -999;
  }

  //______________________________________________________________________________
  double EventHandler::selectedVertexPhi()
  {
    if (var::selectedVertexPhi.exists())
    { return var::selectedVertexPhi(); }

    if (!m_store->contains<ConstDataVector<xAOD::VertexContainer> >("HGamVertices"))
    { return -999; }

    ConstDataVector<xAOD::VertexContainer> *hgamvertices = nullptr;

    if (m_store->retrieve(hgamvertices, "HGamVertices").isFailure())
    { return -999; }

    if (hgamvertices && hgamvertices->size() > 0) {
      var::selectedVertexPhi.setValue(xAOD::PVHelpers::getVertexMomentum((*hgamvertices)[0]).Phi());
      return var::selectedVertexPhi();
    }

    return -999;
  }

  //______________________________________________________________________________
  double EventHandler::hardestVertexSumPt2()
  {
    if (var::hardestVertexSumPt2.exists())
    { return var::hardestVertexSumPt2(); }

    const xAOD::VertexContainer *vertices = nullptr;

    if (m_event->retrieve(vertices, "PrimaryVertices").isFailure())
    { HG::fatal("Cannot access PrimaryVertices"); }

    const xAOD::Vertex *hardest = xAOD::PVHelpers::getHardestVertex(vertices);

    if (hardest) {
      var::hardestVertexSumPt2.setValue(xAOD::PVHelpers::getVertexSumPt(hardest, 2));
      return var::hardestVertexSumPt2();
    }

    return -999;
  }

  //______________________________________________________________________________
  double EventHandler::hardestVertexZ()
  {
    if (var::hardestVertexZ.exists())
    { return var::hardestVertexZ(); }

    const xAOD::VertexContainer *vertices = nullptr;

    if (m_event->retrieve(vertices, "PrimaryVertices").isFailure())
    { HG::fatal("Cannot access PrimaryVertices"); }

    const xAOD::Vertex *hardest = xAOD::PVHelpers::getHardestVertex(vertices);

    if (hardest) {
      var::hardestVertexZ.setValue(hardest->z());
      return var::hardestVertexZ();
    }

    return -999;
  }

  //______________________________________________________________________________
  double EventHandler::hardestVertexPhi()
  {
    if (var::hardestVertexPhi.exists())
    { return var::hardestVertexPhi(); }

    const xAOD::VertexContainer *vertices = nullptr;

    if (m_event->retrieve(vertices, "PrimaryVertices").isFailure())
    { HG::fatal("Cannot access PrimaryVertices"); }

    const xAOD::Vertex *hardest = xAOD::PVHelpers::getHardestVertex(vertices);

    if (hardest) {
      var::hardestVertexPhi.setValue(xAOD::PVHelpers::getVertexMomentum(hardest).Phi());
      return var::hardestVertexPhi();
    }

    return -999;
  }

  //______________________________________________________________________________
  double EventHandler::pileupVertexZ()
  {
    if (var::pileupVertexZ.exists())
    { return var::pileupVertexZ(); }

    // To avoid duplicate code, all decorations are added in the sumPt2 function
    pileupVertexSumPt2();

    return var::pileupVertexZ();
  }

  //______________________________________________________________________________
  double EventHandler::pileupVertexPhi()
  {
    if (var::pileupVertexPhi.exists())
    { return var::pileupVertexPhi(); }

    // To avoid duplicate code, all decorations are added in the sumPt2 function
    pileupVertexSumPt2();

    return var::pileupVertexPhi();
  }

  //______________________________________________________________________________
  double EventHandler::pileupVertexSumPt2()
  {
    if (var::pileupVertexSumPt2.exists())
    { return var::pileupVertexSumPt2(); }

    // Default values
    var::pileupVertexSumPt2.setValue(-99);
    var::pileupVertexZ.setValue(-999);
    var::pileupVertexPhi.setValue(-99);

    const xAOD::VertexContainer *vertices = nullptr;

    if (m_event->retrieve(vertices, "PrimaryVertices").isFailure())
    { HG::fatal("Cannot access PrimaryVertices"); }

    // Check for leading, sub-leading vertex in collection
    double hards = 0.0, subhards = 0.0;
    const xAOD::Vertex *hardv = nullptr, *subhardv = nullptr;

    for (auto vertex : *vertices) {
      double sumPt2 = xAOD::PVHelpers::getVertexSumPt(vertex, 2);

      if (sumPt2 > hards) {
        subhards = hards;
        subhardv = hardv;

        hards = sumPt2;
        hardv = vertex;
      } else if (sumPt2 > subhards) {
        subhards = sumPt2;
        subhardv = vertex;
      }
    }

    // If there aren't two vertices, return default -999
    if (!hardv || !subhardv)
    { return -99; }

    // Get the diphoton pointing vertex
    if (!m_store->contains<ConstDataVector<xAOD::VertexContainer> >("HGamVertices"))
    { return -99; }

    ConstDataVector<xAOD::VertexContainer> *hgamvertices = nullptr;

    if (m_store->retrieve(hgamvertices, "HGamVertices").isFailure())
    { return -99; }

    // If there is a pointing vertex
    if (hgamvertices && hgamvertices->size() > 0) {
      const xAOD::Vertex *diphotonv = (*hgamvertices)[0];

      // If the pointing vertex is the hardest vertex
      if (fabs(diphotonv->z() - hardv->z()) < 1e-6) {
        var::pileupVertexSumPt2.setValue(subhards);
        var::pileupVertexZ.setValue(subhardv->z());
        var::pileupVertexPhi.setValue(xAOD::PVHelpers::getVertexMomentum(subhardv).Phi());
        return subhards;
      }

      // Otherwise the pointing vertex isn't the hardest vertex, so the pileup is
      var::pileupVertexSumPt2.setValue(hards);
      var::pileupVertexZ.setValue(hardv->z());
      var::pileupVertexPhi.setValue(xAOD::PVHelpers::getVertexMomentum(hardv).Phi());
      return hards;
    }

    // No pointing vertex, check for sub-leading vertex
    if (subhardv) {
      var::pileupVertexSumPt2.setValue(subhards);
      var::pileupVertexZ.setValue(subhardv->z());
      var::pileupVertexPhi.setValue(xAOD::PVHelpers::getVertexMomentum(subhardv).Phi());
      return subhards;
    }

    return -99;
  }

  //______________________________________________________________________________
  double EventHandler::eventShapeDensity()
  {
    const xAOD::EventInfo *eventInfo = nullptr;

    if (m_event->retrieve(eventInfo, "EventInfo").isFailure())
    { HG::fatal("EventHandler::eventShapeDensity() : Cannot access EventInfo"); }

    static SG::AuxElement::ConstAccessor<float> acc_density("eventShapeDensity");

    if (acc_density.isAvailable(*eventInfo))
    { return acc_density(*eventInfo); }

    const xAOD::EventShape *eventShape = nullptr;

    if (m_event->retrieve(eventShape, "Kt4EMTopoOriginEventShape").isFailure())
    { HG::fatal("EventHandler::eventShapeDensity() : Cannot access EventShape"); }

    double ED = 0.0;
    eventShape->getDensity(xAOD::EventShape::Density, ED);

    static SG::AuxElement::Decorator<float> dec_density("eventShapeDensity");
    dec_density(*eventInfo) = ED;

    return ED;
  }

  //______________________________________________________________________________
  double EventHandler::mu()
  {
    const xAOD::EventInfo *eventInfo = nullptr;

    if (m_event->retrieve(eventInfo, "EventInfo").isFailure()) {
      HG::fatal("Cannot access EventInfo in mu()");
    }

    double mu = eventInfo->averageInteractionsPerCrossing();

    if (HG::isData()) {
      mu = m_pileupRWdata->getCorrectedAverageInteractionsPerCrossing(*eventInfo) * m_prwSF;
    }

    var::mu.setValue(mu);

    return mu;
  }

  //______________________________________________________________________________
  void EventHandler::prwApply()
  {
    const xAOD::EventInfo *eventInfo = nullptr;

    if (m_event->retrieve(eventInfo, "EventInfo").isFailure()) {
      HG::fatal("Cannot access EventInfo in runNumber()");
    }

    m_pileupRW->apply(*eventInfo);
  }

  //______________________________________________________________________________
  int EventHandler::runNumber()
  {
    const xAOD::EventInfo *eventInfo = nullptr;

    if (m_event->retrieve(eventInfo, "EventInfo").isFailure())
    { HG::fatal("Cannot access EventInfo in runNumber()"); }

    int RunNumber = eventInfo->runNumber();

    if (HG::isMC()) {
      if (RandomRunNumber.isAvailable(*eventInfo)) {
        RunNumber = RandomRunNumber(*eventInfo);
      } else {
        RunNumber = m_pileupRW->getRandomRunNumber(*eventInfo);
        RandomRunNumber(*eventInfo) = RunNumber;
      }
    }

    return RunNumber;
  }

  //______________________________________________________________________________
  int EventHandler::numberOfPrimaryVertices()
  {
    if (var::numberOfPrimaryVertices.exists())
    { return var::numberOfPrimaryVertices(); }

    const xAOD::VertexContainer *vertices = nullptr;

    if (m_event->retrieve(vertices, "PrimaryVertices").isFailure())
    { HG::fatal("Cannot access PrimaryVertices"); }

    int NPV = 0;

    for (auto vertex : *vertices) {
      if (vertex->vertexType() == xAOD::VxType::PriVtx ||
          vertex->vertexType() == xAOD::VxType::PileUp)
      { NPV++; }
    }

    var::numberOfPrimaryVertices.setValue(NPV);

    return NPV;
  }

  //______________________________________________________________________________
  bool EventHandler::passGRL(const xAOD::EventInfo *eventInfo)
  {
    if (HG::isData() &&
        !m_grl->passRunLB(*eventInfo))
    { return false; }

    return true;
  }

  //______________________________________________________________________________
  bool EventHandler::passTile(const xAOD::EventInfo *eventInfo)
  {
    if (HG::isData() &&
        eventInfo->errorState(xAOD::EventInfo::Tile) == xAOD::EventInfo::Error)
    { return false; }

    return true;
  }

  //______________________________________________________________________________
  bool EventHandler::passLAr(const xAOD::EventInfo *eventInfo)
  {
    if (HG::isData() &&
        eventInfo->errorState(xAOD::EventInfo::LAr) == xAOD::EventInfo::Error)
    { return false; }

    return true;
  }

  //______________________________________________________________________________
  bool EventHandler::passBackground(const xAOD::EventInfo *eventInfo)
  {
    if (HG::isData() &&
        eventInfo->isEventFlagBitSet(xAOD::EventInfo::Background, 20))
    { return false; }

    return true;
  }

  //______________________________________________________________________________
  bool EventHandler::passCore(const xAOD::EventInfo *eventInfo)
  {
    if (HG::isData() &&
        eventInfo->isEventFlagBitSet(xAOD::EventInfo::Core, 18))
    { return false; }

    return true;
  }

  //______________________________________________________________________________
  bool EventHandler::passVertex(const xAOD::EventInfo * /*eventInfo*/)
  {
    // Retrieve PV collection from TEvent
    const xAOD::VertexContainer *vertices = nullptr;

    if (m_event->retrieve(vertices, "PrimaryVertices").isFailure()) {
      HG::fatal("Couldn't retrieve PrimaryVertices, exiting.");
      return false;
    }

    for (auto vertex : *vertices)
      if (vertex->vertexType() == xAOD::VxType::VertexType::PriVtx ||
          vertex->vertexType() == xAOD::VxType::VertexType::PileUp)
      { return true; }

    return false;
  }


  //______________________________________________________________________________
  bool EventHandler::passSCT(const xAOD::EventInfo *eventInfo)
  {
    if (HG::isData() &&
        eventInfo->errorState(xAOD::EventInfo::SCT) == xAOD::EventInfo::Error)
    { return false; }

    return true;
  }

  //______________________________________________________________________________
  bool EventHandler::eventClean_LooseBad()
  {
    const xAOD::EventInfo *eventInfo = nullptr;

    if (m_event->retrieve(eventInfo, "EventInfo").isFailure())
    { HG::fatal("Cannot access EventInfo"); }

    static SG::AuxElement::ConstAccessor<char> acc_LooseBad("DFCommonJets_eventClean_LooseBad");

    if (HG::isData())
    { return acc_LooseBad(*eventInfo); }

    // MC is always clean
    return true;
  }

  //______________________________________________________________________________
  bool EventHandler::isBadBatman()
  {
    const xAOD::EventInfo *eventInfo = nullptr;

    if (m_event->retrieve(eventInfo, "EventInfo").isFailure())
    { HG::fatal("Cannot access EventInfo"); }

    static SG::AuxElement::ConstAccessor<char> isBadBatman("DFCommonJets_isBadBatman");

    if (HG::isData())
    { return isBadBatman(*eventInfo); }

    // MC never has bad batman issues
    return false;
  }

  //______________________________________________________________________________
  void EventHandler::getElectronTriggerScaleFactorTwoLeptons(xAOD::ElectronContainer &electrons, double &trigSF)
  {
    // This routine covers single-e and symmetric di-e triggers, applied to exactly 2 leptons, only.
    // If you only use single-e, or only use di-e, you can use this. But you must require two leptons,
    // otherwise you need to write another function.

    if (electrons.size() != 2 || HG::isData()) {
      trigSF = 0.;
      return;
    }

    auto el1 = electrons.begin();
    auto el2 = electrons.begin() + 1;
    double cl_eta = 10.;
    const xAOD::CaloCluster *cluster;

    double _SF_singe_1 = 0.;
    double _mcEff_singe_1 = 0.;
    double _SF_singe_2 = 0.;
    double _mcEff_singe_2 = 0.;

    double _SF_die_1 = 0.;
    double _mcEff_die_1 = 0.;
    double _SF_die_2 = 0.;
    double _mcEff_die_2 = 0.;

    // first lepton
    if (m_trigElectronScaleFactors) {
      if (m_trigElectronScaleFactors->getEfficiencyScaleFactor(**el1, _SF_singe_1) == CP::CorrectionCode::Error)
      { fatal("ElectronEfficiencyTrigCorrection returned CP::CorrectionCode::Error"); }
    }

    if (m_trigElectronMCEfficiency) {
      if (m_trigElectronMCEfficiency->getEfficiencyScaleFactor(**el1, _mcEff_singe_1) == CP::CorrectionCode::Error)
      { fatal("ElectronEfficiencyTrigMCEffCorrection returned CP::CorrectionCode::Error"); }
    }

    if (m_trigDiElectronScaleFactors) {
      if (m_trigDiElectronScaleFactors->getEfficiencyScaleFactor(**el1, _SF_die_1) == CP::CorrectionCode::Error)
      { fatal("ElectronEfficiencyTrigCorrection returned CP::CorrectionCode::Error"); }
    }

    if (m_trigDiElectronMCEfficiency) {
      if (m_trigDiElectronMCEfficiency->getEfficiencyScaleFactor(**el1, _mcEff_die_1) == CP::CorrectionCode::Error)
      { fatal("ElectronEfficiencyTrigMCEffCorrection returned CP::CorrectionCode::Error"); }
    }

    cluster = (*el1)->caloCluster();

    if (cluster) { cl_eta = cluster->eta(); }

    if (std::abs(cl_eta) > 2.47) {
      _SF_singe_1 = 0;
      _mcEff_singe_1 = 0;
      _SF_die_1 = 0;
      _mcEff_die_1 = 0;
    }

    // second lepton
    cl_eta = 10.;

    if (m_trigElectronScaleFactors) {
      if (m_trigElectronScaleFactors->getEfficiencyScaleFactor(**el2, _SF_singe_2) == CP::CorrectionCode::Error)
      { fatal("ElectronEfficiencyTrigCorrection returned CP::CorrectionCode::Error"); }
    }

    if (m_trigElectronMCEfficiency) {
      if (m_trigElectronMCEfficiency->getEfficiencyScaleFactor(**el2, _mcEff_singe_2) == CP::CorrectionCode::Error)
      { fatal("ElectronEfficiencyTrigMCEffCorrection returned CP::CorrectionCode::Error"); }
    }

    if (m_trigDiElectronScaleFactors) {
      if (m_trigDiElectronScaleFactors->getEfficiencyScaleFactor(**el2, _SF_die_2) == CP::CorrectionCode::Error)
      { fatal("ElectronEfficiencyTrigCorrection returned CP::CorrectionCode::Error"); }
    }

    if (m_trigDiElectronMCEfficiency) {
      if (m_trigDiElectronMCEfficiency->getEfficiencyScaleFactor(**el2, _mcEff_die_2) == CP::CorrectionCode::Error)
      { fatal("ElectronEfficiencyTrigMCEffCorrection returned CP::CorrectionCode::Error"); }
    }

    cluster = (*el2)->caloCluster();

    if (cluster) { cl_eta = cluster->eta(); }

    if (std::abs(cl_eta) > 2.47) {
      _SF_singe_2 = 0;
      _mcEff_singe_2 = 0;
      _SF_die_2 = 0;
      _mcEff_die_2 = 0;
    }

    double _dataEff_singe_1 = _SF_singe_1 * _mcEff_singe_1;
    double _dataEff_singe_2 = _SF_singe_2 * _mcEff_singe_2;
    double _dataEff_die_1   = _SF_die_1   * _mcEff_die_1  ;
    double _dataEff_die_2   = _SF_die_2   * _mcEff_die_2  ;

    double _dataEff_singe = _dataEff_singe_1 + _dataEff_singe_2 - _dataEff_singe_1 * _dataEff_singe_2;
    double _mcEff_singe   = _mcEff_singe_1   + _mcEff_singe_2   - _mcEff_singe_1   * _mcEff_singe_2  ;

    if (!m_trigElectronScaleFactors || !m_trigElectronMCEfficiency) {
      _dataEff_singe = 0;
      _mcEff_singe = 0;
    }

    double _dataEff_die   = _dataEff_die_1 * _dataEff_die_2;
    double _mcEff_die     = _mcEff_die_1   * _mcEff_die_2  ;

    if (!m_trigDiElectronScaleFactors || !m_trigDiElectronMCEfficiency) {
      _dataEff_die = 0;
      _mcEff_die = 0;
    }

    double _dataEff_overlap = _dataEff_singe_1 * _dataEff_die_2 + _dataEff_singe_2 * _dataEff_die_1 - _dataEff_singe_1 * _dataEff_singe_2;
    double _mcEff_overlap   = _mcEff_singe_1   * _mcEff_die_2   + _mcEff_singe_2   * _mcEff_die_1   - _mcEff_singe_1   * _mcEff_singe_2  ;

    if (!m_trigDiElectronScaleFactors || !m_trigDiElectronMCEfficiency || !m_trigElectronScaleFactors || !m_trigElectronMCEfficiency) {
      _dataEff_overlap = 0;
      _mcEff_overlap = 0;
    }

    double numerator   = _dataEff_singe + _dataEff_die - _dataEff_overlap;
    double denominator = _mcEff_singe   + _mcEff_die   - _mcEff_overlap  ;

    if (denominator != 0)
    { trigSF = numerator / denominator; }

    return;

  }

  //______________________________________________________________________________
  void EventHandler::getMuonTriggerScaleFactor(xAOD::MuonContainer &muons, double &trigSF)
  {

    trigSF = 1.;

    if (HG::isData()) { return; }

    TString singleMuTrigger = "";
    TString diMuTrigger = "";

    for (auto trig : m_requiredTriggers) {

      if (m_trigRunNumbers.find(trig) != m_trigRunNumbers.end() && !m_trigRunNumbers.at(trig).count(runNumber())) { continue; }

      auto pos = trig.Index("mu");

      if (pos == -1) {
        // Not a muon trigger continue
        continue;
      }

      //Let work out if its a single  of  dimuon trigger
      auto  count = TPRegexp("_mu").Substitute(trig, "_mu", "g");

      if (count == 1) {
        if (singleMuTrigger != "") {
          singleMuTrigger += "_OR_";
        }

        singleMuTrigger += trig;
      } else if (count == 2) {
        if (diMuTrigger != "") {
          Warning("getMuonTriggerScaleFactor", "Check the configuration more than one di muon trigger detected");
          return;
        }

        diMuTrigger = trig;
      } else {
        Warning("getMuonTriggerScaleFactor", TString("muon trigger not supported ") +  trig);
        return;
      }
    }

    if (diMuTrigger == ""  && singleMuTrigger == "") {
      Warning("getMuonTriggerScaleFactor", "no muon triggers found");
      return;
    }

    std::vector<double> singleEffMC;
    std::vector<double> singleEffData;
    auto ok = getSingleMuonEfficiency(singleEffMC, muons, singleMuTrigger, false);

    if (!ok) {
      Warning("getMuonTriggerScaleFactor", TString("Check the configuration trigger (ensure single muons triggers are in correct order): ") +  singleMuTrigger);
      return ;
    }

    ok = getSingleMuonEfficiency(singleEffData, muons, singleMuTrigger, true);

    if (!ok) { return ; }

    std::vector<double> dieffMC;
    std::vector<double> dieffData;

    if (diMuTrigger != "") {
      ok = getAsymDimuonEfficiency(dieffMC, muons, diMuTrigger, false);

      if (!ok) { return ; }

      ok = getAsymDimuonEfficiency(dieffData, muons, diMuTrigger, true);

      if (!ok) { return ; }
    }

    double dataEff_singmu = singleEffData[0] + singleEffData[1] - singleEffData[0] * singleEffData[1];
    double mcEff_singmu   = singleEffMC[0] + singleEffMC[1] - singleEffMC[0] * singleEffMC[1];

    double dataEff_dimu   = dieffData[0] * dieffData[1];
    double mcEff_dimu     = dieffMC[0] * dieffMC[1];

    double dataEff_overlap = singleEffData[0] * dieffData[1] + singleEffData[1] * dieffData[0] - singleEffData[0] * singleEffData[1];
    double mcEff_overlap   = singleEffMC[0] * dieffMC[1] + singleEffMC[1] * dieffMC[0] - singleEffMC[0] * singleEffMC[1];

    double effData        = dataEff_singmu + dataEff_dimu - dataEff_overlap;
    double effMC          = mcEff_singmu   + mcEff_dimu   - mcEff_overlap  ;

    if (fabs(1. - effMC) > 0.0001  && effMC > 0) {
      trigSF = effData / effMC;
    }

    return;
  }


  //______________________________________________________________________________
  bool EventHandler::getSingleMuonEfficiency(std::vector<Double_t> &eff,
                                             xAOD::MuonContainer &mucont,
                                             const TString &trigger,
                                             Bool_t dataType)
  {
    auto mu1 = *(mucont.begin());

    double eff1 = 0;
    double eff2 = 0;

    auto result1 =  m_trigMuonScaleFactors->getTriggerEfficiency(*mu1,
                    eff1,
                    trigger.Data(),
                    dataType);

    if (result1 != CP::CorrectionCode::Ok) { return false; }

    if (mucont.size() > 1) {
      auto mu2 = *(mucont.begin() + 1);
      auto result2 =  m_trigMuonScaleFactors->getTriggerEfficiency(*mu2,
                      eff2,
                      trigger.Data(),
                      dataType);

      if (result2 != CP::CorrectionCode::Ok) { return false; }
    }

    eff.clear();
    eff.push_back(eff1);
    eff.push_back(eff2);
    return true;

  }

  //______________________________________________________________________________
  bool EventHandler::getAsymDimuonEfficiency(std::vector<Double_t> &eff,
                                             xAOD::MuonContainer &mucont,
                                             const TString &trigger,
                                             Bool_t dataType)
  {
    double threshold_leg1 = 10.;
    double threshold_leg2 = 10.;

    TString leg1 =  trigger;
    auto pos = leg1.Index("_mu8noL1");

    if (pos == -1) {
      Warning("getAsymDimuonEfficiency", "Invalid dimuon trigger chain name given");
    } else {
      leg1.ReplaceAll("_mu8noL1", "");
    }


    pos = leg1.Index("_mu");

    if (pos == -1) {
      Warning("getAsymDimuonEfficiency", "Invalid dimuon trigger chain name given");
    } else {
      TPRegexp  matchTo("(?<=_mu)(.*)$");
      auto  hm = leg1(matchTo);
      threshold_leg1 = std::stoi(hm) * 1.05;
    }


    TString leg2 = "HLT_mu8noL1";


    auto mu1 = mucont.begin();
    auto mu2 = mucont.begin() + 1;

    // data
    //double eff1 = 0;
    double eff1 = 1.0;
    /*
        if ((**mu1).pt() * 0.001 > threshold_leg1) {
          auto result1 =  m_trigMuonScaleFactors->getTriggerEfficiency(**mu1,
                          eff1,
                          leg1.Data(),
                          dataType);

          if (result1 != CP::CorrectionCode::Ok) { return false; }
        }
    */
    //double eff2 = 0;
    double eff2 = 1.0;
    /*
        if ((**mu2).pt() * 0.001 > threshold_leg2) {
          auto result2 =  m_trigMuonScaleFactors-> getTriggerEfficiency(**mu2,
                          eff2,
                          leg2.Data(),
                          dataType);

          if (result2 != CP::CorrectionCode::Ok) { return false; }
        }
    */
    eff.clear();
    eff.push_back(eff1);
    eff.push_back(eff2);

    return true;
  }



  //______________________________________________________________________________
  float EventHandler::getSF_g35_medium_g25_medium_L12EM20VH(const xAOD::PhotonContainer &photons)
  {
    if (!HG::isMC())
    { fatal("The g35_g25 trigger SF should only be called on MC, never on data."); }

    if (runNumber() < 325713) { return 1.0; } // only good in 2017

    if (photons.size() < 2) { return 1.0; }

    std::vector<const xAOD::Photon *> trigPhotons;
    trigPhotons.push_back(photons[0]);
    trigPhotons.push_back(photons[1]);

    if (trigPhotons[0]->pt() < 36 * HG::GeV || trigPhotons[1]->pt() < 26 * HG::GeV)
    { return 1.0; }

    double sf = 1.;
    auto cc = m_trigGlobalTool->getEfficiencyScaleFactor(trigPhotons, sf);

    if (cc != CP::CorrectionCode::Ok) {
      Warning("EventHandler", "Scale factor evaluation failed for global trigger tool");
    }

    return sf;
  }


  //______________________________________________________________________________
  EL::StatusCode EventHandler::writeVars(TString eventName)
  {
    if (eventName == "")
    { eventName = HG::VarHandler::getInstance()->getEventInfoName(); }

    const xAOD::EventInfo *eventInfo = 0;

    if (m_store->retrieve(eventInfo, eventName.Data()).isFailure()) {
      if (m_event->retrieve(eventInfo, eventName.Data()).isFailure()) {
        HG::fatal("EventHandler::write() cannot access " + eventName + ". Exiting.");
      }
    }

    // create containers
    xAOD::EventInfo *output       = new xAOD::EventInfo();
    xAOD::AuxInfoBase *outputAux = new xAOD::AuxInfoBase();
    output->setStore(outputAux);

    *output = *eventInfo;

    // record event info (yes, can be done before setting the actual values)
    if (!m_event->record(output, eventName.Data())) { return EL::StatusCode::FAILURE; }

    eventName += "Aux.";

    if (!m_event->record(outputAux, eventName.Data())) { return EL::StatusCode::FAILURE; }

    return EL::StatusCode::SUCCESS;
  }

  //______________________________________________________________________________
  EL::StatusCode EventHandler::writeEventInfo()
  {
    if (m_event->copy("EventInfo").isFailure())
    { Warning("EventHandler::writeEventInfo()", "Couldn't copy EventInfo to output."); }

    return EL::StatusCode::SUCCESS;
  }

  //______________________________________________________________________________
  CP::SystematicCode EventHandler::applySystematicVariation(const CP::SystematicSet &sys)
  {
    bool isAffected = false;

    for (auto var : sys) {
      if (m_pileupRW->isAffectedBySystematic(var) ||
          m_trigMuonScaleFactors->isAffectedBySystematic(var) ||
          m_trigElectronScaleFactors->isAffectedBySystematic(var) ||
          m_trigElectronMCEfficiency->isAffectedBySystematic(var)
         ) {
        isAffected = true;
        break;
      }
    }

    if (isAffected) {
      CP_CHECK("EventHandler", m_pileupRW->applySystematicVariation(sys));
      CP_CHECK("EventHandler", m_trigMuonScaleFactors->applySystematicVariation(sys));
      CP_CHECK("EventHandler", m_trigElectronScaleFactors->applySystematicVariation(sys));
      CP_CHECK("EventHandler", m_trigElectronMCEfficiency->applySystematicVariation(sys));

      if (m_trigDiElectronScaleFactors) { CP_CHECK("EventHandler", m_trigDiElectronScaleFactors->applySystematicVariation(sys)); }

      if (m_trigDiElectronMCEfficiency) { CP_CHECK("EventHandler", m_trigDiElectronMCEfficiency->applySystematicVariation(sys)); }

      m_sysName = sys.name() == "" ? "" : "_" + sys.name();
    } else {
      CP_CHECK("EventHandler", m_pileupRW->applySystematicVariation(CP::SystematicSet()));
      CP_CHECK("EventHandler", m_trigMuonScaleFactors->applySystematicVariation(CP::SystematicSet()));
      CP_CHECK("EventHandler", m_trigElectronScaleFactors->applySystematicVariation(CP::SystematicSet()));
      CP_CHECK("EventHandler", m_trigElectronMCEfficiency->applySystematicVariation(CP::SystematicSet()));

      if (m_trigDiElectronScaleFactors) { CP_CHECK("EventHandler", m_trigDiElectronScaleFactors->applySystematicVariation(CP::SystematicSet())); }

      if (m_trigDiElectronMCEfficiency) { CP_CHECK("EventHandler", m_trigDiElectronMCEfficiency->applySystematicVariation(CP::SystematicSet())); }

      m_sysName = "";
    }

    return CP::SystematicCode::Ok;
  }

  //______________________________________________________________________________
  int EventHandler::mcChannelNumber()
  {
    const xAOD::EventInfo *eventInfo = nullptr;

    if (m_event->retrieve(eventInfo, "EventInfo").isFailure()) {
      HG::fatal("Cannot access EventInfo");
    }

    if (HG::isMC()) {
      return eventInfo->mcChannelNumber();
    }

    return -1.0;
  }
} // namespace HG
