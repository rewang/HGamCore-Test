// STL include(s):
#include <stdexcept>

// EDM include(s):
#include "AsgTools/MsgStream.h"
#include "AsgTools/MsgStreamMacros.h"
#include "InDetTrackSelectionTool/InDetTrackSelectionTool.h"
#include "JetCalibTools/JetCalibrationTool.h"
#include "JetJvtEfficiency/JetJvtEfficiency.h"
#include "JetMomentTools/JetOriginCorrectionTool.h"
#include "JetResolution/JERSmearingTool.h"
#include "JetResolution/JERTool.h"
#include "JetUncertainties/JetUncertaintiesTool.h"
#include "xAODBTaggingEfficiency/BTaggingEfficiencyTool.h"
#include "xAODBTaggingEfficiency/BTaggingSelectionTool.h"

// Local include(s):
#include "HGamAnalysisFramework/JetHandler.h"
#include "HGamAnalysisFramework/HgammaUtils.h"


namespace HG {
  SG::AuxElement::Accessor<float> JetHandler::DetectorEta("DetectorEta");
  SG::AuxElement::Accessor<std::vector<float> > JetHandler::JVF("JVF");
  SG::AuxElement::Accessor<float> JetHandler::Jvf("Jvf");
  SG::AuxElement::Accessor<float> JetHandler::Jvt("Jvt");
  SG::AuxElement::Accessor<float> JetHandler::hardVertexJvt("hardVertexJvt");
  SG::AuxElement::Accessor<char>  JetHandler::isClean("isClean");
  SG::AuxElement::Accessor<float> JetHandler::scaleFactor("scaleFactor");

  // ______________________________________________________________________________
  JetHandler::JetHandler(const char *name, xAOD::TEvent *event, xAOD::TStore *store)
    : HgammaHandler(name, event, store)
    , m_jetCalibTool(nullptr)
    , m_jetCleaning(nullptr)
    , m_jesProvider(nullptr)
    , m_jerTool(nullptr)
    , m_jerSmear(nullptr)
    , m_trackTool(nullptr)
    , m_jvtLikelihood(nullptr)
    , m_jetOriginTool(nullptr)
  {}

  // ______________________________________________________________________________
  JetHandler::~JetHandler()
  {
    SafeDelete(m_jetCalibTool);
    SafeDelete(m_jetCleaning);
    SafeDelete(m_jesProvider);
    SafeDelete(m_jerTool);
    SafeDelete(m_jerSmear);
    SafeDelete(m_trackTool);
    SafeDelete(m_jvtLikelihood);
    SafeDelete(m_jvtSF);
    SafeDelete(m_fjvtSF);
    SafeDelete(m_jetOriginTool);

    for (auto name : m_bTagNames) {
      for (auto tool : m_bTagEffTools[name]) {
        SafeDelete(tool);
      }
    }
  }

  // ______________________________________________________________________________
  EL::StatusCode JetHandler::initialize(Config &config)
  {
    HgammaHandler::initialize(config);

    // General options
    const xAOD::EventInfo *eventInfo = nullptr;

    if (m_event->retrieve(eventInfo, "EventInfo").isFailure()) {
      ATH_MSG_FATAL("Cannot access EventInfo, exiting.");
      return EL::StatusCode::FAILURE;
    }

    // Read in configuration information
    m_containerName = config.getStr(m_name + ".ContainerName");
    m_correctVertex = config.getBool(m_name + ".Calibration.CorrectVertex");
    m_truthName     = config.getStr(m_name + ".TruthContainerName", "AntiKt4TruthJets");

    m_rapidityCut = config.getNum(m_name + ".Selection.MaxAbsRapidity", 4.4);
    m_ptCut       = config.getNum(m_name + ".Selection.PtPreCutGeV", 25.0) * GeV;
    m_jvf         = config.getNum(m_name + ".Selection.JVF", 0.25);
    m_jvt         = config.getNum(m_name + ".Selection.JVT", 0.64);
    m_doFJVT      = config.getBool(m_name + ".Selection.DoFJVT", true);

    // B-tagging
    m_enableBTagging = config.getBool(m_name + ".EnableBTagging", false);

    if (m_enableBTagging) {
      TString bTagCDI, bTagCDIFullPath;

      if (HG::isMC()) {
        bTagCDI = config.getStr(m_name + ".BTagging.ScaleFactorFileName" + HG::mcType());
      } else {
        bTagCDI = config.getStr(m_name + ".BTagging.DataWorkingPointFileName");
      }

      bTagCDIFullPath = PathResolverFindCalibFile(bTagCDI.Data());

      m_defaultBJetWP   = config.getStr(m_name + ".BTagging.DefaultWP", "FixedCutBEff_77");
      m_bTagRapidityCut = config.getNum(m_name + ".BTagging.MaxAbsRapidity", 2.5);
      m_bTagNames       = config.getStrV(m_name + ".BTagging.TaggerNames");
      m_bTagEVReduction = config.getStr(m_name + ".BTagging.EVReduction");

      for (auto name : m_bTagNames) {
        // Use the operating points defined by the Tagging tool.
        m_bTagOPs[name] = config.getStrV(m_name + "." + name + ".OperatingPoints");

        ATH_MSG_WARNING("MC/MC calibration for b-tagging is not yet available in r21 and may result in errors.");

        for (size_t i = 0; i < m_bTagOPs[name].size(); ++i) {
          TString op = m_bTagOPs[name].at(i);

          // Configure efficiency tool
          BTaggingEfficiencyTool *tempTool = new BTaggingEfficiencyTool("BTaggingEfficiencyTool");
          CP_CHECK(m_name, tempTool->setProperty("TaggerName", name.Data()));
          CP_CHECK(m_name, tempTool->setProperty("OperatingPoint", op.Data()));
          CP_CHECK(m_name, tempTool->setProperty("JetAuthor", m_containerName.Data()));
          CP_CHECK(m_name, tempTool->setProperty("ScaleFactorFileName", bTagCDIFullPath.Data()));

          if (!op.Contains("Continuous")) {
            CP_CHECK(m_name, tempTool->setProperty("EigenvectorReductionB", m_bTagEVReduction.Data()));
            CP_CHECK(m_name, tempTool->setProperty("EigenvectorReductionC", m_bTagEVReduction.Data()));
            CP_CHECK(m_name, tempTool->setProperty("EigenvectorReductionLight", m_bTagEVReduction.Data()));
          }

          // Set calibration for MCMC scale factors.
          // In order, these DSIDs are Pythia6, Herwig++, Sherpa 2.11, Sherpa 2.2, Pythia8, Herwig7
          // TString btagCalibs = "410000;410004;410021;410187;410500;410525";
          TString btagCalibs = "default;default;default;410250;410501;default";

          if (op.Contains("Continuous") || m_jetAlgo.Contains("PFlow"))
          { btagCalibs = "default;default;default;default;default;default"; }

          CP_CHECK(m_name, tempTool->setProperty("EfficiencyBCalibrations", btagCalibs.Data()));
          CP_CHECK(m_name, tempTool->setProperty("EfficiencyCCalibrations", btagCalibs.Data()));
          CP_CHECK(m_name, tempTool->setProperty("EfficiencyTCalibrations", btagCalibs.Data()));
          CP_CHECK(m_name, tempTool->setProperty("EfficiencyLightCalibrations", btagCalibs.Data()));

          // Initialize tool
          if (tempTool->initialize().isFailure()) {
            ATH_MSG_FATAL("Failed to initialize BTaggingEfficiencyTool, exiting.");
            return EL::StatusCode::FAILURE;
          }

          // Configure selection tool
          BTaggingSelectionTool *tempSelTool = new BTaggingSelectionTool("BTaggingSelectionTool");
          CP_CHECK(m_name, tempSelTool->setProperty("MaxEta", m_bTagRapidityCut));
          CP_CHECK(m_name, tempSelTool->setProperty("MinPt", m_ptCut));
          CP_CHECK(m_name, tempSelTool->setProperty("TaggerName", name.Data()));
          CP_CHECK(m_name, tempSelTool->setProperty("OperatingPoint", op.Data()));
          CP_CHECK(m_name, tempSelTool->setProperty("JetAuthor", m_containerName.Data()));
          CP_CHECK(m_name, tempSelTool->setProperty("FlvTagCutDefinitionsFileName", bTagCDIFullPath.Data()));

          // Initialize tool
          if (tempSelTool->initialize().isFailure()) {
            ATH_MSG_FATAL("Failed to initialize BTaggingSelectionTool, exiting.");
            return EL::StatusCode::FAILURE;
          }

          // Add both tools to the map
          m_bTagEffTools[name].push_back(tempTool);
          m_bTagSelTools[name].push_back(tempSelTool);
        }
      }
    }

    // Calibration tool
    m_jetCalibTool = new JetCalibrationTool("JetCalibrationTool");
    m_jetAlgo = m_containerName;
    m_jetAlgo.ReplaceAll("Jets", "");
    TString jetconfig = config.getStr(m_name + ".Calibration.ConfigFile" + (HG::isAFII() ? "AFII" : ""));

    // Construct calibration sequence: there is currently no in-situ calibration available for data in r21
    TString calibSeq = config.getStr(m_name + ".Calibration.CalibSeq");
    calibSeq = calibSeq + (HG::isData() ? "_Insitu" : "");

    // Use origin correction from derivations if requested
    const bool applyOriginCorrection = config.getBool("HgammaAnalysis.SelectVertex", false) && !config.getBool("HgammaAnalysis.UseHardestVertex", true);

    if (m_correctVertex && applyOriginCorrection) {
      calibSeq = calibSeq.ReplaceAll("Residual", "Residual_Origin");
      CP_CHECK(m_name, m_jetCalibTool->setProperty("OriginScale", "Hgg_JetOriginConstitScaleMomentum"));
      ATH_MSG_INFO("Using jet origin scale: Hgg_JetOriginConstitScaleMomentum");
    }

    ATH_MSG_INFO("Using jet calibration sequence: " << calibSeq.Data());

    CP_CHECK(m_name, m_jetCalibTool->setProperty("JetCollection", m_jetAlgo.Data()));
    CP_CHECK(m_name, m_jetCalibTool->setProperty("ConfigFile", jetconfig.Data()));
    CP_CHECK(m_name, m_jetCalibTool->setProperty("CalibSequence", calibSeq.Data()));
    CP_CHECK(m_name, m_jetCalibTool->setProperty("IsData", HG::isData()));
    CP_CHECK(m_name, m_jetCalibTool->setProperty("CalibArea", config.getStr(m_name + ".Calibration.CalibArea").Data()));

    if (m_jetCalibTool->initializeTool("JetCalibrationTool").isFailure()) {
      ATH_MSG_FATAL("Failed to initialize JetCalibrationTool, exiting.");
      return EL::StatusCode::FAILURE;
    }

    // Resolution and resolution smearing tools needed for MC only
    if (HG::isMC()) {
      // Jet energy resolution
      m_jerTool = new JERTool("JERTool");
      CP_CHECK(m_name, m_jerTool->setProperty("PlotFileName", PathResolverFindCalibFile(config.getStr(m_name + ".Resolution.PlotFileName").Data())));
      CP_CHECK(m_name, m_jerTool->setProperty("CollectionName", m_containerName.Data()));

      // ... check the tool was set up
      if (m_jerTool->initialize().isFailure()) {
        ATH_MSG_FATAL("Failed to initialize JERTool, exiting.");
        return EL::StatusCode::FAILURE;
      }

      ToolHandle<IJERTool> jerHandle(m_jerTool->name());

      // Jet energy resolution smearing
      m_jerSmear = new JERSmearingTool("JERSmearingTool");
      CP_CHECK(m_name, m_jerSmear->setProperty("JERTool", jerHandle));
      CP_CHECK(m_name, m_jerSmear->setProperty("ApplyNominalSmearing", config.getBool(m_name + ".Resolution.ApplyNominalSmearing")));
      CP_CHECK(m_name, m_jerSmear->setProperty("isMC", HG::isMC()));
      CP_CHECK(m_name, m_jerSmear->setProperty("SystematicMode", config.getStr(m_name + ".Resolution.SystematicMode").Data()));

      // ... check the tool was set up
      if (m_jerSmear->initialize().isFailure()) {
        ATH_MSG_FATAL("Failed to initialize JERSmearingTool, exiting.");
        return EL::StatusCode::FAILURE;
      }
    }

    // Cleaning tool
    m_doCleaning  = config.getBool(m_name + ".Selection.DoCleaning", true);
    m_jetCleaning = new JetCleaningTool("JetCleaningTool");
    CP_CHECK(m_name, m_jetCleaning->setProperty("CutLevel", config.getStr(m_name + ".Selection.CutLevel").Data()));
    CP_CHECK(m_name, m_jetCleaning->setProperty("DoUgly", config.getBool(m_name + ".Selection.DoUgly")));

    // ... check the tool was set up
    if (m_jetCleaning->initialize().isFailure()) {
      ATH_MSG_FATAL("Failed to initialize JetCleaningTool, exiting.");
      return EL::StatusCode::FAILURE;
    }

    // JES uncertainty provider
    m_jesProvider = new JetUncertaintiesTool("JetUncertaintiesTool");
    TString jetdef = m_jetAlgo.Data();

    // if (m_jetAlgo == "AntiKt4EMPFlow") {
    //   ATH_MSG_WARNING("Initializing JetUncertaintiesTool with AntiKt4EMTopo, since PFlow uncertainties aren't available yet.");
    //   jetdef = "AntiKt4EMTopo";
    // }

    CP_CHECK(m_name, m_jesProvider->setProperty("JetDefinition", jetdef.Data()));
    CP_CHECK(m_name, m_jesProvider->setProperty("MCType", config.getStr(m_name + ".Uncertainty.MCType").Data()));
    CP_CHECK(m_name, m_jesProvider->setProperty("ConfigFile", config.getStr(m_name + ".Uncertainty.ConfigFile").Data()));
    CP_CHECK(m_name, m_jesProvider->setProperty("CalibArea", config.getStr(m_name + ".Uncertainty.CalibArea").Data()));

    // ... check the tool was set up
    if (m_jesProvider->initialize().isFailure()) {
      ATH_MSG_FATAL("Failed to initialize JetUncertaintiesTool, exiting.");
      return EL::StatusCode::FAILURE;
    }

    // Track selection tool
    m_trackTool = new InDet::InDetTrackSelectionTool("TrackSelectionTool");
    CP_CHECK(m_name, m_trackTool->setProperty("CutLevel", "Loose"));

    // ... check the tool was set up
    if (m_trackTool->initialize().isFailure()) {
      ATH_MSG_FATAL("Failed to initialize InDetTrackSelectionTool, exiting.");
      return EL::StatusCode::FAILURE;
    }

    // JVT likelihood histogram
    TString jvtFile = "JetMomentTools/JVTlikelihood_20140805.root";
    TString jvtName = "JVTRootCore_kNN100trim_pt20to50_Likelihood";
    m_jvtLikelihood = (TH2F *)HG::getHistogramFromFile(jvtName, jvtFile);

    // ... check the tool was set up
    if (m_jvtLikelihood == nullptr) {
      ATH_MSG_FATAL("Failed to retrieve JVT likelihood file, exiting.");
      return EL::StatusCode::FAILURE;
    }

    // JVT re-scaling tool
    m_jvtTool.setTypeAndName("JetVertexTaggerTool/UpdateJVT");

    if (m_jvtTool->initialize().isFailure()) {
      ATH_MSG_FATAL("Failed to initialize JetVertexTaggerTool, exiting.");
      return EL::StatusCode::FAILURE;
    }

    // Forward JVT tool
    m_fJvtTool.setTypeAndName("JetForwardJvtTool/fJvt");

    if (m_fJvtTool.retrieve().isFailure()) {
      ATH_MSG_FATAL("Failed to initialize JetForwardJvtTool, exiting.");
      return EL::StatusCode::FAILURE;
    }

    // JVT efficiency SF
    m_jvtSF = new CP::JetJvtEfficiency("JetJvtEfficiency");
    CP_CHECK(m_name, m_jvtSF->setProperty("WorkingPoint", "Medium"));

    if (m_jetAlgo.Contains("LC")) {
      CP_CHECK(m_name, m_jvtSF->setProperty("SFFile", "JetJvtEfficiency/Moriond2017/JvtSFFile_LC.root"));
    } else {
      CP_CHECK(m_name, m_jvtSF->setProperty("SFFile", "JetJvtEfficiency/Moriond2017/JvtSFFile_EM.root"));
    }

    CP_CHECK(m_name, m_jvtSF->setProperty("ScaleFactorDecorationName", "SF_jvt"));
    CP_CHECK(m_name, m_jvtSF->setProperty("JetJvtMomentName", "Jvt"));
    CP_CHECK(m_name, m_jvtSF->setProperty("JetEtaName", "DetectorEta"));

    if (m_jvtSF->initialize().isFailure()) {
      fatal("Failed to initialize JetJvtEfficiency, exiting.");
    }

    // fJVT efficiency SF
    m_fjvtSF = new CP::JetJvtEfficiency("fJetJvtEfficiency");
    CP_CHECK(m_name, m_fjvtSF->setProperty("SFFile", "JetJvtEfficiency/Moriond2016_v2/fJvtSFFile.root"));
    CP_CHECK(m_name, m_fjvtSF->setProperty("ScaleFactorDecorationName", "SF_fjvt"));
    CP_CHECK(m_name, m_fjvtSF->setProperty("JetEtaName", "DetectorEta"));

    if (m_fjvtSF->initialize().isFailure()) {
      fatal("Failed to initialize JetJvtEfficiency, exiting.");
    }

    // Jet origin correction tool
    m_jetOriginTool = new JetOriginCorrectionTool("JOT");

    return EL::StatusCode::SUCCESS;
  }

  // ______________________________________________________________________________
  xAOD::JetContainer JetHandler::getCorrectedContainer()
  {
    // Get shallow copy from TEvent/TStore
    bool calib = false;
    xAOD::JetContainer shallowContainer = getShallowContainer(calib);

    if (calib) { return shallowContainer; }

    if (m_jetAlgo.Contains("EM")) {
      ATH_MSG_DEBUG("Setting jet constituent state to UNCALIBRATED.");

      for (auto jet : shallowContainer) {
        jet->setConstituentsSignalState(xAOD::UncalibratedJetConstituent);
      }
    }

    // Get the photon pointed vertex, if available
    if (m_correctVertex && m_store->contains<ConstDataVector<xAOD::VertexContainer> >("HGamVertices")) {
      // Load the HGam vertex collection
      ConstDataVector<xAOD::VertexContainer> *vertices = nullptr;

      if (m_store->retrieve(vertices, "HGamVertices").isFailure()) {
        ATH_MSG_WARNING("Failed to retrieve HGamVertices from TStore.");
      }

      // Identify the photon-pointed vertex
      const xAOD::Vertex *vertex = nullptr;

      if (vertices && (vertices->size() > 0)) {
        vertex = (*vertices)[0];
      }
    }

    // calibrate and decorate jets
    for (auto jet : shallowContainer) {
      scaleFactor(*jet) = 1.0;

      // Apply initial decorations necessary for MxAOD
      decorateJVF(jet);
      isClean(*jet) = m_jetCleaning->accept(*jet);

      if (HG::isMC()) { decorateBJetTruth(jet); } // add truth-tagging for BJES unc.

      DetectorEta(*jet) = jet->getAttribute<xAOD::JetFourMom_t>("JetConstitScaleMomentum").eta();

      // Since calibrateJet applies systematic uncertainties, do this after decorations are applied
      calibrateJet(jet);

      // Reco b-tagging and JVT recalculation rely on the jet being fully calibrated
      recalculateJVT(*jet);

      if (m_enableBTagging) { decorateBJetReco(jet); }
    }

    // Add forward passFJVT flag, and JVT scale factors
    decorateJVTExtras(shallowContainer);

    // Some further corrections rely on the initial calibrated energy
    decorateRawCalib(shallowContainer);

    // Sort the Jets
    shallowContainer.sort(comparePt);

    return shallowContainer;
  }

  // ______________________________________________________________________________
  xAOD::JetContainer JetHandler::applySelectionNoCleaning(xAOD::JetContainer &container)
  {
    bool saveCleaning = m_doCleaning;
    m_doCleaning = false;

    xAOD::JetContainer sel = applySelection(container);

    m_doCleaning = saveCleaning;
    return sel;
  }

  // ______________________________________________________________________________
  xAOD::JetContainer JetHandler::applySelectionNoJvt(xAOD::JetContainer &container)
  {
    double saveJvt = m_jvt;
    m_jvt = -1.0;

    xAOD::JetContainer sel = applySelection(container);

    m_jvt = saveJvt;
    return sel;
  }

  // ______________________________________________________________________________
  xAOD::JetContainer JetHandler::applySelection(xAOD::JetContainer &container)
  {
    xAOD::JetContainer selected(SG::VIEW_ELEMENTS);

    for (auto jet : container) {
      // Apply pT and eta selection cuts
      if (!passPtEtaCuts(jet)) { continue; }

      // Apply cleaning cuts. MxAODs do not have isAvailable as cut is already applied
      if (m_doCleaning && isClean.isAvailable(*jet) && !isClean(*jet)) { continue; }

      // JVF cuts
      if (!passJVFCut(jet)) { continue; }

      // JVT cuts
      if (!passJVTCut(jet)) { continue; }

      // All the cuts are passed so we keep this jet
      selected.push_back(jet);
    }

    return selected;
  }

  // ______________________________________________________________________________
  xAOD::JetContainer JetHandler::applyBJetSelection(xAOD::JetContainer &container, TString wp)
  {
    if (wp == "") { wp = m_defaultBJetWP; }

    xAOD::JetContainer selected(SG::VIEW_ELEMENTS);

    if (!m_enableBTagging) { return selected; }

    for (auto jet : container) {
      if (jet->auxdata<char>(wp.Data())) { selected.push_back(jet); }
    }

    return selected;
  }

  // ______________________________________________________________________________
  CP::SystematicCode JetHandler::applySystematicVariation(const CP::SystematicSet &sys)
  {
    setVertexCorrected(false);

    bool isAffected = false;

    for (auto var : sys) {
      if (m_jesProvider->isAffectedBySystematic(var)) {
        isAffected = true;
        break;
      }

      if (m_jvtSF->isAffectedBySystematic(var)) {
        isAffected = true;
        break;
      }

      if (m_fjvtSF->isAffectedBySystematic(var)) {
        isAffected = true;
        break;
      }

      if (HG::isMC()) {
        if (m_jerSmear->isAffectedBySystematic(var)) {
          isAffected = true;
          break;
        }

        for (auto name : m_bTagNames) {
          for (auto tool : m_bTagEffTools[name]) {
            if (tool->isAffectedBySystematic(var)) {
              isAffected = true;
              break;
            }
          }
        }
      }
    }

    // This should mean that the jets can be shifted by this systematic
    if (isAffected) {
      CP_CHECK(m_name, m_jesProvider->applySystematicVariation(sys));
      CP_CHECK(m_name, m_jvtSF->applySystematicVariation(sys));
      CP_CHECK(m_name, m_fjvtSF->applySystematicVariation(sys));

      if (HG::isMC()) {
        CP_CHECK(m_name, m_jerSmear->applySystematicVariation(sys));

        for (auto name : m_bTagNames) {
          for (auto tool : m_bTagEffTools[name]) {
            CP_CHECK(m_name, tool->applySystematicVariation(sys));
          }
        }
      }

      m_sysName = (sys.name() == "" ? "" : "_" + sys.name());

      // Jets are not affected by this systematic
    } else {
      CP_CHECK(m_name, m_jesProvider->applySystematicVariation(CP::SystematicSet()));
      CP_CHECK(m_name, m_jvtSF->applySystematicVariation(CP::SystematicSet()));
      CP_CHECK(m_name, m_fjvtSF->applySystematicVariation(CP::SystematicSet()));

      if (HG::isMC()) {
        CP_CHECK(m_name, m_jerSmear->applySystematicVariation(CP::SystematicSet()));

        for (auto name : m_bTagNames) {
          for (auto tool : m_bTagEffTools[name]) {
            CP_CHECK(m_name, tool->applySystematicVariation(CP::SystematicSet()));
          }
        }
      }

      m_sysName = "";
    }

    return CP::SystematicCode::Ok;
  }

  // ______________________________________________________________________________
  void JetHandler::calibrateJet(xAOD::Jet *jet)
  {
    // applies calibration to a jet
    double E_before = jet->e();

    m_jetCalibTool->applyCorrection(*jet).ignore();

    if (HG::isMC()) {
      m_jerSmear->applyCorrection(*jet).ignore();
    }

    // Uncertainty shifting
    if ((jet->pt() > 20.0 * HG::GeV) && (fabs(jet->rapidity()) < 4.4)) {
      m_jesProvider->applyCorrection(*jet).ignore();
    }

    // decorate the photon with the calibration factor
    jet->auxdata<float>("Ecalib_ratio") = jet->e() / E_before;
  }

  // ______________________________________________________________________________
  bool JetHandler::passPtEtaCuts(const xAOD::Jet *jet)
  {
    /// applies kinematic preselection cuts: not-in-crack + pT cut

    // eta cuts
    if (fabs(jet->rapidity()) > m_rapidityCut) { return false; }

    // pt cuts
    if (jet->pt() < m_ptCut) { return false; }

    return true;
  }

  // ______________________________________________________________________________
  void JetHandler::decorateRescaledJVT(xAOD::Jet &jet)
  {
    Jvt(jet) = m_jvtTool->updateJvt(jet);
  }

  // ______________________________________________________________________________
  void JetHandler::recalculateJVT(xAOD::Jet &jet)
  {
    // Always decorate the hardVertex value (used by MET maker)
    hardVertexJvt(jet) = m_jvtTool->updateJvt(jet);

    if (m_containerName.Contains("PFlow")) {
      ATH_MSG_DEBUG("Can't recalculate JVT for PFlow jets, instead rescaling.");
      decorateRescaledJVT(jet);
      return;
    }

    // Retrieve HGamVertices, using default JVT if this fails
    ConstDataVector<xAOD::VertexContainer> *vertices = nullptr;

    if (m_store->retrieve(vertices, "HGamVertices").isFailure()) {
      ATH_MSG_WARNING("Failed to retrieve HGamVertices from TStore.");
      decorateRescaledJVT(jet);
      return;
    }

    // Find selected vertex, using default JVT if this fails
    const xAOD::Vertex *diphoton_vertex = nullptr;

    if (vertices && (vertices->size() > 0)) { diphoton_vertex = vertices->at(0); }

    if (diphoton_vertex == nullptr) {
      ATH_MSG_WARNING("Selected vertex could not be determined.");
      decorateRescaledJVT(jet);
      return;
    }

    // Get track container
    const xAOD::TrackParticleContainer *tracks = nullptr;

    if (m_event->retrieve(tracks, "InDetTrackParticles").isFailure()) {
      ATH_MSG_FATAL("Failed to retrieve InDetTrackParticles, exiting.");
      throw std::invalid_argument("Failed to retrieve InDetTrackParticles, exiting.");
    }

    // Check tracks, using default JVT if this fails
    if (tracks == nullptr) {
      ATH_MSG_WARNING("InDetTrackParticles retrieved as a nullptr!");
      decorateRescaledJVT(jet);
      return;
    }

    // Count the number of pileup tracks
    int nPileupTracks = 0;

    for (auto track : *tracks) {
      if (track == nullptr) { continue; }

      if (m_trackTool->accept(*track, diphoton_vertex) && track->vertex() && track->vertex() != diphoton_vertex && (track->pt() < 30e3)) {
        nPileupTracks++;
      }
    }

    if (nPileupTracks == 0) { nPileupTracks = 1; }

    // Get all tracks ghost-associated to the jet
    std::vector<const xAOD::IParticle *> jetTracks;
    jet.getAssociatedObjects<xAOD::IParticle>(xAOD::JetAttribute::GhostTrack, jetTracks);

    // Iterate over ghost-tracks, rejecting invalid ones and calculating sum(pT)
    double ptSum_all(0.0), ptSum_pv(0.0), ptSum_pu(0.0);

    for (auto jetTrack : jetTracks) {
      if (jetTrack == nullptr) { continue; }

      const xAOD::TrackParticle *track = static_cast<const xAOD::TrackParticle *>(jetTrack);

      // Add accepted tracks to pT sums for all, primary vertex, pileup
      if (m_trackTool->accept(*track, diphoton_vertex) && track->pt() > 500) {
        ptSum_all += track->pt();

        if (track->vertex() == diphoton_vertex || (!track->vertex() && (fabs((track->z0() + track->vz() - diphoton_vertex->z()) * sin(track->theta())) < 3.0))) {
          ptSum_pv += track->pt();
        }

        if (track->vertex() && track->vertex() != diphoton_vertex) {
          ptSum_pu += track->pt();
        }
      }
    }

    // Perform explicit JVT calculation
    double _Rpt     = ptSum_pv / jet.pt();
    double _CorrJVF = ptSum_pv + ptSum_pu > 0 ? ptSum_pv / (ptSum_pv + 100 * ptSum_pu / nPileupTracks) : -1;
    double _JVT     = _CorrJVF >= 0 ? m_jvtLikelihood->Interpolate(_CorrJVF, std::min(_Rpt, 1.0)) : -0.1;
    Jvt(jet) = _JVT;
  }

  //______________________________________________________________________________
  double JetHandler::multiplyJvtWeights(const xAOD::JetContainer *jets_noJvtCut)
  {
    // Static function so can't use asg::Messaging
    static SG::AuxElement::Decorator<float> SF_jvt("SF_jvt");

    double weight = 1.0;

    for (auto jet : *jets_noJvtCut) {
      float current_sf = SF_jvt(*jet);

      if (current_sf <= 0.0) {
        Warning("JetHandler::multiplyJvtWeights", "Found a jet which failed JVT calibration, skipping!");
        continue;
      }

      weight *= current_sf;
    }

    return weight;
  }

  //______________________________________________________________________________
  double JetHandler::multiplyFJvtWeights(const xAOD::JetContainer *jets_noJvtCut)
  {
    // Static function so can't use asg::Messaging
    static SG::AuxElement::Decorator<float> SF_fjvt("SF_fjvt");

    double weight = 1.0;

    for (auto jet : *jets_noJvtCut) {
      float current_sf = SF_fjvt(*jet);

      if (current_sf <= 0.0) {
        // Warning("JetHandler::multiplyFJvtWeights", "Found a jet which failed fJVT calibration, skipping!");
        continue;
      }

      weight *= current_sf;
    }

    return weight;
  }

  // ______________________________________________________________________________
  void JetHandler::decorateJVTExtras(xAOD::JetContainer &jets)
  {
    // Now decorate JVT and fJVT scale factors
    static SG::AuxElement::Decorator<float> SF_jvt("SF_jvt");
    static SG::AuxElement::Decorator<float> SF_fjvt("SF_fjvt");
    float _unused;

    // Calling this decorates isJvtHS and SF_jvt
    if (HG::isMC()) {
      m_jvtSF->applyAllEfficiencyScaleFactor(&jets, _unused);
    } else {
      for (auto jet : jets) { SF_jvt(*jet) = 1.0; }
    }

    // Tag jets with "passFJVT" forward JVT decoration
    if (m_doFJVT) {
      m_fJvtTool->modify(jets);
    }

    // Calling this decorates SF_fjvt
    if (HG::isMC() && m_doFJVT) {
      m_fjvtSF->applyAllEfficiencyScaleFactor(&jets, _unused);
    } else {
      for (auto jet : jets) { SF_fjvt(*jet) = 1.0; }
    }
  }

  // ______________________________________________________________________________
  float JetHandler::weightJvt(const xAOD::JetContainer &jets)
  {
    if (HG::isData()) { return 1.0; }

    float weight = 1.0;
    m_jvtSF->applyAllEfficiencyScaleFactor(&jets, weight);
    return weight;
  }

  // ______________________________________________________________________________
  float JetHandler::weightFJvt(const xAOD::JetContainer &jets)
  {
    if (HG::isData()) { return 1.0; }

    float weight = 1.0;
    m_fjvtSF->applyAllEfficiencyScaleFactor(&jets, weight);
    return weight;
  }

  // ______________________________________________________________________________
  bool JetHandler::passJVTCut(const xAOD::Jet *jet)
  {
    // Normal jet check
    if (m_jvt < 0) {
      return true;
    }

    if ((jet->pt() > 60.0 * HG::GeV) ||
        (fabs(DetectorEta(*jet)) > 2.4) ||
        m_jvtSF->passesJvtCut(*jet)) {
      return true;
    }

    return false;
  }

  // ______________________________________________________________________________
  bool JetHandler::passJVFCut(const xAOD::Jet *jet, bool useBTagCut)
  {
    // If the cut is disabled, just return true
    if (m_jvf < 0) { return true; }

    float jvf = useBTagCut ? m_bTagJvfCut : m_jvf;

    if ((jet->pt() < 50.0 * HG::GeV) && (fabs(jet->eta()) < 2.4) &&
        (fabs(Jvf(*jet)) < jvf)) {
      return false;
    }

    return true;
  }

  // ______________________________________________________________________________
  void JetHandler::decorateJVF(xAOD::Jet *jet)
  {
    // JVF decoration
    const xAOD::VertexContainer *vertices = 0;

    if (!m_event->retrieve(vertices, "PrimaryVertices").isSuccess()) {
      ATH_MSG_FATAL("Failed to retrieve PrimaryVertices, exiting.");
      throw std::invalid_argument("Failed to retrieve PrimaryVertices, exiting.");
    }

    size_t pv = 0, npv = vertices->size();

    for (; pv < npv; ++pv) {
      if ((*vertices)[pv]->vertexType() == xAOD::VxType::VertexType::PriVtx) {
        if (JVF.isAvailable(*jet)) {
          Jvf(*jet) = JVF(*jet).at(pv);
        } else {
          Jvf(*jet) = -99;
        }

        return;
      }
    }

    Jvf(*jet) = -1.0;
  }

  // ______________________________________________________________________________
  TString JetHandler::getFlavorLabel(xAOD::Jet *jet)
  {
    static SG::AuxElement::ConstAccessor<int> PartonTruthLabelID("PartonTruthLabelID");
    int truthID = PartonTruthLabelID(*jet);

    if (truthID == 5)  { return "B"; }

    if (truthID == 4)  { return "C"; }

    if (truthID == 15) { return "T"; }

    return "Light";
  }


  // ______________________________________________________________________________
  void JetHandler::setMCGen(TString sampleName)
  {
    m_mcIndex = 4;  // Default: Pythia8

    if (sampleName.Contains("Py6"))        { m_mcIndex = 0; }
    else if (sampleName.Contains("Pythia6"))    { m_mcIndex = 0; }
    else if (sampleName.Contains("Hwpp"))       { m_mcIndex = 1; }
    else if (sampleName.Contains("Sherpa2"))    { m_mcIndex = 3; } // must come before the Sherpa check
    else if (sampleName.Contains("Sherpa"))     { m_mcIndex = 2; }
    else if (sampleName.Contains("Py8"))        { m_mcIndex = 4; }
    else if (sampleName.Contains("Pythia8"))    { m_mcIndex = 4; }
    else if (sampleName.Contains("Herwig7"))    { m_mcIndex = 5; }
    else {
      ATH_MSG_WARNING("Could not identify generator for MCMC SFs. Will use Pythia8.");
    }

    // Print message identifying showering
    if (m_mcIndex == 0)  { ATH_MSG_INFO(sampleName.Data() << " identified as Pythia6 showering."); }
    else if (m_mcIndex == 1)  { ATH_MSG_INFO(sampleName.Data() << " identified as Herwig++ showering."); }
    else if (m_mcIndex == 2)  { ATH_MSG_INFO(sampleName.Data() << " identified as Sherpa showering (v2.11 or lower)."); }
    else if (m_mcIndex == 3)  { ATH_MSG_INFO(sampleName.Data() << " identified as Sherpa showering (v2.2 or higher)."); }
    else if (m_mcIndex == 4)  { ATH_MSG_INFO(sampleName.Data() << " identified as Pythia8 showering."); }
    else if (m_mcIndex == 5)  { ATH_MSG_INFO(sampleName.Data() << " identified as Herwig7 showering."); }
  }


  // ______________________________________________________________________________
  void JetHandler::decorateBJetReco(xAOD::Jet *jet)
  {
    // Jets always need to be decorated with something, for writting to ROOT files
    for (auto name : m_bTagNames) {
      for (auto op : m_bTagOPs[name]) {
        jet->auxdata<char>((name + "_" + op).Data())            = false;
        jet->auxdata<float>(("Eff_" + name + "_" + op).Data())   = 1.0;
        jet->auxdata<float>(("InEff_" + name + "_" + op).Data()) = 1.0;
        jet->auxdata<float>(("SF_" + name + "_" + op).Data())    = 1.0;

        if (name.Contains("MV2"))
        { jet->auxdata<double>((name + "_discriminant").Data())    = -99; }
      }

      jet->auxdata<double>("MV2c100_discriminant") = -99;
      jet->auxdata<double>("MV2cl100_discriminant") = -99;
    }

    // Ensure the jet is inside the rapidity region
    if (fabs(jet->eta()) >= m_bTagRapidityCut) { return; }

    // Require JVT cut for MC15 tagging
    if (!passJVTCut(jet)) { return; }

    double tagging_discriminant(-99);

    // Add c-tagging discriminants
    if (jet->btagging() == 0 or not jet->btagging()->MVx_discriminant("MV2c100", tagging_discriminant)) {
      ATH_MSG_WARNING("Couldn't retrieve value of MV2c100 discriminant!");
    }

    jet->auxdata<double>("MV2c100_discriminant") = tagging_discriminant;

    if (jet->btagging() == 0 or not jet->btagging()->MVx_discriminant("MV2cl100", tagging_discriminant)) {
      ATH_MSG_WARNING("Couldn't retrieve value of MV2cl100 discriminant!");
    }

    jet->auxdata<double>("MV2cl100_discriminant") = tagging_discriminant;

    // Loop over b-tagging WPs
    for (auto name : m_bTagNames) {
      // Look for raw discriminant value and decorate jet
      if (name.Contains("MV2")) {
        tagging_discriminant = -99;

        if (jet->btagging() == 0 or not jet->btagging()->MVx_discriminant(name.Data(), tagging_discriminant))
        { ATH_MSG_WARNING("Couldn't retrieve value of " + name + " discriminant!"); }

        jet->auxdata<double>((name + "_discriminant").Data()) = tagging_discriminant;
      }

      for (size_t i = 0; i < m_bTagOPs[name].size(); ++i) {
        if (m_bTagSelTools[name].at(i)->accept(*jet)) {
          jet->auxdata<char>((name + "_" + m_bTagOPs[name].at(i)).Data()) = true;
        }

        if (HG::isMC()) {
          float effsf = 1.0;

          // Set MCMC
          if (not m_bTagEffTools[name].at(i)->setMapIndex(getFlavorLabel(jet).Data(), m_mcIndex)) {
            ATH_MSG_WARNING("Couldn't set MC/MC index properly. Results will be biased.");
          }

          // Decorate with efficiency
          if (m_bTagEffTools[name].at(i)->getEfficiency(*jet, effsf) == CP::CorrectionCode::Ok) {
            jet->auxdata<float>(("Eff_" + name + "_" + m_bTagOPs[name].at(i)).Data()) = effsf;
          }

          // Decorate with inefficiency
          if (m_bTagEffTools[name].at(i)->getInefficiency(*jet, effsf) == CP::CorrectionCode::Ok) {
            jet->auxdata<float>(("InEff_" + name + "_" + m_bTagOPs[name].at(i)).Data()) = effsf;
          }

          // Decorate with the correct SF depending on whether the jet is tagged or untagged.
          // ... for continuous b-tagging we will always use "passed"
          CP::CorrectionCode retrievedSF = (m_bTagSelTools[name].at(i)->accept(*jet) || m_bTagOPs[name].at(i).Contains("Continuous")) ?
                                           m_bTagEffTools[name].at(i)->getScaleFactor(*jet, effsf) :
                                           m_bTagEffTools[name].at(i)->getInefficiencyScaleFactor(*jet, effsf);

          if (retrievedSF == CP::CorrectionCode::Ok) {
            jet->auxdata<float>(("SF_" + name + "_" + m_bTagOPs[name].at(i)).Data()) = effsf;
          }
        }
      }
    }
  }

  // ______________________________________________________________________________
  void JetHandler::decorateBJetTruth(xAOD::Jet *jet)
  {
    if (HG::isData()) {
      ATH_MSG_FATAL("Only MC jets can be decorated with truth-b-tagging information!");
      throw std::invalid_argument("Only MC jets can be decorated with truth-b-tagging information!");
    }

    // If this is MC, then add the isBjet decoration for use by BJES
    SG::AuxElement::Accessor<char> accIsBjet("IsBjet");
    accIsBjet(*jet) = (jet->auxdata<int>("HadronConeExclTruthLabelID") == 5);
  }

  // ______________________________________________________________________________
  void JetHandler::printJet(const xAOD::Jet *jet, TString comment)
  {
    // Static function so need to use ROOT::Info instead of use asg::Messaging
    Info("JetHandler::printJet", "Jet %2zu  %s", jet->index(), comment.Data());
    Info("JetHandler::printJet", "   (pT,eta,phi,m) = (%5.1f GeV,%6.3f,%6.3f,%4.1f GeV)", jet->pt() / GeV, jet->eta(), jet->phi(), jet->m() / GeV);

    // print some more information
    TString str;

    if (jet->isAvailable<float>("Ecalib_ratio")) {
      str += Form("   calibFactor = %.3f", jet->auxdata<float>("Ecalib_ratio"));
    }

    Info("JetHandler::printJet", str);
  }
}
