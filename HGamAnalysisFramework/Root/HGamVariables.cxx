#include "HGamAnalysisFramework/HGamVariables.h"

namespace HG {

  //____________________________________________________________________________
  double tauJet(const xAOD::IParticle *g1, const xAOD::IParticle *g2, const xAOD::IParticle *jet)
  {
    return sqrt(pow(jet->pt(), 2) + pow(jet->m(), 2))
           / (2.0 * cosh(jet->rapidity() - (g1->p4() + g2->p4()).Rapidity()));
  }

}

namespace var {
  HG::pT_h1 pT_h1;
  HG::pT_h2 pT_h2;
  HG::y_h1 y_h1;
  HG::y_h2 y_h2;
  HG::m_h1 m_h1;
  HG::m_h2 m_h2;
  HG::yAbs_yy yAbs_yy;
  HG::pTt_yy pTt_yy;
  HG::m_yy m_yy;
  HG::passMeyCut passMeyCut;
  HG::pT_yy pT_yy;
  HG::pT_y1 pT_y1;
  HG::pT_y2 pT_y2;
  HG::E_y1 E_y1;
  HG::E_y2 E_y2;
  HG::pT_hard pT_hard;
  HG::cosTS_yy cosTS_yy;
  HG::phiStar_yy phiStar_yy;
  HG::Dphi_y_y Dphi_y_y;
  HG::Dy_y_y Dy_y_y;
  HG::N_e N_e;
  HG::N_mu N_mu;
  HG::N_lep N_lep;
  HG::N_lep_15 N_lep_15;
  HG::weightN_lep_15 weightN_lep_15;
  HG::N_j N_j;
  HG::N_j_30 N_j_30;
  HG::N_j_50 N_j_50;
  HG::N_j_central N_j_central;
  HG::N_j_central30 N_j_central30;
  HG::N_j_btag30 N_j_btag30;
  HG::pT_j1_30 pT_j1_30;
  HG::pT_j1 pT_j1;
  HG::pT_j2_30 pT_j2_30;
  HG::pT_j2 pT_j2;
  HG::pT_j3_30 pT_j3_30;
  HG::yAbs_j1 yAbs_j1;
  HG::yAbs_j1_30 yAbs_j1_30;
  HG::yAbs_j2 yAbs_j2;
  HG::yAbs_j2_30 yAbs_j2_30;
  HG::pT_jj pT_jj;
  HG::pT_yyj pT_yyj;
  HG::m_yyj m_yyj;
  HG::m_jj m_jj;
  HG::m_jj_30 m_jj_30;
  HG::Dy_j_j Dy_j_j;
  HG::Deta_j_j Deta_j_j;
  HG::Dy_j_j_30 Dy_j_j_30;
  HG::Dy_yy_jj Dy_yy_jj;
  HG::Dy_yy_jj_30 Dy_yy_jj_30;
  HG::Dphi_j_j Dphi_j_j;
  HG::Dphi_j_j_30 Dphi_j_j_30;
  HG::Dphi_j_j_30_signed Dphi_j_j_30_signed;
  HG::Dphi_yy_jj Dphi_yy_jj;
  HG::Dphi_yy_jj_30 Dphi_yy_jj_30;
  HG::m_yyjj m_yyjj;
  HG::pT_yyjj pT_yyjj;
  HG::pT_yyjj_30 pT_yyjj_30;
  HG::m_ee m_ee;
  HG::m_mumu m_mumu;
  HG::DRmin_y_j DRmin_y_j;
  HG::DRmin_y_j_2 DRmin_y_j_2;
  HG::DR_y_y DR_y_y;
  HG::Zepp Zepp;
  HG::cosTS_yyjj cosTS_yyjj;
  HG::sumTau_yyj_30 sumTau_yyj_30;
  HG::maxTau_yyj_30 maxTau_yyj_30;
  HG::met_TST met_TST;
  HG::sumet_TST sumet_TST;
  HG::phi_TST phi_TST;
  HG::N_conv N_conv;
  HG::HT_30 HT_30;
  HG::HTall_30 HTall_30;
  HG::massTrans massTrans;
  HG::pTlepMET pTlepMET;
  HG::philepMET philepMET;
  HG::m_alljet_30 m_alljet_30;
  HG::m_alljet m_alljet;
  HG::pT_alljet pT_alljet;
  HG::eta_alljet eta_alljet;
  HG::score_top1 score_top1;
  HG::pT_top1 pT_top1;
  HG::eta_top1 eta_top1;
  HG::phi_top1 phi_top1;
  HG::m_top1 m_top1;
  HG::pT_leftover pT_leftover;
  HG::eta_leftover eta_leftover;
  HG::phi_leftover phi_leftover;
  HG::m_leftover m_leftover;
  HG::eta_lep_W1 eta_lep_W1;
  HG::pT_lep_top1 pT_lep_top1;
  HG::eta_lep_top1 eta_lep_top1;
  HG::phi_lep_top1 phi_lep_top1;
  HG::m_lep_top1 m_lep_top1;
  HG::pT_lep_leftover pT_lep_leftover;
  HG::eta_lep_leftover eta_lep_leftover;
  HG::phi_lep_leftover phi_lep_leftover;
  HG::m_lep_leftover m_lep_leftover;

  HG::isPassedBasic isPassedBasic;
  HG::isPassed isPassed;
  HG::isPassedJetEventClean isPassedJetEventClean;
  HG::isFiducial isFiducial;
  HG::isFiducialKinOnly isFiducialKinOnly;
  HG::isDalitzEvent isDalitzEvent;
  HG::cutFlow cutFlow;
  HG::weightInitial weightInitial;
  HG::weight weight;
  HG::weightSF weightSF;
  HG::weightTrigSF weightTrigSF;
  HG::vertexWeight vertexWeight;
  HG::pileupWeight pileupWeight;
  HG::weightCatCoup_Moriond2017 weightCatCoup_Moriond2017;
  HG::weightCatCoup_SFMoriond2017 weightCatCoup_SFMoriond2017;
  HG::catCoup_Moriond2017 catCoup_Moriond2017;
  HG::weightCatCoup_Moriond2017BDT_qqH2jet weightCatCoup_Moriond2017BDT_qqH2jet;
  HG::weightCatCoup_SFMoriond2017BDT_qqH2jet weightCatCoup_SFMoriond2017BDT_qqH2jet;
  HG::catCoup_Moriond2017BDT_qqH2jet catCoup_Moriond2017BDT_qqH2jet;
  HG::weightCatCoup_Moriond2017BDT weightCatCoup_Moriond2017BDT;
  HG::weightCatCoup_SFMoriond2017BDT weightCatCoup_SFMoriond2017BDT;
  HG::catCoup_Moriond2017BDT catCoup_Moriond2017BDT;
  HG::weightCatCoup_Moriond2017BDTlep weightCatCoup_Moriond2017BDTlep;
  HG::weightCatCoup_SFMoriond2017BDTlep weightCatCoup_SFMoriond2017BDTlep;
  HG::catCoup_Moriond2017BDTlep catCoup_Moriond2017BDTlep;
  HG::catMass_Run1 catMass_Run1;
  HG::catMass_pT catMass_pT;
  HG::catMass_eta catMass_eta;
  HG::catMass_conv catMass_conv;
  HG::catMass_mu catMass_mu;
  HG::catXS_VBF catXS_VBF;
  HG::numberOfPrimaryVertices numberOfPrimaryVertices;
  HG::selectedVertexSumPt2 selectedVertexSumPt2;
  HG::selectedVertexZ selectedVertexZ;
  HG::selectedVertexPhi selectedVertexPhi;
  HG::hardestVertexSumPt2 hardestVertexSumPt2;
  HG::hardestVertexZ hardestVertexZ;
  HG::hardestVertexPhi hardestVertexPhi;
  HG::pileupVertexSumPt2 pileupVertexSumPt2;
  HG::pileupVertexZ pileupVertexZ;
  HG::pileupVertexPhi pileupVertexPhi;
  HG::zCommon zCommon;
  HG::vertexZ vertexZ;
  HG::catCoup catCoup;
  HG::procCoup procCoup;
  HG::mu mu;
  HG::isVyyOverlap isVyyOverlap;

  HG::weightCatCoup_SFXGBoost_ttH weightCatCoup_SFXGBoost_ttH;
  HG::weightCatCoup_XGBoost_ttH weightCatCoup_XGBoost_ttH;
  HG::catCoup_XGBoost_ttH catCoup_XGBoost_ttH;
  HG::score_ttH score_ttH;

  HG::weightCatCoup_SFXGBoost_ttH_topReco weightCatCoup_SFXGBoost_ttH_topReco;
  HG::weightCatCoup_XGBoost_ttH_topReco weightCatCoup_XGBoost_ttH_topReco;
  HG::catCoup_XGBoost_ttH_topReco catCoup_XGBoost_ttH_topReco;
  HG::score_ttH_topReco score_ttH_topReco;
}
