// Local includes
#include "HGamAnalysisFramework/HGamXSecTool.h"
#include "HGamAnalysisFramework/HGamVariables.h"

namespace HG {

  //____________________________________________________________________________
  HGamXSecTool::HGamXSecTool(const std::string &name)
    : asg::AsgTool(name)
  { }

  //____________________________________________________________________________
  HGamXSecTool::~HGamXSecTool()
  { }

  //____________________________________________________________________________
  StatusCode HGamXSecTool::initialize()
  {
    ATH_MSG_INFO("Initializing PhotonVertexSelectionTool...");

    return StatusCode::SUCCESS;
  }

  //____________________________________________________________________________
  StatusCode HGamXSecTool::saveSystematicVariables(xAOD::PhotonContainer *    /*photons*/,
                                                   xAOD::ElectronContainer *  /*electrons*/,
                                                   xAOD::MuonContainer *      /*muons*/,
                                                   xAOD::JetContainer *       /*jets*/,
                                                   xAOD::MissingETContainer * /*mets*/)
  {
    saveBaselineVariables(false);

    return StatusCode::SUCCESS;
  }

  //____________________________________________________________________________
  StatusCode HGamXSecTool::saveStandardVariables(xAOD::PhotonContainer *    /*photons*/,
                                                 xAOD::ElectronContainer *  /*electrons*/,
                                                 xAOD::MuonContainer *      /*muons*/,
                                                 xAOD::JetContainer *       /*jets*/,
                                                 xAOD::MissingETContainer * /*mets*/)
  {
    saveBaselineVariables(false);

    return StatusCode::SUCCESS;
  }

  //____________________________________________________________________________
  StatusCode HGamXSecTool::saveDetailedVariables(xAOD::PhotonContainer *    /*photons*/,
                                                 xAOD::ElectronContainer *  /*electrons*/,
                                                 xAOD::MuonContainer *      /*muons*/,
                                                 xAOD::JetContainer *       /*jets*/,
                                                 xAOD::MissingETContainer * /*mets*/)
  {
    // Currently the standard detailed variables are sufficient
    return StatusCode::SUCCESS;
  }

  //____________________________________________________________________________
  StatusCode HGamXSecTool::saveTruthVariables(xAOD::TruthParticleContainer * /*photons*/,
                                              xAOD::TruthParticleContainer * /*electrons*/,
                                              xAOD::TruthParticleContainer * /*muons*/,
                                              xAOD::JetContainer *           /*jets*/,
                                              xAOD::MissingETContainer *     /*mets*/)
  {
    saveBaselineVariables(true);

    return StatusCode::SUCCESS;
  }

  //____________________________________________________________________________
  StatusCode HGamXSecTool::saveBaselineVariables(bool isTruth)
  {
    // Fit variable
    var::m_yy.addToStore(isTruth);

    // Fiducial regions
    var::catXS_VBF.addToStore(isTruth);
    var::N_lep_15.addToStore(isTruth);

    // Differential variables
    var::pT_yy.addToStore(isTruth);
    var::yAbs_yy.addToStore(isTruth);
    var::cosTS_yy.addToStore(isTruth);

    var::N_j_30.addToStore(isTruth);
    var::N_j_50.addToStore(isTruth);
    var::pT_j1_30.addToStore(isTruth);
    var::pT_j2_30.addToStore(isTruth);
    var::pT_j3_30.addToStore(isTruth);
    var::yAbs_j1_30.addToStore(isTruth);
    var::yAbs_j2_30.addToStore(isTruth);
    var::HT_30.addToStore(isTruth);
    var::m_jj_30.addToStore(isTruth);

    var::Dy_j_j_30.addToStore(isTruth);
    var::Dphi_j_j_30.addToStore(isTruth);
    var::Dphi_j_j_30_signed.addToStore(isTruth);
    var::Dy_yy_jj_30.addToStore(isTruth);

    var::pTt_yy.addToStore(isTruth);
    var::Dy_y_y.addToStore(isTruth);
    var::sumTau_yyj_30.addToStore(isTruth);
    var::maxTau_yyj_30.addToStore(isTruth);
    var::pT_yyjj_30.addToStore(isTruth);

    return StatusCode::SUCCESS;
  }
} // namespace HG
