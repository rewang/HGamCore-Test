#!/bin/bash

# Setup ATLAS local ROOT base if necessary
[ "`compgen -a | grep localSetupROOT`x" == "x" ] \
   && echo "Going to set up ATLAS local ROOT base from cvmfs" \
   && export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase \
   && source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh --quiet 1>/dev/null 2>&1

START=`pwd`

# Change to the compilation directory
cd packages/

# RootCore configs
export CERN_USER="$USER"
export RUCIO_ACCOUNT="$USER"
export ROOTCORE_NCPUS=4
export ROOTCORE_AUTHOR="HGAM_USER_TAG"
 
base="rcSetup Base,HGAM_BASE_VERSION"
[ -d "RootCoreBin" ] \
  && base="rcSetup"

# Full setup string
echo lsetup fax panda \"${base}\"
lsetup fax panda "${base}"

cd $START

# A few useful aliases after things are setup
alias vprox='voms-proxy-init -voms atlas -valid 96:0'
alias oprox='voms-proxy-init -voms atlas:/atlas/phys-higgs/Role=production -valid 96:0'

