#!/bin/bash

origdir=${PWD}

#--- Find basic working paths
cd $ROOTCOREBIN/../
WorkArea=${PWD}
HGamDir=${WorkArea}/HGamAnalysisFramework

#--- Get release
Release=""
[ "$ROOTCORE_RELEASE_SERIES" == "23" ] && Release="Rel20p1"
[ "$ROOTCORE_RELEASE_SERIES" == "24" ] && Release="Rel20p7"

#--- Exit gracefully if script can't determine release version
[ "x$Release" == "x" ] && echo "You aren't running in 2.3.X or 2.4.X, the only versions supported by this tag." && echo "Exiting without finishing!" && return 1

#--- Setup HGamAnalysisFramework for proper release
echo "Setting up HGamAnalysisFramework to compile with ${Release}"

# Get a list of the packages which will be checked out locally
localpacks=""
for line in `cat ${HGamDir}/data/packages${Release}.txt`; do
  pack=${line%/trunk}
  pack=${pack##*/}
  pack=${pack%%-*}
  localpacks="$pack $localpacks"
done

# Make a list of those that have already been checked out
templist=`mktemp -t tmp.XXXXXXXXXX`
for pack in $localpacks; do
  if [ -d "${PWD}/${pack}" ]; then
    for line in `cat ${HGamDir}/data/packages${Release}.txt`; do
      if [[ "$line" == *"$pack"* ]]; then
        echo $line >> $templist
      fi
    done
  fi
done

# Setup correct Makefile
cp ${HGamDir}/cmt/Makefile.RootCore.${Release} ${HGamDir}/cmt/Makefile.RootCore

# Update the packages that have already been checked out to the correct tag
rc find_packages > /dev/null 2>&1
rc update ${templist}

# Checkout remaining packages
rc checkout ${HGamDir}/data/packages${Release}.txt

# Add the hhTruthWeightTools package from git
if [ -d "hhTruthWeightTools" ]; then
  cd hhTruthWeightTools
  git pull origin master
else
  git clone https://:@gitlab.cern.ch:8443/johnda/hhTruthWeightTools.git
fi

# Check if any extra packages are lying around from previous tags of this script
echo ""
echo "Checking for packages which were possibly installed by this script"
echo "in the past, but are no longer needed. If you aren't personally"
echo "developing/using these, they should probably be removed:"
echo ""

sorted="false"
for line in `rc find_packages`; do
  # Want to avoid printing packages twice
  if [[ "$line" == *"sorted"* ]]; then sorted="true"; fi
  if [ $sorted == "false" ]; then continue; fi

  reduced=${line#$PWD/}
  if [ "$reduced" == "$line" ]; then continue; fi
  if [ "$reduced" == "HGamAnalysisFramework" ]; then continue; fi
  if [ "$reduced" == "DerivationFramework" ]; then continue; fi
  if [ "$reduced" == "H2Zy" ]; then continue; fi
  if [ "$reduced" == "H2yyMET" ]; then continue; fi
  if [ "$reduced" == "HH2yybb" ]; then continue; fi
  if [ "$reduced" == "HgammaSandbox" ]; then continue; fi
  if [ "$reduced" == "hhTruthWeightTools" ]; then continue; fi
  if [ "$reduced" == "LowHighMyy" ]; then continue; fi
  if [ "$reduced" == "ttH2yy" ]; then continue; fi
  if [ "$reduced" == "HGamTools" ]; then continue; fi

  if [[ "$localpacks" == *"$reduced"* ]]; then continue; fi

  echo "  $reduced"
done

echo ""

cd $origdir
