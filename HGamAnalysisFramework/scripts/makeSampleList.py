#!/usr/bin/env python

import re
import subprocess
import logging
import os
from itertools import chain
import tempfile
from fnmatch import fnmatch
try:
    from rucio.client import didclient
    for logger_name in 'rucio', 'requests', 'urllib3', 'dogpile':
        logging.getLogger(logger_name).setLevel(logging.WARNING)

except ImportError:
    print """rucio is not setup, run
lsetup rucio
"""
    raise

logging.basicConfig(level=logging.INFO)

def get_nevents_sample(client, scope, sample_name):
    list_files = client.list_files(scope, sample_name)
    events = sum(f['events'] for f in list_files)
    return events

def filter_useful_sample(sample_name):
    """ remove log and TAG and NTUP_PILEUP """
    to_remove = '.log', '.TAG.', '.NTUP_PILEUP.'
    return not any(r in sample_name for r in to_remove)


r_dsid = re.compile('\.([0-9]{6})\.')
def parse_sample_name(sample_name):
    m = parse_sample_name.re.search(sample_name)
    if m is None:
        raise ValueError("cannot parse sample %s" % sample_name)
    tags = m.group(3).split("_")
    is_fast = any('a' in tag for tag in tags)
    m_dsid = r_dsid.search(sample_name)
    if not m_dsid:
        raise ValueError('sample %s has no dsid' % sample)
    
    return {'step': m.group(1), 'format': m.group(2), 'tags': tags, 'is_fast': is_fast, 'dsid': m_dsid.group(1)}
parse_sample_name.re = re.compile(r'(simul|recon|merge|deriv)\.(.*?)\.([esarp_0-9]+)')


def sorting_key(sample):
    return ({'EVNT': 0, 'HITS': 1, 'AOD': 2, 'DAOD_HIGG1D1': 3, 'DAOD_HIGG1D2': 4}.get(sample['format'], 999),
            len(sample['tags']))


def remove_previous_steps(samples):
    """ keep only the highest level sample (simul instead of env, reco intead of simul, ...) """

    # right if the one to keep. Keep them in order!
    rules = ((('recon', 'AOD'), ('merge', 'AOD')),
             (('simul', 'HITS'), ('recon', 'AOD')),
             (('merge', 'EVNT'), ('simul', 'HITS')))

    results = []
    for sample in samples:
        for rule_bottom, rule_top in rules:
            if sample['step'] == rule_bottom[0] and sample['format'] == rule_bottom[1]:
                break
        else:  # for loop exits normally, no break
            results.append(sample)
            continue

        for sample_super in samples:
            if sample_super['step'] == rule_top[0] and sample_super['format'] == rule_top[1] and sample_super['tags'][:-1] == sample['tags']:
                logging.debug("removing sample %s since sample %s is present", sample['name'], sample_super['name'])
                break
        else:  # top sample not found
            results.append(sample)

    return results


def get_all_samples(scope, query):
    """ return all samples matching with the required properties """
    dataset_type = "container"

    filters = {'name': query}

    result = list(didclient.DIDClient().list_dids(scope, filters=filters, type=dataset_type, long=True))
    result = filter(lambda r: filter_useful_sample(r['name']), result)
    result = [dict({'name': r['name']}, **parse_sample_name(r['name'])) for r in result]

    result = sorted(result, key=sorting_key)

    return result


def split_full_fast(samples):
    full, fast = [], []
    for sample in samples:
        (full, fast)[sample['is_fast']].append(sample)
    return full, fast


def filter_derivation_type(sample, derivation):
    if sample['step'] == 'deriv':
        return re.search(sample['format'], derivation) is not None
    else:
        return True


def is_ptag(sample, ptag):
    return ptag in sample


def is_ptags(sample, ptags):
    """ given a list of ptags filter samples with the right ptags """
    if ptags is None:
        return True
    return any(is_ptag(sample, ptag) for ptag in ptags)

def is_blacklisted(sample, blacklist):
    if any(fnmatch(sample, b) for b in blacklist):
        logging.debug("sample %s matching blacklist" % sample)
        return True
    return False

def remove_duplicate_ptags(samples, ptags_sets):
    # keep only derivations
    samples = [s for s in samples if s['step'] == 'deriv']

    results = []
    for ptags in ptags_sets:
        for ptag in ptags:
            ptag_set_found = False
            for sample in samples:
                if ptag in sample['tags']:
                    results.append(sample)
                    ptag_set_found = True
            if ptag_set_found:
                # if one sample with one ptag is found
                # do not look for other ptags
                # (ptags are sorted by priority)
                break
    return results
            

def make_file_list(input_fns, suffix, scope, rtags, ptags, derivation_type, blacklist=None, previous_list=None):
    all_ptags = [item for sublist in ptags for item in sublist]

    all_original_query = set()

    if blacklist is not None:
        with open(blacklist) as f:
            blacklist = sorted([x.strip() for x in f.readlines()])
    else:
        blacklist = []

    old_daod= {}
    if previous_list is not None:
        with open(previous_list) as f:
            lines = [line.strip() for line in f.read().split('\n')]
            lines = [line for line in lines if not line=='' and not line.startswith('#')]
            for line in lines:
                l, r = line.split(' ')
                old_daod[l.strip()] = r.strip()

    client = didclient.DIDClient()
    nmissing = 0

    with open('%s_list.txt' % suffix, 'w') as f_derivation, \
         open('%s.txt' % suffix, 'w') as f_derivation_filtered, \
         open('output_all_%s.txt' % suffix, 'w') as f_all, \
         open('output_xAOD_%s.txt' % suffix, 'w') as f_xaod, \
         open('missing_%s.txt' % suffix, 'w') as f_missing:
        for line in chain(*(open(input_fn) for input_fn in input_fns)):
            li = line.strip()
            if li.startswith("#") or not li:
                f_derivation.write(line)
                f_derivation_filtered.write(line)
                f_xaod.write(line)
                f_all.write(line)
                continue

            values = li.split()
            short_sample_name = values[0]

            query = values[1].strip()
            if query in all_original_query:
                logging.error("query %s %s is duplicate", short_sample_name, query)
            all_original_query.add(query)
            if query.isdigit():
                # just the dsid
                query = "*.%s.*" % query
            elif query[-1] != "*":
                query += "*"
            if rtags and not re.search('[_*]r[0-9]{4}', query):
                # if rtags are specified from command line and not present
                # in the list of files use them
                query += rtags + "*"

            all_samples = get_all_samples(scope, query)
            all_samples = remove_previous_steps(all_samples)
            all_samples = [sample for sample in all_samples if not is_blacklisted(sample['name'], blacklist)]
            if len(all_samples) == 0:
                nmissing += 1
                f_missing.write(line)

            # remove derived datasets not the right derivation (e.g. HIGG1D1 only), but keep non-derived samples
            all_samples_right_derivation = filter(lambda x: filter_derivation_type(x, derivation_type), all_samples)
            # remove not needed ptag
            all_samples_right_derivation_ptag_filter = remove_duplicate_ptags(all_samples_right_derivation, ptags)
            all_samples_AOD = [sample for sample in all_samples if sample['format'] == 'AOD']
            if len(all_samples_AOD) == 0:
                logging.warning("no AOD sample for %s", short_sample_name)
            all_samples_AOD_full, all_samples_AOD_fast = split_full_fast(all_samples_AOD)
            if len(all_samples_AOD_full) > 1:
                logging.error("more than one (%d) full samples for %s: %s", len(all_samples_AOD_full), short_sample_name, [s['name'] for s in all_samples_AOD_full])
            if len(all_samples_AOD_fast) > 1:
                logging.error("more than one (%d) fast samples for %s: %s", len(all_samples_AOD_fast), short_sample_name, [s['name'] for s in all_samples_AOD_fast])

            for sample in all_samples_right_derivation:
                short_sample_name_with_af2 = short_sample_name  # in case add _AF2 suffix
                if sample['is_fast'] and "_AF2" not in short_sample_name_with_af2:
                    short_sample_name_with_af2 += "_AF2"
                if sample['step'] == 'deriv':
                    # write only derivation only if for the right ptag (from command line)
                    if is_ptags(sample['name'], all_ptags):
                        if re.search('_r[0-9]+_r[0-9]+', sample['name']):
                            f_derivation.write(short_sample_name_with_af2 + " " + sample['name'] + '\n')
                            if any(sample['name'] == s['name'] for s in all_samples_right_derivation_ptag_filter):
                                if short_sample_name_with_af2 in old_daod:
                                    f_derivation_filtered.write(short_sample_name_with_af2 + " " + old_daod[short_sample_name_with_af2] + '\n')
                                    logging.info("derivation %s found in previous list, skipping", short_sample_name_with_af2)
                                else:
                                    f_derivation_filtered.write(short_sample_name_with_af2 + " " + sample['name'] + '\n')
                        else:
                            logging.warning('it seems that %s is a derivation from recon (not from merge)' % sample['name'])
                if sample['format'] == 'AOD':
                    if sample['step'] == 'merge':
                        # in the AOD file write only the merge (not the recon)
                        f_xaod.write(short_sample_name_with_af2 + " " + sample['name'] + '\n')
                    else:
                        logging.warning("missing merge step for sample %s", sample['name'])
                nevents = get_nevents_sample(client, scope, sample['name'])
                f_all.write("{} {} {}\n".format(short_sample_name_with_af2, sample['name'], nevents))
    if nmissing != 0:
        logging.warning('there are missing samples, see missing_%s.txt', suffix)


def makeCrossSectionList(filename, suffix):
    samples = []
    for line in open(filename):
        line = line.strip()
        if line.startswith("#") or not line:
            continue

        dsid = line.split()[1]
        if ":" in dsid:
            dsid = dsid.split[":"][1]
        samples.append(dsid)

    with tempfile.NamedTemporaryFile(bufsize=0) as tmp_file:
        tmp_file.write('\n'.join(sorted(samples)))
        tmp_file.flush()  # probably not needed thanks to bufsize=0

        try:
            logging.info("calling getMetaData.py")
            subprocess.call(["getMetadata.py", "--inDsTxt", tmp_file.name, "--outFile", "xs_%s.txt" % suffix])
        except OSError:
            print """cannot find getMetadata.py, setup
asetup AthAnalysis,21.2,latest
lsetup PyAMI
"""
            raise


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description="Get DxAOD samples and their cross-sections",
                                     usage="Usage: %(prog)s [options] input.txt",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                     epilog="""
mc16a example (HIGG1D1):
./makeSampleList.py ../data/mc16a_only_samples_HIGG1D1.txt ../data/mc16_samples_HIGG1D1.txt --suffix=mc16a_HIGG1D1 -r9364 --scope mc16_13TeV

mc16c example (HIGG1D1):
./makeSampleList.py ../data/mc16_samples_HIGG1D1.txt --suffix=mc16c_HIGG1D1 -r9781 --scope mc16_13TeV

mc16a example (HIGG1D2):
./makeSampleList.py ../data/mc16_samples_HIGG1D2.txt --suffix=mc16a_HIGG1D2 -r9364 --scope mc16_13TeV

mc16c example (HIGG1D2):
./makeSampleList.py ../data/mc16_samples_HIGG1D2.txt --suffix=mc16c_HIGG1D2 -r9781 --scope mc16_13TeV


""")
    parser.add_argument('filenames', nargs='+', help='input filename(s)')
    parser.add_argument('--suffix', help='suffix to output filenames', default='mc16a')
    parser.add_argument('-r', '--rtag', help='reconstruction tag', default='r9364')
    parser.add_argument('-p', '--ptags', action='append', nargs='*', help='derivation tag, use multiple time this argument, the first ptag on the left have precedence when multiple ptag are present, e.g. --ptags p3415 p3401 p3469 --ptags p3418 p3404 p3472')
    parser.add_argument('-s', '--scope', help='specify the scope', default="mc16_13TeV")
    parser.add_argument('--derivation-type', help='regex to filter derivations', default='DAOD_HIGG1D1|DAOD_HIGG1D2')
    parser.add_argument('--blacklist', help='list of samples to avoid')
    parser.add_argument('--only-update-derivation-list', help='file containing derivation samples that you don\'t want to update. This will affect only the list of derivation samples, if it is present it won\'t be updated. Use with care.')

    args = parser.parse_args()
    if 'r' not in args.rtag:
        args.rtag = 'r' + args.rtag
    if args.ptags is None:
        args.ptags = [['p3500', 'p3469', 'p3415', 'p3401'], ['p3502', 'p3472', 'p3418', 'p3404']]

    logging.info("searching datasets")
    make_file_list(args.filenames, args.suffix, args.scope, args.rtag, args.ptags, args.derivation_type, args.blacklist, args.only_update_derivation_list)
    logging.info("fetching cross metadata")
    makeCrossSectionList('output_xAOD_%s.txt' % args.suffix, args.suffix)
