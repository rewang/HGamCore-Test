#!/bin/bash
function current_tag_too_old()
{
    local __CURRENT_TAG=$1
    local __DESIRED_TAG=$2
    local __current_is_older="1"
    # Check each element in Package-XX-YY-ZZ
    if (( $(echo ${__CURRENT_TAG} | cut -d\- -f2)*1 >= $(echo ${__DESIRED_TAG} | cut -d\- -f2)*1 )); then     # check XX
      if (( $(echo ${__CURRENT_TAG} | cut -d\- -f3)*1 >= $(echo ${__DESIRED_TAG} | cut -d\- -f3)*1 )); then   # check YY
        if (( $(echo ${__CURRENT_TAG} | cut -d\- -f4)*1 >= $(echo ${__DESIRED_TAG} | cut -d\- -f4)*1 )); then # check ZZ
          __current_is_older="0"
        fi
      fi
    fi
    echo ${__current_is_older}
}

#--- Get release
Release=""
[[ "$ROOTCORE_RELEASE_SERIES" == "25" ]] && Release="Rel21"
[[ "$ROOTCORE_RELEASE_SERIES" == "26" ]] && Release="Rel21"

#--- Exit gracefully if script can't determine release version
[[ "${Release}" == "" ]] && echo "You aren't running in 2.6.X the only versions supported with CMake." && echo "Exiting without finishing!" && return 1

#--- Find basic working paths
ORIGINAL_DIR=$(pwd)
SOURCE_DIR=${TestArea}
if [ -d "${SOURCE_DIR}/HGamCore/HGamAnalysisFramework" ]; then
  HGAMDIR=${SOURCE_DIR}/HGamCore/HGamAnalysisFramework
else
  HGAMDIR=${SOURCE_DIR}/HGamAnalysisFramework
fi

#--- Setup HGamAnalysisFramework for proper release
echo "Setting up HGamAnalysisFramework to compile with ${Release}"
cd ${SOURCE_DIR}

# --- Check each of the packages in the required list
KNOWN_PACKAGES=""
for PACKAGE_LOCATION in $(cat ${HGAMDIR}/data/packages${Release}.txt); do
  DESIRED_TAG=$(echo ${PACKAGE_LOCATION} | rev | cut -d'/' -f1 | rev)
  PACKAGE=$(echo ${DESIRED_TAG} | cut -d'-' -f1)
  ALLOW_REMOVAL=0
  echo "Checking for: ${PACKAGE}."
  # Update git packages
  if [[ "${PACKAGE}" == *".git" ]]; then
    PACKAGE=$(echo ${PACKAGE} | sed "s/.git//")
    if [ -d "${PACKAGE}" ]; then
      echo "  => will check for update..."
      cd ${PACKAGE}
      git pull origin master
      cd ..
    else
      echo "  => checking out from git"
      git clone ${PACKAGE_LOCATION}
    fi
  # Check SVN packages
  else
    SVNTEST="$(svnco -a | grep " ${PACKAGE}-")"; rm repofile
    NEEDS_CHECKOUT=1
    if [ "${SVNTEST}" != "" ]; then
      CURRENT_TAG=$(echo ${SVNTEST} | cut -d' ' -f2)
      NEEDS_CHECKOUT=$(current_tag_too_old ${CURRENT_TAG} ${DESIRED_TAG})
      echo "  requested tag:   ${DESIRED_TAG}."
      echo "  release has tag: ${CURRENT_TAG}."
      if [ ${NEEDS_CHECKOUT} -eq 1 ]; then
        echo "  => will check for update..."
      else
        if [ -d ${PACKAGE} ]; then
          ALLOW_REMOVAL=1
          echo " => package already checked out - may need removal"
        else
          echo "  => no action needed - release version is higher than request"
        fi
      fi
    fi

    # If we need checkout, do it here
    if [ ${NEEDS_CHECKOUT} -eq 1 ]; then
      if [ -d ${PACKAGE} ]; then
        cd ${PACKAGE}
        EXISTING_TAG=$(svn info | grep URL | rev | cut -d'/' -f1 | rev)
        NEEDS_CHECKOUT=$(current_tag_too_old ${EXISTING_TAG} ${DESIRED_TAG})
        cd - > /dev/null
        if [ ${NEEDS_CHECKOUT} -eq 1 ]; then
          rm -rf ${PACKAGE}
        else
          echo "  => no action needed - already checked out ${DESIRED_TAG}"
        fi
      fi
      if [ ${NEEDS_CHECKOUT} -eq 1 ]; then
        echo "  => checking out ${DESIRED_TAG}"
        svn co ${SVNOFF}/${PACKAGE_LOCATION} ${PACKAGE}
      fi
    fi
  fi
  # Add to known packages
  if [ ${ALLOW_REMOVAL} -eq 0 ]; then
    KNOWN_PACKAGES="${KNOWN_PACKAGES} ${PACKAGE}"
  fi
done

# Check if any extra packages are lying around from previous tags of this script
echo ""
echo "Checking for packages which were possibly installed by this script"
echo "in the past, but are no longer needed. If you aren't personally"
echo "developing/using these, they should probably be removed:"
echo ""

# Add packages to known package list
KNOWN_PACKAGES="${KNOWN_PACKAGES} DerivationFramework"
KNOWN_PACKAGES="${KNOWN_PACKAGES} HGamAnalysisFramework"
KNOWN_PACKAGES="${KNOWN_PACKAGES} H2Zy"
KNOWN_PACKAGES="${KNOWN_PACKAGES} H2yyMET"
KNOWN_PACKAGES="${KNOWN_PACKAGES} HGamCore"
KNOWN_PACKAGES="${KNOWN_PACKAGES} HGamTools"
KNOWN_PACKAGES="${KNOWN_PACKAGES} HH2yybb"
KNOWN_PACKAGES="${KNOWN_PACKAGES} LowHighMyy"
KNOWN_PACKAGES="${KNOWN_PACKAGES} ttH2yy"

# Check all directories
for LOCAL_DIR in $(ls -d */); do
  if [[ "${KNOWN_PACKAGES}" == *"${LOCAL_DIR%%/}"* ]]; then continue; fi
  echo "  ${LOCAL_DIR%%/}"
done

cd ${ORIGINAL_DIR}
