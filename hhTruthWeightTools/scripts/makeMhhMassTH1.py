inFile = open("scripts/mhh_all_Kfactors_20GeV.txt","r")

import ROOT

hMhhWeight = ROOT.TH1F("mhh_kfactor","mhh_kfactor", 33, 270., 930.)

iBin = 0

for line in inFile:
    words = line.split()
    if not len(words): continue
    if line[0] == "#": continue
    
    kfactor    = float(words[1])
    kfactorErr = float(words[2])
    print iBin, kfactor, kfactorErr

    hMhhWeight.SetBinContent(iBin,kfactor) 
    hMhhWeight.SetBinError  (iBin,kfactorErr) 
    iBin +=1


outFile = ROOT.TFile("SMhh_mhh_kfactor.root","RECREATE")
hMhhWeight.Write()
outFile.Close()

for i in range(hMhhWeight.GetNbinsX()+1):
    print hMhhWeight.GetBinLowEdge(i)+hMhhWeight.GetBinWidth(i) ,"--->",hMhhWeight.GetBinContent(i)


print hMhhWeight.GetBinContent(hMhhWeight.GetNbinsX()+1)
