import optparse
parser = optparse.OptionParser()
parser.add_option('-r', '--refernceFileName',     dest="refFileName",         default=None, help="Run in loop mode")
parser.add_option('-t', '--targetFileName',       dest="tarFileName",         default=None, help="Run in loop mode")
parser.add_option('-o', '--outFileName',          dest="outfileName",        default=None, help="Run in loop mode")
o, a = parser.parse_args()


import ROOT

numFile = ROOT.TFile(o.tarFileName, "READ")
denFile = ROOT.TFile(o.refFileName, "READ")

h_num = numFile.Get("mhh")
h_den = denFile.Get("mhh")

h_ratio = h_num.Clone("mhh_kfactor")
h_ratio.Divide(h_den)

outFile = ROOT.TFile(o.outfileName,"RECREATE")
outFile.cd()
h_ratio.Write()
outFile.Close()

