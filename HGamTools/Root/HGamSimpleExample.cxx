#include "HGamTools/HGamSimpleExample.h"
#include "HGamAnalysisFramework/HgammaIncludes.h"
#include "HGamAnalysisFramework/HGamVariables.h"

// this is needed to distribute the algorithm to the workers
ClassImp(HGamSimpleExample)



HGamSimpleExample::HGamSimpleExample(const char *name)
  : HgammaAnalysis(name)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
}



HGamSimpleExample::~HGamSimpleExample()
{
  // Here you delete any memory you allocated during your analysis.
}



EL::StatusCode HGamSimpleExample::createOutput()
{
  // Here you setup the histograms needed for you analysis. This method
  // gets called after the Handlers are initialized, so that the systematic
  // registry is already filled.

  m_muRanges = {0.0, 10.0, 20.0, 30.0};
  m_muNames = {"0_10", "10_20", "20_30"};

  // We'll look at pT_yy for spin-2
  for (size_t i = 0; i < m_muNames.size(); ++i) {
    std::string name = "pT_yy" + m_muNames[i];
    histoStore()->createTH1F(name.c_str(), 100, 0, 500);
  }

  // We'll check phi_y1 for spin-0
  histoStore()->createTH1F("phi_y1", 50, -TMath::PiOver2(), TMath::PiOver2());

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode HGamSimpleExample::execute()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  // Important to keep this, so that internal tools / event variables
  // are filled properly.
  HgammaAnalysis::execute();

  // In MxAODs, this forces the updating of the pileup weight based on configured lumicalc file (MC only)
  setSelectedObjects();

  // Retrieve various event variables
  bool isPassedLowHighMyy = eventHandler()->getVar<char>("isPassedLowHighMyy");
  bool isPassedExotic = eventHandler()->getVar<char>("isPassedExotic");
  bool isPassedIsolationLowHighMyy = eventHandler()->getVar<char>("isPassedIsolationLowHighMyy");

  // Consider spin-2 tight selection
  if (isPassedExotic && isPassedIsolationLowHighMyy) {
    // Grab mu, which is updated using the configured lumicalc file
    double mu = eventHandler()->mu();

    // Check which mu range to fill
    for (size_t i = 0; i < m_muNames.size(); ++i) {
      std::string name = "pT_yy" + m_muNames[i];

      if (m_muRanges[i] < mu && mu <= m_muRanges[i + 1])
      { histoStore()->fillTH1F(name.c_str(), var::pT_yy() / HG::GeV, weight()); }
    }
  }

  // Consider spin-0 selection
  if (isPassedLowHighMyy) {
    // When running over an MxAOD this returns the corrected AND selected photons
    xAOD::PhotonContainer photons = photonHandler()->getCorrectedContainer();

    histoStore()->fillTH1F("phi_y1", photons[0]->phi());
  }

  return EL::StatusCode::SUCCESS;
}
