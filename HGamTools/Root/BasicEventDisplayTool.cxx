#include "HGamTools/BasicEventDisplayTool.h"


/******************************************************************************//**
* Default constructor
**********************************************************************************/
BasicEventDisplayTool::BasicEventDisplayTool()
{
  m_canvas = 0;
  m_transverseFrame = 0;
  m_sideFrame = 0;
  m_angularFrame = 0;
}


/******************************************************************************//**
* Destructor
**********************************************************************************/
BasicEventDisplayTool::~BasicEventDisplayTool()
{
  delete m_angularFrame;
  delete m_sideFrame;
  delete m_transverseFrame;
  delete m_canvas;
}



/******************************************************************************//**
* Initialise the parameters according to the config file and creates the pdf file
**********************************************************************************/
void BasicEventDisplayTool::initialize(InputSettings *config)
{
  //Style for canvas
  double tsize = 0.035; // text size
  gStyle->SetTextSize(tsize);
  gStyle->SetLabelSize(tsize, "x");
  gStyle->SetTitleSize(tsize, "x");
  gStyle->SetLabelSize(tsize, "y");
  gStyle->SetTitleSize(tsize, "y");

  //Margins
  gStyle->SetPadLeftMargin(0.1);
  gStyle->SetPadRightMargin(0.04);
  gStyle->SetPadBottomMargin(0.12);
  gStyle->SetPadTopMargin(0.04);
  gStyle->SetOptTitle(0);
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(0);

  //Axis
  gStyle->SetTitleXOffset(1.3);
  gStyle->SetTitleYOffset(1.3);
  gStyle->SetTickLength(0.02, "X");
  gStyle->SetTickLength(0.02, "Y");

  //Creates the canvas
  m_pdf = config->getStr("PDFfile", "Events.pdf");
  m_canvas = new TCanvas("", "", 500, 500);
  m_canvas->Print(m_pdf + "[");

  //Creates the histograms
  m_transverseFrame = new TH1F("transverse", ";#it{p}_{x} [GeV]; #it{p}_{y} [GeV]", 200, -TRANSVERSE_RANGE, TRANSVERSE_RANGE);
  m_sideFrame = new TH1F("side", ";#it{p}_{z} [GeV]; #it{p}_{T} [GeV]", 200, -SIDE_RANGE, SIDE_RANGE);
  m_angularFrame = new TH1F("angular", "; #it{y}; #it{#phi}", 200, -ANGULAR_RANGE, ANGULAR_RANGE);
  m_blankFrame = new TH1F("text", "", 200, 0, 1);

  //Sets drawing options
  m_numEventsToDraw = config->getInt("NeventsToDraw", 5);

  if (m_numEventsToDraw < 1) { //Ensures the number of events to draw is valid
    printf("ERROR: Number of events to draw out of range.\n");
    return;
  }

  m_drawDetector = config->getBool("DrawDetector", false);

  StrV defaultReferential;
  defaultReferential.emplace_back("Laboratory");
  m_referentials  = config->getStrV("ReferenceFrame", defaultReferential);

  m_drawTransverse = config->getBool("DrawTransverse", true);
  m_drawSide = config->getBool("DrawSide", true);
  m_drawAngular = config->getBool("DrawAngular", true);
  m_printText = config->getBool("PrintText", false);

  m_drawEventInfo = config->getBool("DrawEventInfo", true);
  m_drawObjectsInfo = config->getBool("DrawObjectsInfo", true);

  m_useDynamicZoom = config->getBool("DynamicZoom", true);
}



/******************************************************************************//**
* Initialise the parameters using a config file name
**********************************************************************************/
void BasicEventDisplayTool::initialize(const TString &configFileName)
{
  InputSettings *settings = new InputSettings(configFileName);
  initialize(settings);
}



/******************************************************************************//**
* Initialise the parameters using a TEnv variable
**********************************************************************************/
void BasicEventDisplayTool::initialize(const TEnv *env)
{
  InputSettings *settings = new InputSettings(env);
  initialize(settings);
}



/******************************************************************************//**
* Draws the event using a struct if the number of event drawn is smaller that the
*  number to draw
**********************************************************************************/
void BasicEventDisplayTool::draw(EventDrawingInput &input, const StrV lines)
{
  static int NumEventDrawn = 0;
  EventDrawingInput labFrameInput = input;

  if (NumEventDrawn < m_numEventsToDraw) {
    NumEventDrawn++;

    for (TString referential : m_referentials) {
      //Ensures that particles and detector are in laboratory frame before boost
      input = labFrameInput;
      resetDetector();

      //Apply boost to particles, if needed
      if (referential == "Laboratory");

      else if (referential == "DiphotonZBoost")
        if (input.photons.size() > 1)
        { applyZBoost(input, (input.photons[0] + input.photons[1]).Rapidity()); }
        else {
          printf("WARNING: Not enough photons for this boost: %s\n", referential.Data());
          referential = "Laboratory";
        }

      else if (referential == "DiphotonFullBoost")
        if (input.photons.size() > 1)
        { applyFullBoost(input, input.photons[0] + input.photons[1]); }
        else {
          printf("WARNING: Not enough photons for this boost: %s\n", referential.Data());
          referential = "Laboratory";
        }

      else if (referential == "ParticlesZBoost") {
        TLorentzVector boostVector;

        for (auto photon : input.photons)
        { boostVector += photon; }

        for (auto jet : input.jets)
        { boostVector += jet; }

        for (auto electron : input.electrons)
        { boostVector += electron; }

        for (auto muon : input.muons)
        { boostVector += muon; }

        applyZBoost(input, boostVector.Rapidity());
      }

      else if (referential == "ParticlesFullBoost") {
        TLorentzVector boostVector;

        for (auto photon : input.photons)
        { boostVector += photon; }

        for (auto jet : input.jets)
        { boostVector += jet; }

        for (auto electron : input.electrons)
        { boostVector += electron; }

        for (auto muon : input.muons)
        { boostVector += muon; }

        applyFullBoost(input, boostVector);
      }

      else {
        printf("WARNING: invalid referential from InputSettings. Default referential is Laboratory frame.\n");
        referential = "Laboratory";
      }

      //Calculates object imbalance
      input.objectImbalance = getObjectImbalance(input);

      //Data transverse view, if requested
      if (m_drawTransverse) {
        drawTransverseEvent(input);

        if (m_drawEventInfo) { drawEventInfo(input.textLines, referential, input.photons, input.electrons, input.muons); }

        if (m_drawObjectsInfo) { drawVectorsInfo(input); }

        m_canvas->Print(m_pdf);
      }

      //Data side view, if requested
      if (m_drawSide) {
        drawSideEvent(input, referential == "Laboratory");

        if (m_drawEventInfo) { drawEventInfo(input.textLines, referential, input.photons, input.electrons, input.muons); }

        if (m_drawObjectsInfo) { drawVectorsInfo(input); }

        m_canvas->Print(m_pdf);
      }

      //Data angular view, if requested
      if (m_drawAngular) {
        drawAngularEvent(input);

        if (m_drawEventInfo) { drawEventInfo(input.textLines, referential, input.photons, input.electrons, input.muons); }

        if (m_drawObjectsInfo) { drawVectorsInfo(input); }

        m_canvas->Print(m_pdf);
      }

      //Print text, if requested
      if (m_printText) {
        m_blankFrame->Draw();
        printText(lines);
        m_canvas->Print(m_pdf);
      }
    }
  }
}



/******************************************************************************//**
* Draws the event both at data and particle level using a struct if the number of
*  event drawn is smaller that the number to draw
**********************************************************************************/
void BasicEventDisplayTool::draw(EventDrawingInput &input, EventDrawingInput &truthinput, const StrV lines)
{
  static int NumEventDrawn = 0;
  EventDrawingInput labFrameInput = input;
  EventDrawingInput labFrameTruthInput = truthinput;

  if (NumEventDrawn < m_numEventsToDraw) {
    NumEventDrawn++;

    for (TString referential : m_referentials) {
      //Ensures that particles and detector are in laboratory frame before boost
      input = labFrameInput;
      truthinput = labFrameTruthInput;
      resetDetector();

      //Apply boost to particles, if needed
      if (referential == "Laboratory");

      else if (referential == "DiphotonZBoost")
        if (input.photons.size() > 1 && truthinput.photons.size() > 1) {
          applyZBoost(input, (input.photons[0] + input.photons[1]).Rapidity());
          applyZBoost(truthinput, (truthinput.photons[0] + truthinput.photons[1]).Rapidity());
        } else {
          printf("WARNING: Not enough photons for this boost: %s\n", referential.Data());
          referential = "Laboratory";
        }

      else if (referential == "DiphotonFullBoost")
        if (input.photons.size() > 1 && truthinput.photons.size() > 1) {
          applyFullBoost(input, input.photons[0] + input.photons[1]);
          applyFullBoost(truthinput, truthinput.photons[0] + truthinput.photons[1]);
        } else {
          printf("WARNING: Not enough photons for this boost: %s\n", referential.Data());
          referential = "Laboratory";
        }

      else if (referential == "ParticlesZBoost") {
        TLorentzVector boostVector, truthboostVector;

        for (auto photon : input.photons)
        { boostVector += photon; }

        for (auto jet : input.jets)
        { boostVector += jet; }

        for (auto electron : input.electrons)
        { boostVector += electron; }

        for (auto muon : input.muons)
        { boostVector += muon; }

        applyZBoost(input, boostVector.Rapidity());

        for (auto photon : truthinput.photons)
        { truthboostVector += photon; }

        for (auto jet : truthinput.jets)
        { truthboostVector += jet; }

        for (auto electron : truthinput.electrons)
        { truthboostVector += electron; }

        for (auto muon : truthinput.muons)
        { truthboostVector += muon; }

        applyZBoost(truthinput, truthboostVector.Rapidity());
      }

      else if (referential == "ParticlesFullBoost") {
        TLorentzVector boostVector, truthboostVector;

        for (auto photon : input.photons)
        { boostVector += photon; }

        for (auto jet : input.jets)
        { boostVector += jet; }

        for (auto electron : input.electrons)
        { boostVector += electron; }

        for (auto muon : input.muons)
        { boostVector += muon; }

        applyFullBoost(input, boostVector);

        for (auto photon : truthinput.photons)
        { truthboostVector += photon; }

        for (auto jet : truthinput.jets)
        { truthboostVector += jet; }

        for (auto electron : truthinput.electrons)
        { truthboostVector += electron; }

        for (auto muon : truthinput.muons)
        { truthboostVector += muon; }

        applyFullBoost(truthinput, truthboostVector);
      }

      else {
        printf("WARNING: invalid referential from InputSettings. Default referential is Laboratory frame.\n");
        referential = "Laboratory";
      }

      //Calculates object imbalance
      input.objectImbalance = getObjectImbalance(input);
      truthinput.objectImbalance = getObjectImbalance(truthinput);

      //Data transverse view, if requested
      if (m_drawTransverse) {
        drawTransverseEvent(input);

        if (m_drawEventInfo) { drawEventInfo(input.textLines, referential, input.photons, input.electrons, input.muons); }

        if (m_drawObjectsInfo) { drawVectorsInfo(input); }

        m_canvas->Print(m_pdf);

        //Truth transverse view
        drawTransverseEvent(truthinput);

        if (m_drawEventInfo) { drawEventInfo(truthinput.textLines, referential, truthinput.photons, truthinput.electrons, truthinput.muons); }

        if (m_drawObjectsInfo) { drawVectorsInfo(truthinput); }

        m_canvas->Print(m_pdf);
      }

      //Data side view, if requested
      if (m_drawSide) {
        drawSideEvent(input, referential == "Laboratory");

        if (m_drawEventInfo) { drawEventInfo(input.textLines, referential, input.photons, input.electrons, input.muons); }

        if (m_drawObjectsInfo) { drawVectorsInfo(input); }

        m_canvas->Print(m_pdf);

        //Truth side view
        drawSideEvent(truthinput, referential == "Laboratory");

        if (m_drawEventInfo) { drawEventInfo(truthinput.textLines, referential, truthinput.photons, truthinput.electrons, truthinput.muons); }

        if (m_drawObjectsInfo) { drawVectorsInfo(truthinput); }

        m_canvas->Print(m_pdf);
      }

      //Data angular view, if requested
      if (m_drawAngular) {
        drawAngularEvent(input);

        if (m_drawEventInfo) { drawEventInfo(input.textLines, referential, input.photons, input.electrons, input.muons); }

        if (m_drawObjectsInfo) { drawVectorsInfo(input); }

        m_canvas->Print(m_pdf);

        //Truth angular view
        drawAngularEvent(truthinput);

        if (m_drawEventInfo) { drawEventInfo(truthinput.textLines, referential, truthinput.photons, truthinput.electrons, truthinput.muons); }

        if (m_drawObjectsInfo) { drawVectorsInfo(truthinput); }

        m_canvas->Print(m_pdf);
      }

      //Print text, if requested
      if (m_printText) {
        m_blankFrame->Draw();
        printText(lines);
        m_canvas->Print(m_pdf);
      }
    }
  }
}



/******************************************************************************//**
* Draws the event using vectors of TLorentzVector for photons and jets if the
*  number of event drawn is smaller that the number to draw
**********************************************************************************/
void BasicEventDisplayTool::draw(TLVs &photons, TLVs &jets, const StrV &textLines, const StrV lines)
{
  EventDrawingInput input;

  input.photons   = photons;
  input.jets      = jets;
  input.textLines = textLines;

  draw(input, lines);
}



/******************************************************************************//**
* Draws the event both at data and particle level using vectors of TLorentzVector
*  for photons and jets if the number of event drawn is smaller that the number
*  to draw
**********************************************************************************/
void BasicEventDisplayTool::draw(TLVs &photons, TLVs &jets, const StrV &textLines, TLVs &truthphotons, TLVs &truthjets, const StrV &truthtextLines, const StrV lines)
{
  EventDrawingInput input, truthinput;

  input.photons = photons;
  input.jets = jets;
  input.textLines = textLines;

  truthinput.photons = truthphotons;
  truthinput.jets = truthjets;
  truthinput.textLines = truthtextLines;

  draw(input, truthinput, lines);
}



/******************************************************************************//**
* Draws the event using vectors of TLorentzVector for photons, jets and met if the
*  number of event drawn is smaller that the number to draw
**********************************************************************************/
void BasicEventDisplayTool::draw(TLVs &photons, TLVs &jets, const TLorentzVector &met, const StrV &textLines, const StrV lines)
{
  EventDrawingInput input;

  input.photons = photons;
  input.jets = jets;
  input.met = met;
  input.textLines = textLines;

  draw(input, lines);
}



/******************************************************************************//**
* Draws the event both at data and particle level using vectors of TLorentzVector
*  for photons, jets and met if the number of event drawn is smaller that the
*  number to draw
**********************************************************************************/
void BasicEventDisplayTool::draw(TLVs &photons, TLVs &jets, const TLorentzVector &met, const StrV &textLines, TLVs &truthphotons, TLVs &truthjets, const TLorentzVector &truthmet, const StrV &truthtextLines, const StrV lines)
{
  EventDrawingInput input, truthinput;

  input.photons = photons;
  input.jets = jets;
  input.met = met;
  input.textLines = textLines;

  truthinput.photons = truthphotons;
  truthinput.jets = truthjets;
  truthinput.met = truthmet;
  truthinput.textLines = truthtextLines;

  draw(input, truthinput, lines);
}



/******************************************************************************//**
* Draws the event using vectors of TLorentzVector for all particles if the
*  number of event drawn is smaller that the number to draw
**********************************************************************************/
void BasicEventDisplayTool::draw(TLVs &photons, TLVs &jets, TLVs &electrons, TLVs &muons, const TLorentzVector &met, const StrV &textLines, const StrV lines)
{
  EventDrawingInput input;

  input.photons = photons;
  input.jets = jets;
  input.electrons = electrons;
  input.muons = muons;
  input.met = met;
  input.textLines = textLines;

  draw(input, lines);
}



/******************************************************************************//**
* Draws the event both at data and particle level using vectors of TLorentzVector
*  for all particles if the number of event drawn is smaller that the number
*  to draw
**********************************************************************************/
void BasicEventDisplayTool::draw(TLVs &photons, TLVs &jets, TLVs &electrons, TLVs &muons, const TLorentzVector &met, const StrV &textLines, TLVs &truthphotons, TLVs &truthjets, TLVs &truthelectrons, TLVs &truthmuons, const TLorentzVector &truthmet, const StrV &truthtextLines, const StrV lines)
{
  EventDrawingInput input, truthinput;

  input.photons = photons;
  input.jets = jets;
  input.electrons = electrons;
  input.muons = muons;
  input.met = met;
  input.textLines = textLines;

  truthinput.photons = truthphotons;
  truthinput.jets = truthjets;
  truthinput.electrons = truthelectrons;
  truthinput.muons = truthmuons;
  truthinput.met = truthmet;
  truthinput.textLines = truthtextLines;

  draw(input, truthinput, lines);
}



/******************************************************************************//**
* Closes the pdf file
**********************************************************************************/
void BasicEventDisplayTool::finalize()
{
  m_canvas->Print(m_pdf + "]");
}



/******************************************************************************//**
* Apply boost in Z only to particles and detector
**********************************************************************************/
void BasicEventDisplayTool::applyZBoost(EventDrawingInput &input, const Double_t &zBoost)
{
  //Apply the boost to particles and met
  for (auto &photon : input.photons)
  { photon.SetPtEtaPhiM(photon.Pt(), photon.Eta() - zBoost, photon.Phi(), photon.M()); }

  for (auto &jet : input.jets)
  { jet.SetPtEtaPhiM(jet.Pt(), jet.Eta() - zBoost, jet.Phi(), jet.M()); }

  for (auto &electron : input.electrons)
  { electron.SetPtEtaPhiM(electron.Pt(), electron.Eta() - zBoost, electron.Phi(), electron.M()); }

  for (auto &muon : input.muons)
  { muon.SetPtEtaPhiM(muon.Pt(), muon.Eta() - zBoost, muon.Phi(), muon.M()); }

  input.met.SetPtEtaPhiM(input.met.Pt(), input.met.Eta() - zBoost, input.met.Phi(), input.met.M());

  //Resets the detector
  resetDetector();

  //Apply the boost to the detector
  m_detector.barrelEtaLeft -= zBoost;
  m_detector.barrelEtaRight -= zBoost;
  m_detector.endcapMinEtaLeft -= zBoost;
  m_detector.endcapMaxEtaLeft -= zBoost;
  m_detector.endcapMinEtaRight -= zBoost;
  m_detector.endcapMaxEtaRight -= zBoost;
  m_detector.hecMinEtaLeft -= zBoost;
  m_detector.hecMaxEtaLeft -= zBoost;
  m_detector.hecMinEtaRight -= zBoost;
  m_detector.hecMaxEtaRight -= zBoost;
  m_detector.fcalMinEtaLeft -= zBoost;
  m_detector.fcalMaxEtaLeft -= zBoost;
  m_detector.fcalMinEtaRight -= zBoost;
  m_detector.fcalMaxEtaRight -= zBoost;
}



/******************************************************************************//**
* Apply boost to particles and detector
**********************************************************************************/
void BasicEventDisplayTool::applyFullBoost(EventDrawingInput &input, const TLorentzVector &boost)
{
  //Apply the boost to particles and met
  for (auto &photon : input.photons)
  { photon.Boost(-boost.BoostVector()); }

  for (auto &jet : input.jets)
  { jet.Boost(-boost.BoostVector()); }

  for (auto &electron : input.electrons)
  { electron.Boost(-boost.BoostVector()); }

  for (auto &muon : input.muons)
  { muon.Boost(-boost.BoostVector()); }

  input.met.Boost(-boost.BoostVector());

  //Resets the detector
  resetDetector();

  //Apply the boost to the detector
  m_detector.barrelEtaLeft -= boost.Rapidity();
  m_detector.barrelEtaRight -= boost.Rapidity();
  m_detector.endcapMinEtaLeft -= boost.Rapidity();
  m_detector.endcapMaxEtaLeft -= boost.Rapidity();
  m_detector.endcapMinEtaRight -= boost.Rapidity();
  m_detector.endcapMaxEtaRight -= boost.Rapidity();
  m_detector.hecMinEtaLeft -= boost.Rapidity();
  m_detector.hecMaxEtaLeft -= boost.Rapidity();
  m_detector.hecMinEtaRight -= boost.Rapidity();
  m_detector.hecMaxEtaRight -= boost.Rapidity();
  m_detector.fcalMinEtaLeft -= boost.Rapidity();
  m_detector.fcalMaxEtaLeft -= boost.Rapidity();
  m_detector.fcalMinEtaRight -= boost.Rapidity();
  m_detector.fcalMaxEtaRight -= boost.Rapidity();
}



/******************************************************************************//**
* Resets the detector geometry to the laboratory frame
**********************************************************************************/
void BasicEventDisplayTool::resetDetector()
{
  m_detector.barrelEtaLeft = -BARREL_ETA;
  m_detector.barrelEtaRight = BARREL_ETA;
  m_detector.endcapMinEtaLeft = -ENDCAP_MIN_ETA;
  m_detector.endcapMaxEtaLeft = -ENDCAP_MAX_ETA;
  m_detector.endcapMinEtaRight = ENDCAP_MIN_ETA;
  m_detector.endcapMaxEtaRight = ENDCAP_MAX_ETA;
  m_detector.hecMinEtaLeft = -HEC_MIN_ETA;
  m_detector.hecMaxEtaLeft = -HEC_MAX_ETA;
  m_detector.hecMinEtaRight = HEC_MIN_ETA;
  m_detector.hecMaxEtaRight = HEC_MAX_ETA;
  m_detector.fcalMinEtaLeft = -FCAL_MIN_ETA;
  m_detector.fcalMaxEtaLeft = -FCAL_MAX_ETA;
  m_detector.fcalMinEtaRight = FCAL_MIN_ETA;
  m_detector.fcalMaxEtaRight = FCAL_MAX_ETA;
}



/******************************************************************************//**
* Calculates the missing energy TLorentzVector
**********************************************************************************/
TLorentzVector BasicEventDisplayTool::getObjectImbalance(const EventDrawingInput &input)
{
  TLorentzVector objectImbalance;

  for (auto photon : input.photons)
  { objectImbalance -= photon; }

  for (auto jet : input.jets)
  { objectImbalance -= jet; }

  for (auto electron : input.electrons)
  { objectImbalance -= electron; }

  for (auto muon : input.muons)
  { objectImbalance -= muon; }

  return objectImbalance;
}



/******************************************************************************//**
* Calculates the invariant mass of the diphoton
**********************************************************************************/
Double_t BasicEventDisplayTool::getM_yy(const TLVs &photons)
{
  if (photons.size() < 2) {
    printf("WARNING: Unable to calculate diphoton invariant mass.\n");
    return 0.0;
  }

  TLorentzVector p_yy = photons[0] + photons[1];
  Double_t m_yy = TMath::Sqrt(p_yy.E() * p_yy.E() - p_yy.Px() * p_yy.Px() - p_yy.Py() * p_yy.Py() - p_yy.Pz() * p_yy.Pz());

  return m_yy;
}



/******************************************************************************//**
* Calculates the invariant mass of the four leptons
**********************************************************************************/
Double_t BasicEventDisplayTool::getM_4l(const TLVs &electrons, const TLVs &muons)
{
  if ((electrons.size() + muons.size()) < 2) {
    printf("WARNING: Unable to calculate four leptons invariant mass.\n");
    return 0.0;
  }

  //Stores all leptons in a same vector, and sort them
  TLVs leptons;

  for (auto electron : electrons)
  { leptons.emplace_back(electron); }

  for (auto muon : muons)
  { leptons.emplace_back(muon); }

  std::sort(leptons.begin(), leptons.end(), comparePt);

  TLorentzVector p_4l = leptons[0] + leptons[1] + leptons[2] + leptons[3];
  Double_t m_4l = TMath::Sqrt(p_4l.E() * p_4l.E() - p_4l.Px() * p_4l.Px() - p_4l.Py() * p_4l.Py() - p_4l.Pz() * p_4l.Pz());

  return m_4l;
}



/******************************************************************************//**
* Draws the event in the transverse frame (p_y vs. p_x)
**********************************************************************************/
void BasicEventDisplayTool::drawTransverseEvent(const EventDrawingInput &input)
{
  //Sets the range of the frame, if requested
  if (m_useDynamicZoom) {
    double max_pt = 0;

    for (auto photon : input.photons)
      if (photon.Pt() > max_pt)
      { max_pt = photon.Pt(); }

    for (auto jet : input.jets)
      if (jet.Pt() > max_pt)
      { max_pt = jet.Pt(); }

    for (auto electron : input.electrons)
      if (electron.Pt() > max_pt)
      { max_pt = electron.Pt(); }

    for (auto muon : input.muons)
      if (muon.Pt() > max_pt)
      { max_pt = muon.Pt(); }

    if (input.met.Pt() > max_pt)
    { max_pt = input.met.Pt(); }

    if (input.objectImbalance.Pt() > max_pt)
    { max_pt = input.objectImbalance.Pt(); }

    if (max_pt > TRANSVERSE_RANGE * GeV)
    { max_pt = TRANSVERSE_RANGE; }
    else if (max_pt > 50 * GeV) {
      int max = max_pt / (100 * GeV);
      max_pt = max * 100.0 + 100.0;
    } else
    { max_pt = 50; }

    setAxisRange(m_transverseFrame, max_pt, max_pt);
  }

  //Draws the transverse frame
  m_transverseFrame->Draw("0");

  //Draws particles and met
  for (auto jet : input.jets)
  { drawTransverseJet(jet); }

  for (auto photon : input.photons)
  { drawTransversePhoton(photon); }

  for (auto electron : input.electrons)
  { drawTransverseElectron(electron); }

  for (auto muon : input.muons)
  { drawTransverseMuon(muon); }

  if (input.met.Pt() > 10)
  { drawTransverseMet(input.met); }

  if (input.objectImbalance.Pt() > 10)
  { drawTransverseImbalance(input.objectImbalance); }
}



/******************************************************************************//**
* Draws the event in the side frame (p_t vs. p_z, -p_t if p_y < 0)
**********************************************************************************/
void BasicEventDisplayTool::drawSideEvent(const EventDrawingInput &input, const Bool_t drawDetector)
{
  //Sets the Y range of the frame
  double max_Y = 0;

  for (auto photon : input.photons)
    if (photon.Pt() > max_Y)
    { max_Y = photon.Pt(); }

  for (auto jet : input.jets)
    if (jet.Pt() > max_Y)
    { max_Y = jet.Pt(); }

  for (auto electron : input.electrons)
    if (electron.Pt() > max_Y)
    { max_Y = electron.Pt(); }

  for (auto muon : input.muons)
    if (muon.Pt() > max_Y)
    { max_Y = muon.Pt(); }

  if (input.met.Pt() > max_Y)
  { max_Y = input.met.Pt(); }

  if (input.objectImbalance.Pt() > max_Y)
  { max_Y = input.objectImbalance.Pt(); }

  if (max_Y > TRANSVERSE_RANGE * GeV)
  { max_Y = TRANSVERSE_RANGE; }
  else if (max_Y > 50 * GeV) {
    int max = max_Y / (100 * GeV);
    max_Y = max * 100.0 + 100.0;
  } else
  { max_Y = 50; }


  //Sets the X range of the frame, if requested
  if (m_useDynamicZoom) {
    double max_X = 0;

    for (auto photon : input.photons)
      if (TMath::Abs(photon.Pz()) > max_X)
      { max_X = TMath::Abs(photon.Pz()); }

    for (auto jet : input.jets)
      if (TMath::Abs(jet.Pz()) > max_X)
      { max_X = TMath::Abs(jet.Pz()); }

    for (auto electron : input.electrons)
      if (TMath::Abs(electron.Pz()) > max_X)
      { max_X = TMath::Abs(electron.Pz()); }

    for (auto muon : input.muons)
      if (TMath::Abs(muon.Pz()) > max_X)
      { max_X = TMath::Abs(muon.Pz()); }

    if (TMath::Abs(input.met.Pz()) > max_X)
    { max_X = TMath::Abs(input.met.Pz()); }

    if (TMath::Abs(input.objectImbalance.Pz()) > max_X)
    { max_X = TMath::Abs(input.objectImbalance.Pz()); }

    //Ensures that the detector width is in the range
    Double_t maxDetector = calculateMaxWidthDetector(max_Y * GeV);

    if (maxDetector > max_X)
    { max_X = maxDetector; }

    if (max_X > 800 * GeV)
    { max_X = SIDE_RANGE; }
    else if (max_X > 400 * GeV)
    { max_X = 1000; }
    else if (max_X > 75 * GeV)
    { max_X = 500; }
    else
    { max_X = 100; }

    if (max_X > 200) //To have smaller range difference between axis
    { max_Y *= 2; }

    setAxisRange(m_sideFrame, max_X, max_Y);
  }

  //Draws the side frame
  m_sideFrame->Draw("0");

  //Draws the detector, if needed
  if (m_drawDetector && drawDetector) {
    drawSideDetector();
    redrawBorders();
  }

  //Draws the jets, photons and met
  for (auto jet : input.jets)
  { drawSideJet(jet); }

  for (auto photon : input.photons)
  { drawSidePhoton(photon); }

  for (auto electron : input.electrons)
  { drawSideElectron(electron); }

  for (auto muon : input.muons)
  { drawSideMuon(muon); }

  if (input.met.Pt() > 10)
  { drawSideMet(input.met); }

  if (input.objectImbalance.Pt() > 10)
  { drawSideImbalance(input.objectImbalance); }
}



/******************************************************************************//**
* Draws the event in the angular frame (phi vs. rapidity)
**********************************************************************************/
void BasicEventDisplayTool::drawAngularEvent(const EventDrawingInput &input)
{
  //Sets the range of the frame
  setAxisRange(m_angularFrame, ANGULAR_RANGE, 4);

  //Draws the angular frame
  m_angularFrame->Draw("0");

  //Draws the detector, if needed
  if (m_drawDetector) {
    drawAngularDetector();
    redrawBorders();
  }

  //Draws the jets, photons and met
  for (auto jet : input.jets)
  { drawAngularJet(jet); }

  for (auto photon : input.photons)
  { drawAngularPhoton(photon); }

  for (auto electron : input.electrons)
  { drawAngularElectron(electron); }

  for (auto muon : input.muons)
  { drawAngularMuon(muon); }

  if (input.met.Pt() > 10)
  { drawAngularMet(input.met); }

  if (input.objectImbalance.Pt() > 10)
  { drawAngularImbalance(input.objectImbalance); }

  drawHorizontalLine(ANGULAR_RANGE, TMath::Pi());
  drawHorizontalLine(ANGULAR_RANGE, -TMath::Pi());

}



/******************************************************************************//**
* Prints detailed information about the event in two columns
**********************************************************************************/
void BasicEventDisplayTool::printText(const StrV lines)
{
  gPad->Clear();
  double y = 1.;

  for (auto line : lines) {
    if (y > 0.05)
    { drawText(0.05, y -= 0.022, line, kBlack, 0.02); }
    else
    { drawText(0.55, (y -= 0.022) + 0.95, line, kBlack, 0.02); }
  }
}



/******************************************************************************//**
* Draws a description of the event, pile-up informations, invariant mass, etc. over
*  the current histogram
**********************************************************************************/
void BasicEventDisplayTool::drawEventInfo(const StrV &textLines, const TString &referential, const TLVs &photons, const TLVs &electrons, const TLVs &muons)
{
  TString text;
  double x = 0.15, y = 0.97;

  //Draws Event informatons
  for (TString text : textLines)
  { drawText(x, y -= 0.04, text); }

  //Draws referential
  text.Form("%s frame", referential.Data());
  drawText(x, y -= 0.04, text);

  //Draws the diphoton invariant mass, if possible
  if (photons.size() > 1) {
    text.Form("#it{m}_{#gamma#gamma} = %.1f GeV", getM_yy(photons) / GeV);
    drawText(x, y -= 0.04, text);
  }

  //Draws the four leptons invariant mass, if possible
  if ((electrons.size() + muons.size()) > 3) {
    text.Form("#it{m}_{4l} = %.1f GeV", getM_4l(electrons, muons) / GeV);
    drawText(x, y -= 0.04, text);
  }
}



/******************************************************************************//**
* Draws TLorentzVector informations about the particles (pt, eta, phi)
**********************************************************************************/
void BasicEventDisplayTool::drawVectorsInfo(const EventDrawingInput &input)
{
  //Draws the jets, electrons and muons vectors
  double x = 0.95, y = 0.96;

  for (auto jet : input.jets)
  { draw4vectorInfo(x, y -= 0.04, jet, kBlue, "Jet"); }

  for (auto electron : input.electrons)
  { draw4vectorInfo(x, y -= 0.04, electron, kViolet - 4, "Electron"); }

  for (auto muon : input.muons)
  { draw4vectorInfo(x, y -= 0.04, muon, kOrange - 2, "Muon"); }

  //Draws the met and photons vectors
  if (input.met.Pt() != 0)
  { draw4vectorInfo(x, 0.27, input.met, kGreen - 2, "#it{E}_{T}^{miss}"); }

  draw4vectorInfo(x, 0.23, input.objectImbalance, kGreen + 1, "Object imbalance");
  y = 0.22;

  for (auto photon : input.photons)
  { draw4vectorInfo(x, y -= 0.04, photon, kRed, "Photon"); }
}



/******************************************************************************//**
* Draws the detector in the side frame
**********************************************************************************/
void BasicEventDisplayTool::drawSideDetector()
{
  Double_t x_min_left, x_max_left, x_min_right, x_max_right;
  Double_t y_min, y_max;
  Double_t angle_min_left, angle_max_left, angle_min_right, angle_max_right;

  //Draws barrels
  y_min = m_sideFrame->GetMaximum() / 2;
  y_max = 3 * m_sideFrame->GetMaximum() / 5;
  angle_min_left = 2 * TMath::ATan(TMath::Exp(-m_detector.barrelEtaLeft));
  angle_min_right = 2 * TMath::ATan(TMath::Exp(-m_detector.barrelEtaRight));

  drawTrapezoid(y_min, y_max, angle_min_left, angle_min_right, kRed - 10);
  drawTrapezoid(-y_min, -y_max, -angle_min_left, -angle_min_right, kRed - 10);

  //Draws endcap
  x_min_left = y_min / TMath::Tan(angle_min_left);
  x_max_left = y_max / TMath::Tan(angle_min_left);
  x_min_right = y_min / TMath::Tan(angle_min_right);
  x_max_right = y_max / TMath::Tan(angle_min_right);

  angle_min_left = 2 * TMath::ATan(TMath::Exp(-m_detector.endcapMinEtaLeft));
  angle_max_left = 2 * TMath::ATan(TMath::Exp(-m_detector.endcapMaxEtaLeft));
  angle_min_right = 2 * TMath::ATan(TMath::Exp(-m_detector.endcapMinEtaRight));
  angle_max_right = 2 * TMath::ATan(TMath::Exp(-m_detector.endcapMaxEtaRight));

  drawTrapezoid(x_min_right, x_max_right, angle_min_right, angle_max_right, kRed - 10, false);
  drawTrapezoid(x_min_left, x_max_left, angle_min_left, angle_max_left, kRed - 10, false);
  drawTrapezoid(x_min_right, x_max_right, -angle_min_right, -angle_max_right, kRed - 10, false);
  drawTrapezoid(x_min_left, x_max_left, -angle_min_left, -angle_max_left, kRed - 10, false);

  //Draws HEC
  angle_min_left = 2 * TMath::ATan(TMath::Exp(-m_detector.hecMinEtaLeft));
  angle_max_left = 2 * TMath::ATan(TMath::Exp(-m_detector.hecMaxEtaLeft));
  angle_min_right = 2 * TMath::ATan(TMath::Exp(-m_detector.hecMinEtaRight));
  angle_max_right = 2 * TMath::ATan(TMath::Exp(-m_detector.hecMaxEtaRight));

  drawTrapezoid(x_min_right, x_max_right, angle_min_right, angle_max_right, kBlue - 10, false);
  drawTrapezoid(x_min_left, x_max_left, angle_min_left, angle_max_left, kBlue - 10, false);
  drawTrapezoid(x_min_right, x_max_right, -angle_min_right, -angle_max_right, kBlue - 10, false);
  drawTrapezoid(x_min_left, x_max_left, -angle_min_left, -angle_max_left, kBlue - 10, false);

  //Draws FCal
  Double_t spacing_left = (x_max_left - x_min_left) / 2;
  Double_t spacing_right = (x_max_right - x_min_right) / 2;
  x_min_left += spacing_left;
  x_max_left += spacing_left;
  x_min_right += spacing_right;
  x_max_right += spacing_right;

  angle_min_left = 2 * TMath::ATan(TMath::Exp(-m_detector.fcalMinEtaLeft));
  angle_max_left = 2 * TMath::ATan(TMath::Exp(-m_detector.fcalMaxEtaLeft));
  angle_min_right = 2 * TMath::ATan(TMath::Exp(-m_detector.fcalMinEtaRight));
  angle_max_right = 2 * TMath::ATan(TMath::Exp(-m_detector.fcalMaxEtaRight));

  drawTrapezoid(x_min_right, x_max_right, angle_min_right, angle_max_right, kBlue - 10, false);
  drawTrapezoid(x_min_left, x_max_left, angle_min_left, angle_max_left, kBlue - 10, false);
  drawTrapezoid(x_min_right, x_max_right, -angle_min_right, -angle_max_right, kBlue - 10, false);
  drawTrapezoid(x_min_left, x_max_left, -angle_min_left, -angle_max_left, kBlue - 10, false);
}



/******************************************************************************//**
* Draws the detector in the angular frame
**********************************************************************************/
void BasicEventDisplayTool::drawAngularDetector()
{
  //Draws FCal
  drawAngularDetectorPart(m_detector.fcalMaxEtaLeft, m_detector.fcalMinEtaLeft, kBlue - 10);
  drawAngularDetectorPart(m_detector.fcalMinEtaRight, m_detector.fcalMaxEtaRight, kBlue - 10);

  //Draws HEC
  drawAngularDetectorPart(m_detector.hecMaxEtaLeft, m_detector.hecMinEtaLeft, kBlue - 10);
  drawAngularDetectorPart(m_detector.hecMinEtaRight, m_detector.hecMaxEtaRight, kBlue - 10);

  //Draws endcap
  drawAngularDetectorPart(m_detector.endcapMaxEtaLeft, m_detector.endcapMinEtaLeft, kRed - 10);
  drawAngularDetectorPart(m_detector.endcapMinEtaRight, m_detector.endcapMaxEtaRight, kRed - 10);

  //Draws barrels
  drawAngularDetectorPart(m_detector.barrelEtaLeft, m_detector.barrelEtaRight, kRed - 10);
}



/******************************************************************************//**
* Draws a detector part ensuring that it stays within the range of the histogram
**********************************************************************************/
void BasicEventDisplayTool::drawAngularDetectorPart(const Double_t x_min, const Double_t x_max, const Color_t color)
{
  if (x_max > -ANGULAR_RANGE && x_min < ANGULAR_RANGE) {
    if (x_min < -ANGULAR_RANGE)
    { drawRectangle(-ANGULAR_RANGE, -4, x_max, 4, color); }
    else if (x_max > ANGULAR_RANGE)
    { drawRectangle(x_min, -4, ANGULAR_RANGE, 4, color); }
    else
    { drawRectangle(x_min, -4, x_max, 4, color); }
  }
}



/******************************************************************************//**
* Draws a photon in the transverse frame as a red arrow
**********************************************************************************/
void BasicEventDisplayTool::drawTransversePhoton(const TLorentzVector &photon)
{
  drawArrow(photon.Px() / GeV, photon.Py() / GeV, kRed, 1);
}


/******************************************************************************//**
* Draws a photon in the side frame as a red arrow
**********************************************************************************/
void BasicEventDisplayTool::drawSidePhoton(const TLorentzVector &photon)
{
  if (photon.Py() > 0)
  { drawArrow(photon.Pz() / GeV, photon.Pt() / GeV, kRed, 1); }
  else
  { drawArrow(photon.Pz() / GeV, -photon.Pt() / GeV, kRed, 1); }
}


/******************************************************************************//**
* Draws a photon in the angular frame as a red dot located at phi and rapidity
*  positions
**********************************************************************************/
void BasicEventDisplayTool::drawAngularPhoton(const TLorentzVector &photon)
{
  drawMarker(photon.Rapidity(), photon.Phi(), kRed);
}



/******************************************************************************//**
* Draws a jet in the transverse frame as a blue triangle and an arrow
**********************************************************************************/
void BasicEventDisplayTool::drawTransverseJet(const TLorentzVector &jet)
{
  Double_t angle1, angle2, x1, x2, y1, y2;
  Double_t length;
  Double_t x[4];
  Double_t y[4];

  //Draws the triangle
  length = jet.Pt() / GeV;

  angle1 = jet.Phi() + R;
  x1 = length * TMath::Cos(angle1);
  y1 = length * TMath::Sin(angle1);

  angle2 = jet.Phi() - R;
  x2 = length * TMath::Cos(angle2);
  y2 = length * TMath::Sin(angle2);

  x[0] = 0.0;
  y[0] = 0.0;
  x[1] = x1;
  y[1] = y1;
  x[2] = x2;
  y[2] = y2;
  x[3] = 0.0;
  y[3] = 0.0;

  static TPolyLine *triangle = new TPolyLine();
  triangle->SetFillColor(kAzure - 4);
  triangle->SetLineColor(kBlue);
  triangle->DrawPolyLine(4, x, y, "f");
  triangle->DrawPolyLine(4, x, y);

  //Draws the arrow
  drawArrow(jet.Px() / GeV, jet.Py() / GeV, kBlue, 1);
}


/******************************************************************************//**
* Draws a jet in the side frame as a blue triangle and an arrow
**********************************************************************************/
void BasicEventDisplayTool::drawSideJet(const TLorentzVector &jet)
{
  Double_t angle1, angle2, x1, x2, y1, y2;
  Double_t length;
  Double_t x[4];
  Double_t y[4];

  //Draws the triangle
  length = TMath::Sqrt(jet.Pt() * jet.Pt() + jet.Pz() * jet.Pz()) / GeV;

  angle1 = 2 * TMath::ATan(TMath::Exp(-jet.Eta() - R));
  x1 = length * TMath::Cos(angle1);
  y1 = length * TMath::Sin(angle1);

  angle2 = 2 * TMath::ATan(TMath::Exp(-jet.Eta() + R));
  x2 = length * TMath::Cos(angle2);
  y2 = length * TMath::Sin(angle2);

  x[0] = 0.0;
  y[0] = 0.0;
  x[1] = x1;
  y[1] = y1;
  x[2] = x2;
  y[2] = y2;
  x[3] = 0.0;
  y[3] = 0.0;

  if (jet.Py() < 0)
    for (int i = 0; i < 4; i++)
    { y[i] *= -1; }

  static TPolyLine *triangle = new TPolyLine();
  triangle->SetFillColor(kAzure - 4);
  triangle->SetLineColor(kBlue);
  triangle->DrawPolyLine(4, x, y, "f");
  triangle->DrawPolyLine(4, x, y);

  //Draws the arrow
  if (jet.Py() > 0)
  { drawArrow(jet.Pz() / GeV, jet.Pt() / GeV, kBlue, 1); }
  else
  { drawArrow(jet.Pz() / GeV, -jet.Pt() / GeV, kBlue, 1); }
}



/******************************************************************************//**
* Draws a jet in the angular frame as a blue circle and a cross
**********************************************************************************/
void BasicEventDisplayTool::drawAngularJet(const TLorentzVector &jet)
{
  //Draws the circle
  static TEllipse *circle = new TEllipse();
  circle->SetFillColor(kAzure - 4);
  circle->SetLineColor(kBlue);
  circle->DrawEllipse(jet.Rapidity(), jet.Phi(), R, 0, 0, 360, 0);

  //Draws the marker
  drawMarker(jet.Rapidity(), jet.Phi(), kBlue, 1, 5);
}



/******************************************************************************//**
* Draws an electron in the transverse frame as a pink arrow
**********************************************************************************/
void BasicEventDisplayTool::drawTransverseElectron(const TLorentzVector &electron)
{
  drawArrow(electron.Px() / GeV, electron.Py() / GeV, kViolet - 4, 1);
}


/******************************************************************************//**
* Draws an electron in the side frame as a pink arrow
**********************************************************************************/
void BasicEventDisplayTool::drawSideElectron(const TLorentzVector &electron)
{
  if (electron.Py() > 0)
  { drawArrow(electron.Pz() / GeV, electron.Pt() / GeV, kViolet - 4, 1); }
  else
  { drawArrow(electron.Pz() / GeV, -electron.Pt() / GeV, kViolet - 4, 1); }
}


/******************************************************************************//**
* Draws an electron in the angular frame as a pink square located at phi and
*  rapidity positions
**********************************************************************************/
void BasicEventDisplayTool::drawAngularElectron(const TLorentzVector &electron)
{
  drawMarker(electron.Rapidity(), electron.Phi(), kViolet - 4, 1, 21);
}



/******************************************************************************//**
* Draws a photon in the transverse frame as an orange arrow
**********************************************************************************/
void BasicEventDisplayTool::drawTransverseMuon(const TLorentzVector &muon)
{
  drawArrow(muon.Px() / GeV, muon.Py() / GeV, kOrange - 2, 1);
}


/******************************************************************************//**
* Draws a muon in the side frame as an orange arrow
**********************************************************************************/
void BasicEventDisplayTool::drawSideMuon(const TLorentzVector &muon)
{
  if (muon.Py() > 0)
  { drawArrow(muon.Pz() / GeV, muon.Pt() / GeV, kOrange - 2, 1); }
  else
  { drawArrow(muon.Pz() / GeV, -muon.Pt() / GeV, kOrange - 2, 1); }
}


/******************************************************************************//**
* Draws a photon in the angular frame as an orange diamond located at phi and
*  rapidity positions
**********************************************************************************/
void BasicEventDisplayTool::drawAngularMuon(const TLorentzVector &muon)
{
  drawMarker(muon.Rapidity(), muon.Phi(), kOrange - 2, 1, 33);
}



/******************************************************************************//**
* Draws the missing energy in the transverse frame as a green dashed arrow
**********************************************************************************/
void BasicEventDisplayTool::drawTransverseMet(const TLorentzVector &met)
{
  drawArrow(met.Px() / GeV, met.Py() / GeV, kGreen - 2, 7);
}



/******************************************************************************//**
* Draws the missing energy in the side frame as a green dashed arrow
**********************************************************************************/
void BasicEventDisplayTool::drawSideMet(const TLorentzVector &met)
{
  if (met.Py() > 0)
  { drawArrow(met.Pz() / GeV, met.Pt() / GeV, kGreen - 2, 7); }
  else
  { drawArrow(met.Pz() / GeV, -met.Pt() / GeV, kGreen - 2, 7); }
}



/******************************************************************************//**
* Draws the missing energy in the angular frame as a green dashed horizontal line
**********************************************************************************/
void BasicEventDisplayTool::drawAngularMet(const TLorentzVector &met)
{
  drawHorizontalLine(5, met.Phi(), kGreen - 2, 7);
}



/******************************************************************************//**
* Draws the object imbalance in the transverse frame as a green dashed arrow
**********************************************************************************/
void BasicEventDisplayTool::drawTransverseImbalance(const TLorentzVector &objImbalance)
{
  drawArrow(objImbalance.Px() / GeV, objImbalance.Py() / GeV, kGreen + 1, 7);
}



/******************************************************************************//**
* Draws the object imbalance in the side frame as a green dashed arrow
**********************************************************************************/
void BasicEventDisplayTool::drawSideImbalance(const TLorentzVector &objImbalance)
{
  if (objImbalance.Py() > 0)
  { drawArrow(objImbalance.Pz() / GeV, objImbalance.Pt() / GeV, kGreen + 1, 7); }
  else
  { drawArrow(objImbalance.Pz() / GeV, -objImbalance.Pt() / GeV, kGreen + 1, 7); }
}



/******************************************************************************//**
* Draws the object imbalance in the angular frame as a green dashed horizontal line
**********************************************************************************/
void BasicEventDisplayTool::drawAngularImbalance(const TLorentzVector &objImbalance)
{
  drawHorizontalLine(5, objImbalance.Phi(), kGreen + 1, 7);
}



/******************************************************************************//**
* Draws a TLorentzVector components (p_t, eta, phi)
**********************************************************************************/
void BasicEventDisplayTool::draw4vectorInfo(const Double_t x, const Double_t y, const TLorentzVector p4, const Color_t col, const TString type)
{
  TString text;
  text.Form("%s (#it{p}_{T},#it{#eta},#it{#phi}) = (%.1f GeV, %.2f, %.2f)", type.Data(), p4.Pt() / GeV, p4.Eta(), p4.Phi());
  drawText(x, y, text, col, 0.025, 31);
}



/******************************************************************************//**
* Draws an arrow starting at the origin
**********************************************************************************/
void BasicEventDisplayTool::drawArrow(const Double_t x, const Double_t y, const Color_t color, const Style_t style)
{
  static TArrow *arrow = new TArrow();
  arrow->SetAngle(40);
  arrow->SetFillColor(color);
  arrow->SetLineColor(color);
  arrow->SetLineStyle(style);
  arrow->DrawArrow(0, 0, x, y, 0.02, "|>");
}



/******************************************************************************//**
* Draws text using NDC coordinates
**********************************************************************************/
void BasicEventDisplayTool::drawText(const Double_t x, const Double_t y, TString txt, const Color_t col, const Double_t size, const Int_t align, const Bool_t ndc)
{
  //Draws text in the upper right corner
  static TLatex *tex = new TLatex();
  tex->SetTextFont(42);
  tex->SetTextColor(col);
  tex->SetTextAlign(align);
  tex->SetTextSize(size);
  tex->SetNDC(ndc);
  tex->DrawLatex(x, y, txt.ReplaceAll("\\", "#"));
}



/******************************************************************************//**
* Draws a marker in user's coordinates
**********************************************************************************/
void BasicEventDisplayTool::drawMarker(const Double_t x, const Double_t y, const Color_t color, const Size_t size, const Style_t style)
{
  //Draws a marker with the specified attributes
  static TMarker *mark = new TMarker();
  mark->SetMarkerColor(color);
  mark->SetMarkerSize(size);
  mark->SetMarkerStyle(style);
  mark->DrawMarker(x, y);
}



/******************************************************************************//**
* Draws an horizontal line from -xRange to xRange
**********************************************************************************/
void BasicEventDisplayTool::drawHorizontalLine(const Double_t xRange, const Double_t y, const Color_t color, const Style_t style)
{
  //Draws a horizontal line with the specified attributes
  static TLine *line = new TLine();
  line->SetLineColor(color);
  line->SetLineStyle(style);
  line->DrawLine(-xRange, y, xRange, y);
}



/******************************************************************************//**
* Draws a trapezoid shape using two angles and two positions on the x or y axis
*      IMPORTANT: Difference between the angles must be smaller than pi
**********************************************************************************/
void BasicEventDisplayTool::drawTrapezoid(const Double_t x_min, const Double_t x_max, const Double_t angle_min, const Double_t angle_max, const Color_t color, const Bool_t horizontal)
{
  Double_t x1, x2, x3, x4, y1, y2, y3, y4;
  Double_t x[5];
  Double_t y[5];

  //If the angles are too big to draw a trapezoid
  if (TMath::Abs(angle_max - angle_min) > TMath::Pi())
  { return; }

  if (horizontal) { //Draws a trapezoid with parallel horizontal lines
    y1 = x_min;
    y2 = x_max;
    y3 = x_max;
    y4 = x_min;
    x1 = x_min / TMath::Tan(angle_min);
    x2 = x_max / TMath::Tan(angle_min);
    x3 = x_max / TMath::Tan(angle_max);
    x4 = x_min / TMath::Tan(angle_max);
  } else { //Draws a trapezoid with parallel vertical lines
    x1 = x_min;
    x2 = x_max;
    x3 = x_max;
    x4 = x_min;
    y1 = x_min * TMath::Tan(angle_min);
    y2 = x_max * TMath::Tan(angle_min);
    y3 = x_max * TMath::Tan(angle_max);
    y4 = x_min * TMath::Tan(angle_max);
  }

  x[0] = x4;
  y[0] = y4;
  x[1] = x1;
  y[1] = y1;
  x[2] = x2;
  y[2] = y2;
  x[3] = x3;
  y[3] = y3;
  x[4] = x4;
  y[4] = y4;

  static TPolyLine *trapezoid = new TPolyLine();
  trapezoid->SetFillColor(color);
  trapezoid->DrawPolyLine(5, x, y, "f");
}



/******************************************************************************//**
* Draws a rectangle
**********************************************************************************/
void BasicEventDisplayTool::drawRectangle(const Double_t x_min, const Double_t y_min, const Double_t x_max, const Double_t y_max, const Color_t color)
{
  static TBox *rectangle = new TBox();
  rectangle->SetFillColor(color);
  rectangle->DrawBox(x_min, y_min, x_max, y_max);
}



/******************************************************************************//**
* Zoom in the histogram range (User's range)
**********************************************************************************/
void BasicEventDisplayTool::setAxisRange(TH1 *histo, const Double_t max_x, const Double_t max_y)
{
  histo->GetXaxis()->SetRangeUser(-max_x, max_x);
  histo->GetYaxis()->SetRangeUser(-max_y, max_y);
}



/******************************************************************************//**
* Redraws the borders of the histogram, in case an objet hides those
**********************************************************************************/
void BasicEventDisplayTool::redrawBorders()
{
  gPad->Update();
  gPad->RedrawAxis();
  TLine l;
  l.DrawLine(gPad->GetUxmin(), gPad->GetUymax(), gPad->GetUxmax(), gPad->GetUymax());
  l.DrawLine(gPad->GetUxmax(), gPad->GetUymin(), gPad->GetUxmax(), gPad->GetUymax());
}



/******************************************************************************//**
* Calculates the maximal width of the detector from the side view
**********************************************************************************/
Double_t BasicEventDisplayTool::calculateMaxWidthDetector(const Double_t maxRange)
{
  Double_t maxWidth = 0, y_min, y_max, angle_min_left, angle_min_right, x_min_left, x_max_left, x_min_right, x_max_right;

  y_min = maxRange / 2;
  y_max = 3 * maxRange / 5;
  angle_min_left = 2 * TMath::ATan(TMath::Exp(-m_detector.barrelEtaLeft));
  angle_min_right = 2 * TMath::ATan(TMath::Exp(-m_detector.barrelEtaRight));
  x_min_left = y_min / TMath::Tan(angle_min_left);
  x_max_left = y_max / TMath::Tan(angle_min_left);
  x_min_right = y_min / TMath::Tan(angle_min_right);
  x_max_right = y_max / TMath::Tan(angle_min_right);

  Double_t spacing_left = (x_max_left - x_min_left) / 2;
  Double_t spacing_right = (x_max_right - x_min_right) / 2;
  x_max_left += spacing_left;
  x_max_right += spacing_right;

  if (TMath::Abs(x_max_left) > maxWidth)
  { maxWidth = TMath::Abs(x_max_left); }

  if (TMath::Abs(x_max_right) > maxWidth)
  { maxWidth = TMath::Abs(x_max_right); }

  return maxWidth;
}



/******************************************************************************//**
* Compares the energy of two particles
**********************************************************************************/
bool BasicEventDisplayTool::comparePt(TLorentzVector &a, TLorentzVector &b)
{
  return a.Pt() > b.Pt();
}
