#include "HGamTools/HGamEventDisplay.h"
#include "HGamAnalysisFramework/HgammaIncludes.h"
#include "HGamAnalysisFramework/HGamVariables.h"

// this is needed to distribute the algorithm to the workers
ClassImp(HGamEventDisplay)



HGamEventDisplay::HGamEventDisplay(const char *name)
  : HgammaAnalysis(name)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
}



HGamEventDisplay::~HGamEventDisplay()
{
  // Here you delete any memory you allocated during your analysis.
}



EL::StatusCode HGamEventDisplay::createOutput()
{
  // Here you setup the histograms needed for you analysis. This method
  // gets called after the Handlers are initialized, so that the systematic
  // registry is already filled.

  //Initialize eventDisplay
  InputSettings *settings = new InputSettings(config()->getDB());
  m_eventDisplay.initialize(settings);

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode HGamEventDisplay::execute()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.


  // Important to keep this, so that internal tools / event variables
  // are filled properly.
  HgammaAnalysis::execute();


  //DATA
  xAOD::PhotonContainer photons       = photonHandler()->getCorrectedContainer();
  // Only apply pre-selection
  xAOD::PhotonContainer preselphotons = photonHandler()->applyPreSelection(photons);
  // Apply all selections (tight PID, isolation, etc.)
  xAOD::PhotonContainer selphotons    = photonHandler()->applySelection(photons);

  // Get all other selected objects
  xAOD::JetContainer jets    = jetHandler()->getCorrectedContainer();
  xAOD::JetContainer seljets = jetHandler()->applySelection(jets);

  xAOD::MuonContainer mus    = muonHandler()->getCorrectedContainer();
  xAOD::MuonContainer selmus = muonHandler()->applySelection(mus);

  xAOD::ElectronContainer els    = electronHandler()->getCorrectedContainer();
  xAOD::ElectronContainer selels = electronHandler()->applySelection(els);

  // Apply overlap removal on objects
  overlapHandler()->removeOverlap(selphotons, seljets, selels, selmus);

  // Get missing energy
  xAOD::MissingETContainer met = etmissHandler()->getCorrectedContainer(&selphotons, &seljets, &selels, &selmus);
  //xAOD::MissingETContainer selmet = etmissHandler()->applySelection(met);
  TLorentzVector selmissET;

  if (met["TST"] != nullptr)
  { selmissET.SetPxPyPzE(met["TST"]->mpx(), met["TST"]->mpy(), 0, 0); }


  /*
  //TRUTH PARTICLES
  // Get selected truth particles
  xAOD::TruthParticleContainer truthphotons    = truthHandler()->getPhotons();
  xAOD::TruthParticleContainer seltruthphotons = truthHandler()->applyPhotonSelection(truthphotons);

  xAOD::TruthParticleContainer truthelectrons    = truthHandler()->getElectrons();
  xAOD::TruthParticleContainer seltruthelectrons = truthHandler()->applyElectronSelection(truthelectrons);

  xAOD::TruthParticleContainer truthmuons    = truthHandler()->getMuons();
  xAOD::TruthParticleContainer seltruthmuons = truthHandler()->applyMuonSelection(truthmuons);

  xAOD::JetContainer truthjets    = truthHandler()->getJets();
  xAOD::JetContainer seltruthjets = truthHandler()->applyJetSelection(truthjets);

  // Apply overlap removal
  truthHandler()->removeOverlap(seltruthphotons, seltruthjets, seltruthelectrons, seltruthmuons);

  xAOD::MissingETContainer truthmet    = truthHandler()->getMissingET();
  xAOD::MissingETContainer seltruthmet = truthHandler()->applyMissingETSelection(truthmet);
  */


  //Wrap the information to print on the drawings into String vector
  StrV eventinfos  = createEventInfos(false);

  //Wrap the information to print on the separate page into String vector
  StrV lines  = createLines(selphotons, seljets, selels, selmus);

  //Draws events
  if (selphotons.size() == 2) {
    Double_t mass = (selphotons[0]->p4() + selphotons[1]->p4()).M() / HG::GeV;

    if (mass > 700 && mass < 800)
    { m_eventDisplay.draw(selphotons, seljets, selels, selmus, selmissET, eventinfos, lines); }
  }


  return EL::StatusCode::SUCCESS;
}



EL::StatusCode HGamEventDisplay::finalize()
{
  //Closes the pdf file
  m_eventDisplay.finalize();

  return EL::StatusCode::SUCCESS;
}



/******************************************************************************//**
* Example of a method to wrap the information to print on the drawings into
*  String vector
**********************************************************************************/
StrV HGamEventDisplay::createEventInfos(const Bool_t particleLevel)
{
  StrV vector;
  TString text;

  //Writes run number or MC channel number
  text.Form("event: %llu", eventInfo()->eventNumber());

  if (HG::isData())
  { text += Form("   run: %d", eventInfo()->runNumber()); }
  else {
    int mcID = eventInfo()->mcChannelNumber();

    switch (mcID) {
      case 341000: {
        text += "   gg#rightarrow H MC";
        break;
      }

      case 341001: {
        text += "   VBF MC";
        break;
      }

      case 341069:
      case 341081: {
        text += "   ttH MC";
        break;
      }

      default:
        text += Form("   MC ID: %d", eventInfo()->mcChannelNumber());
    }
  }

  vector.emplace_back(text);

  //Writes the type of data displayed
  if (particleLevel)
  { vector.emplace_back("Particle Level"); }
  else
  { vector.emplace_back("Reconstructed Level"); }

  //Writes pileup informations
  if (event()->contains<xAOD::EventShape *>("Kt4EMTopoEventShape")) {
    text.Form("#it{N}_{PV} = %d, #it{#rho} = %.1f GeV, #it{#mu} = %.1f", eventHandler()->numberOfPrimaryVertices(), eventHandler()->eventShapeDensity() / HG::GeV, eventHandler()->mu());
    vector.emplace_back(text);
  }

  return vector;
}



/******************************************************************************//**
* Example of a method to wrap the information to print on the separate page into
*  String vector
**********************************************************************************/
StrV HGamEventDisplay::createLines(xAOD::PhotonContainer photons, xAOD::JetContainer jets, xAOD::ElectronContainer electrons, xAOD::MuonContainer muons)
{
  StrV lines;
  TString txt;

  //Writes event infos
  lines.emplace_back("#bf{Event}");
  txt += Form("event: %llu", eventInfo()->eventNumber());

  if (HG::isData())
  { txt += Form("   run: %d", eventInfo()->runNumber()); }
  else {
    int mcID = eventInfo()->mcChannelNumber();

    switch (mcID) {
      case 341000: {
        txt += "   gg#rightarrow H MC";
        break;
      }

      case 341001: {
        txt += "   VBF MC";
        break;
      }

      case 341069:
      case 341081: {
        txt += "   ttH MC";
        break;
      }

      default:
        txt += Form("   MC ID: %d", eventInfo()->mcChannelNumber());
    }
  }

  lines.emplace_back(Form("%s", txt.Data()));
  lines.emplace_back(Form("BC ID: %d", eventInfo()->bcid()));

  txt.Clear();

  if (event()->contains<xAOD::EventShape *>("Kt4EMTopoEventShape"))              { txt += Form("#it{#rho} = %.1f GeV", eventHandler()->eventShapeDensity() / HG::GeV); }

  if (event()->contains<xAOD::EventShape *>("TopoClusterIsoCentralEventShape"))  { txt += Form("   #it{ #rho }_{ central } = %.1f GeV", eventHandler()->centralEventShapeDensity() / HG::GeV); }

  if (event()->contains<xAOD::EventShape *>("TopoClusterIsoForwardEventShape"))  { txt += Form("   #it{ #rho }_{ forward } = %.1f GeV", eventHandler()->forwardEventShapeDensity() / HG::GeV); }

  if (txt.Sizeof() > 1)
  { lines.emplace_back(Form("%s", txt.Data())); }

  lines.emplace_back(Form("#it{N}_{PV} = %d   #it{#mu} = %.1f", eventHandler()->numberOfPrimaryVertices(), eventHandler()->mu()));
  lines.emplace_back(Form("Beam spot (X, Y, Z) = (%.0f, %.0f, %.0f) mm", eventInfo()->beamPosX(), eventInfo()->beamPosY(), eventInfo()->beamPosZ()));
  lines.emplace_back(Form("Selected vertex Z = %.1f mm", eventHandler()->selectedVertexZ()));
  lines.emplace_back(Form("Hardest vertex Z = %.1f mm", eventHandler()->hardestVertexZ()));
  lines.emplace_back("");


  //Writes the photons infos
  lines.emplace_back("#bf{Photons}");

  for (auto photon : photons) {
    lines.emplace_back(Form("(#it{p}_{T},#it{#eta},#it{#phi}) = (%.1f GeV, %.2f, %.2f)", photon->p4().Pt() / HG::GeV, photon->p4().Eta(), photon->p4().Phi()));

    if (photon->auxdata<int>("conversionType") == 0)
    { lines.emplace_back("Unconverted photon"); }
    else
    { lines.emplace_back(Form("conv type: %i   #it{R}_{conv} = %.0f mm   #it{Z}_{conv} = %.0f mm", photon->auxdata<int>("conversionType"), photon->auxdata<float>("Rconv"), photon->auxdata<float>("Zconv"))); }

    lines.emplace_back(Form("Isolation: #it{E}_{T}^{cone20} = %.1f GeV   #it{p}_{T}^{cone20} = %.1f GeV", photon->auxdata<float>("topoetcone20") / HG::GeV, photon->auxdata<float>("ptcone20") / HG::GeV));
    lines.emplace_back(Form("max-E-cell: gain = %i   time = %.1f ns", photon->auxdata<int>("maxEcell_gain"), photon->auxdata<float>("maxEcell_time")));
    lines.emplace_back(Form("#it{Z}_{common} = %.1f mm", photon->auxdata<float>("zCommon")));
    lines.emplace_back(Form("#it{#eta}_{cluster} = %.2f", photon->auxdata<float>("cl_eta")));
    lines.emplace_back("");
  }


  //Writes the electons infos
  if (electrons.size() > 0)
  { lines.emplace_back("#bf{Electrons}"); }

  for (auto electron : electrons) {
    lines.emplace_back(Form("(#it{p}_{T},#it{#eta},#it{#phi}) = (%.1f GeV, %.2f, %.2f)", electron->p4().Pt() / HG::GeV, electron->p4().Eta(), electron->p4().Phi()));
    lines.emplace_back("");
  }


  //Writes the muons infos
  if (muons.size() > 0)
  { lines.emplace_back("#bf{Muons}"); }

  for (auto muon : muons) {
    lines.emplace_back(Form("(#it{p}_{T},#it{#eta},#it{#phi}) = (%.1f GeV, %.2f, %.2f)", muon->p4().Pt() / HG::GeV, muon->p4().Eta(), muon->p4().Phi()));
    lines.emplace_back("");
  }


  //Writes the jets infos
  if (jets.size() > 0)
  { lines.emplace_back("#bf{Jets}"); }

  for (auto jet : jets) {
    Bool_t BEff_60 = jet->auxdata<char>("MV2c10_FixedCutBEff_60");
    Bool_t BEff_70 = jet->auxdata<char>("MV2c10_FixedCutBEff_70");
    Bool_t BEff_77 = jet->auxdata<char>("MV2c10_FixedCutBEff_77");
    Bool_t BEff_85 = jet->auxdata<char>("MV2c10_FixedCutBEff_85");
    TString BTag = "b-tags: ";

    if (!BEff_60 && !BEff_70 && !BEff_77 && !BEff_85)
    { BTag += "None"; }
    else {
      if (BEff_60)
      { BTag += "BEff_60 "; }

      if (BEff_70)
      { BTag += "BEff_70 "; }

      if (BEff_77)
      { BTag += "BEff_77 "; }

      if (BEff_85)
      { BTag += "BEff_85"; }
    }

    lines.emplace_back(Form("(#it{p}_{T},#it{#eta},#it{#phi}, #it{m}) = (%.1f GeV, %.2f, %.2f, %.2f GeV)", jet->p4().Pt() / HG::GeV, jet->p4().Eta(), jet->p4().Phi(), jet->p4().M() / HG::GeV));
    lines.emplace_back(Form("#it{#eta}_{det} = %.2f   JVT = %.2f", jet->auxdata<float>("DetectorEta"), jet->auxdata<float>("Jvt")));
    lines.emplace_back(BTag);
    lines.emplace_back("");
  }


  //Writes event topology infos
  lines.emplace_back("#bf{Event topology}");

  if (photons.size() > 1) {
    TLorentzVector m_yy = photons[0]->p4() + photons[1]->p4();
    lines.emplace_back(Form("#it{m}_{#gamma#gamma} = %.1f GeV", m_yy.M() / HG::GeV));
  }

  if (jets.size() > 1) {
    lines.emplace_back(Form("#it{m}_{jj} = %.1f GeV", (jets[0]->p4() + jets[1]->p4()).M() / HG::GeV));
    lines.emplace_back(Form("#Delta#it{#eta}_{jj} = %.2f", TMath::Abs(jets[0]->p4().Rapidity() - jets[1]->p4().Rapidity())));
  }

  double H_t = 0;

  for (auto jet : jets)
  { H_t += jet->p4().Pt(); }

  lines.emplace_back(Form("#it{H}_{T, Jets} = %.1f GeV", H_t / HG::GeV));
  lines.emplace_back("");


  return lines;
}
