#include <HGamTools/PDFTool.h>

#include "HGamTools/HGamCutflowAndMxAOD.h"
#include "HGamTools/HGamExample.h"
#include "HGamTools/HggTwoSidedCBPdf.h"
#include <HGamTools/HGamYieldExample.h>
#include <HGamTools/SkimAndSlim.h>
#include <HGamTools/DumpNTUP.h>
#include <HGamTools/BasicEventDisplayTool.h>
#include <HGamTools/HGamEventDisplay.h>
#include <HGamTools/HGamSimpleExample.h>

#include <HGamTools/XSecSkimAndSlim.h>

#ifdef __CINT__

  #pragma link off all globals;
  #pragma link off all classes;
  #pragma link off all functions;
  #pragma link C++ nestedclass;

  #pragma link C++ class HGamCutflowAndMxAOD+;
  #pragma link C++ class HGamExample+;
  #pragma link C++ class HggTwoSidedCBPdf+;
  #pragma link C++ class HGamYieldExample+;
  #pragma link C++ class SkimAndSlim+;
  #pragma link C++ class DumpNTUP+;
  #pragma link C++ class BasicEventDisplayTool+;
  #pragma link C++ class HGamEventDisplay+;
  #pragma link C++ class HGamSimpleExample+;

  #pragma link C++ class XSecSkimAndSlim+;

#endif

#ifdef __CINT__
  #pragma link C++ class PDFTool+;
#endif
