#include "HGamTools/BkgParam.h"

#include <algorithm>
#include <fstream>

#include "TCanvas.h"
#include "TFile.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TH1.h"
#include "TTree.h"
#include "TChain.h"
#include "TSystem.h"
#include "TError.h"
#include "TMath.h"

#include "TLine.h"
#include "TLegend.h"

#include "RooAddPdf.h"
#include "RooArgList.h"
#include "RooArgSet.h"
#include "RooGenericPdf.h"
#include "RooPlot.h"
#include "RooDataHist.h"

#include <iostream>
using std::cout;
using std::endl;
using std::ofstream;

using namespace BkgTool;


Selector::Selector(const TString &configFileName, const TString &outputDir)
  : Configurable("Selection"), m_outputDir(outputDir), m_rangeMin("Range.Min"), m_rangeMax("Range.Max"),
    m_targetLumi(-1), m_data("Dataset"), m_scanVar(0), m_perPointPlots(false)
{
  gErrorIgnoreLevel = kWarning; // suppress TCanvas::Print messages
  TString foundConfigName = find_file(configFileName);

  if (foundConfigName == "") {
    cout << Form("ERROR: cannot access configuration file '%s'.", configFileName.Data()) << endl;
    return;
  }

  m_reader.ReadFile(foundConfigName, kEnvAll);

  if (!Configurable::setup(m_reader)) {
    cout << Form("ERROR: invalid configuration file '%s'.", foundConfigName.Data()) << endl;
    return;
  }

  if (verbosity(2)) { cout << "INFO: reading configuration from " << foundConfigName << endl; }

  if (verbosity() <= 1) {
    gErrorIgnoreLevel = kWarning;
    RooMsgService::instance().setSilentMode(true);
    RooMsgService::instance().setGlobalKillBelow(RooFit::ERROR);
  }

  std::vector<RooCmdArg> fitOptions;
  fitOptions.push_back(RooFit::Minimizer("Minuit2", "migrad"));
  fitOptions.push_back(RooFit::Save());
  fitOptions.push_back(RooFit::Offset());
  //if (verbosity() == 0) fitOptions.push_back(RooFit::Silence());

  // Bkg-only is not extended.
  for (std::vector<RooCmdArg>::const_iterator opt = fitOptions.begin(); opt != fitOptions.end(); opt++) {
    m_fitOptionsBkgT.Add(new RooCmdArg(*opt));
    m_fitOptionsBkgF.Add(new RooCmdArg(*opt));
  }

  // S+B is extended.
  fitOptions.push_back(RooFit::Extended());

  for (std::vector<RooCmdArg>::const_iterator opt = fitOptions.begin(); opt != fitOptions.end(); opt++) {
    m_fitOptionsT.Add(new RooCmdArg(*opt));
    m_fitOptionsF.Add(new RooCmdArg(*opt));
  }

  m_fitOptionsT.Add(new RooCmdArg(RooFit::SumW2Error(true)));
  m_fitOptionsF.Add(new RooCmdArg(RooFit::SumW2Error(false)));
  m_fitOptionsBkgT.Add(new RooCmdArg(RooFit::SumW2Error(true)));
  m_fitOptionsBkgF.Add(new RooCmdArg(RooFit::SumW2Error(false)));
}


bool Selector::setup()
{
  // Get list of PDF candidates
  TString pdfStr = m_reader.GetValue("Background.PDFs", "");
  std::vector<TString> pdfs;
  split(pdfStr, pdfs, " ");

  if (pdfs.size() == 0) {
    cout << "ERROR: At least one background PDF choice must be specified using the 'Background.PDFs: <PDF1> <PDF2> ... ' syntax" << endl;
    return false;
  }

  // Make the output directory
  mkdir();
  // Define the scan
  TString scanVarName = m_reader.GetValue("Scan.Var", "");

  if (scanVarName == "") {
    cout << "ERROR: a scan variable must be specified with the 'Scan.Var: <var>' statement" << endl;
    return false;
  }

  m_scanMin = m_reader.GetValue("Scan.Min", -DBL_MAX);

  if (m_scanMin < -DBL_MAX / 2) {
    cout << "ERROR : the lower end of the test range must be specified using the 'Scan.Min = ...' syntax" << endl;
    return false;
  }

  m_scanMax = m_reader.GetValue("Scan.Max", -DBL_MAX);

  if (m_scanMax < -DBL_MAX / 2) {
    cout << "ERROR : the upper end of the test range must be specified using the 'Scan.Max = ...' syntax" << endl;
    return false;
  }

  // Setup the PDF candidates
  for (auto pdfName : pdfs) {
    m_candidates.push_back(Parameterization(pdfName));
    Parameterization *bkgParam = &m_candidates.back();

    if (!bkgParam->setup(m_reader, scanVarName)) { return false; }
  }

  m_targetLumi = m_reader.GetValue(GetName() + ".IntegratedLuminosity", -1.0);

  if (m_targetLumi < 0) {
    cout << Form("ERROR : an integrated luminosity for the selection must be specified using the '%s.IntegratedLuminosity = <lumi>' syntax", GetName().Data()) << endl;
    return false;
  }

  if (!m_data.setup(m_reader, *m_candidates[0].obsVar(), m_targetLumi)) { return false; }

  for (auto pdf : m_candidates) {
    if (m_reader.GetValue(Form("%s.SetInitialValuesFromDataset", pdf.GetName().Data()), false)) {
      if (verbosity(2)) { cout << Form("INFO : setting initial parameter values of PDF '%s' from data.", pdf.GetName().Data()) << endl; }

      RooRealVar *nSignal = pdf.nSignal();
      double nsig = nSignal->getVal();
      nSignal->setConstant(true);
      nSignal->setVal(0);

      if (!pdf.fit(*m_data.data(), m_fitOptionsF)) { return false; }

      nSignal->setConstant(false);
      nSignal->setVal(nsig);
    }
  }

  m_scanVar = m_candidates[0].scanVar();

  if (verbosity(1)) cout << Form("INFO: will perform selection from %s = %g %s to %g %s ", m_scanVar->GetName(),
                                   m_scanMin, m_scanVar->getUnit(), m_scanMax, m_scanVar->getUnit()) << endl;

  if (!m_rangeMin.setup(m_reader, m_scanVar->GetName())) { return false; }

  if (!m_rangeMax.setup(m_reader, m_scanVar->GetName())) { return false; }

  for (auto pdf : m_candidates) { pdf.scanVar()->setRange(m_rangeMin.value(m_scanMin), m_rangeMax.value(m_scanMax)); }

  m_scanStep = m_reader.GetValue("Scan.Step", -1.0);

  if (m_scanStep < 0) {
    cout << "ERROR : a scan step must be specified using the 'Scan.Step = ...' syntax" << endl;
    return false;
  }

  if (verbosity(1)) cout << Form("INFO: will perform fits from %g %s to %g %s in steps of %g %s",  m_scanVar->getMin(), m_scanVar->getUnit(),
                                   m_scanVar->getMax(), m_scanVar->getUnit(), m_scanStep, m_scanVar->getUnit()) << endl;

  m_perPointPlots = m_reader.GetValue("PerPointPlots", false);
  return true;
}


TString Selector::path(const TString &frag1, const TString &frag2, const TString &frag3, const TString &frag4) const
{
  return m_outputDir
         + (frag1 != "" ? "/" + frag1 : "")
         + (frag2 != "" ? "/" + frag2 : "")
         + (frag3 != "" ? "/" + frag3 : "")
         + (frag4 != "" ? "/" + frag4 : "");
}


bool Selector::mkdir(const TString &frag1, const TString &frag2, const TString &frag3) const
{
  TString fullName = path(frag1, frag2, frag3);

  if (!gSystem->AccessPathName(fullName)) { return false; }

  gSystem->Exec(Form("mkdir -p %s", fullName.Data()));
  return true;
}


TGraphErrors *Selector::fitValues(const TString &varName, const Parameterization &pdf, bool sumW2Errors, const TString &name, const TString &yTitle)
{
  TGraphErrors *tge = new TGraphErrors(nSteps());
  tge->SetTitle("");
  tge->SetName((name != "" ? name : varName) + (sumW2Errors ? "_sumW2Errors" : ""));

  for (unsigned int i = 0; i < nSteps(); i++) {
    double val = m_scanVar->getMin() + i * m_scanStep;
    TFile *f = TFile::Open(path(pdf.GetName(), pointDir(i), "results.root"));

    if (!f || !f->IsOpen()) {
      cout << Form("ERROR: missing file %s  at position %g.", path(pdf.GetName(), pointDir(i), "results.root").Data(), val) << endl;
      return 0;
    }

    RooWorkspace *ws  = f ? (RooWorkspace *)f->Get(pdf.GetName() + (sumW2Errors ? "_sumW2Errors" : "")) : 0;

    if (!ws) {
      cout << Form("ERROR : invalid file %s, please delete and re-run the computation", path(pdf.GetName(), pointDir(i), "results.root").Data()) << endl;
      return 0;
    }

    RooRealVar *var = ws ? ws->var(varName) : 0;

    if (!var) {
      cout << Form("ERROR : variable %s was not found in the workspace.", varName.Data()) << endl;
      return 0;
    }

    if (verbosity(3)) cout << Form("INFO: at %s = %g, setting %s = %g (scanning from %g to %g in steps of %g)", m_scanVar->GetName(), val, varName.Data(), var->getVal(),
                                     m_scanVar->getMin(), m_scanVar->getMax(), m_scanStep) << endl;

    tge->SetPoint(i, val, var->getVal());
    tge->SetPointError(i, 0, var->getError());
    delete f;
  }

  tge->GetXaxis()->SetTitle(Form("%s [%s]", m_scanVar->GetTitle(), m_scanVar->getUnit()));
  tge->GetYaxis()->SetTitle(yTitle);
  return tge;
}


TGraphErrors *Selector::functionValues(const ConfigValue &func, const TString &name, const TString &yTitle)
{
  TGraphErrors *tge = new TGraphErrors(nSteps());
  tge->SetName(name != "" ? name : func.GetName());
  tge->SetTitle("");

  for (unsigned int i = 0; i < nSteps(); i++) {
    double val = m_scanVar->getMin() + i * m_scanStep;
    tge->SetPoint(i, val, func.value(val));
  }

  tge->GetXaxis()->SetTitle(Form("%s [%s]", m_scanVar->GetTitle(), m_scanVar->getUnit()));
  tge->GetYaxis()->SetTitle(yTitle);
  return tge;
}


bool Selector::printResults(const std::vector<Result *> &results)
{
  if (results.size() == 0) {
    cout << "ERROR: empty results list!" << endl;
    return false;
  }

  std::vector<Result *> sorted_results = results;
  // For the sorting options see SpuriousSignalResult::operator< in BkgParam.cxx
  std::sort(sorted_results.rbegin(), sorted_results.rend(), pointerSort); // sort in descending order
  TString resultsStr = results[0]->headerStr() + "\n";
  bool found = false;

  for (auto result : sorted_results) {
    resultsStr += result->valueStr();

    if (result->isPassing() && !found) { found = true; resultsStr += "  <== Selected"; } // take the first one, as the vector is sorted

    resultsStr += "\n";
  }

  if (sorted_results[0]->isPassing())
  { resultsStr += "==> PDF " + sorted_results[0]->GetName() + " is selected\n"; }
  else
  { resultsStr += "==> No PDF passes all the selections\n"; }

  ofstream os(path("results.txt"));
  os << resultsStr;
  os.close();

  if (verbosity(1)) { cout << endl << " === Final Results === " << endl << resultsStr << endl; }

  return true;
}


bool Selector::run()
{
  std::vector<Result *> results;

  if (!makeResults(results)) { return false; }

  if (!printResults(results)) { return false; }

  for (auto result : results) { delete result; }

  return true;
}


TGraphErrors *Selector::winMax(const TGraphErrors &graph)
{
  TGraphErrors *winMaxGraph = new TGraphErrors();
  winMaxGraph->SetName(graph.GetName() + TString("_winMax"));
  winMaxGraph->GetXaxis()->SetTitle(graph.GetXaxis()->GetTitle());
  winMaxGraph->GetYaxis()->SetTitle(graph.GetYaxis()->GetTitle());

  for (int i = 0; i < graph.GetN(); i++) {
    int jMax = 0;
    double pos = graph.GetX()[i];

    // Only consider points within the specified scan range
    if (pos < m_scanVar->getMin() || pos > m_scanVar->getMax()) { continue; }

    // For each point, scan the specified range around it and report the highest SS value
    double rangeMin = m_rangeMin.value(pos);
    double rangeMax = m_rangeMax.value(pos);

    if (verbosity(3)) { cout << Form("INFO: winMax of graph %s at position %g, using range %g to %g", graph.GetName(), pos, rangeMin, rangeMax) << endl; }

    double maxVal = 0;

    for (int j = 0; j < graph.GetN(); j++) {
      double rangePos = graph.GetX()[j];

      if (rangePos < rangeMin || rangePos > rangeMax) { continue; }

      double val = graph.GetY()[j];

      if (fabs(val) > fabs(maxVal)) {
        maxVal = val;
        jMax = j;
      }
    }

    int n = winMaxGraph->GetN();
    winMaxGraph->SetPoint(n, pos, maxVal);
    winMaxGraph->SetPointError(n, 0, graph.GetErrorY(jMax));
  }

  return winMaxGraph;
}


TString Result::headerStr() const
{
  TString s = Form(format("Name", "", true) + "s", "Name");

  for (auto name : m_names) { s += Form("   " + format(name, ".3g") + "s", name.Data()); }

  s +=  Form("   " + format("Result") + "s", "Result");
  return s;
}


TString Result::valueStr() const
{
  TString s = Form(format("Name", "", true) + "s", GetName().Data());

  for (auto name : m_names) { s += Form("   " + format(name, ".3g") + ".3g", m_vals.find(name)->second); }

  s +=  Form("   " + format("Result") + "s", isPassing() ? "PASS" : "FAIL");
  return s;
}


TString Result::format(const TString &name, const TString &valueSuffix, bool rev) const
{
  int l = name.Length();

  for (auto n : m_names) {
    TString s;

    if (name == "Name") { s = n; }
    else if (name == "Result") { s = "PASS"; }
    else { s = Form(valueSuffix, m_vals.find(n)->second); }

    if (s.Length() > l) { l = s.Length(); }
  }

  return TString("%") + (rev ? "-" : "") + Form("%d", l);
}


bool Configurable::setup(TEnv &reader)
{
  m_verbosity = reader.GetValue("Verbosity", 1);
  return true;
}


bool TF1Value::setup(TEnv &reader, const TString &scanVarName, bool allowFail)
{
  if (!Configurable::setup(reader)) { return false; }

  TString spec = reader.GetValue(GetName(), "");

  if (spec == "") {
    if (allowFail) {
      if (verbosity(2)) { cout << Form("INFO: no expression found for function '%s', failing silently", GetName().Data()) << endl; }

      return false;
    }

    cout << Form("ERROR: an expression for function '%s' should be provided using the '%s = ...' syntax", GetName().Data(), GetName().Data()) << endl;
    return false;
  }

  m_tf1 = new TF1(GetName(), spec.ReplaceAll(scanVarName, "x"));

  if (m_tf1->IsZombie()) {
    delete m_tf1;
    m_tf1 = 0;
    cout << Form("ERROR: invalid expression '%s' for function '%s', please fix '%s = ...' syntax", spec.Data(), GetName().Data(), GetName().Data()) << endl;
    return false;
  }

  if (verbosity(2)) { cout << Form("INFO: using defining function '%s' using expression '%s'", GetName().Data(), spec.Data()) << endl; }

  return true;
}


bool VarValue::setup(TEnv &reader, const TString &scanVarName, bool allowFail)
{
  if (!Configurable::setup(reader)) { return false; }

  TString varName = reader.GetValue(GetName() + ".Name", "");

  if (varName == "") {
    if (allowFail) {
      if (verbosity(2)) { cout << Form("INFO: no function name found for variable '%s', failing silently", GetName().Data()) << endl; }

      return false;
    }

    cout <<  Form("ERROR: could not find name specification '%s.Name = ...' for variable '%s'", GetName().Data(), GetName().Data()) << endl;
    return false;
  }

  TString varFile = reader.GetValue(GetName() + ".File", "");

  if (varFile == "") {
    if (allowFail) {
      if (verbosity(2)) { cout << Form("INFO: no filename found for variable '%s', failing silently", GetName().Data()) << endl; }

      return false;
    }

    cout <<  Form("ERROR: could not find filename specification '%s.File = ...' for variable '%s'", GetName().Data(), GetName().Data()) << endl;
    return false;
  }

  TString varWS = reader.GetValue(GetName() + ".Workspace", "");

  if (varWS == "") {
    if (allowFail) {
      if (verbosity(2)) { cout << Form("INFO: no workspace name found for variable '%s', failing silently", GetName().Data()) << endl; }

      return false;
    }

    cout <<  Form("ERROR: could not find workspace name specification '%s.Workspace = ...' for variable '%s'", GetName().Data(), GetName().Data()) << endl;
    return false;
  }

  TFile *fRS = TFile::Open(Selector::find_file(varFile));

  if (!fRS || !fRS->IsOpen()) {
    cout << Form("ERROR: could not open file '%s', please check value of %s.File.", varFile.Data(), GetName().Data()) << endl;
    return false;
  }

  RooWorkspace *wRS = (RooWorkspace *)fRS->Get(varWS);

  if (!wRS) {
    cout << Form("ERROR: could not locate workspace '%s' in file '%s', please check %s.Workspace and %s.File statements.",
                 varWS.Data(), varFile.Data(), GetName().Data(), GetName().Data()) << endl;
    return false;
  }

  m_var = wRS->function(varName);

  if (!m_var) {
    cout << Form("ERROR: could not locate real function '%s' in workspace '%s' of file '%s', please check %s.Name, %s.Workspace and %s.File statements.",
                 varName.Data(), varWS.Data(), varFile.Data(), GetName().Data(), GetName().Data(), GetName().Data()) << endl;
    return false;
  }

  m_scanVar = wRS->var(scanVarName);

  if (!m_scanVar) {
    cout << Form("ERROR: could not locate scanning variable '%s' in workspace '%s' of file '%s', please check %s.Name, %s.Workspace and %s.File statements.",
                 scanVarName.Data(), varWS.Data(), varFile.Data(), GetName().Data(), GetName().Data(), GetName().Data()) << endl;
    return false;
  }

  if (verbosity(2)) { cout << Form("INFO: defined variable '%s'", GetName().Data()) << endl; }

  return true;
}


bool Function::setup(TEnv &reader, const TString &scanVarName, bool allowFail)
{
  if (m_tf1.setup(reader, scanVarName, true)) {
    if (verbosity(2)) { cout << Form("INFO: defined function '%s' using functional expression '%s'", GetName().Data(), m_tf1.GetName().Data()) << endl; }

    return true;
  }

  if (m_var.setup(reader, scanVarName, true)) {
    if (verbosity(2)) { cout << Form("INFO: defined function '%s' using RooRealVar '%s'", GetName().Data(), m_var.GetName().Data()) << endl; }

    return true;
  }

  if (!allowFail) {
    cout << Form("ERROR: could not set up function '%s' using either TF1 or variable options.", GetName().Data()) << endl;
    return false;
  }

  if (verbosity(2)) { cout << Form("INFO: could not set up function '%s', no valid configuration found for either TF1 or RooRealVar options.", GetName().Data()) << endl; }

  return false;
}


bool SignalYield::setup(TEnv &reader, const TString &scanVarName, double targetLumi)
{
  if (!Configurable::setup(reader)) { return false; }

  m_targetLumi = targetLumi;

  if (!m_xs.setup(reader, scanVarName, true) && !m_yield.setup(reader, scanVarName, true)) {
    cout << Form("ERROR: could not set up signal yield '%s', no valid configuration found for either yield or cross-section options", GetName().Data()) << endl;
    return false;
  }

  if (m_xs.hasValue()) {
    if (verbosity(2)) { cout << Form("INFO: defined signal yield '%s' from cross-section expression.", GetName().Data()) << endl; }

    m_lumi = 1;
    return true;
  }

  m_lumi = reader.GetValue(GetName() + "Yield.IntegratedLuminosity", -1.0);

  if (m_lumi < 0) {
    cout << Form("ERROR: a luminosity corresponding to the signal yield must be specified using the '%sYield.IntegratedLuminosity = <value>' syntax", GetName().Data()) << endl;
    return false;
  }

  if (verbosity(2)) { cout << Form("INFO: defined signal yield '%s' from yield and luminosity expressions.", GetName().Data()) << endl; }

  return true;
}


double SignalYield::value(double position, double def) const
{
  if (!hasValue()) { return def; }

  if (m_xs.hasValue()) { return m_targetLumi * m_xs.value(position); }

  return m_targetLumi / m_lumi * m_yield.value(position);
}


bool Data::setup(TEnv &reader, RooRealVar &obsVar, double targetLumi)
{
  if (!Configurable::setup(reader)) { return false; }

  TString dataFile = reader.GetValue(GetName() + ".File", "");

  if (dataFile == "") {
    cout << Form("ERROR: a filename containing the dataset to use must be specified using the '%s.File: <filename>' syntax", GetName().Data()) << endl;
    return false;
  }

  TFile *f = TFile::Open(Selector::find_file(dataFile));

  if (!f || !f->IsOpen()) {
    cout << Form("ERROR: file %s cannot be opened, please fix the '%s.File: <filename>' entry", dataFile.Data(), GetName().Data()) << endl;
    return false;
  }

  TString histName = reader.GetValue(GetName() + ".HistogramName", "");

  if (dataFile == "") {
    cout << Form("ERROR: the name of a histogram to use as dataset must be specified using the '%s.HistogramName: <name>' syntax", GetName().Data()) << endl;
    return false;
  }

  TH1 *hist = (TH1 *)f->Get(histName);

  if (!hist) {
    cout << Form("ERROR: histogram %s not found in file %s, please fix dataset configuration.", histName.Data(), dataFile.Data()) << endl;
    return false;
  }

  double dataLumi = reader.GetValue(GetName() + ".IntegratedLuminosity", -1.0);

  if (dataLumi < 0) {
    cout << Form("ERROR : an integrated luminosity for the dataset must be specified using the '%s.IntegratedLuminosity = <lumi>' syntax",
                 GetName().Data()) << endl;
    return false;
  }

  m_data = new RooDataHist(GetName(), "", obsVar, hist, targetLumi / dataLumi);

  if (verbosity(2)) { cout << "INFO: successfully imported data from histogram " << histName << " in file " << dataFile << endl; }

  // Do this last!
  // If error bars are >40% or so then uncertainty is no longer gaussian and prob(chi2) is not correct.
  TH1 *h_rebinned = RebinHistforChi2(*hist, obsVar.getMin(), obsVar.getMax());
  m_data_forChi2 = new RooDataHist(GetName() + "_ForChi2", "", obsVar, h_rebinned, targetLumi / dataLumi);

  return true;
}

TH1 *Data::RebinHistforChi2(TH1 &hist, double histmin, double histmax)
{
  // Find all factors that can evenly rebin histogram.
  // Also force the binning to be at least 1 GeV/bin.
  std::vector<int> factors;

  for (int i = 1; i < hist.GetNbinsX(); ++i) {
    if (!(hist.GetNbinsX() % i) &&
        (hist.GetXaxis()->GetBinWidth(1)*i >= 1.)) { factors.push_back(i); }
  }

  TH1 *ret_hist = (TH1 *)hist.Clone(Form("%s_rebin", hist.GetName()));

  double max_relative_bin_error = 0;

  int the_factor = 1;

  for (unsigned int i = 0; i < factors.size(); ++i) {
    TH1 *clone = (TH1 *)hist.Clone(Form("%s_clone_%d", hist.GetName(), i));

    if (factors[i] > 1) { clone->Rebin(factors[i]); }

    int new_lowedge  = clone->GetXaxis()->GetBinLowEdge(clone->GetXaxis()->FindBin(histmin));
    int new_highedge = clone->GetXaxis()->GetBinLowEdge(clone->GetXaxis()->FindBin(histmax));

    if (fabs(new_lowedge - histmin) > FLT_MIN * 2) {
      if (verbosity(2)) { cout << "bin edge " << new_lowedge << " is not consistent with obsVar min " << histmin << endl; }

      continue;
    }

    if (fabs(new_highedge - histmax) > FLT_MIN * 2) {
      if (verbosity(2)) { cout << "bin edge " << new_highedge << " is not consistent with obsVar max " << histmax << endl; }

      continue;
    }

    // A reasonable factor to rebin.
    the_factor = factors[i];

    max_relative_bin_error = 0;

    for (int i = 0; i < clone->GetNbinsX(); ++i) {
      // histmin and histmax should be taken from obsVar.getMin() and getMax() (the region of the fit).
      if (clone->GetBinCenter(i + 1) < histmin) { continue; }

      if (clone->GetBinCenter(i + 1) > histmax) { continue; }

      if (clone->GetBinContent(i + 1) == 0) {
        max_relative_bin_error = 1;
        continue;
      }

      double relative_error = clone->GetBinError(i + 1) / float(clone->GetBinContent(i + 1));

      if (relative_error > max_relative_bin_error) { max_relative_bin_error = relative_error; }
    }

    // If the relative error is > 0.4 then the error are not yet gaussian.
    if (max_relative_bin_error > 0.4) {
      delete clone;
      continue;
    }

    // If you got to here then the relative error is more gaussian. Rebin with factors[i].
    if (factors[i] > 1) {
      if (verbosity(1)) cout << "INFO: Rebinning a copy of the histogram by " << factors[i]
                               << " (bin width " << clone->GetBinWidth(1)
                               << ") to ensure a well-behaving chi2." << endl;
    } else if (verbosity(1)) { cout << "INFO: histogram has sufficient statistics for chi2." << endl; }

    break;
  }

  if (max_relative_bin_error > 0.4) {
    if (verbosity(1)) cout << "WARNING: Maximum bin error is " << max_relative_bin_error
                             << " meaning p(chi2) might not be valid." << endl;
  }

  ret_hist->Rebin(the_factor);
  return ret_hist;
}

Parameterization::Parameterization(const TString &name)
  : Configurable(name), m_ws(0), m_obsVar(0), m_scanVar(0), m_totalPdf(0), m_sigPdf(0), m_bkgPdf(0), m_nSignal(0), m_nBkg(0)
{
}


bool Parameterization::setup(TEnv &reader, const TString &scanVarName)
{
  if (!Configurable::setup(reader)) { return false; }

  if (verbosity(1)) { cout << "INFO: setting up PDF '" << GetName() << "'" << endl; }

  // Create the workspace
  m_ws = new RooWorkspace(GetName(), "");
  m_ws->autoImportClassCode(true);

  // Build the observable
  TString obsSpec = reader.GetValue("Observable", "");

  if (obsSpec == "") {
    cout << "ERROR: an observable must be specified using the 'Observable: var[min,max]' syntax" << endl;
    return false;
  }

  m_obsVar = (RooRealVar *)m_ws->factory(obsSpec);

  if (!m_obsVar) {
    cout << Form("ERROR: observable specification %s is invalid", obsSpec.Data()) << endl;
    return false;
  }

  if (verbosity(2)) { cout << "INFO: successfully created observable " << obsSpec << endl; }

  int obsNBins = reader.GetValue("Observable.NBins", -1);

  if (obsNBins > 0) {
    m_obsVar->setBins(obsNBins);

    if (verbosity(2)) { cout << "INFO: setting observable bins to " << obsNBins << endl; }
  }

  // Build the signal and background normalizations
  TString nSigSpec = reader.GetValue("Signal.Norm", "");

  if (nSigSpec == "") {
    cout << "ERROR: the signal normalization parameter must be specified using the 'Signal.Norm: var[val,min,max]' syntax" << endl;
    return false;
  }

  m_nSignal = (RooRealVar *)m_ws->factory(nSigSpec);

  if (!m_nSignal) {
    cout << Form("ERROR: signal normalization specification %s is invalid", nSigSpec.Data()) << endl;
    return false;
  }

  TString nBkgSpec = reader.GetValue("Background.Norm", "");

  if (nBkgSpec == "") {
    cout << "ERROR: the background normalization parameter must be specified using the 'Background.Norm: var[val,min,max]' syntax" << endl;
    return false;
  }

  m_nBkg = (RooRealVar *)m_ws->factory(nBkgSpec);

  if (!m_nBkg) {
    cout << Form("ERROR: background normalization specification %s is invalid", nBkgSpec.Data()) << endl;
    return false;
  }

  // Retrieve the signal PDF
  TString sigPdfFile = reader.GetValue("Signal.PDF.File", "");

  if (sigPdfFile != "") {
    TFile *spf = TFile::Open(Selector::find_file(sigPdfFile));

    if (!spf || !spf->IsOpen()) {
      cout << Form("ERROR: file '%s' cannot be opened, please fix the 'Signal.PDF.File: <filename>' entry", sigPdfFile.Data()) << endl;
      return false;
    }

    TString wsName = reader.GetValue("Signal.PDF.Workspace", "");

    if (wsName == "") {
      cout << "ERROR: the name of the workspace containing the signal PDF must be specified using the 'Signal.PDF.Workspace: <name>' syntax" << endl;
      return false;
    }

    RooWorkspace *signalWS = (RooWorkspace *)spf->Get(wsName);

    if (!signalWS) {
      cout << Form("ERROR: workspace '%s' not found in file '%s', please fix the Signal.PDF configuration.", wsName.Data(), sigPdfFile.Data()) << endl;
      return false;
    }

    TString sigPdfName = reader.GetValue("Signal.PDF.Name", "");

    if (sigPdfName == "") {
      cout << "ERROR: the name of the signal PDF must be specified using the 'Signal.PDF.Name: <name>' syntax" << endl;
      return false;
    }

    m_sigPdf = signalWS->pdf(sigPdfName);

    if (!m_sigPdf) {
      cout << Form("ERROR: PDF '%s' not found in workspace '%s' of file '%s', please fix the Signal.PDF configuration.",
                   sigPdfName.Data(), wsName.Data(), sigPdfFile.Data()) << endl;
      return false;
    }

    if (verbosity(2)) cout << "INFO: successfully retrieved signal PDF '" << sigPdfName << "' from workspace '" << wsName
                             << "' in file " << sigPdfFile << endl;
  } else {
    TString sigPdfExpr = reader.GetValue("Signal.PDF.Expression", "");

    if (sigPdfExpr != "") {
      TString sigPars = reader.GetValue("Signal.PDF.Parameters", "");
      RooArgSet sigParSet(*m_obsVar);
      TString sigPdfName = reader.GetValue("Signal.PDF.Name", "Signal");
      m_sigPdf = makePdf(sigPdfName, sigPdfExpr, sigPars, sigParSet);

      if (!m_sigPdf) { return false; }

      if (verbosity(2)) { cout << "INFO: successfully created signal PDF '" << sigPdfName << "' from expression '" << sigPdfExpr << "'" << endl; }
    } else {
      cout << "ERROR: a signal PDF must be specified using either 'Signal.PDF.File: <filename>' or 'Signal.PDF.Expression: <expr>" << endl;
      return false;
    }
  }

  RooAbsCollection *freeSignalPars = m_sigPdf->getParameters(RooArgSet())->selectByAttrib("Constant", false);

  if (freeSignalPars->getSize() > 0) {
    if (reader.GetValue("Signal.PDF.FixParameters", false)) {
      freeSignalPars->setAttribAll("Constant", true);
    } else if (verbosity(1)) {
      cout << "INFO: signal PDF includes the following free parameters, please check that this is intended: ";
      freeSignalPars->Print();
    }
  }

  delete freeSignalPars;

  // Build the Background PDF
  TString expr = reader.GetValue(Form("%s.Expression", GetName().Data()), "");

  if (expr == "") {
    cout << Form("ERROR: an expression must be specified for '%s' using the '%s.Expression: <expr>' syntax",
                 GetName().Data(), GetName().Data()) << endl;
    return false;
  }

  TString pars = reader.GetValue(Form("%s.Parameters", GetName().Data()), "");
  RooArgSet parSet("parSet");
  parSet.add(*m_obsVar);
  m_bkgPdf = makePdf(GetName(), expr, pars, parSet);

  if (!m_bkgPdf) { return false; }

  if (verbosity(2)) { cout << "INFO: successfully created PDF '" << GetName() << "'" << endl; }

  // Build the total PDF
  m_totalPdf = new RooAddPdf(GetName() + "_total", "", RooArgList(*m_sigPdf, *m_bkgPdf), RooArgList(*m_nSignal, *m_nBkg));
  m_ws->import(*m_totalPdf, RooFit::Silence());
  // Now reconnect the pointers to the imported objects
  m_totalPdf = m_ws->pdf(m_totalPdf->GetName());
  m_obsVar = m_ws->var(m_obsVar->GetName());
  m_sigPdf = m_ws->pdf(m_sigPdf->GetName());
  m_bkgPdf = m_ws->pdf(m_bkgPdf->GetName());
  m_nSignal = m_ws->var(m_nSignal->GetName());
  m_nBkg = m_ws->var(m_nBkg->GetName());
  m_parameters.add(*m_totalPdf->getParameters(RooArgSet())->selectByAttrib("Constant", false));
  m_bkgParameters.add(*m_bkgPdf->getParameters(RooArgSet())->selectByAttrib("Constant", false));
  m_parameters.remove(*m_obsVar);
  m_bkgParameters.remove(*m_obsVar);
  m_scanVar = m_ws->var(scanVarName);

  if (!m_scanVar) {
    cout << Form("ERROR: scan variable with name '%s' not found.", scanVarName.Data()) << endl;
    return false;
  }

  if (verbosity(2)) { cout << "INFO: setup of PDF '" << GetName() << "' complete" << endl; }

  return true;
}


bool Parameterization::fit(RooAbsData &data, const RooLinkedList &opts, const RooLinkedList &opts_sumW2Errors, const TString &dir, TCanvas *c1, bool BkgOnly, RooAbsData *data_rebinned)
{
  if (verbosity(2)) {
    cout << "INFO: fit initial state" << endl;
    m_ws->allVars().selectByAttrib("Constant", false)->Print("V");

    if (verbosity(3)) { m_ws->allVars().Print("V"); }

    scanVar()->Print();
  }

  TFile *f = (dir != "" ? TFile::Open(dir + "/results.root", "RECREATE") : 0);
  RooFitResult *result;

  if (!BkgOnly) { result = m_totalPdf->fitTo(data, opts); }
  else { result = m_bkgPdf->fitTo(data, opts); }

  if (verbosity(2)) {
    cout << "INFO: fit final state" << endl;
    m_ws->allVars().selectByAttrib("Constant", false)->Print("V");

    if (verbosity(3)) { m_ws->allVars().Print("V"); }
  }

  if (f) {
    m_ws->Write(GetName());
    result->Write("result");
    data.Write("data");
    delete result;
  }

  if (c1 && !BkgOnly) {
    plot(data, c1);
    c1->Print(dir + "/fit.pdf");
    c1->SetLogy();
    c1->Print(dir + "/fit-logy.pdf");
    c1->SetLogy(false);
  }

  if (opts_sumW2Errors.GetSize() > 0) {
    RooFitResult *result_sumW2Errors;

    if (!BkgOnly) {
      result_sumW2Errors = m_totalPdf->fitTo(data, opts_sumW2Errors);
      // Fix background parameters. We want the error bars to reflect the hist stat errors, not the
      // parameter errors.
      setAllBkgParametersConstant(true);
      result_sumW2Errors = m_totalPdf->fitTo(data, opts_sumW2Errors);
    } else {
      result_sumW2Errors = m_bkgPdf->fitTo(data, opts_sumW2Errors);
      writeBkgOnlyChiSquare(data, data_rebinned, dir, c1);
    }

    if (f) {
      m_ws->Write(GetName() + "_sumW2Errors");
      result_sumW2Errors->Write("result_sumW2Errors");
    }

    delete result_sumW2Errors;

    if (!BkgOnly) { setAllBkgParametersConstant(false); } // reset background parameters.
  }

  if (f) { delete f; }

  return true;
}

void Parameterization::setAllBkgParametersConstant(bool b)
{
  // Does setConstant(true) or false on all background parameters.
  TIterator *it = m_bkgParameters.createIterator();

  for (RooRealVar *p = (RooRealVar *)it->Next(); p != 0; p = (RooRealVar *)it->Next()) {
    p->setConstant(b);
  }

  return;
}

void Parameterization::writeBkgOnlyChiSquare(RooAbsData &data, RooAbsData *data_rebinned, TString dir, TCanvas *c1)
{
  RooPlot *tmp_plot = m_obsVar->frame();
  data.plotOn(tmp_plot);
  m_bkgPdf->plotOn(tmp_plot);

  if (c1) {
    c1->cd();
    tmp_plot->Draw();
    c1->Print(dir + "/fit.pdf");
  }

  RooPlot *plot = m_obsVar->frame();
  data_rebinned->plotOn(plot);
  RooHist *hist = plot->getHist();
  m_bkgPdf->plotOn(plot);
  RooCurve *curve = plot->getCurve();


  if (c1) {
    c1->cd();
    plot->Draw();
    c1->Print(dir + "/fit_forChi2.pdf");
  }

  /////////////////////////////////////////
  // Taken from RooCurve.cxx to make sure the same ndof is used.
  /////////////////////////////////////////
  int nbins = 0;
  Int_t i, np = hist->GetN() ;
  Double_t x, y;

  // Find starting and ending bin of histogram based on range of RooCurve
  Double_t xstart, xstop ;

  curve->GetPoint(0, xstart, y) ;
  curve->GetPoint(curve->GetN() - 1, xstop, y) ;

  for (i = 0 ; i < np ; i++) {
    // Retrieve histogram contents
    hist->GetPoint(i, x, y) ;

    // Check if point is in range of curve
    if (x < xstart || x > xstop) { continue ; }

    // Add to chisq
    if (y != 0) {
      nbins++ ;
    }
  }

  /////////////////////////////////////////

  if (verbosity(3)) { cout << "nbins (hist): " << hist->GetN() << " nbins (curve): " << curve->GetN() << " nbins (result): " << nbins << endl; }

  // get chi2. Ndof is n parameters of the bkg function + 1 normalization parameter.
  int npars = m_bkgParameters.getSize() + 1;
  double chi2 = plot->chiSquare(npars);
  double prob = TMath::Prob(chi2 * (nbins - npars), nbins - npars);

  // write to file
  TString chi2string = Form("%2.5f %d %2.5f\n", chi2, nbins - npars, prob);
  ofstream os(dir + "/chi2.txt");
  os << chi2string;
  os.close();

  return;
}

void Parameterization::plot(RooAbsData &data, TCanvas *c1)
{
  RooPlot *plot = m_obsVar->frame();
  data.plotOn(plot);
  m_totalPdf->plotOn(plot);
  m_totalPdf->plotOn(plot, RooFit::LineStyle(kDashed), RooFit::Components(*m_bkgPdf));
  c1->cd();
  plot->SetMinimum(1E-7);
  plot->Draw();
}


RooAbsPdf *Parameterization::makePdf(const TString &name, const TString &expression, const TString &parameters,
                                     RooArgSet &parSet)
{
  // Build the list of parameters
  std::vector<TString> params;
  Selector::split(parameters, params, " ");

  for (std::vector<TString>::const_iterator param = params.begin(); param != params.end(); param++) {
    RooAbsArg *par = m_ws->factory(*param);

    if (!par) {
      cout << "ERROR: could not parse parameter expression '" << *param << "' while creating PDF '" << name << "'" << endl;
      return 0;
    }

    parSet.add(*par);
  }

  RooAbsPdf *pdf = 0;

  // First check if we have a specified class
  std::vector<TString> tokens;
  Selector::split(expression, tokens, "(");

  if (tokens.size() > 0) {
    bool ok = false;

    if (TClass::GetClass(tokens[0])) {
      if (verbosity(2)) { cout << "INFO: creating PDF '" << name << "' as instance of class '" << tokens[0] << "'." << endl; }

      ok = true;
    }

    if (tokens[0](0, 5) == "SUM::") {
      if (verbosity(2)) { cout << "INFO: creating PDF '" << name << "' as sum PDF '" << tokens[0] << "'." << endl; }

      ok = true;
    }

    if (ok) {
      pdf = dynamic_cast<RooAbsPdf *>(m_ws->factory(expression));

      if (!pdf) {
        cout << "ERROR: could not create PDF '" << name << "' from expression '" << expression << "'" << endl;
        return 0;
      }

      pdf->SetName(name);
      return pdf;
    }
  }

  // If not, assume an expression to use with RooGenericPdf
  RooFormula *formula = new RooFormula("test", expression, parSet);

  if (!formula->ok()) {
    cout << "ERROR: expression '" << expression << "' for PDF '" << name << "' is invalid." << endl;
    return 0;
  }

  pdf = new RooGenericPdf(name, "", expression, parSet);

  if (!pdf) {
    cout << "ERROR: could not create RooGenericPdf '" << name << "' from expression '" << expression << "'" << endl;
    return 0;
  }

  if (verbosity(2)) { cout << "INFO: creating PDF '" << name << "' as a RooGenericPDF." << endl; }

  return pdf;
}


void Selector::drawGraph(TGraphErrors &graph, const TString &opt, int lineColor, int fillColor, double line1, double line2)
{
  if (opt.Index("X") >= 0) {
    double maxVal, maxPos;
    find_max(graph, maxPos, maxVal);
    graph.GetHistogram()->SetMaximum(+1.2 * maxVal);
    graph.GetHistogram()->SetMinimum(-1.2 * maxVal);
  }

  graph.SetFillStyle(3001);
  graph.SetFillColor(fillColor);
  graph.Draw(opt);
  TGraph *g = new TGraph();
  *g = graph;
  g->SetLineWidth(2);
  g->SetLineColor(lineColor);
  g->Draw("LSAME");

  TLine line;
  line.SetLineStyle(kDashed);
  line.SetLineWidth(2);
  line.SetLineColor(2);

  if (line1 > -999) { line.DrawLine(graph.GetXaxis()->GetXmin(), line1, graph.GetXaxis()->GetXmax(), line1); }

  if (line2 > -999) { line.DrawLine(graph.GetXaxis()->GetXmin(), line2, graph.GetXaxis()->GetXmax(), line2); }
}


void Selector::drawGraphs(const std::vector<TGraphErrors *> &graphs, const TString &opt, double line1, double line2)
{
  unsigned int nColors[14] = {kBlack + 0, kRed + 1, kAzure - 2, kGreen + 1, kMagenta + 1, kCyan + 1, kOrange + 1,
                              kBlack + 2, kRed + 3, kBlue + 3,  kGreen + 3, kMagenta + 3, kCyan + 3, kOrange + 3
                             };
  double maxMaxVal = 0, maxVal, maxPos;

  for (unsigned int i = 0; i < graphs.size(); i++) {
    find_max(*graphs[i], maxPos, maxVal);

    if (fabs(maxVal) > maxMaxVal) { maxMaxVal = fabs(maxVal); }
  }

  graphs[0]->GetHistogram()->SetMaximum(+1.2 * maxMaxVal);
  graphs[0]->GetHistogram()->SetMinimum(-1.2 * maxMaxVal);

  for (unsigned int i = 0; i < graphs.size(); i++) {
    graphs[i]->SetFillColor(nColors[i % 14]);
    graphs[i]->SetLineColor(nColors[i % 14]);
    graphs[i]->Draw(opt + (i == 0 ? "A" : "SAME"));
    TGraph *g = new TGraph();
    *g = *graphs[i];
    g->SetLineWidth(2);
    g->Draw("LSAME");
  }

  TLine line;
  line.SetLineStyle(kDashed);
  line.SetLineWidth(2);
  line.SetLineColor(2);

  if (line1 > -999) { line.DrawLine(graphs[0]->GetXaxis()->GetXmin(), line1, graphs[0]->GetXaxis()->GetXmax(), line1); }

  if (line2 > -999) { line.DrawLine(graphs[0]->GetXaxis()->GetXmin(), line2, graphs[0]->GetXaxis()->GetXmax(), line2); }
}


TGraphErrors *Selector::ratio(const TGraphErrors &graph, const TGraph &denom, const TString &name, const TString &yTitle, bool difference)
{
  TGraphErrors *ratioGraph = new TGraphErrors();
  ratioGraph->SetName(name);

  for (int i = 0; i < graph.GetN(); i++) {
    ratioGraph->SetPoint(i, graph.GetX()[i], graph.GetY()[i] / denom.GetY()[i] - (difference ? 1 : 0));
    ratioGraph->SetPointError(i, 0, graph.GetErrorY(i) / denom.GetY()[i]);
  }

  ratioGraph->GetXaxis()->SetTitle(graph.GetXaxis()->GetTitle());
  ratioGraph->GetYaxis()->SetTitle(yTitle);
  return ratioGraph;
}

TGraphErrors *Selector::ratio_af2_1sigma(const TGraphErrors &graph, const TGraph &denom, const TString &name, const TString &yTitle, bool difference, int nSigma)
{
  TGraphErrors *ratioGraph = new TGraphErrors();
  ratioGraph->SetName(name);

  for (int i = 0; i < graph.GetN(); i++) {
    double mu_fit = graph.GetY()[i];
    double mu_err = graph.GetErrorY(i);
    double mu_af2_1sigma = 0;

    if (mu_fit - nSigma * mu_err > 0) { mu_af2_1sigma = mu_fit - nSigma * mu_err; }
    else if (mu_fit + nSigma * mu_err < 0) { mu_af2_1sigma = mu_fit + nSigma * mu_err; }

    ratioGraph->SetPoint(i, graph.GetX()[i], mu_af2_1sigma / denom.GetY()[i] - (difference ? 1 : 0));
    ratioGraph->SetPointError(i, 0, 0);
  }

  ratioGraph->GetXaxis()->SetTitle(graph.GetXaxis()->GetTitle());
  ratioGraph->GetYaxis()->SetTitle(yTitle);
  return ratioGraph;
}

TGraphErrors *Selector::ratio_af2_2sigma(const TGraphErrors &graph, const TGraph &denom, const TString &name, const TString &yTitle, bool difference)
{
  return ratio_af2_1sigma(graph, denom, name, yTitle, difference, 2);
}

TGraphErrors *Selector::Z(const TGraphErrors &graph, const TGraphErrors &uncert, const TString &name, const TString &yTitle)
{
  TGraphErrors *zGraph = new TGraphErrors();
  zGraph->SetName(name);

  for (int i = 0; i < graph.GetN(); i++) {
    zGraph->SetPoint(i, graph.GetX()[i], graph.GetY()[i] / graph.GetErrorY(i));
    zGraph->SetPointError(i, 0, uncert.GetErrorY(i) / graph.GetErrorY(i));
  }

  zGraph->GetXaxis()->SetTitle(graph.GetXaxis()->GetTitle());
  zGraph->GetYaxis()->SetTitle(yTitle);
  return zGraph;
}


TGraphErrors *Selector::Z_af2_1sigma(const TGraphErrors &graph, const TGraphErrors &uncert, const TString &name, const TString &yTitle, int nSigma)
{
  TGraphErrors *zGraph = new TGraphErrors();
  zGraph->SetName(name);

  for (int i = 0; i < graph.GetN(); i++) {
    double z_fit = graph.GetY()[i];
    double z_err = uncert.GetErrorY(i);
    double z_af2_1sigma = 0;

    if (z_fit - nSigma * z_err > 0) { z_af2_1sigma = z_fit - nSigma * z_err; }
    else if (z_fit + nSigma * z_err < 0) { z_af2_1sigma = z_fit + nSigma * z_err; }

    zGraph->SetPoint(i, graph.GetX()[i], z_af2_1sigma / graph.GetErrorY(i));
    zGraph->SetPointError(i, 0, 0);
  }

  zGraph->GetXaxis()->SetTitle(graph.GetXaxis()->GetTitle());
  zGraph->GetYaxis()->SetTitle(yTitle);
  return zGraph;
}

TGraphErrors *Selector::Z_af2_2sigma(const TGraphErrors &graph, const TGraphErrors &uncert, const TString &name, const TString &yTitle)
{
  return Z_af2_1sigma(graph, uncert, name, yTitle, 2);
}

void Selector::find_max(TGraph &graph, double &maxPos, double &maxVal, bool useAbs)
{
  maxPos = maxVal = (useAbs ? 0 : -DBL_MAX);

  for (int i = 0; i < graph.GetN(); i++) {
    if (useAbs ? fabs(graph.GetY()[i]) > fabs(maxVal) : graph.GetY()[i] > maxVal) {
      maxPos = graph.GetX()[i];
      maxVal = graph.GetY()[i];
    }
  }
}


void Selector::split(const TString &str, std::vector<TString> &splits, const TString &delimiter)
{
  TObjArray *splitsArray = str.Tokenize(delimiter);

  for (int i = 0; i < splitsArray->GetEntries(); i++) { splits.push_back(splitsArray->At(i)->GetName()); }

  delete splitsArray;
}


TString Selector::find_file(const TString &name)
{
  TString candidate = name;

  if (!gSystem->AccessPathName(candidate)) { return candidate; }

  candidate = "../" + name;

  if (!gSystem->AccessPathName(candidate)) { return candidate; }

  TString rcb = gSystem->Getenv("ROOTCOREBIN");
  rcb = rcb(0, rcb.Length() - 11); // remove "RootCoreBin"
  candidate = rcb + name;

  if (!gSystem->AccessPathName(candidate)) { return candidate; }

  return "";
}
