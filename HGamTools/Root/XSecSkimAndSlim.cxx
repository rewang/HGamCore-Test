#include "HGamTools/XSecSkimAndSlim.h"

#include "HGamAnalysisFramework/HGamVariables.h"

// this is needed to distribute the algorithm to the workers
ClassImp(SkimAndSlim)



XSecSkimAndSlim::XSecSkimAndSlim(const char *name)
  : SkimAndSlim(name)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
}



EL::StatusCode XSecSkimAndSlim::createOutput()
{
  // Check whether unfolding skimming should be applied
  m_unfoldSkim = config()->getBool("XSecSkimAndSlim.UnfoldingSkim", true);

  // Here you create any histograms you want to fill in execute()
  if (HG::isMC()) {
    histoStore()->createTH1F("pT_H", 100, 0, 1000);
  }

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode XSecSkimAndSlim::execute()
{
  ANA_CHECK_SET_TYPE(EL::StatusCode);

  //============================================================================
  // Here you do everything that needs to be done on all events, e.g. fill
  // efficiency histograms. You can only retrieve const containers and info
  // objects at this point.
  if (HG::isMC()) {
    histoStore()->fillTH1F("pT_H", var::pT_h1.truth() / HG::GeV, var::weight());
  }



  //============================================================================
  // Check if the event passes possible skimming configuration. You can add
  // your own event skimming here, though you can't update object decorations to
  // make your decision. You can only retrieve const containers and info objects
  // at this point.
  bool skip = false;
  ANA_CHECK(SkimAndSlim::skipEvent(skip));

  if (skip) { return EL::StatusCode::SUCCESS; }

  // XSec defined function checking if the event (isPassed || isFiducial)
  // If it's data, the cutFlow flag is used (skip this)
  if (m_unfoldSkim && HG::isMC()) {
    ANA_CHECK(skipXSecEvent(skip));

    if (skip) { return EL::StatusCode::SUCCESS; }
  }



  //============================================================================
  // Record configured containers to output. After calling recordContainer()
  // you are able to retrieve non-const containers and info objects you're
  // saving to the output. After this function call, you can
  // do everything that needs to be done on events passing the skimming,
  // e.g. add additional variables, object decorations.
  // After calling recordContainers() you always need to call fillEvent()
  ANA_CHECK(SkimAndSlim::recordContainers());

  // Add a few variables
  static SG::AuxElement::ConstAccessor<int> N_lep_15("N_lep_15");
  static SG::AuxElement::ConstAccessor<int> N_j_30("N_j_30");
  static SG::AuxElement::ConstAccessor<float> met_TST("met_TST");
  static SG::AuxElement::ConstAccessor<float> pT_yy("pT_yy");
  static SG::AuxElement::ConstAccessor<float> met_NonHad("met_NonHad");
  static SG::AuxElement::ConstAccessor<float> weightJvt_30("weightJvt_30");
  static SG::AuxElement::ConstAccessor<float> weightN_lep_15("weightN_lep_15");

  xAOD::EventInfo *eventInfo = nullptr;

  static SG::AuxElement::Accessor<char> catXS_lepton("catXS_lepton");
  static SG::AuxElement::Accessor<char> catXS_MET("catXS_MET");

  static SG::AuxElement::Accessor<float> pT_j2("pT_j2_30");
  static SG::AuxElement::Accessor<float> pT_j3("pT_j3_30");
  static SG::AuxElement::Accessor<float> Dy_j_j("Dy_j_j_30");
  static SG::AuxElement::Accessor<float> weightCatXS_lepton("weightCatXS_lepton");
  static SG::AuxElement::Accessor<float> weightCatXS_VBF("weightCatXS_VBF");
  static SG::AuxElement::Accessor<float> weightCatXS_ttH("weightCatXS_ttH");

  // Update nominal and systematic detector-level
  std::vector<std::string> syslist = getSystematicNames();
  syslist.push_back("");

  for (std::string sys : syslist) {
    std::string cont = "HGamEventInfo" + sys;
    eventInfo = nullptr;
    ANA_CHECK(getContainer<xAOD::EventInfo>(eventInfo, cont));

    catXS_lepton(*eventInfo) = N_lep_15(*eventInfo) > 0;
    catXS_MET(*eventInfo) = met_TST(*eventInfo) > 80.0 * HG::GeV && pT_yy(*eventInfo) > 80.0 * HG::GeV;

    if (N_j_30(*eventInfo) == 1) { pT_j2(*eventInfo) = 0; }

    if (N_j_30(*eventInfo) == 2) { pT_j3(*eventInfo) = 0; }

    if (N_j_30(*eventInfo) <  2) { Dy_j_j(*eventInfo) = -99; }

    if (HG::isMC()) {
      float weightJvt = weightJvt_30.isAvailable(*eventInfo) ? weightJvt_30(*eventInfo) : 1.0;
      float oldWeight_ttH = weightCatXS_ttH(*eventInfo);
      weightCatXS_ttH(*eventInfo) = oldWeight_ttH * weightJvt;
      weightCatXS_VBF(*eventInfo) = weightJvt;
      weightCatXS_lepton(*eventInfo) = weightN_lep_15(*eventInfo);
    }

    // if (sys == "" && wk()->xaodEvent()->contains<xAOD::PhotonContainer>("HGamPhotons")) {
    //   static SG::AuxElement::ConstAccessor<char> isPassedRelPtCuts("isPassedRelPtCuts");
    //   static SG::AuxElement::ConstAccessor<char> isPassedIsolation("isPassedIsolation");
    //   static SG::AuxElement::ConstAccessor<char> isTight("isTight");
    //   static SG::AuxElement::ConstAccessor<char> isIso("isIsoFixedCutLoose");
    //   static SG::AuxElement::ConstAccessor<unsigned int> isEMTight("isEMTight");

    //   static SG::AuxElement::Accessor<char> catXS_ttH_loose("catXS_ttH_loose");
    //   static SG::AuxElement::Accessor<char> isPassedYJ("isPassedYJ");
    //   static SG::AuxElement::Accessor<char> isPassedJY("isPassedJY");
    //   static SG::AuxElement::Accessor<char> isPassedJJ("isPassedJJ");

    //   const xAOD::PhotonContainer *photons = nullptr;
    //   ANA_CHECK( getConstContainer<xAOD::PhotonContainer>(photons, "HGamPhotons") );
    //   const xAOD::Photon *lead = (*photons)[0];
    //   const xAOD::Photon *subl = (*photons)[1];

    //   bool lead_looseNotTight = !isTight(*lead) || !isIso(*lead);
    //   bool subl_looseNotTight = !isTight(*subl) || !isIso(*subl);

    //   bool lead_looseP4 = !(isEMTight(*lead) & HG::PhotonLoosePrime4) && !isTight(*lead);
    //   bool subl_looseP4 = !(isEMTight(*subl) & HG::PhotonLoosePrime4) && !isTight(*subl);

    //   bool ttH_semi = N_lep_15(*eventInfo) >= 1 && N_j_30(*eventInfo) >=3;
    //   bool ttH_had  = N_lep_15(*eventInfo) == 0 && N_j_30(*eventInfo) >=4;
    //   catXS_ttH_loose(*eventInfo) = isPassedRelPtCuts(*eventInfo) && (lead_looseNotTight || subl_looseNotTight) && (ttH_semi || ttH_had);

    //   bool basicLoose = isPassedRelPtCuts(*eventInfo) && isPassedIsolation(*eventInfo);
    //   isPassedYJ(*eventInfo) = basicLoose && isTight(*lead) && subl_looseP4;
    //   isPassedJY(*eventInfo) = basicLoose && lead_looseP4 && isTight(*subl);
    //   isPassedJJ(*eventInfo) = basicLoose && lead_looseP4 && subl_looseP4;
    // }
  }

  std::vector<std::string> contToSave = getContainersToSave();

  if (HG::isMC() && std::find(contToSave.begin(), contToSave.end(), "HGamTruthEventInfo") != contToSave.end()) {
    // Update particle-level
    eventInfo = nullptr;
    ANA_CHECK(getContainer<xAOD::EventInfo>(eventInfo, "HGamTruthEventInfo"));

    catXS_lepton(*eventInfo) = N_lep_15(*eventInfo) > 0;
    catXS_MET(*eventInfo) = (met_NonHad.isAvailable(*eventInfo) ? met_NonHad(*eventInfo) > 80.0 * HG::GeV : false) && pT_yy(*eventInfo) > 80.0 * HG::GeV;

    if (N_j_30(*eventInfo) == 1) { pT_j2(*eventInfo) = 0; }

    if (N_j_30(*eventInfo) == 2) { pT_j3(*eventInfo) = 0; }

    if (N_j_30(*eventInfo) <  2) { Dy_j_j(*eventInfo) = -99; }
  }



  //============================================================================
  // Fill output file with event. This writes the event to disk, so should be
  // the last thing that happens.
  SkimAndSlim::fillEvent();

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode XSecSkimAndSlim::skipXSecEvent(bool &skip)
{
  // Check if the reco event passes selection for any systematic variation, or
  // if the truth event passes selection. Otherwise skip.
  ANA_CHECK_SET_TYPE(EL::StatusCode);

  // Default behavior is to keep event
  skip = false;

  const xAOD::EventInfo *cEventInfo = nullptr;
  static SG::AuxElement::ConstAccessor<char> isPassedBasic("isPassedBasic");
  static SG::AuxElement::ConstAccessor<char> isPassed("isPassed");
  static SG::AuxElement::ConstAccessor<char> isFiducial("isFiducial");

  // Check nominal true pass
  ANA_CHECK(getConstContainer<xAOD::EventInfo>(cEventInfo, "HGamTruthEventInfo"));

  if (isFiducial.isAvailable(*cEventInfo) && isFiducial(*cEventInfo))
  { return EL::StatusCode::SUCCESS; }

  // Check nominal and systematic reco pass
  std::vector<std::string> syslist = getSystematicNames();
  syslist.push_back("");

  for (std::string sys : syslist) {
    std::string cont = "HGamEventInfo" + sys;
    ANA_CHECK(getConstContainer<xAOD::EventInfo>(cEventInfo, cont));

    if (isPassedBasic(*cEventInfo) && isPassed(*cEventInfo))
    { return EL::StatusCode::SUCCESS; }
  }

  skip = true;
  return EL::StatusCode::SUCCESS;
}



