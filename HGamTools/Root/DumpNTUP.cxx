#include "HGamTools/DumpNTUP.h"
#include "HGamAnalysisFramework/HgammaIncludes.h"

// this is needed to distribute the algorithm to the workers
ClassImp(DumpNTUP)

DumpNTUP::DumpNTUP(const char *name)
  : HgammaAnalysis(name)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
}

DumpNTUP::~DumpNTUP()
{
  // Here you delete any memory you allocated during your analysis.
}

EL::StatusCode DumpNTUP::execute()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  //HgammaAnalysis::execute();

  clear_variables();
  // Get TEvent
  xAOD::TEvent *event = wk()->xaodEvent();

  const xAOD::EventInfo *HgameventInfo = 0;

  if (! event->retrieve(HgameventInfo, "HGamEventInfo").isSuccess()) {
    Error("execute()", "Failed to retrieve Hgam event info collection. Exiting.");
    return EL::StatusCode::FAILURE;
  }

  const xAOD::EventInfo *eventInfo = 0;

  if (! event->retrieve(eventInfo, "EventInfo").isSuccess()) {
    Error("execute()", "Failed to retrieve event info collection. Exiting.");
    return EL::StatusCode::FAILURE;
  }

  const xAOD::PhotonContainer *Hgamphotons = 0;

  if (! event->retrieve(Hgamphotons, "HGamPhotons").isSuccess()) {
    Error("execute()", "Failed to retrieve hgam photons collection. Exiting.");
    return EL::StatusCode::FAILURE;
  }

  bool is_mc = eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION);

  // pass flags
  isPassedExoticLoose = (bool) HgameventInfo->auxdataConst<char>("isPassedExotic");
  isPassedLowHighMyy = (bool) HgameventInfo->auxdataConst<char>("isPassedLowHighMyy");
  isPassedIsolationExotic = (bool) HgameventInfo->auxdataConst<char>("isPassedIsolationExotic");
  isPassedlPtCutsExotic = (bool) HgameventInfo->auxdataConst<char>("isPassedlPtCutsExotic");
  isPassedIsolationLowHighMyy = (bool) HgameventInfo->auxdataConst<char>("isPassedIsolationLowHighMyy");
  isPassedRelPtCutsLowHighMyy = (bool) HgameventInfo->auxdataConst<char>("isPassedRelPtCutsLowHighMyy");
  isPassedPID = (bool) HgameventInfo->auxdataConst<char>("isPassedPID");

  if (!(Hgamphotons->size() >= 2)) { return EL::StatusCode::SUCCESS; }

  auto leading_photon = (*Hgamphotons)[0];
  auto subleading_photon = (*Hgamphotons)[1];

  topoetcone40_leading = leading_photon->auxdataConst<float>("topoetcone40");
  topoetcone40_subleading = subleading_photon->auxdataConst<float>("topoetcone40");

  topoetcone20_leading = leading_photon->auxdataConst<float>("topoetcone20");
  topoetcone20_subleading = subleading_photon->auxdataConst<float>("topoetcone20");

  ptcone20_leading = leading_photon->auxdataConst<float>("ptcone20");
  ptcone40_leading = leading_photon->auxdataConst<float>("ptcone20");

  ptcone20_subleading = subleading_photon->auxdataConst<float>("ptcone20");
  ptcone40_subleading = subleading_photon->auxdataConst<float>("ptcone40");

  bool pass_iso_lead = (topoetcone40_leading < (pt_leading * 0.022 + 7000.));
  bool pass_iso_sublead = (topoetcone40_subleading < (pt_subleading * 0.022 + 7000.));

  if (isPassedlPtCutsExotic && isPassedPID && pass_iso_lead &&  pass_iso_sublead) { isPassedExoticLoose = true; }
  else { isPassedExoticLoose = false; }

  pass_iso_lead = ((topoetcone40_leading < (pt_leading * 0.022 + 2450.)) && (ptcone20_leading < (pt_leading * 0.05)));
  pass_iso_sublead = ((topoetcone40_subleading < (pt_subleading * 0.022 + 2450.)) && (ptcone20_subleading < (pt_subleading * 0.05)));

  if (isPassedlPtCutsExotic && isPassedPID && pass_iso_lead &&  pass_iso_sublead) { isPassedExoticTight = true; }
  else { isPassedExoticTight = false; }

  etaS2_leading = leading_photon->auxdataConst<float>("eta_s2");
  etaS2_subleading = subleading_photon->auxdataConst<float>("eta_s2");

  mass = HgameventInfo->auxdataConst<float>("m_yy");
  mass_gev = HgameventInfo->auxdataConst<float>("m_yy") * 0.001;
  mgg = mass_gev;

  pt_leading = HgameventInfo->auxdataConst<float>("pT_y1");
  pt_subleading = HgameventInfo->auxdataConst<float>("pT_y2");

  tight_leading = (bool) leading_photon->auxdataConst<char>("isTight");
  tight_subleading = (bool) subleading_photon->auxdataConst<char>("isTight");

  isEM_subleading = subleading_photon->auxdataConst<unsigned int>("isEMTight");
  isEM_leading = leading_photon->auxdataConst<unsigned int>("isEMTight");

  pass_2g50_loose_trigger = (bool) eventInfo->auxdataConst<char>("passTrig_HLT_2g50_loose");
  pass_g35_g25_loose_trigger = (bool) eventInfo->auxdataConst<char>("passTrig_HLT_g35_loose_g25_loose");

  averageIntPerXing = eventInfo->auxdataConst<float>("averageInteractionsPerCrossing");
  actualIntPerXing = 0;
  run_number = eventInfo->runNumber();
  bcid = eventInfo->bcid();

  dist_front = eventInfo->auxdataConst<int>("bunchDistanceFromFront");
  gap_before_train = eventInfo->auxdataConst<int>("bunchGapBeforeTrain");

  event_number = eventInfo->auxdataConst<unsigned long long>("eventNumber");
  lumiblock_number = -1;
  time_stamp = eventInfo->auxdataConst<unsigned int>("timeStamp");
  mc_channel_number = -1;
  mc_event_number = -1;

  E0_leading = leading_photon->auxdataConst<float>("rawcl_Es0");
  E1_leading = leading_photon->auxdataConst<float>("rawcl_Es1");
  E2_leading = leading_photon->auxdataConst<float>("rawcl_Es2");
  E3_leading = leading_photon->auxdataConst<float>("rawcl_Es3");
  E0_subleading = subleading_photon->auxdataConst<float>("rawcl_Es0");
  E1_subleading = subleading_photon->auxdataConst<float>("rawcl_Es1");
  E2_subleading = subleading_photon->auxdataConst<float>("rawcl_Es2");
  E3_subleading = subleading_photon->auxdataConst<float>("rawcl_Es3");

  E1_E2_leading = E1_leading / E2_leading;

  raw_energy_leading = E1_leading + E2_leading + E3_leading;
  raw_energy_subleading = E1_subleading + E2_subleading + E3_subleading;

  raw_et_leading = raw_energy_leading / cosh(etaS2_leading);
  raw_et_subleading = raw_energy_subleading / cosh(etaS2_subleading);

  e_on_eraw_leading =  leading_photon->e() / raw_energy_leading;
  e_on_eraw_subleading = subleading_photon->e() / raw_energy_subleading;

  mass_raw = mass_gev * sqrt((raw_energy_leading * raw_energy_subleading) / (leading_photon->e() * subleading_photon->e()));

  LV_leading = leading_photon->p4();
  LV_subleading = subleading_photon->p4();
  LV_diphoton = LV_leading + LV_subleading;

  deltaR = LV_leading.DeltaR(LV_subleading);

  Rconv_leading = leading_photon->auxdataConst<float>("Rconv"); //conversionRadius");
  Rconv_subleading = subleading_photon->auxdataConst<float>("Rconv"); //conversionRadius");

  Zconv_leading = leading_photon->auxdata< float >("zconv");
  Zconv_subleading = subleading_photon->auxdata< float >("zconv");

  PVz = HgameventInfo->auxdataConst<float>("hardestVertexZ");

  npvs = HgameventInfo->auxdataConst<int>("numberOfPrimaryVertices");

  if (is_mc) {
    total_weight = weight() * lumiXsecWeight();

    pileup_weight = HgameventInfo->auxdataConst<float>("pileupWeight");
    vertex_weight = HgameventInfo->auxdataConst<float>("vertexWeight");
    xs_weight = lumiXsecWeight();

    total_weight_nopu = vertex_weight * xs_weight;
  }

  ED_central = eventInfo->auxdataConst<float>("centralEventShapeDensity");
  ED_forward = eventInfo->auxdataConst<float>("forwardEventShapeDensity");

  mass_MVA = 0;
  costhetastar = HgameventInfo->auxdataConst<float>("cosTS_yy");
  deltaphi = LV_leading.DeltaPhi(LV_subleading);

  phi_subleading = subleading_photon->phi();
  phi_leading = leading_photon->phi();
  eta_subleading = subleading_photon->eta();
  eta_leading = leading_photon->eta();

  conv_leading = leading_photon->auxdataConst<int>("conversionType");
  conv_subleading = subleading_photon->auxdataConst<int>("conversionType");

  author_leading = 0;
  author_subleading = 0;

  Z_pointing_leading = 0;
  Z_pointing_subleading = 0;

  loose_leading = 0;
  loose_subleading = 0;

  leading_photon->showerShapeValue(Rhad1_leading, xAOD::EgammaParameters::ShowerShapeType::Rhad1);
  leading_photon->showerShapeValue(Rhad_leading, xAOD::EgammaParameters::ShowerShapeType::Rhad);
  leading_photon->showerShapeValue(e277_leading, xAOD::EgammaParameters::ShowerShapeType::e277);
  leading_photon->showerShapeValue(Reta_leading, xAOD::EgammaParameters::ShowerShapeType::Reta);
  leading_photon->showerShapeValue(Rphi_leading, xAOD::EgammaParameters::ShowerShapeType::Rphi);
  leading_photon->showerShapeValue(weta2_leading, xAOD::EgammaParameters::ShowerShapeType::weta2);
  leading_photon->showerShapeValue(f1_leading, xAOD::EgammaParameters::ShowerShapeType::f1);
  leading_photon->showerShapeValue(DeltaE_leading, xAOD::EgammaParameters::ShowerShapeType::DeltaE);
  leading_photon->showerShapeValue(wtots1_leading, xAOD::EgammaParameters::ShowerShapeType::wtots1);
  leading_photon->showerShapeValue(fracs1_leading, xAOD::EgammaParameters::ShowerShapeType::fracs1);
  leading_photon->showerShapeValue(weta1_leading, xAOD::EgammaParameters::ShowerShapeType::weta1);
  leading_photon->showerShapeValue(Eratio_leading, xAOD::EgammaParameters::ShowerShapeType::Eratio);

  subleading_photon->showerShapeValue(Rhad1_subleading, xAOD::EgammaParameters::ShowerShapeType::Rhad1);
  subleading_photon->showerShapeValue(Rhad_subleading, xAOD::EgammaParameters::ShowerShapeType::Rhad);
  subleading_photon->showerShapeValue(e277_subleading, xAOD::EgammaParameters::ShowerShapeType::e277);
  subleading_photon->showerShapeValue(Reta_subleading, xAOD::EgammaParameters::ShowerShapeType::Reta);
  subleading_photon->showerShapeValue(Rphi_subleading, xAOD::EgammaParameters::ShowerShapeType::Rphi);
  subleading_photon->showerShapeValue(weta2_subleading, xAOD::EgammaParameters::ShowerShapeType::weta2);
  subleading_photon->showerShapeValue(f1_subleading, xAOD::EgammaParameters::ShowerShapeType::f1);
  subleading_photon->showerShapeValue(DeltaE_subleading, xAOD::EgammaParameters::ShowerShapeType::DeltaE);
  subleading_photon->showerShapeValue(wtots1_subleading, xAOD::EgammaParameters::ShowerShapeType::wtots1);
  subleading_photon->showerShapeValue(fracs1_subleading, xAOD::EgammaParameters::ShowerShapeType::fracs1);
  subleading_photon->showerShapeValue(weta1_subleading, xAOD::EgammaParameters::ShowerShapeType::weta1);
  subleading_photon->showerShapeValue(Eratio_subleading, xAOD::EgammaParameters::ShowerShapeType::Eratio);

  sumpt2_tracks = 0;

  if (is_mc) {
    topoetcone40_DDcorrected_leading = leading_photon->auxdataConst<float>("topoetcone40_DDcorrected");
    topoetcone40_DDcorrected_subleading = subleading_photon->auxdataConst<float>("topoetcone40_DDcorrected");

    parent_pdgID_leading = leading_photon->auxdataConst<int>("parentPdgId");
    pdgID_leading = leading_photon->auxdataConst<int>("pdgId");
    origin_leading = 0;
    type_leading = 0;

    parent_pdgID_subleading = subleading_photon->auxdataConst<int>("parentPdgId");
    pdgID_subleading = subleading_photon->auxdataConst<int>("pdgId");
    origin_subleading = 0;
    type_subleading = 0;

    isEM_nofudge_subleading = subleading_photon->auxdataConst<unsigned int>("isEMTight_nofudge");
    isEM_nofudge_leading = leading_photon->auxdataConst<unsigned int>("isEMTight_nofudge");
  }

  if (sample_NTUP) { sample_NTUP->setFilterPassed(); }

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode DumpNTUP::finalize()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.

  sample_NTUP->setFilterPassed();

  /*
    TFile *file = wk()->getOutputFile ("MxAOD");
    if (wk()->xaodEvent()->finishWritingTo(file).isFailure()) {
      Error("finalize()","Failed to finish writing event to output file!");
      return EL::StatusCode::FAILURE;
    }
  */
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode DumpNTUP::initialize()
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.

  HgammaAnalysis::initialize();

  std::cout << "In Initialize()\n";
  /*
    // Setup output file
    TFile *file = wk()->getOutputFile("MxAOD");
    if (wk()->xaodEvent()->writeTo(file).isFailure()) {
      Error("initialize()", "Failed to write event to output file!");
      return EL::StatusCode::FAILURE;
    }
  */
  std::string outputStreamName = "NTUP_output";

  if (!outputStreamName.empty()) {
    sample_NTUP = EL::getNTupleSvc(wk(), outputStreamName);
    std::cout << "Creating output NTUPLE\n";
  } else {
    sample_NTUP = 0;
  }

  if (sample_NTUP) {
    // Event variables
    sample_NTUP->tree()->Branch("isPassedExoticLoose", &isPassedExoticLoose);
    sample_NTUP->tree()->Branch("isPassedExoticTight", &isPassedExoticTight);
    sample_NTUP->tree()->Branch("isPassedLowHighMyy", &isPassedLowHighMyy);
    sample_NTUP->tree()->Branch("isPassedIsolationExotic", &isPassedIsolationExotic);
    sample_NTUP->tree()->Branch("isPassedlPtCutsExotic", &isPassedlPtCutsExotic);
    sample_NTUP->tree()->Branch("isPassedIsolationLowHighMyy", &isPassedIsolationLowHighMyy);
    sample_NTUP->tree()->Branch("isPassedRelPtCutsLowHighMyy", &isPassedRelPtCutsLowHighMyy);
    sample_NTUP->tree()->Branch("isPassedPID", &isPassedPID);

    sample_NTUP->tree()->Branch("mgg", &mgg)->SetTitle("Invariant mass in GeV");

    sample_NTUP->tree()->Branch("etaS2_leading", &etaS2_leading);
    sample_NTUP->tree()->Branch("etaS2_subleading", &etaS2_subleading);

    sample_NTUP->tree()->Branch("mass_gev", &mass_gev)->SetTitle("Invariant mass in GeV");
    sample_NTUP->tree()->Branch("mass_MVA", &mass_MVA)->SetTitle("Invariant mass in GeV");
    sample_NTUP->tree()->Branch("mass_raw", &mass_raw);
    sample_NTUP->tree()->Branch("mass_egamma", &mass_egamma);
    sample_NTUP->tree()->Branch("mass", &mass)->SetTitle("Invariant mass in MeV");

    sample_NTUP->tree()->Branch("pass_2g50_loose_trigger", &pass_2g50_loose_trigger);
    sample_NTUP->tree()->Branch("pass_g35_g25_loose_trigger", &pass_g35_g25_loose_trigger);

    sample_NTUP->tree()->Branch("averageIntPerXing", &averageIntPerXing);
    sample_NTUP->tree()->Branch("ED_central", &ED_central);
    sample_NTUP->tree()->Branch("ED_forward", &ED_forward);

    sample_NTUP->tree()->Branch("run_number", &run_number);
    sample_NTUP->tree()->Branch("event_number", &event_number);
    sample_NTUP->tree()->Branch("lumiblock_number", &lumiblock_number);
    sample_NTUP->tree()->Branch("time_stamp", &time_stamp);
    sample_NTUP->tree()->Branch("bcid", &bcid);

    sample_NTUP->tree()->Branch("dist_front", &dist_front);
    sample_NTUP->tree()->Branch("gap_before_train", &gap_before_train);

    sample_NTUP->tree()->Branch("mc_channel_number", &mc_channel_number);
    sample_NTUP->tree()->Branch("mc_event_number", &mc_event_number);

    sample_NTUP->tree()->Branch("nPV", &npvs);
    sample_NTUP->tree()->Branch("xs", &xs);
    sample_NTUP->tree()->Branch("xs_ami", &xs_ami);
    sample_NTUP->tree()->Branch("filter_eff", &filter_eff);
    sample_NTUP->tree()->Branch("filter_eff_ami", &filter_eff_ami);

    sample_NTUP->tree()->Branch("one_photon", &one_photon);
    sample_NTUP->tree()->Branch("two_photons", &two_photons);

    // Weights
    sample_NTUP->tree()->Branch("pileup_weight", &pileup_weight);
    sample_NTUP->tree()->Branch("vertex_weight", &vertex_weight);
    sample_NTUP->tree()->Branch("MC_weight", &MC_weight);
    sample_NTUP->tree()->Branch("xs_weight", &xs_weight);
    sample_NTUP->tree()->Branch("event_weight", &event_weight);
    sample_NTUP->tree()->Branch("prel_weight", &prel_weight);
    sample_NTUP->tree()->Branch("total_weight", &total_weight);
    sample_NTUP->tree()->Branch("total_weight_nopu", &total_weight_nopu);
    sample_NTUP->tree()->Branch("total_events", &total_events);
    sample_NTUP->tree()->Branch("non_derived_total_events", &non_derived_total_events);

    sample_NTUP->tree()->Branch("SF_leading", &SF_leading);
    sample_NTUP->tree()->Branch("SF_unc_leading", &SF_unc_leading);
    sample_NTUP->tree()->Branch("SF_subleading", &SF_subleading);
    sample_NTUP->tree()->Branch("SF_unc_subleading", &SF_unc_subleading);
    sample_NTUP->tree()->Branch("SF_diphoton", &SF_diphoton);
    sample_NTUP->tree()->Branch("SF_1UP_diphoton", &SF_1UP_diphoton);
    sample_NTUP->tree()->Branch("SF_1DOWN_diphoton", &SF_1DOWN_diphoton);

    // Cinematic variables
    sample_NTUP->tree()->Branch("sumpt2_tracks", &sumpt2_tracks);
    sample_NTUP->tree()->Branch("costhetastar", &costhetastar);
    sample_NTUP->tree()->Branch("deltaphi", &deltaphi);

    sample_NTUP->tree()->Branch("LV_leading", &LV_leading);
    sample_NTUP->tree()->Branch("LV_subleading", &LV_subleading);
    sample_NTUP->tree()->Branch("LV_diphoton", &LV_diphoton);
    sample_NTUP->tree()->Branch("LV_egamma", &LV_egamma);

    sample_NTUP->tree()->Branch("pt_subleading", &pt_subleading);
    sample_NTUP->tree()->Branch("pt_leading", &pt_leading);
    sample_NTUP->tree()->Branch("phi_subleading", &phi_subleading);
    sample_NTUP->tree()->Branch("phi_leading", &phi_leading);
    sample_NTUP->tree()->Branch("eta_subleading", &eta_subleading);
    sample_NTUP->tree()->Branch("eta_leading", &eta_leading);

    sample_NTUP->tree()->Branch("author_leading", &author_leading);
    sample_NTUP->tree()->Branch("author_subleading", &author_subleading);

    sample_NTUP->tree()->Branch("raw_energy_leading", &raw_energy_leading);
    sample_NTUP->tree()->Branch("raw_energy_subleading", &raw_energy_subleading);
    sample_NTUP->tree()->Branch("raw_et_leading", &raw_et_leading);
    sample_NTUP->tree()->Branch("raw_et_subleading", &raw_et_subleading);
    sample_NTUP->tree()->Branch("e_on_eraw_leading", &e_on_eraw_leading);
    sample_NTUP->tree()->Branch("e_on_eraw_subleading", &e_on_eraw_subleading);

    sample_NTUP->tree()->Branch("deltaR", &deltaR);

    sample_NTUP->tree()->Branch("max_cell_gain_leading", &max_cell_gain_leading);
    sample_NTUP->tree()->Branch("max_cell_time_leading", &max_cell_time_leading);
    sample_NTUP->tree()->Branch("max_cell_e_leading", &max_cell_e_leading);

    sample_NTUP->tree()->Branch("max_cell_gain_subleading", &max_cell_gain_subleading);
    sample_NTUP->tree()->Branch("max_cell_time_subleading", &max_cell_time_subleading);
    sample_NTUP->tree()->Branch("max_cell_e_subleading", &max_cell_e_subleading);

    sample_NTUP->tree()->Branch("Rconv_leading", &Rconv_leading);
    sample_NTUP->tree()->Branch("Rconv_subleading", &Rconv_subleading);

    sample_NTUP->tree()->Branch("E0_leading", &E0_leading);
    sample_NTUP->tree()->Branch("E1_leading", &E1_leading);
    sample_NTUP->tree()->Branch("E2_leading", &E2_leading);
    sample_NTUP->tree()->Branch("E3_leading", &E3_leading);
    sample_NTUP->tree()->Branch("E0_subleading", &E0_subleading);
    sample_NTUP->tree()->Branch("E1_subleading", &E1_subleading);
    sample_NTUP->tree()->Branch("E2_subleading", &E2_subleading);
    sample_NTUP->tree()->Branch("E3_subleading", &E3_subleading);

    sample_NTUP->tree()->Branch("Zconv_leading", &Zconv_leading);
    sample_NTUP->tree()->Branch("Zconv_subleading", &Zconv_subleading);

    sample_NTUP->tree()->Branch("PVz", &PVz);

    sample_NTUP->tree()->Branch("topoetcone40_leading", &topoetcone40_leading);
    sample_NTUP->tree()->Branch("topoetcone40_DDcorrected_leading", &topoetcone40_DDcorrected_leading);
    sample_NTUP->tree()->Branch("topoetcone40_rel17_leading", &topoetcone40_rel17_leading);

    sample_NTUP->tree()->Branch("topoetcone40_electron_leading", &topoetcone40_electron_leading);
    sample_NTUP->tree()->Branch("topoetcone40_trouble_electron_leading", &topoetcone40_trouble_electron_leading);
    sample_NTUP->tree()->Branch("topoetcone40_rel17_electron_leading", &topoetcone40_rel17_electron_leading);
    sample_NTUP->tree()->Branch("author_electron_leading", &author_electron_leading);

    sample_NTUP->tree()->Branch("topoetcone40_subleading", &topoetcone40_subleading);
    sample_NTUP->tree()->Branch("topoetcone40_DDcorrected_subleading", &topoetcone40_DDcorrected_subleading);
    sample_NTUP->tree()->Branch("topoetcone20_leading", &topoetcone20_leading);
    sample_NTUP->tree()->Branch("topoetcone20_subleading", &topoetcone20_subleading);

    sample_NTUP->tree()->Branch("truth_etcone40_leading", &truth_etcone40_leading);
    sample_NTUP->tree()->Branch("truth_etcone40_subleading", &truth_etcone40_subleading);
    sample_NTUP->tree()->Branch("truth_etcone20_leading", &truth_etcone20_leading);
    sample_NTUP->tree()->Branch("truth_etcone20_subleading", &truth_etcone20_subleading);

    sample_NTUP->tree()->Branch("truth_local_etcone40_leading", &truth_local_etcone40_leading);
    sample_NTUP->tree()->Branch("truth_local_etcone40_subleading", &truth_local_etcone40_subleading);
    sample_NTUP->tree()->Branch("truth_local_etcone20_leading", &truth_local_etcone20_leading);
    sample_NTUP->tree()->Branch("truth_local_etcone20_subleading", &truth_local_etcone20_subleading);

    sample_NTUP->tree()->Branch("truth_etcone40_PUcorr_leading", &truth_etcone40_PUcorr_leading);
    sample_NTUP->tree()->Branch("truth_etcone40_PUcorr_subleading", &truth_etcone40_PUcorr_subleading);
    sample_NTUP->tree()->Branch("truth_etcone20_PUcorr_leading", &truth_etcone20_PUcorr_leading);
    sample_NTUP->tree()->Branch("truth_etcone20_PUcorr_subleading", &truth_etcone20_PUcorr_subleading);

    sample_NTUP->tree()->Branch("truth_ptcone40_leading", &truth_ptcone40_leading);
    sample_NTUP->tree()->Branch("truth_ptcone40_subleading", &truth_ptcone40_subleading);
    sample_NTUP->tree()->Branch("truth_ptcone20_leading", &truth_ptcone20_leading);
    sample_NTUP->tree()->Branch("truth_ptcone20_subleading", &truth_ptcone20_subleading);

    sample_NTUP->tree()->Branch("truth_ptcone40_PUcorr_leading", &truth_ptcone40_PUcorr_leading);
    sample_NTUP->tree()->Branch("truth_ptcone40_PUcorr_subleading", &truth_ptcone40_PUcorr_subleading);
    sample_NTUP->tree()->Branch("truth_ptcone20_PUcorr_leading", &truth_ptcone20_PUcorr_leading);
    sample_NTUP->tree()->Branch("truth_ptcone20_PUcorr_subleading", &truth_ptcone20_PUcorr_subleading);

    sample_NTUP->tree()->Branch("truth_rho_central", &truth_rho_central);
    sample_NTUP->tree()->Branch("truth_rho_forward", &truth_rho_forward);

    sample_NTUP->tree()->Branch("tight_leading", &tight_leading);
    sample_NTUP->tree()->Branch("my_tight_leading", &my_tight_leading);
    sample_NTUP->tree()->Branch("loose_leading", &loose_leading);
    sample_NTUP->tree()->Branch("loose_prime_leading", &loose_prime_leading);
    sample_NTUP->tree()->Branch("tight_subleading", &tight_subleading);
    sample_NTUP->tree()->Branch("loose_subleading", &loose_subleading);
    sample_NTUP->tree()->Branch("loose_prime_subleading", &loose_prime_subleading);

    sample_NTUP->tree()->Branch("bg_truth_match_leading", &bg_truth_match_leading);
    sample_NTUP->tree()->Branch("bg_truth_match_origin_leading", &bg_truth_match_origin_leading);

    sample_NTUP->tree()->Branch("pdgID_leading", &pdgID_leading);
    sample_NTUP->tree()->Branch("parent_pdgID_leading", &parent_pdgID_leading);
    sample_NTUP->tree()->Branch("type_leading", &type_leading);
    sample_NTUP->tree()->Branch("origin_leading", &origin_leading);
    sample_NTUP->tree()->Branch("truth_match_leading", &truth_match_leading);

    sample_NTUP->tree()->Branch("pdgID_subleading", &pdgID_subleading);
    sample_NTUP->tree()->Branch("parent_pdgID_subleading", &parent_pdgID_subleading);
    sample_NTUP->tree()->Branch("type_subleading", &type_subleading);
    sample_NTUP->tree()->Branch("origin_subleading", &origin_subleading);
    sample_NTUP->tree()->Branch("truth_match_subleading", &truth_match_subleading);

    sample_NTUP->tree()->Branch("conv_leading", &conv_leading);
    sample_NTUP->tree()->Branch("conv_subleading", &conv_subleading);

    sample_NTUP->tree()->Branch("isEM_leading", &isEM_leading);
    sample_NTUP->tree()->Branch("isEM_subleading", &isEM_subleading);
    sample_NTUP->tree()->Branch("isEM_nofudge_leading", &isEM_nofudge_leading);
    sample_NTUP->tree()->Branch("isEM_nofudge_subleading", &isEM_nofudge_subleading);
    sample_NTUP->tree()->Branch("photonOQ_leading", &photonOQ_leading);
    sample_NTUP->tree()->Branch("photonOQ_subleading", &photonOQ_subleading);

    sample_NTUP->tree()->Branch("Z_pointing_leading", &Z_pointing_leading);
    sample_NTUP->tree()->Branch("Z_pointing_subleading", &Z_pointing_subleading);

    sample_NTUP->tree()->Branch("Rhad1_leading", &Rhad1_leading);
    sample_NTUP->tree()->Branch("Rhad_leading", &Rhad_leading);
    sample_NTUP->tree()->Branch("e277_leading", &e277_leading);
    sample_NTUP->tree()->Branch("Reta_leading", &Reta_leading);
    sample_NTUP->tree()->Branch("Rphi_leading", &Rphi_leading);
    sample_NTUP->tree()->Branch("weta2_leading", &weta2_leading);
    sample_NTUP->tree()->Branch("f1_leading", &f1_leading);
    sample_NTUP->tree()->Branch("DeltaE_leading", &DeltaE_leading);
    sample_NTUP->tree()->Branch("wtots1_leading", &wtots1_leading);
    sample_NTUP->tree()->Branch("fracs1_leading", &fracs1_leading);
    sample_NTUP->tree()->Branch("weta1_leading", &weta1_leading);
    sample_NTUP->tree()->Branch("Eratio_leading", &Eratio_leading);

    sample_NTUP->tree()->Branch("Rhad1_subleading", &Rhad1_subleading);
    sample_NTUP->tree()->Branch("Rhad_subleading", &Rhad_subleading);
    sample_NTUP->tree()->Branch("e277_subleading", &e277_subleading);
    sample_NTUP->tree()->Branch("Reta_subleading", &Reta_subleading);
    sample_NTUP->tree()->Branch("Rphi_subleading", &Rphi_subleading);
    sample_NTUP->tree()->Branch("weta2_subleading", &weta2_subleading);
    sample_NTUP->tree()->Branch("f1_subleading", &f1_subleading);
    sample_NTUP->tree()->Branch("DeltaE_subleading", &DeltaE_subleading);
    sample_NTUP->tree()->Branch("wtots1_subleading", &wtots1_subleading);
    sample_NTUP->tree()->Branch("fracs1_subleading", &fracs1_subleading);
    sample_NTUP->tree()->Branch("weta1_subleading", &weta1_subleading);
    sample_NTUP->tree()->Branch("Eratio_subleading", &Eratio_subleading);

    sample_NTUP->tree()->Branch("Rhad1_leading_nofudge", &Rhad1_leading_nofudge);
    sample_NTUP->tree()->Branch("Rhad_leading_nofudge", &Rhad_leading_nofudge);
    sample_NTUP->tree()->Branch("e277_leading_nofudge", &e277_leading_nofudge);
    sample_NTUP->tree()->Branch("Reta_leading_nofudge", &Reta_leading_nofudge);
    sample_NTUP->tree()->Branch("Rphi_leading_nofudge", &Rphi_leading_nofudge);
    sample_NTUP->tree()->Branch("weta2_leading_nofudge", &weta2_leading_nofudge);
    sample_NTUP->tree()->Branch("f1_leading_nofudge", &f1_leading_nofudge);
    sample_NTUP->tree()->Branch("DeltaE_leading_nofudge", &DeltaE_leading_nofudge);
    sample_NTUP->tree()->Branch("wtots1_leading_nofudge", &wtots1_leading_nofudge);
    sample_NTUP->tree()->Branch("fracs1_leading_nofudge", &fracs1_leading_nofudge);
    sample_NTUP->tree()->Branch("weta1_leading_nofudge", &weta1_leading_nofudge);
    sample_NTUP->tree()->Branch("Eratio_leading_nofudge", &Eratio_leading_nofudge);

    sample_NTUP->tree()->Branch("Rhad1_subleading_nofudge", &Rhad1_subleading_nofudge);
    sample_NTUP->tree()->Branch("Rhad_subleading_nofudge", &Rhad_subleading_nofudge);
    sample_NTUP->tree()->Branch("e277_subleading_nofudge", &e277_subleading_nofudge);
    sample_NTUP->tree()->Branch("Reta_subleading_nofudge", &Reta_subleading_nofudge);
    sample_NTUP->tree()->Branch("Rphi_subleading_nofudge", &Rphi_subleading_nofudge);
    sample_NTUP->tree()->Branch("weta2_subleading_nofudge", &weta2_subleading_nofudge);
    sample_NTUP->tree()->Branch("f1_subleading_nofudge", &f1_subleading_nofudge);
    sample_NTUP->tree()->Branch("DeltaE_subleading_nofudge", &DeltaE_subleading_nofudge);
    sample_NTUP->tree()->Branch("wtots1_subleading_nofudge", &wtots1_subleading_nofudge);
    sample_NTUP->tree()->Branch("fracs1_subleading_nofudge", &fracs1_subleading_nofudge);
    sample_NTUP->tree()->Branch("weta1_subleading_nofudge", &weta1_subleading_nofudge);
    sample_NTUP->tree()->Branch("Eratio_subleading_nofudge", &Eratio_subleading_nofudge);

    sample_NTUP->tree()->Branch("E1_E2_leading", &E1_E2_leading);

    sample_NTUP->tree()->Branch("ptvarcone20_leading", &ptvarcone20_leading);
    sample_NTUP->tree()->Branch("ptcone20_leading", &ptcone20_leading);
    sample_NTUP->tree()->Branch("ptvarcone40_leading", &ptvarcone40_leading);
    sample_NTUP->tree()->Branch("ptcone40_leading", &ptcone40_leading);
    sample_NTUP->tree()->Branch("topoetcone20_Pt_leading", &topoetcone20_Pt_leading);
    sample_NTUP->tree()->Branch("ptvarcone20_Pt_leading", &ptvarcone20_Pt_leading);

    sample_NTUP->tree()->Branch("ptcone20_subleading", &ptcone20_subleading);
    sample_NTUP->tree()->Branch("ptcone40_subleading", &ptcone40_subleading);

    sample_NTUP->tree()->Branch("DD_corr_40_leading", &DD_corr_40_leading);
    sample_NTUP->tree()->Branch("DD_corr_40_subleading", &DD_corr_40_subleading);
    sample_NTUP->tree()->Branch("DD_corr_20_leading", &DD_corr_20_leading);
    sample_NTUP->tree()->Branch("DD_corr_20_subleading", &DD_corr_20_subleading);


    // Isolation variables
    sample_NTUP->tree()->Branch("topoetcone40", &topoetcone40);
    sample_NTUP->tree()->Branch("topoetcone30", &topoetcone30);
    sample_NTUP->tree()->Branch("topoetcone20", &topoetcone20);

    sample_NTUP->tree()->Branch("ptcone40", &ptcone40);
    sample_NTUP->tree()->Branch("ptcone30", &ptcone30);
    sample_NTUP->tree()->Branch("ptcone20", &ptcone20);

    sample_NTUP->tree()->Branch("ptvarcone40", &ptvarcone40);
    sample_NTUP->tree()->Branch("ptvarcone30", &ptvarcone30);
    sample_NTUP->tree()->Branch("ptvarcone20", &ptvarcone20);

    sample_NTUP->tree()->Branch("FixedCutTightCaloOnly_ld", &FixedCutTightCaloOnly_ld);
    sample_NTUP->tree()->Branch("FixedCutTight_ld", &FixedCutTight_ld);
    sample_NTUP->tree()->Branch("FixedCutLoose_ld", &FixedCutLoose_ld);

    sample_NTUP->tree()->Branch("FixedCutTightCaloOnly_subld", &FixedCutTightCaloOnly_subld);
    sample_NTUP->tree()->Branch("FixedCutTight_subld", &FixedCutTight_subld);
    sample_NTUP->tree()->Branch("FixedCutLoose_subld", &FixedCutLoose_subld);

    sample_NTUP->tree()->Branch("pass_truth_match", &pass_truth_match);
    sample_NTUP->tree()->Branch("parent_pdgID_truth_ld", &parent_pdgID_truth_ld);
    sample_NTUP->tree()->Branch("parent_pdgID_truth_subld", &parent_pdgID_truth_subld);

    sample_NTUP->tree()->Branch("origin_truth_ld", &origin_truth_ld);
    sample_NTUP->tree()->Branch("origin_truth_subld", &origin_truth_subld);

    // truth analysis variables
    sample_NTUP->tree()->Branch("two_truth_photons", &two_truth_photons);
    sample_NTUP->tree()->Branch("pass_eta_truth_analysis", &pass_eta_truth_analysis);
    sample_NTUP->tree()->Branch("truth_leading_LV", &truth_leading_LV);
    sample_NTUP->tree()->Branch("truth_subleading_LV", &truth_subleading_LV);
    sample_NTUP->tree()->Branch("truth_diphoton_LV", &truth_diphoton_LV);
    sample_NTUP->tree()->Branch("truth_leading_matched_leading_ph", &truth_leading_matched_leading_ph);
    sample_NTUP->tree()->Branch("truth_subleading_matched_subleading_ph", &truth_subleading_matched_subleading_ph);

  }

  return EL::StatusCode::SUCCESS;
}

void DumpNTUP::clear_variables()
{
  for (unsigned int i = 0; i < isEM_flags_lead.size(); i++) {
    isEM_flags_lead[i].second = 0;
    isEM_flags_sublead[i].second = 0;
    isEM_flags_nofudge_lead[i].second = 0;
    isEM_flags_nofudge_sublead[i].second = 0;
  }

  accepted_nominal = false;
  pass_egamma_selection = false;

  pass_truth_match = false;
  parent_pdgID_truth_ld = 0;
  parent_pdgID_truth_subld = 0;
  origin_truth_ld = 0;
  origin_truth_subld = 0;

  truth_match_leading = false;
  truth_match_subleading = false;
  tight_leading = false;
  my_tight_leading = false;
  tight_subleading = false;

  mass = 0.;
  pt_leading = 0.;

  topoetcone40.clear();
  topoetcone30.clear();
  topoetcone20.clear();

  ptcone40.clear();
  ptcone30.clear();
  ptcone20.clear();

  ptvarcone40.clear();
  ptvarcone30.clear();
  ptvarcone20.clear();

  FixedCutTightCaloOnly_ld = false;
  FixedCutTight_ld = false;
  FixedCutLoose_ld = false;

  FixedCutTightCaloOnly_subld = false;
  FixedCutTight_subld = false;
  FixedCutLoose_subld = false;

  SF_leading = 0;
  SF_unc_leading = 0;
  SF_subleading = 0;
  SF_unc_subleading = 0;
  SF_diphoton = 0;
  SF_1UP_diphoton = 0;
  SF_1DOWN_diphoton = 0;

  pass_2g50_loose_trigger = 0;
  pass_g35_g25_loose_trigger = 0;

  averageIntPerXing = 0;
  actualIntPerXing = 0;
  run_number = -1;
  bcid = -1;
  is_filled = 0;
  in_train = 0;
  from_beam_1or2 = -1;
  dist_front = -1;
  dist_tail = -1;
  bc_type = 0;
  gap_before_train = -1;
  gap_after_train = -1;
  gap_before_bunch = -1;
  gap_after_bunch = -1;
  bunch_spacing = -1;

  event_number = -1;
  lumiblock_number = -1;
  time_stamp = -1;
  mc_channel_number = -1;
  mc_event_number = -1;

  raw_energy_leading = 0;
  raw_energy_subleading = 0;

  raw_et_leading = 0;
  raw_et_subleading = 0;

  e_on_eraw_leading = 0;
  e_on_eraw_subleading = 0;

  mass_raw = 0;

  deltaR = 0;

  max_cell_gain_leading = -1;
  max_cell_time_leading = -1;
  max_cell_e_leading = -1;

  max_cell_gain_subleading = -1;
  max_cell_time_subleading = -1;
  max_cell_e_subleading = -1;

  Rconv_leading = -1;
  Rconv_subleading = -1;

  E0_leading = -1;
  E1_leading = -1;
  E2_leading = -1;
  E3_leading = -1;
  E0_subleading = -1;
  E1_subleading = -1;
  E2_subleading = -1;
  E3_subleading = -1;

  Zconv_leading = -1;
  Zconv_subleading = -1;

  PVz = -1;

  category = 0;

  npvs = 0;
  xs = 0;
  xs_ami = 0;
  filter_eff = 0;
  filter_eff_ami = 0;
  pileup_weight = 0;
  vertex_weight = 0;
  MC_weight = 0;
  xs_weight = 0;
  event_weight = 0;
  prel_weight = 0;
  total_weight = 0;
  total_weight_nopu = 0;

  SF_leading = 0;
  SF_unc_leading = 0;
  SF_subleading = 0;
  SF_unc_subleading = 0;
  SF_diphoton = 0;
  SF_1UP_diphoton = 0;
  SF_1DOWN_diphoton = 0;

  total_events = 0;
  non_derived_total_events = 0;

  ED_central = -1;
  ED_forward = -1;

  // cinematic variables
  mass = 0;
  mass_gev = 0;
  mgg = 0;
  mass_MVA = 0;
  mass_egamma = 0;
  costhetastar = -99;
  deltaphi = 0;

  rho_median = 0;
  pt_subleading = 0;
  pt_leading = 0;
  phi_subleading = 0;
  phi_leading = 0;
  eta_subleading = -99;
  eta_leading = -99;
  conv_subleading = 0;
  conv_leading = 0;
  isEM_subleading = 0;
  isEM_leading = 0;
  isEM_nofudge_subleading = 0;
  isEM_nofudge_leading = 0;

  photonOQ_leading = 0;
  photonOQ_subleading = 0;

  DD_corr_40_leading = 0;
  DD_corr_40_subleading = 0;
  DD_corr_20_leading = 0;
  DD_corr_20_subleading = 0;

  author_leading = 0;
  author_subleading = 0;

  topoetcone40_DDcorrected_leading = 0;
  topoetcone40_DDcorrected_subleading = 0;

  topoetcone40_leading = 0;
  topoetcone40_rel17_leading = 0;

  topoetcone40_rel17_electron_leading = 0;
  topoetcone40_trouble_electron_leading = 0;
  topoetcone40_electron_leading = 0;
  author_electron_leading = 0;

  topoetcone40_subleading = 0;
  topoetcone20_leading = 0;
  topoetcone20_subleading = 0;

  truth_etcone40_leading = 0;
  truth_etcone40_subleading = 0;
  truth_etcone20_leading = 0;
  truth_etcone20_subleading = 0;

  truth_etcone40_PUcorr_leading = 0;
  truth_etcone40_PUcorr_subleading = 0;
  truth_etcone20_PUcorr_leading = 0;
  truth_etcone20_PUcorr_subleading = 0;

  truth_ptcone40_leading = 0;
  truth_ptcone40_subleading = 0;
  truth_ptcone20_leading = 0;
  truth_ptcone20_subleading = 0;

  truth_ptcone40_PUcorr_leading = 0;
  truth_ptcone40_PUcorr_subleading = 0;
  truth_ptcone20_PUcorr_leading = 0;
  truth_ptcone20_PUcorr_subleading = 0;

  truth_local_etcone40_leading = 0;
  truth_local_etcone40_subleading = 0;
  truth_local_etcone20_leading = 0;
  truth_local_etcone20_subleading = 0;

  truth_rho_central = 0;
  truth_rho_forward = 0;

  Z_pointing_leading = 0;
  Z_pointing_subleading = 0;

  loose_leading = 0;
  loose_prime_leading = 0;
  tight_leading = 0;
  my_tight_leading = 0;
  tight_subleading = 0;
  loose_subleading = 0;
  loose_prime_subleading = 0;
  truth_match_leading = 0;
  truth_match_subleading = 0;

  bg_truth_match_leading = 0;
  bg_truth_match_origin_leading = 0;

  Rhad_leading = 0;
  e277_leading = 0;
  Reta_leading = 0;
  Rphi_leading = 0;
  weta2_leading = 0;
  f1_leading = 0;
  DeltaE_leading = 0;
  wtots1_leading = 0;
  weta1_leading = 0;
  fracs1_leading = 0;
  Eratio_leading = 0;

  Rhad_subleading = 0;
  e277_subleading = 0;
  Reta_subleading = 0;
  Rphi_subleading = 0;
  weta2_subleading = 0;
  f1_subleading = 0;
  DeltaE_subleading = 0;
  wtots1_subleading = 0;
  weta1_subleading = 0;
  fracs1_subleading = 0;
  Eratio_subleading = 0;

  Rhad_leading_nofudge = 0;
  e277_leading_nofudge = 0;
  Reta_leading_nofudge = 0;
  Rphi_leading_nofudge = 0;
  weta2_leading_nofudge = 0;
  f1_leading_nofudge = 0;
  DeltaE_leading_nofudge = 0;
  wtots1_leading_nofudge = 0;
  weta1_leading_nofudge = 0;
  fracs1_leading_nofudge = 0;
  Eratio_leading_nofudge = 0;

  Rhad_subleading_nofudge = 0;
  e277_subleading_nofudge = 0;
  Reta_subleading_nofudge = 0;
  Rphi_subleading_nofudge = 0;
  weta2_subleading_nofudge = 0;
  f1_subleading_nofudge = 0;
  DeltaE_subleading_nofudge = 0;
  wtots1_subleading_nofudge = 0;
  weta1_subleading_nofudge = 0;
  fracs1_subleading_nofudge = 0;
  Eratio_subleading_nofudge = 0;

  E1_E2_leading = 0;
  etaS2_leading = -99;
  etaS2_subleading = -99;

  ptvarcone20_leading = 0;
  ptcone20_leading = 0;
  ptvarcone40_leading = 0;
  ptcone40_leading = 0;
  topoetcone20_Pt_leading = 0;
  ptvarcone20_Pt_leading = 0;

  ptcone20_subleading = 0;
  ptcone40_subleading = 0;

  parent_pdgID_leading = 0;
  pdgID_leading = 0;
  origin_leading = 0;
  type_leading = 0;

  parent_pdgID_subleading = 0;
  pdgID_subleading = 0;
  origin_subleading = 0;
  type_subleading = 0;

  pass_truth_match = false;

  FixedCutTightCaloOnly_ld = false;
  FixedCutTight_ld = false;
  FixedCutLoose_ld = false;

  FixedCutTightCaloOnly_subld = false;
  FixedCutTight_subld = false;
  FixedCutLoose_subld = false;

  sumpt2_tracks = 0;

  truth_leading_LV.SetPtEtaPhiM(0., 0., 0., 0.);
  truth_subleading_LV.SetPtEtaPhiM(0., 0., 0., 0.);
  truth_diphoton_LV.SetPtEtaPhiM(0., 0., 0., 0.);
  LV_leading.SetPtEtaPhiM(0., 0., 0., 0.);
  LV_subleading.SetPtEtaPhiM(0., 0., 0., 0.);
  LV_diphoton.SetPtEtaPhiM(0., 0., 0., 0.);
  LV_egamma.SetPtEtaPhiM(0., 0., 0., 0.);

  one_photon = false;
  two_photons = false;

  // pass flags
  isPassedExoticLoose = false;
  isPassedExoticTight = false;
  isPassedLowHighMyy = false;
  isPassedIsolationExotic = false;
  isPassedlPtCutsExotic = false;
  isPassedIsolationLowHighMyy = false;
  isPassedRelPtCutsLowHighMyy = false;
  isPassedPID = false;
}
