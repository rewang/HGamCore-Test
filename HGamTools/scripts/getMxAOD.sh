#!/bin/bash

scriptName=$0

SetupIssue() {
  echo
  echo "$1"
  echo
  echo "To setup do:"
  echo "  export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase"
  echo "  source \${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh"
  echo "  localSetupRucioClients"
  echo "  rcSetup -q"
  exit 1
}

usage() {
  printf "\n%s\n" "$1"
  printf "\nUsage:\n   %s [OPTIONS] INPUT HTAG CONFIG FILELIST\n" "$scriptName"
  printf "\nExample:\n  %s -dm period2016 h017 MxAOD.config data_13TeV.txt\n" "$scriptName"
  printf "\nExample:\n  %s -dm -o /tmp periodE h010 MxAOD.config \n" "$scriptName"
  printf "\nOPTIONS, -d for downloading files, -m for merging files, -f for official group files \n -o [directory] to specify directory, -e to save to EOS area,\n -g to save to grid area, -h for help, -t [htag] to specify htag for EOS uploading (defaults to one for file), -x to delelte files as they finishing merging as uploading to EOS\n\n"
  printf "\nINPUT, see HGamTools/data/input/  examples:\n  periodC, dataPeriodAll25ns, allMC, PowhegPy8_ggH125, PowhegPy8_VBF145 ...\n\n"
  printf "\nCONFIGS, see HGamTools/data/  examples:\n  MxAOD.config, MxAODAllSys.config, MxAODPhotonSys.config\n"
  printf "\nFILES, see HGamTools/data/input/  examples:\n  mc16a_HIGG1D1.txt, data_13TeV.txt ...\n\n"
  exit 1
}

[[ $# < 4 ]] && usage "Not enough options !"



# arguments...
OPTIND=1
downloadDir=$(pwd)
copyToEOS="false"
mergeFiles="false"
copyToGrid="false" # TODO
official="false"
downloadFiles="false"
EOS_htag=""
deleteFinished="false"
while getopts "h?emo:dfgt:x" opt; do
    case "$opt" in
    h|\?)
        usage
        ;;
    e)  copyToEOS="true"
        ;;
    m)  mergeFiles="true"
        ;;
    o)  downloadDir=$OPTARG
        ;;
    d)  downloadFiles="true"
        ;;
    f)  official="true"
        ;;
    g)  copyToGrid="true"
        ;;
    t)  EOS_htag=$OPTARG
        ;;
    x)  deleteFinished="true"
        ;;
    esac
done

options=()
[[ "$downloadFiles"  == "true" ]]  && options+=("download")
[[ "$mergeFiles"     == "true" ]]  && options+=("merge")
[[ "$copyToEOS"      == "true" ]]  && options+=("EOScopy")
[[ "$official"       == "true" ]]  && options+=("officialProduction")
[[ "$copyToGrid"     == "true" ]]  && options+=("GridCopy")
[[ ! -z "$EOS_htag" ]]            && options+=("differentEOShtag")
[[ "$deleteFinished" == "true" ]]  && options+=("deleteAsFinished")

# need to setup environment first! 
[[ "$mergeFiles"    == "true" ]] && [[ -z $AnalysisBase_DIR ]] && SetupIssue "you need to setup AnalysisBase"
[[ "$downloadFiles" == "true" ]] && [[ -z "$(command -v rucio)" ]] && SetupIssue "you need to setup rucio: lsetup \"rucio -w\""

sample=${@:$OPTIND:1}
htag=${@:$OPTIND+1:1}
configName=${@:$OPTIND+2:1}
cfg=$WorkDir_DIR/data/HGamTools/$configName

list=$WorkDir_DIR/data/HGamTools/input/${@:$OPTIND+3:1}
# make sure the input list exist
[[ ! -e $list ]] && usage "Cannot find $4 in $WorkDir_DIR/data/HGamTools/input"

echo "sample:             $sample"
echo "htag:               $htag"
echo "config:             $cfg"
echo "file list:          $list"
echo "download directory: $downloadDir"
echo "options:            ${options[@]}" 


[[ ! -e $cfg ]] && usage "Cannot find $configName in this location: $config"
cfgName=${configName%.*}
config=HGamTools/$configName

EOSdir=root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/${htag}_stage
[[ ! "$EOS_htag" == "" ]] && EOSdir=root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/${EOS_htag}_stage

EOSdirPhotonSys=$EOSdir/PhotonSys
EOSdirJetSys=$EOSdir/JetSys
EOSdirLeptonMETSys=$EOSdir/LeptonMETSys
EOSdirFlavorSys=$EOSdir/FlavorSys
EOSdirFlavAll1Sys=$EOSdir/FlavorAllSys1
EOSdirFlavAll2Sys=$EOSdir/FlavorAllSys2
EOSdirAllSys=$EOSdir/AllSys
EOSdirPhotonAllSys=$EOSdir/PhotonAllSys
EOSdirDetail=$EOSdir/Detailed
EOSdirJetSys1=$EOSdir/JetSysCorr1
EOSdirJetSys2=$EOSdir/JetSysCorr2
EOSdirJetSys3=$EOSdir/JetSysCorr3
EOSdirJetSys4=$EOSdir/JetSysCorr4
EOSdirMC25ns=$EOSdir/mc_25ns
EOSdirMC50ns=$EOSdir/mc_50ns
EOSdirDATA25ns=$EOSdir/data_25ns
EOSdirDATA50ns=$EOSdir/data_50ns

if [ ! -d "$downloadDir" ]; then
  echo specified download directory $downloadDir does not exist! Please create/change.
  exit 0
fi


getOutputDSnameData() {
  ds=$(echo $dataset | cut -f2 -d:)
  ds2=$(echo $ds | awk -F '.deriv' {'print $1'})
  ds3=$(echo $ds2 | awk -F '.PhysCont' {'print $1'})
  ptag=${dataset/\//}
  ptag=${ptag: -5}
  outputMxAOD=user.${RUCIO_ACCOUNT}.${ds3}.${cfgName}.${ptag}.${htag}_MxAOD.root
  [[ "$official" == "true" ]] && outputMxAOD=group.phys-higgs.${ds3}.${cfgName}.${ptag}.${htag}_MxAOD.root
}
getOutputDSnameMC() {
  ptag=${dataset/\//}
  ptag=${ptag: -5}
  outputMxAOD=user.${RUCIO_ACCOUNT}.mc15a.${sample}.${cfgName}.${ptag}.${htag}_MxAOD.root
  [[ "$official" == "true" ]] && outputMxAOD=group.phys-higgs.mc15a.${sample}.${cfgName}.${ptag}.${htag}_MxAOD.root
  [[ "$dataset" =~ r7326 ]] && outputMxAOD=${outputMxAOD/mc15a/mc15b}
  [[ "$dataset" =~ r7267 ]] && outputMxAOD=${outputMxAOD/mc15a/mc15b}
  [[ "$dataset" =~ r7725 ]] && outputMxAOD=${outputMxAOD/mc15a/mc15c}
  [[ "$dataset" =~ a818 ]] && outputMxAOD=${outputMxAOD/mc15a/mc15c}
  [[ "$dataset" =~ r9364 ]] && outputMxAOD=${outputMxAOD/mc15a/mc16a}
  [[ "$dataset" =~ r9781 ]] && outputMxAOD=${outputMxAOD/mc15a/mc16c}
  [[ "$dataset" =~ r10201 ]] && outputMxAOD=${outputMxAOD/mc15a/mc16d}
}

notFoundDownloads=()
failedDownloads=()

RucioDownload() {
  NfilesGrid=$(rucio list-files $outputMxAOD |& grep "\.MxAOD\.root" | wc -l)
  [ "$NfilesGrid" -eq "0" ] && echo "ERROR! MxAOD $outputMxAOD does not exist on grid! Check name or grid status!" && \
    notFoundDownloads+=($outputMxAOD) && return 1
  echo "Starting Rucio Download of file $outputMxAOD..."
  echo
  cmd="rucio download $outputMxAOD --dir $downloadDir"
  $cmd 
  files=$(echo $downloadDir/$outputMxAOD/*)
  Nfiles=$(echo $files | awk '{print NF}')
  if [ ! "$Nfiles" -eq "$NfilesGrid" ]; then
    echo "ERROR! Dataset $outputMxAOD did not download all files! Files on Grid: $NfilesGrid, Files locally: $Nfiles!"
    failedDownloads+=($outputMxAOD) && return 1
  fi
}

downloadData() {
  getOutputDSnameData
  RucioDownload || return 1
}
downloadMC() {
  getOutputDSnameMC
  RucioDownload || return 1
}

directorySetup(){
  DataDir=$downloadDir/$outputMxAOD
  files=$(echo $DataDir/*)
  Nfiles=$(echo $files | awk '{print NF}')
  echo $outputMxAOD
  if [[ "$official" == "true" ]] ; then
    newDSname=${outputMxAOD#group.phys-higgs.}
  else
    newDSname=${outputMxAOD#user.${RUCIO_ACCOUNT}.}
  fi
  newDSname=${newDSname%_MxAOD.root}.root  
}

mergeMxAOD() {
  echo "merging MxAOD $outputMxAOD ..."
  NfilesGrid=$(rucio list-files $outputMxAOD |& grep "\.MxAOD\.root" | wc -l)
  if [ ! "$Nfiles" -eq "$NfilesGrid" ]; then
    echo "ERROR! Dataset $outputMxAOD did not download all files! Files on Grid: $NfilesGrid, Files locally: $Nfiles!"
    return 1
  fi
  xAODMerge -m xAODMaker::FileMetaDataTool $downloadDir/$newDSname $files && echo xAODMerge success! || (\
    echo xAODMerge failed :\( Safe Merging... This may take a while. \(WHY DOES THIS HAPPEN?!\) && \
    OutputFile=$(SafeMerge.sh $DataDir) && echo Safe Merge done! && cp $OutputFile $downloadDir/$newDSname)
  [[ "$deleteFinished" == "true" ]] && echo "deleting $downloadDir/$outputMxAOD" && rm -r $downloadDir/$outputMxAOD
}


mergeData() {
  getOutputDSnameData
  directorySetup || return 1
  [[ ! -d "$DataDir" ]] && echo "dataset directory does not exist! $downloadDir/$outputMxAOD Not found" && return 1
  NfilesGrid=$(rucio list-files $outputMxAOD |& grep "\.MxAOD\.root" | wc -l)
  if [ ! "$Nfiles" -eq "$NfilesGrid" ]; then
    echo "ERROR! Dataset $outputMxAOD did not download all files! Files on Grid: $NfilesGrid, Files locally: $Nfiles!"
    return 1
  fi
  if [[ "$Nfiles" -gt "1" ]]; then
    mergeMxAOD || return 1
    echo "Output file: $downloadDir/$newDSname"
  elif [[ "$Nfiles" -eq "1" ]]; then
    mv $DataDir/* ${downloadDir}/$newDSname
    [[ "$deleteFinished" == "true" ]] && rm -r $DataDir
    echo "Output file: $downloadDir/$newDSname"
  else
    echo "Number of files = 0? check if MxAOD $outputMxAOD downloaded correctly" 
    return 1
  fi

}
mergeMC() {
  getOutputDSnameMC
  directorySetup || return 1
  [[ ! -d "$DataDir" ]] && echo "dataset directory does not exist! $downloadDir/$outputMxAOD Not found" && return 1
  if [[ "$Nfiles" -gt "1" ]]; then
    if [[ "$outputMxAOD" =~ "Sys" || "$outputMxAOD" =~ "3jets" ]]; then
      
      outputDS_size=$(du -s $downloadDir/$outputMxAOD/ | awk '{print $1}')
      if [[ "$outputDS_size" -le 5000000  ]]; then  # 5GB sounds like a reasonable single file size?
        mergeMxAOD || return 1
        echo "Output file: $downloadDir/$newDSname"
      else
        i=$(( 1 ))
        echo "Large (> 5GB) file detected! Will only rename files"
        mkdir $downloadDir/$newDSname
        for f in $files; do
          inputNo=$(printf "%03d" $i)
          AllSysDSname=${newDSname%.root}.${inputNo}.root
          #echo "mv $f $downloadDir/$newDSname/${AllSysDSname}"
          mv $f $downloadDir/$newDSname/${AllSysDSname}
          i=$(( $i + 1 ))
        done
        echo "Output folder: $downloadDir/$newDSname"
      fi
      
    else
      mergeMxAOD || return 1
      echo "Output file: $downloadDir/$newDSname"
    fi
  elif [[ "$Nfiles" -eq "1" ]]; then
    # no need to merge if it's 1 file
    mv $DataDir/* ${downloadDir}/$newDSname
    [[ "$deleteFinished" == "true" ]] && rm -r $DataDir
    echo "Output file: $downloadDir/$newDSname"
  else
    echo "Number of files = 0? check if MxAOD $outputMxAOD downloaded correctly"
  fi
}
EOS_Copy() {
  isFolder="false"
  if [[ $sample = period* ]]; then
    getOutputDSnameData
    if [[ "$sample" =~ "_50ns" ]]; then
      EOSdir=$EOSdirDATA50ns
    else
      EOSdir=$EOSdirDATA25ns
    fi
  elif [[ $sample = run* ]]; then
    getOutputDSnameData
    if [[ "$sample" =~ "_50ns" ]]; then
      EOSdir=$EOSdirDATA50ns
    else
      EOSdir=$EOSdirDATA25ns
    fi 
  else
    getOutputDSnameMC
    if [[ $outputMxAOD =~ "MxAODAllSys" ]]; then
      EOSdir=$EOSdirAllSys
    elif [[ $outputMxAOD =~ "PhotonAllSys" ]]; then
      EOSdir=$EOSdirPhotonAllSys
    elif [[ $outputMxAOD =~ "JetSys" ]]; then
      EOSdir=$EOSdirJetSys
    elif [[ $outputMxAOD =~ "LeptonMETSys" ]]; then
      EOSdir=$EOSdirLeptonMETSys
    elif [[ $outputMxAOD =~ "FlavorSys" ]]; then
      EOSdir=$EOSdirFlavorSys
    elif [[ $outputMxAOD =~ "FlavorAllSys1" ]]; then
      EOSdir=$EOSdirFlavAll1Sys
    elif [[ $outputMxAOD =~ "FlavorAllSys2" ]]; then
      EOSdir=$EOSdirFlavAll2Sys
    elif [[ $outputMxAOD =~ "PhotonSys" ]]; then
      EOSdir=$EOSdirPhotonSys
    elif [[ $outputMxAOD =~ "Detailed" ]]; then
      EOSdir=$EOSdirDetail
    elif [[ $outputMxAOD =~ "JetSysCorr1" ]]; then
      EOSdir=$EOSdirJetSys1
    elif [[ $outputMxAOD =~ "JetSysCorr2" ]]; then
      EOSdir=$EOSdirJetSys2
    elif [[ $outputMxAOD =~ "JetSysCorr3" ]]; then
      EOSdir=$EOSdirJetSys3
    elif [[ $outputMxAOD =~ "JetSysCorr4" ]]; then
      EOSdir=$EOSdirJetSys4
    elif [[ "$sample" =~ "_50ns" ]]; then
      EOSdir=$EOSdirMC50ns
    else
      EOSdir=$EOSdirMC25ns
    fi
  fi
  directorySetup || return 1
  [[ ! -e $downloadDir/$newDSname ]] && echo "dataset $downloadDir/$newDSname does not exist! Check arugments?" && return 1
  [[ -z $newDSname ]] && echo "newDSname not defined! This most likey means the file does not exist. Check your filename!" && return 1
  [[ -d $downloadDir/$newDSname ]] && isFolder="true"

  if [[ "$isFolder" == "false"  ]]; then
    xrdcp $downloadDir/$newDSname $EOSdir/$newDSname
    #echo "xrdcp $downloadDir/$newDSname $EOSdir/$newDSname"
  else
    files=$(echo $downloadDir/$newDSname/*)
    i=$(( 1 ))
    for f in $files; do
        inputNo=$(printf "%03d" $i)
        SysDSname=${newDSname%.root}.${inputNo}.root
        xrdcp $f $EOSdir/$newDSname/${SysDSname}
        #echo "xrdcp $f $EOSdirAllSys/$newDSname/${AllSysDSname}"
        i=$(( $i + 1 ))
    done
  fi
  [[ "$deleteFinished" == "true" ]] && rm -r $downloadDir/$newDSname
}

Grid_Copy() {
  [[ $official == "false" ]] && echo "only Official datasets should be copied to grid, use -f option" && return 1
  [[ ! ${RUCIO_ACCOUNT} == "phys-higgs" ]] && echo "rucio account needs to be set to phys-higgs!, setting..." && export RUCIO_ACCOUNT=phys-higgs
  isFolder="false"
  if [[ $sample = period* ]]; then
    getOutputDSnameData
  elif [[ $sample = run* ]]; then
    getOutputDSnameData
  else
    getOutputDSnameMC
  fi
  directorySetup || return 1 
  [[ ! -e $downloadDir/$newDSname ]] && echo "dataset $downloadDir/$newDSname does not exist! Check arugments?" && return 1
  [[ -z $newDSname ]] && echo "newDSname not defined! This most likey means the file does not exist. Check your filename!" && return 1
  [[ -d $downloadDir/$newDSname ]] && isFolder="true"
  if [[ "$isFolder" == "false"  ]]; then
    rucio upload --rse CERN-PROD_SCRATCHDISK group.phys-higgs:group.phys-higgs.$newDSname $downloadDir/$newDSname --scope group.phys-higgs 
  else
    files=$(echo $downloadDir/$newDSname/*)
    rucio upload --rse CERN-PROD_SCRATCHDISK group.phys-higgs:group.phys-higgs.$newDSname ${files[@]} --scope group.phys-higgs
  fi


}


if [[ $sample = periodAll25ns ]]; then
    n=$(grep -c "^period. " $list)
    [[ $n = 0 ]] && usage "No samples of type $sample in $list"
    datasets=$(grep "^period. " $list | awk '{print $2}')
    for dataset in $datasets ; do
      sample=$(grep "$dataset" $list | awk '{print $1}')
      [[ $downloadFiles == "true" ]]    && (downloadData || continue)
      [[ $mergeFiles    == "true" ]]    && mergeData
      [[ $copyToEOS     == "true" ]]    && EOS_Copy
      [[ $copyToGrid    == "true" ]]    && Grid_Copy
    done

elif [[ $sample = period* ]] ; then
  n=$(grep -c "^$sample " $list)
  [[ $n = 0 ]] && usage "No samples of type $sample in $list"
  datasets=$(grep "^$sample " $list | awk '{print $2}')
  for dataset in $datasets ; do
    [[ $downloadFiles == "true" ]]  && (downloadData || continue)
    [[ $mergeFiles    == "true" ]]  && mergeData
    [[ $copyToEOS     == "true" ]]  && EOS_Copy
    [[ $copyToGrid    == "true" ]]  && Grid_Copy
  done
elif [[ $sample = run* ]] ; then
  n=$(grep -c "${sample/run/}" $list)
  [[ $n = 0 ]] && usage "No samples of type $sample in $list"
  datasets=$(grep "${sample/run/}" $list | awk '{print $2}')
  for dataset in $datasets ; do
    [[ $downloadFiles == "true" ]]  && (downloadData || continue)
    [[ $mergeFiles    == "true" ]]  && mergeData
    [[ $copyToEOS     == "true" ]]  && EOS_Copy
    [[ $copyToGrid    == "true" ]]  && Grid_Copy
  done
elif [[ $sample = allMC ]] ; then
  samples=$(cat $list | grep -v ^\# | awk '{print $1}')
  for sample in $samples ; do
    n=$(grep -c "^$sample " $list)
    [[ $n -gt 1 ]] && usage "$n samples of type $sample in $list ?"
    dataset=$(grep "^$sample " $list | awk '{print $2}')
    [[ $downloadFiles == "true" ]]  && (downloadMC || continue)
    [[ $mergeFiles    == "true" ]]  && mergeMC
    [[ $copyToEOS     == "true" ]]  && EOS_Copy
    [[ $copyToGrid    == "true" ]]  && Grid_Copy
  done
elif [[ $sample = nominalMC ]] ; then
  samples=$(cat $list | awk '{print $1}'| grep "125")
  for sample in $samples ; do
    n=$(grep -c "^$sample " $list)
    [[ $n -gt 1 ]] && usage "$n samples of type $sample in $list ?"
    dataset=$(grep "^$sample " $list | awk '{print $2}')
    [[ $downloadFiles == "true" ]]  && (downloadMC || continue)
    [[ $mergeFiles    == "true" ]]  && mergeMC
    [[ $copyToEOS     == "true" ]]  && EOS_Copy
    [[ $copyToGrid    == "true" ]]  && Grid_Copy
  done
else
  n=$(grep -c "^$sample " $list)
  [[ $n = 0   ]] && usage "No samples of type $sample in $list"
  [[ $n -gt 1 ]] && usage "$n samples of type $sample in $list ?"
  dataset=$(grep "^$sample " $list | awk '{print $2}')
  downloaded="true"
  [[ $downloadFiles == "true" ]]  && (downloadMC || downloaded="false")
  if [[ $downloaded == "true" ]]; then
    [[ $mergeFiles  == "true" ]]  && mergeMC 
    [[ $copyToEOS   == "true" ]]  && EOS_Copy
    [[ $copyToGrid  == "true" ]]  && Grid_Copy
  fi
fi
if [[ $downloadFiles == "true" ]]; then
  echo
  echo The following files were not found on the grid:
  for item in ${notFoundDownloads[*]}
  do
    echo $item
  done
  echo
  echo The following files were not downloaded correctly:
  for item in ${failedDownloads[*]}
  do
    echo $item
  done

fi

echo
[[ $copyToGrid    == "true" ]]  && echo "Files were copied to CERN-PROD_SCRATCHDISK and will be deleted in ~2 weeks!" && \
   echo "go to https://rucio-ui.cern.ch/r2d2/request to request replication to CERN-PROD_PHYS-HIGGS for permanent storage"



