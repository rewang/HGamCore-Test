# source this script after setting up HGamAnalysis environment
# probably save output to make sure all got downloaded properly
#use like source getAllMxAODs.sh h010 2>> errors.txt 1>> stdout.txt
[[ -z "$1" ]] && echo Please enter an htag as and argument! E.G source getAllMxAODs.sh h011 && return
htag=$1
# tempDir to download to, change if you want
tempDir=$(pwd)

CONFIGS=(MxAODAllSys MxAODPhotonSys)

$ROOTCOREBIN/user_scripts/HGamTools/getMxAOD.sh -dmef -o $tempDir allMC $htag MxAOD.config
$ROOTCOREBIN/user_scripts/HGamTools/getMxAOD.sh -dmef -o $tempDir periodAll25ns $htag MxAOD.config

for config in ${CONFIGS[@]}; do
  $ROOTCOREBIN/user_scripts/HGamTools/getMxAOD.sh -dmef '-o' $tempDir nominalMC $htag ${config}.config
done
