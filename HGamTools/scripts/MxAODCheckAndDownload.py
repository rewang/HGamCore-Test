import ConfigParser
from optparse import OptionParser
import json
import os
import subprocess
import sys
import datetime

######################
# Call script:
# python MxAODCheckAndDownload.py <htag> <sysCategory> <mcType> <optional:makeListOnly>
# Possible options for sysCategory defined in MxAODinformations.config
# No need for mcType if category is of type dataXX
######################


startTime = datetime.datetime.now()


parser = OptionParser()
parser.add_option("-t", "--htag", help="htag you want to download", dest="htag")
parser.add_option("-c", "--category", help="category of samples: Detailed or systematics category", dest="cat")
parser.add_option("-m", "--mcType", help="MC type: example mc16a", dest="mcType")
parser.add_option("-p", "--printOnly", help="Print only list of missing files and jobs (not downloading)", action="store_false", default=True, dest="download")
parser.add_option("-f", "--HGamCoreFolderPath", help="Path for HGamCore folder (used to read files lists and MxAODinformations.config)", default="/eos/user/s/smonig/HGam/source/HGamCore" ,dest="path")
(options, args) = parser.parse_args()

print "\n=============================================================================="
htag = options.htag
print "htag: ", htag
category = options.cat
print "category: ", category
mcType = ""
if category.count("data")==0:
	mcType = options.mcType
print "mcType: ", mcType
print "==============================================================================\n"

pandaFile = "pandaOutput_"+startTime.strftime("%d_%b_%y_%H_%M_%S")+".json"

os.system("cern-get-sso-cookie -u https://bigpanda.cern.ch/ -o bigpanda.cookie.txt")
os.system("curl -b bigpanda.cookie.txt -H 'Accept: application/json' -H 'Content-Type: application/json' \"https://bigpanda.cern.ch/tasks/?taskname=group.phys-higgs*"+htag+"_**&days=30&json\" > " + pandaFile)

pandaMap = json.load(open(pandaFile))

inputListPath = options.path+"/HGamTools/data/input/"
eosPath = "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/"

config = ConfigParser.ConfigParser()
config.readfp(open(options.path+"/HGamTools/scripts/MxAODinformations.config"))
dataType = config.get(category, "dataType").replace("<mcType>",mcType)
MxAODconfig = config.get(category, "configFile").replace("<mcType>",mcType)
listOfFiles = config.get(category, "inputList").replace("<mcType>",mcType)
eosFolderStage = config.get(category, "eosFolderStage").replace("<mcType>",mcType)
eosFolder = config.get(category, "eosFolder").replace("<mcType>",mcType)

inFilesList = open(inputListPath+listOfFiles, "r");
eosFolderToCheck = [eosPath+htag+"_stage/"+eosFolderStage, eosPath+htag+"/"+eosFolder]

expectedFiles = inFilesList.readlines()
inFilesList.close()



# Loop over list of expected files and format input
#***************************************************************

i = 0
while i < len(expectedFiles):

	if len(expectedFiles[i])==1:
		del expectedFiles[i]
		continue #do not increase i by 1

	if expectedFiles[i].endswith("\n"):
		expectedFiles[i] = expectedFiles[i][:-1]

	if expectedFiles[i].startswith("#"):
		del expectedFiles[i]
		continue #do not increase i by 1

	if dataType == "data" and not expectedFiles[i].count(category)>0:
		del expectedFiles[i]
		continue #do not increase i by 1

	if expectedFiles[i].count(" ") > 0 and dataType == "MC":
		expectedFiles[i] = expectedFiles[i][:expectedFiles[i].index(" ")]
	elif dataType == "data":
		expectedFiles[i] = expectedFiles[i][expectedFiles[i].index(":")+1:expectedFiles[i].index(".physics")]
		expectedFiles[i] = expectedFiles[i][expectedFiles[i].index(".")+1:]

	i += 1



# Loop over expected files, search for panda jobs
#***************************************************************

filesInEos = list();
unfinishedFiles = list();
downloadedFiles = list();
missingFiles= list();

for fileName in expectedFiles:

	# Check if file is already in eos; if yes, continue
	found = False
	for folder in eosFolderToCheck:
		mypath = folder
		if not os.path.isdir(mypath):
			print "Folder ",mypath," does not exist (yet)."
			continue # Folder does not exist (yet)
		filesInFolder = [f for f in os.listdir(mypath)]
		for f in filesInFolder:
			if f.count(htag)>0 and f.count(category)>0 and f.count(fileName+".")>0:
				if dataType == "MC" and not f.count(mcType)>0 :
					continue
				filesInEos.append(mypath+"/"+f)
				found = True
	if found:
		continue


	# If not yet downloaded, check status from panda and evtl. download file
	chosenJob = "";

	for job in pandaMap:

		if job["taskname"].count(fileName+".") > 0 and job["taskname"].count(category) > 0 and job["taskname"].count(mcType) > 0:

			if job["status"] != "done":
				unfinishedFiles.append(job["taskname"]+"  "+job["status"])

			else:
				job["taskname"] = job["taskname"].replace("/","");
				# If several successful jobs for same file, take last one submitted
				if len(chosenJob) == 0:
					chosenJob = job["taskname"]
				elif job["taskname"][-1:].isalnum() and chosenJob[-1:].isalnum() and job["taskname"][-1:]>chosenJob[-1:]:
					chosenJob = job["taskname"]
				elif job["taskname"][-1:].isalnum() and not chosenJob[-1:].isalnum():
					chosenJob = job["taksname"]

	if len(chosenJob) == 0:
		missingFiles.append(fileName)
		continue;
	else:
		if options.download:
			print "Going to download: ", chosenJob
			jobEnding = chosenJob[chosenJob.find(htag+"_"):]
			if dataType == "MC":
				os.system("getMxAOD.sh -fdmex -t "+htag+" "+fileName+" "+jobEnding+" "+MxAODconfig+" "+mcType+"_HIGG1D1.txt -o /tmp/${USER}")
			elif dataType == "data":
				os.system("getMxAOD.sh -fdmex -t "+htag+" run"+fileName+" "+jobEnding+" "+MxAODconfig+" data_13TeV_HIGG1D1.txt -o /tmp/${USER}")
		downloadedFiles.append(fileName)


print "\nDownloaded/finished files:\n"
print '\n'.join([ str(element) for element in downloadedFiles ])
print "\nMissing files:\n"
print '\n'.join([ str(element) for element in missingFiles ])
print "\nUnfinished jobs:\n"
print '\n'.join([ str(element) for element in unfinishedFiles ])
print '\n'

os.system("rm "+pandaFile);


