//

#ifndef BkgParamCore_h
#define BkgParamCore_h

#include <vector>
#include <map>
#include <cmath>
#include "TString.h"

#include "TEnv.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TF1.h"
#include "TCanvas.h"

// RooFit headers:
#include "RooAbsData.h"
#include "RooAbsPdf.h"
#include "RooAbsReal.h"
#include "RooArgList.h"
#include "RooArgSet.h"
#include "RooRealVar.h"
#include "RooWorkspace.h"
#include "RooFitResult.h"
#include "RooCmdArg.h"
#include "RooLinkedList.h"
#include "RooHist.h"


namespace BkgTool {
  class Configurable {
  public:
    // Constructor and destructor:
    Configurable(const TString &name) : m_name(name), m_verbosity(-1) { }
    virtual ~Configurable() { }
    bool setup(TEnv &reader);
    TString GetName() const { return m_name; }
    int verbosity() const { return m_verbosity; }
    bool verbosity(int v) { return m_verbosity >= v; }
    bool isConfigured() const { return m_verbosity == -1; }
  private:
    TString m_name;
    int m_verbosity;
  };

  class ConfigValue : public Configurable {
  public:
    // Constructor and destructor:
    ConfigValue(const TString &name) : Configurable(name) { }
    virtual ~ConfigValue() { }
    virtual bool hasValue() const = 0;
    virtual double value(double position, double def = 0) const = 0;
  };


  class TF1Value : public ConfigValue {
  public:
    // Constructor and destructor:
    TF1Value(const TString &name) : ConfigValue(name), m_tf1(0) { }
    virtual ~TF1Value() { }
    bool setup(TEnv &reader, const TString &scanVarName, bool allowFail = false);
    bool hasValue() const { return m_tf1; }
    double value(double position, double def = 0) const { return hasValue() ? m_tf1->Eval(position) : def; }

  private:
    TString m_name;
    TF1 *m_tf1;
  };


  class VarValue : public ConfigValue {
  public:
    // Constructor and destructor:
    VarValue(const TString &name) : ConfigValue(name), m_var(0), m_scanVar(0) { }
    virtual ~VarValue() { }
    bool setup(TEnv &reader, const TString &scanVarName, bool allowFail = false);
    bool hasValue() const { return m_var && m_scanVar; }
    double value(double position, double def = 0) const { if (!hasValue()) return def; m_scanVar->setVal(position); return m_var->getVal(); }

  private:
    TString m_name;
    RooAbsReal *m_var;
    RooRealVar *m_scanVar;
  };


  class Function : public ConfigValue {
  public:
    // Constructor and destructor:
    Function(const TString &name) : ConfigValue(name), m_tf1(name), m_var(name + "Var") { }
    virtual ~Function() { }
    bool hasValue() const { return m_tf1.hasValue() || m_var.hasValue(); }
    double value(double position, double def = 0) const { if (m_tf1.hasValue()) return m_tf1.value(position); if (m_var.hasValue()) return m_var.value(position); return def; }
    bool setup(TEnv &reader, const TString &scanVarName, bool allowFail = false);

  private:
    TF1Value m_tf1;
    VarValue m_var;
  };


  class SignalYield : public ConfigValue {
  public:
    // Constructor and destructor:
    SignalYield(const TString &name) : ConfigValue(name),
      m_yield(GetName() + "Yield"), m_xs(GetName() + "CrossSection"), m_targetLumi(-1), m_lumi(-1) { }
    virtual ~SignalYield() { }
    bool hasValue() const { return m_yield.hasValue() || m_xs.hasValue(); }
    double value(double position, double def = 0) const;
    bool setup(TEnv &reader, const TString &scanVarName, double targetLumi);

  private:
    Function m_yield, m_xs;
    double m_targetLumi, m_lumi;
  };


  class Data : public Configurable {
  public:
    Data(const TString &name) : Configurable(name), m_data(0), m_data_forChi2(0) { }
    bool setup(TEnv &reader, RooRealVar &obsVar, double targetLumi);
    RooAbsData *data() const { return m_data; }
    RooAbsData *data_forChi2() const { return m_data_forChi2; }
    TH1 *RebinHistforChi2(TH1 &hist, double histmin, double histmax);
  private:
    RooAbsData *m_data;
    RooAbsData *m_data_forChi2;
  };


  class Parameterization : public Configurable {
  public:
    // Constructor and destructor:
    Parameterization(const TString &name);
    virtual ~Parameterization() {}
    bool setup(TEnv &reader, const TString &scanVarName);

    bool fit(RooAbsData &data, const RooLinkedList &opts, const RooLinkedList &opts_sumW2Errors = RooLinkedList(), const TString &dir = "", TCanvas *c1 = 0, bool BkgOnly = false, RooAbsData *data_rebinned = 0);
    void plot(RooAbsData &data, TCanvas *c1);
    void writeBkgOnlyChiSquare(RooAbsData &data, RooAbsData *data_rebinned, TString dir, TCanvas *c1);

    const RooWorkspace &workspace() const { return *m_ws; }
    RooWorkspace &workspace() { return *m_ws; }
    RooArgSet &parameters() { return m_parameters; }
    RooArgSet &bkgParameters() { return m_bkgParameters; }
    RooAbsPdf *sigPdf() { return m_sigPdf; }
    RooAbsPdf *bkgPdf() { return m_bkgPdf; }
    RooAbsPdf *totalPdf() { return m_totalPdf; }
    RooRealVar *obsVar() { return m_obsVar; }
    RooRealVar *scanVar() { return m_scanVar; }
    RooRealVar *nSignal() { return m_nSignal; }
    RooRealVar *nBkg() { return m_nBkg; }
    void setAllBkgParametersConstant(bool b);

  private:

    RooAbsPdf *makePdf(const TString &name, const TString &expression, const TString &parameters, RooArgSet &parSet);

    RooWorkspace *m_ws;
    RooRealVar *m_obsVar, *m_scanVar;
    RooAbsPdf *m_totalPdf, *m_sigPdf, *m_bkgPdf;
    RooRealVar *m_nSignal, *m_nBkg;
    RooArgSet m_parameters, m_bkgParameters;
  };


  class Result {
  public:
    Result(const TString &name) : m_name(name) { }
    virtual ~Result() { }

    TString GetName() const { return m_name; }
    bool isPassing() const { return m_pass; }
    const std::vector<TString> &names() const { return m_names; }
    double value(const TString &name, double def = 0) const { auto iter = m_vals.find(name); return iter != m_vals.end() ? iter->second : def; }

    void add(const TString &name, double val) { m_names.push_back(name); m_vals[name] = val; }
    void setPassing(bool pass) { m_pass = pass; }

    // For the sorting options see SpuriousSignalResult::operator< in BkgParam.cxx
    virtual bool operator<(const Result &other) const = 0;
    virtual TString headerStr() const;
    virtual TString valueStr() const;
    TString format(const TString &name, const TString &valueSuffix = ".4g", bool rev = false) const;

  private:
    TString m_name;
    std::vector<TString> m_names;
    std::map<TString, double> m_vals;
    bool m_pass;
  };


  class Selector : public Configurable {
  public:
    // Constructor and destructor:
    Selector(const TString &configFileName, const TString &outputDir);
    virtual ~Selector() {}

    virtual bool setup();

    unsigned int nSteps() const { return m_scanVar ? (unsigned int)((m_scanVar->getMax() - m_scanVar->getMin()) / m_scanStep) + 1 : 0; }

    virtual bool makeResults(std::vector<Result *> &results) = 0;
    bool printResults(const std::vector<Result *> &results);
    bool run();

    TString path(const TString &frag1 = "", const TString &frag2 = "", const TString &frag3 = "", const TString &frag4 = "") const;
    bool mkdir(const TString &frag1 = "", const TString &frag2 = "", const TString &frag3 = "") const;
    static TString pointDir(unsigned int i) { return Form("Points/Point%d", i); }

    TGraphErrors *winMax(const TGraphErrors &graph);
    TGraphErrors *fitValues(const TString &varName, const Parameterization &pdf, bool sumW2Errors = false, const TString &name = "", const TString &yTitle = "");
    TGraphErrors *functionValues(const ConfigValue &func, const TString &name = "", const TString &yTitle = "");

    static void split(const TString &str, std::vector<TString> &splits, const TString &delimiter);
    static TString find_file(const TString &name);
    static void drawGraph(TGraphErrors &graph, const TString &opt = "3A", int lineColor = 1, int fillColor = 4, double line1 = -999, double line2 = -999);
    static void drawGraphs(const std::vector<TGraphErrors *> &graphs, const TString &opt = "3", double line1 = -999, double line2 = -999);
    static TGraphErrors *ratio(const TGraphErrors &graph, const TGraph &denom, const TString &name = "", const TString &yTitle = "", bool difference = false);
    static TGraphErrors *ratio_af2_1sigma(const TGraphErrors &graph, const TGraph &denom, const TString &name = "", const TString &yTitle = "", bool difference = false, int nSigma = 1);
    static TGraphErrors *ratio_af2_2sigma(const TGraphErrors &graph, const TGraph &denom, const TString &name = "", const TString &yTitle = "", bool difference = false);
    static TGraphErrors *Z(const TGraphErrors &graph, const TGraphErrors &uncert, const TString &name = "", const TString &yTitle = "");
    static TGraphErrors *Z_af2_1sigma(const TGraphErrors &graph, const TGraphErrors &uncert, const TString &name = "", const TString &yTitle = "", int nSigma = 1);
    static TGraphErrors *Z_af2_2sigma(const TGraphErrors &graph, const TGraphErrors &uncert, const TString &name = "", const TString &yTitle = "");
    static void find_max(TGraph &graph, double &maxPos, double &maxVal, bool useAbs = true);
    static double max_val(TGraph &graph, bool useAbs = true) { double val, pos; find_max(graph, pos, val, useAbs); return val; }

  protected:

    static bool pointerSort(Result *const &result1, Result *const &result2) { return *result1 < *result2; }   // For the sorting options see SpuriousSignalResult::operator< in BkgParam.cxx

    TEnv m_reader;
    std::vector<Parameterization> m_candidates;
    TString m_outputDir;
    Function m_rangeMin, m_rangeMax;
    double m_targetLumi;
    Data m_data;
    RooLinkedList m_fitOptionsT, m_fitOptionsF, m_fitOptionsBkgT, m_fitOptionsBkgF;

    RooRealVar *m_scanVar;
    double m_scanMin, m_scanMax, m_scanStep;

    bool m_perPointPlots;
  };

}
#endif
