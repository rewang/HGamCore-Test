#ifndef HGamTools_PDFTool_H
#define HGamTools_PDFTool_H

#include "HGamAnalysisFramework/HgammaAnalysis.h"

// LHAPDF Tool
#include <vector>
#include "LHAPDF/PDF.h"


class PDFTool : public HgammaAnalysis {
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  // float cutValue;



  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
private:
  std::vector<LHAPDF::PDF *> m_pdfs;   //!
  std::vector<LHAPDF::PDF *> m_pdfsnom; //!



public:
  // this is a standard constructor
  PDFTool() { }
  PDFTool(const char *name);
  virtual ~PDFTool();

  // these are the functions inherited from HgammaAnalysis
  virtual EL::StatusCode createOutput();
  virtual EL::StatusCode execute();


  // this is needed to distribute the algorithm to the workers
  ClassDef(PDFTool, 1);
};

#endif // HGamTools_PDFTool_H
