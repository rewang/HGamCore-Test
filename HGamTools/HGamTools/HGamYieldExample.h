#ifndef HGamTools_HGamYieldExample_H
#define HGamTools_HGamYieldExample_H

#include "HGamAnalysisFramework/HgammaAnalysis.h"

class HGamYieldExample : public HgammaAnalysis {
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  // float cutValue;



  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
private:
  // Tree *myTree; //!
  // TH1 *myHist; //!

  std::map<int, int> m_nEvents;
  TH1F *createAndRegisterTH1F(TString name, int Nbins, double min, double max, TString title = "");

public:
  // this is a standard constructor
  HGamYieldExample() { }
  HGamYieldExample(const char *name);
  virtual ~HGamYieldExample();

  // these are the functions inherited from HgammaAnalysis
  virtual EL::StatusCode createOutput();
  virtual EL::StatusCode execute();


  // this is needed to distribute the algorithm to the workers
  ClassDef(HGamYieldExample, 1);
};

#endif // HGamTools_HGamYieldExample_H
