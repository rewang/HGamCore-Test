#ifndef HGamTools_HGamExample_H
#define HGamTools_HGamExample_H

#include "HGamAnalysisFramework/HgammaAnalysis.h"
#include "PhotonVertexSelection/PhotonVertexSelectionTool.h"

class HGamExample : public HgammaAnalysis {
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  // float cutValue;



  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
private:
  // TTree *myTree; //!
  // TH1 *myHist; //!



public:
  // this is a standard constructor
  HGamExample() { }
  HGamExample(const char *name);
  virtual ~HGamExample() { }

  // these are the functions inherited from HgammaAnalysis
  virtual EL::StatusCode createOutput();
  virtual EL::StatusCode execute();
  virtual EL::StatusCode finalize();


  // this is needed to distribute the algorithm to the workers
  ClassDef(HGamExample, 1);
};

#endif // HGamTools_HGamExample_H
