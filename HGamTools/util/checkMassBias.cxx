// Tool to perform tests of bkg models
// Nicolas Berger, nicolas.berger@cern.ch

#include "HGamTools/BkgParam.h"
#include "TEnv.h"

#include <iostream>
using std::cout;
using std::endl;

int main(int argc, char *argv[])
{
  if (argc < 3) {
    cout << "Usage: " << argv[0] << " <configuration_file> <output directory>" <<  endl;
    return 1;
  }

  BkgTool::MassBiasSelector bias(argv[1], argv[2]);

  if (!bias.setup()) { return 2; }

  if (!bias.run())   { return 3; }

  return 0;
}