# Level of verbosity of the output, can be 0 (no output), 1 (minimal), 2 (verbose), >2 (debug)
Verbosity:                                   1
# If set to True, plots will be generated for fits at each mass point. Useful for debugging but not recommended for large scans.
PerPointPlots:                       	     False

# The observable variable (m_gamgam)
Observable:                                  m_yy[105,160]
Observable.NBins:                            550

# The block below defines the points at which to perform the S+B fits
#----
# Name of the variable giving the mass peak position
Scan.Var:                                    mResonance
# Interval between scan points
Scan.Step:                                   0.5

# The block below defines the parameters of the spurious signal computation and tests
# ---
# Lower range of the interval over which to report spurious signal values
Scan.Min:                                    125
# Upper range of the interval over which to report spurious signal values
Scan.Max:                                    125
# Lower end of the window to maximize over to compute the spurious signal: a number or a formula involving Scan.Var
Range.Min:                                   mResonance - 8
# Upper end of the window to maximize over to compute the spurious signal: a number or a formula involving Scan.Var
Range.Max:                                   mResonance + 8
# Maximum allowed value for S/delta_S
Selection.MaxSignalOverError:                0.20
# Maximum allowed value for S/S_ref
Selection.MaxSignalOverRef:                  0.10
# Integrated luminosity for which to perform the test
Selection.IntegratedLuminosity:              3.21

# The block below defines the reference signal yield to use for the S/S_ref test. 
# One can specify either a) a reference yield, b) a reference cross-section, or c) the name of a variable giving the yield
# A definition needs to be provided if Selection.MaxSignalOverRef is defined above
#---
# Option a), Signal yield: specified either as a number or a function of Scan.Var (See above)
RefSignalYield: 
# Options b) Signal cross-section: specified either as a number or a function of Scan.Var (See above). 
# Will get multiplied by Selection.IntegratedLuminosity (see above) to obtain the yield.
RefSignalCrossSection:
# Options c) variable in a  workspace, defined in the sub-block below:
# name of the variable
RefSignalYieldVar.Name:                 sigYield_SM_c0
# name of the workspace where it it stored
RefSignalYieldVar.Workspace:            signalWS
# name of the file in which the workspace is stored
RefSignalYieldVar.File:                 HGamTools/data/bkgParam/res_SM_workspace_mod.root
# integrated luminosity for which the variable value provides the event yield (1 by default)
RefSignalYield.IntegratedLuminosity: 4.0

# the block below specifies the dataset on which the test is performed
# ---
# file containing the histogram template
Dataset.File:                                HGamTools/data/bkgParam/Sherpa_gamgam_2DP20_100-160_AF2.p2419.h009.root
# Name of histogram inside the file
Dataset.HistogramName:                       hist
# Integrated luminosity for which the histogram is normalized
Dataset.IntegratedLuminosity:                12183

# the lines below specifies the signal and background normalization variables, in RooWorkspace::factory syntax (name[initial_value,min,max])
Signal.Norm:                                 nSignal[0,-100,1000]
Background.Norm:                             nBkg[100,0,1E6]

# the block below specifies the parameterization to use for the signal component, either as a) an expression or b) a PDF object in a workspace
#---
# Option a) Expression: an expression for the signal PDF either in RooWorkspace::factory syntax or as a a formula expression to use with RooGenericPdf
#Signal.PDF.Expression:
# For the case of a RooGenericPdf expression above, provides the comma-separated list of PDF parameters to use
#Signal.PDF.Parameters:
# Option b) PDF object inside a workspace, defined in the sub-block below:
# name of PDF object
Signal.PDF.Name:                             sigPdf_SM_c0
# name of workspace where it is stored
Signal.PDF.Workspace:                        signalWS
# name of file containing the workspace
Signal.PDF.File:                             HGamTools/data/bkgParam/res_SM_workspace_mod.root

# list of names of the background PDFs to test. Each should correspond to a definition block below
Background.PDFs:                             Exponential ExpPoly2 Dijet Bern3 Bern4 Bern5
#Background.PDFs:                             Exponential ExpPoly2

# Expression for the PDF form, either as in RooWorkspace::factory syntax or as a a formula expression to use with RooGenericPdf
Exponential.Expression:                         exp(xi*m_yy);
# For the case of a RooGenericPdf expression above, provides the comma-separated list of PDF parameters to use
Exponential.Parameters:                         xi[-0.02,-0.05,0]
# If set to True, the initial values of the parameters will be set from a B-only fit to the dataset above (recommended)
Exponential.SetInitialValuesFromDataset:        True

ExpPoly2.Expression:                         exp((m_yy - 100)/100*(a1 + a2*(m_yy - 100)/100))
ExpPoly2.Parameters:                         a1[0,-10,10] a2[0,-10,10]
ExpPoly2.SetInitialValuesFromDataset:        True

Dijet.Expression:                            pow(m_yy/13E3, p1)*pow(1 - m_yy/13E3, p2)
Dijet.Parameters:                            p1[0,-20,20] p2[0,-20,20]
Dijet.SetInitialValuesFromDataset:           True

Bern3.Expression:                            RooBernstein(m_yy, { a1[0,-10,10], a2[0,-10,10], a3[0,-10,10], 1 })
Bern3.SetInitialValuesFromDataset:           True

Bern4.Expression:                            RooBernstein(m_yy, { a1[0,-10,10], a2[0,-10,10], a3[0,-10,10], a4[0,-10,10], 1 })
Bern4.SetInitialValuesFromDataset:           True

Bern5.Expression:                            RooBernstein(m_yy, { a1[0,-10,10], a2[0,-10,10], a3[0,-10,10], a4[0,-10,10], a5[0,-10,10], 1 })
Bern5.SetInitialValuesFromDataset:           True
