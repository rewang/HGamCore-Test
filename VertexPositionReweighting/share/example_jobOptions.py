# $Id: CPAnalysisExamples_jobOptions.py 300492 2014-04-30 14:39:05Z krasznaa $

# Set up the file reading:
FNAME = "/afs/cern.ch/atlas/project/PAT/xAODs/r5591/mc14_8TeV.177998.MadGraphPythia_AUET2BCTEQ6L1_ttbargammaPt80_noAllHad_fixed.merge.AOD.e2189_s1933_s1911_r5591_r5625_tid01593004_00/AOD.01593004._000018.pool.root.1"

import AthenaPoolCnvSvc.ReadAthenaPool
ServiceMgr.EventSelector.InputCollections = [ FNAME ]

# Access the algorithm sequence:
from AthenaCommon.AlgSequence import AlgSequence
theJob = AlgSequence()

# Add the test algorithm:
from VertexPositionReweighting.VertexPositionReweightingConf import CP__ExampleAlg
alg = CP__ExampleAlg()
alg.OutputLevel = DEBUG
alg.VertexPositionReweightingTool.DataTag = "IndetBeampos-14TeV-SigmaXY12um-SigmaZ50mm-001"
alg.VertexPositionReweightingTool.BeamspotFile = "../VertexPositionReweighting/data/beamspot.json"
theJob += alg

# Do some additional tweaking:
from AthenaCommon.AppMgr import theApp
theApp.EvtMax = 10
ServiceMgr.MessageSvc.OutputLevel = INFO
ServiceMgr.MessageSvc.defaultLimit = 1000000
