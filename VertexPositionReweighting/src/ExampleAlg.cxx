// $Id$
// vim: ts=3:sw=3:sts=3:expandtab

// VertexPositionReweighting includes
#include "ExampleAlg.h"

namespace CP {

   ExampleAlg::ExampleAlg( const std::string& name, ISvcLocator* pSvcLocator ) :
      AthAlgorithm( name, pSvcLocator ),
      m_vtxTool( "CP::VertexPositionReweightingTool/VertexPositionReweightingTool", this)
   {
      declareProperty( "VertexPositionReweightingTool", m_vtxTool );
   }


   ExampleAlg::~ExampleAlg() {}


   StatusCode ExampleAlg::initialize() {
      ATH_MSG_INFO( "Initializing " << name() << " version " << PACKAGE_VERSION );
      ATH_MSG_DEBUG( "VertexPositionReweightingTool = " << m_vtxTool );

      ATH_CHECK( m_vtxTool.retrieve() );

      return StatusCode::SUCCESS;
   }

   StatusCode ExampleAlg::finalize() {
      ATH_MSG_INFO ("Finalizing " << name());
      return StatusCode::SUCCESS;
   }

   StatusCode ExampleAlg::execute() {  
      ATH_MSG_DEBUG ("Executing " << name() << "...");

      double weight = 1.0;
      switch (m_vtxTool->getWeight(weight)) {
      case CP::CorrectionCode::Ok:
         ATH_MSG_INFO( "Got weight." );
         break;
      case CP::CorrectionCode::OutOfValidityRange:
         ATH_MSG_WARNING( "Out of validity range." );
         break;
      case CP::CorrectionCode::Error:
         ATH_MSG_ERROR( "Error." );
         return StatusCode::FAILURE;
      }
      ATH_MSG_INFO( "Weight: " << weight );

      return StatusCode::SUCCESS;
   }

} // namespace CP

