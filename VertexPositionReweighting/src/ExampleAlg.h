// $Id$
// vim: ts=3:sw=3:sts=3:expandtab
#ifndef VERTEXPOSITIONREWEIGHTING_EXAMPLEALG_H
#define VERTEXPOSITIONREWEIGHTING_EXAMPLEALG_H 1

#include "AthenaBaseComps/AthAlgorithm.h"
#include "GaudiKernel/ToolHandle.h"

#include "VertexPositionReweighting/IVertexPositionReweightingTool.h"

namespace CP {

   class ExampleAlg: public ::AthAlgorithm { 
   public: 
      ExampleAlg( const std::string& name, ISvcLocator* pSvcLocator );
      virtual ~ExampleAlg(); 

      virtual StatusCode  initialize();
      virtual StatusCode  execute();
      virtual StatusCode  finalize();

   private: 
      /// Connection to the reweighting tool
      ToolHandle< IVertexPositionReweightingTool > m_vtxTool;

   }; 

} // namespace CP

#endif //> !VERTEXPOSITIONREWEIGHTING_EXAMPLEALG_H
