// $Id$

// Gaudi/Athena include(s):
#include "GaudiKernel/DeclareFactoryEntries.h"

// Local include(s):
#include "VertexPositionReweighting/VertexPositionReweightingTool.h"

//#include "../ToolTester.h"

DECLARE_NAMESPACE_TOOL_FACTORY( CP, VertexPositionReweightingTool )

//DECLARE_NAMESPACE_ALGORITHM_FACTORY( CP, ToolTester )


// #include "VertexPositionReweighting/xAthExample.h"
// DECLARE_ALGORITHM_FACTORY( xAthExample )


#include "../ExampleAlg.h"
DECLARE_NAMESPACE_ALGORITHM_FACTORY( CP, ExampleAlg )

// DECLARE_FACTORY_ENTRIES( VertexPositionReweighting ) {
//   DECLARE_NAMESPACE_ALGORITHM( CP, ExampleAlg );
//    //DECLARE_ALGORITHM( xAthExample );

//    DECLARE_NAMESPACE_TOOL( CP, VertexPositionReweightingTool )

//    // DECLARE_NAMESPACE_ALGORITHM( CP, ToolTester )

// }
