# echo "setup VertexPositionReweighting VertexPositionReweighting-00-00-00 in /afs/cern.ch/user/j/jmansour/workarea"

if test "${CMTROOT}" = ""; then
  CMTROOT=/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc48-opt/2.3.10/CMT/v1r25; export CMTROOT
fi
. ${CMTROOT}/mgr/setup.sh
cmtVertexPositionReweightingtempfile=`${CMTROOT}/mgr/cmt -quiet build temporary_name`
if test ! $? = 0 ; then cmtVertexPositionReweightingtempfile=/tmp/cmt.$$; fi
${CMTROOT}/mgr/cmt setup -sh -pack=VertexPositionReweighting -version=VertexPositionReweighting-00-00-00 -path=/afs/cern.ch/user/j/jmansour/workarea  -no_cleanup $* >${cmtVertexPositionReweightingtempfile}
if test $? != 0 ; then
  echo >&2 "${CMTROOT}/mgr/cmt setup -sh -pack=VertexPositionReweighting -version=VertexPositionReweighting-00-00-00 -path=/afs/cern.ch/user/j/jmansour/workarea  -no_cleanup $* >${cmtVertexPositionReweightingtempfile}"
  cmtsetupstatus=2
  /bin/rm -f ${cmtVertexPositionReweightingtempfile}
  unset cmtVertexPositionReweightingtempfile
  return $cmtsetupstatus
fi
cmtsetupstatus=0
. ${cmtVertexPositionReweightingtempfile}
if test $? != 0 ; then
  cmtsetupstatus=2
fi
/bin/rm -f ${cmtVertexPositionReweightingtempfile}
unset cmtVertexPositionReweightingtempfile
return $cmtsetupstatus

