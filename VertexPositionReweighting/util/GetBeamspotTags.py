import sys
from PyCool import cool
import PyCool
import json

def main():
    service = cool.DatabaseSvcFactory.databaseService()
    dbname = "COOLOFL_INDET/OFLP200"
    db = service.openDatabase(dbname)
    folder = db.getFolder("/Indet/Beampos")
    results = {}
    for tag in folder.listTags():
        print tag
        # obj = folder.findObject(0, 0, tag)
        itera = folder.browseObjects(0, sys.maxint, cool.ChannelSelection.all(), tag)
        results[tag] = []
        for thing in itera:
            #print "  ", thing
            since = thing.since() >> 32
            until = thing.until() >> 32
            payload = thing.payload()
            posZ = payload['posZ']
            sigmaZ = payload['sigmaZ']
            results[tag].append({'since': since, 'until': until, 'posZ': posZ, 'sigmaZ': sigmaZ})

    with open("beamspot.json", "w") as f:
        json.dump(results, f, indent=2)

if __name__ == "__main__":
    main()
