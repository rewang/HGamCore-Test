// $Id$
// vim: ts=3:sw=3:sts=3:expandtab
#ifndef VERTEXPOSITIONREWEIGHTING_VERTEXPOSITIONREWEIGHTINGTOOL_H
#define VERTEXPOSITIONREWEIGHTING_VERTEXPOSITIONREWEIGHTINGTOOL_H

#include "VertexPositionReweighting/IVertexPositionReweightingTool.h"

#include "AsgTools/AsgTool.h"

#include "TFile.h"
#include "TH1.h"

namespace CP {
   /// Tool to reweight the <i>z</i> position of the primary vertex.
   ///
   /// It works by taking a source and a target distribution, and calculating
   /// the ratio \f$R(z) = P_\mathrm{MC}(z) / P_\mathrm{Data}(z)\f$.  This is
   /// applied as a per-event weight to MC.  Currently, source and target
   /// distributions are taken from the beamspot, and assumed to be gaussian.
   /// Note that the beamspot distribution requested for MC generation or
   /// measured in data is not the same thing as the distribution of
   /// reconstructed vertex positions.  For purposes of calculating the
   /// reweighting, they are close enough however.
   ///
   /// The source (MC) beamspot distribution is taken from the MC EventInfo.
   /// The desired target distribution is specified by setting #BeamspotFile
   /// and #DataTag, or #DataMean and #DataSigma.
   ///
   /// See the <a href="#member-group">list of valid properties</a>.  You must
   /// set either BeamspotFile and DataTag, or DataMean and DataSigma.  MC mean
   /// and sigma are taken from the sample, but can be overridden for testing
   /// purposes.
   ///
   /// \nosubgrouping
   
   class VertexPositionReweightingTool: public virtual IVertexPositionReweightingTool,
                                        public asg::AsgTool {

      ASG_TOOL_CLASS(VertexPositionReweightingTool, CP::IVertexPositionReweightingTool)

   public:
      VertexPositionReweightingTool(const std::string& name);

      virtual StatusCode initialize();

      /// Gets the weight for the current event.
      virtual CorrectionCode getWeight(double& weight);

      /// Gets the mean value of the source (MC) beamspot.
      double getSourceMean() { return m_mcMean; }
      /// Gets the standard deviation of the source (MC) beamspot.
      double getSourceSigma() { return m_mcSigma; }
      /// Gets the mean value of the target (data) beamspot.
      double getTargetMean() { return m_dataMean; }
      /// Gets the standard deviation of the target (data) beamspot.
      double getTargetSigma() { return m_dataSigma; }

      // List the properties in doxygen:
      #ifndef __cplusplus
      #include "../doc/properties.doc"
      #endif

   public:

      /// Gets the z-position of the primary truth vertex.  We usually want the first
      /// truth vertex in the container, but sometimes the event generator puts a
      /// zero in there.
      ///
      /// This is normally called internally, but it is also useful if you want to write
      /// out this value to an ntuple, and then run this tool against the ntuple.
      /// @throws std::runtime_error   Thrown if the vertex position cannot be retrieved.
      double getVertexPosition();
      
      /// Gets the weight for events with a particular PV z position.
      //
      /// Normally (with xAODs) you should just call getWeight() with no arguments.
      /// However, if you wrote the PV z position to an ntuple, you may
      /// use it with this function to get the corresponding weight.
      ///
      /// Note that if you are applying the correction to a flat ntuple, you must
      /// set the source beamspot manually via McMean and McSigma properties,
      /// since these are usually read from the xAOD.
      virtual CorrectionCode getWeight(const double vxp_z, double& weight);
      
   private:
      static const std::string s_truthVertexContainer;
      static const std::string s_truthEventContainer;
      
      /// Mean of beamspot for data (target)
      double m_dataMean;
      /// Sigma of beamspot for data (target)
      double m_dataSigma;
      /// Mean of beamspot for MC (source)
      double m_mcMean;
      /// Sigma of beamspot for MC (source)
      double m_mcSigma;
      /// Conditions tag for data (target) beamspot
      std::string m_dataTag;
      /// File containing beamspot information from conditions database.
      std::string m_beamspotFile;
      
      /// Loads the MC beamspot.  In theory, this is one value for the whole sample,
      /// which corresponds to a conditions tag.  However, there seems to be no way
      /// to get it from the xAOD metadata.  Instead, we read it from the first event.
      void loadMCBeamspot();

   };
}

#endif//VERTEXPOSITIONREWEIGHTING_VERTEXPOSITIONREWEIGHTINGTOOL_H
