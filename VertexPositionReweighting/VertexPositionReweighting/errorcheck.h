// Dear emacs, this is -*- c++ -*-
// $Id$
#ifndef VERTEXPOSITIONREWEIGHTING_ERRORCHECK_H
#define VERTEXPOSITIONREWEIGHTING_ERRORCHECK_H

#define CHECK( ARG )                                     \
   do {                                                  \
      const bool result = ARG;                           \
      if( ! result ) {                                   \
         ::Error( APP_NAME, "Failed to execute: \"%s\"", \
                  #ARG );                                \
         return 1;                                       \
      }                                                  \
   } while( false )

#endif // VERTEXPOSITIONREWEIGHTING_ERRORCHECK_H
