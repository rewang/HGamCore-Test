// $Id$
// vim: ts=3:sw=3:sts=3:expandtab


#ifndef VERTEXPOSITIONREWEIGHTING_IVERTEXPOSITIONREWEIGHTINGTOOL_H
#define VERTEXPOSITIONREWEIGHTING_IVERTEXPOSITIONREWEIGHTINGTOOL_H

// Framework include(s):
#include "AsgTools/IAsgTool.h"

// Local include(s):
#include "PATInterfaces/CorrectionCode.h"

namespace CP {
   /// Interface for retrieving per-event weights
   class IVertexPositionReweightingTool: public virtual asg::IAsgTool {
      ASG_TOOL_INTERFACE(CP::IVertexPositionReweightingTool)

   public:
      virtual CorrectionCode getWeight(double& weight) = 0;
   };
}

#endif//VERTEXPOSITIONREWEIGHTING_IVERTEXPOSITIONREWEIGHTINGTOOL_H

