# This file is NOT licensed under the GPLv3, which is the license for the rest
# of YouCompleteMe.
#
# Here's the license text for this file:
#
# This is free and unencumbered software released into the public domain.
#
# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.
#
# In jurisdictions that recognize copyright laws, the author or authors
# of this software dedicate any and all copyright interest in the
# software to the public domain. We make this dedication for the benefit
# of the public at large and to the detriment of our heirs and
# successors. We intend this dedication to be an overt act of
# relinquishment in perpetuity of all present and future rights to this
# software under copyright law.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# For more information, please refer to <http://unlicense.org/>

import os
import ycm_core

import subprocess

mode = "RootCore"
#mode = "Athena"

flags = []
if mode == "RootCore":
    ldflags = subprocess.Popen(["rc", "get_all_ldclags"], stdout=subprocess.PIPE).communicate()[0]
    flags += ldflags.split()
    cxxflags = subprocess.Popen(["rc", "get_cxxflags", "VertexPositionReweighting"], stdout=subprocess.PIPE).communicate()[0]
    cxxflags = cxxflags.replace('-I', '-isystem ')
    flags += cxxflags.split()
    root_cflags = subprocess.Popen(["root-config", "--cflags"], stdout=subprocess.PIPE).communicate()[0]
    root_cflags = root_cflags.replace('-I', '-isystem ')
    flags += root_cflags.split()
    #flags += ["-x", "c++11", "-stdlib=libstdc++", "-DXAOD_STANDALONE", "-DXAOD_ANALYSIS"]
    flags += ["-x", "c++", "-std=c++11", "-stdlib=libstdc++", "-DXAOD_STANDALONE", "-DXAOD_ANALYSIS"]
    #flags += ["-x", "c++", "-std=c++0x", "-stdlib=libstdc++", "-DXAOD_STANDALONE", "-DXAOD_ANALYSIS"]
    #flags += ["-std=c++11", "-stdlib=libstdc++", "-DXAOD_STANDALONE", "-DXAOD_ANALYSIS"]
    flags += ["-Weverything", "-Wno-c++98-compat"]
else:
    flags_str = "-DNDEBUG -D_GNU_SOURCE -pthread -pipe -ansi -fPIC -W -Wall  -std=c++11 -Wno-deprecated -g  -pedantic -Wwrite-strings -Wpointer-arith -Woverloaded-virtual -Wno-long-long  -ftemplate-depth-99  -D_GNU_SOURCE -DGAUDI_V20_COMPAT  -DBOOST_FILESYSTEM_VERSION=3  -DBOOST_SPIRIT_USE_PHOENIX_V3  -DATLAS_GAUDI_V21  -DNDEBUG   -DXAOD_MANACORE -DXAOD_ANALYSIS  -DHAVE_GAUDI_PLUGINSVC"
    flags = flags_str.split()
    flags += ["-x", "c++", "-std=c++11", "-stdlib=libstdc++"]
    # there is a bug in libstdc++ / incompatibility with clang, that causes an error in StatusCode.h regarding
    # an implicitly deleted constructor.  See: https://stackoverflow.com/questions/7964360/using-stdshared-ptr-with-clang-and-libstdc
    #flags += ["-x", "c++", "-std=c++11", "-stdlib=libc++"]

    include_flags = [
        "-isystem", "/cvmfs/atlas.cern.ch/repo/tools/slc6/cmt/InstallArea/x86_64-slc6-gcc47-opt/include"  ,
        "-isystem", "/afs/cern.ch/user/j/jmansour/CPToolsSprint/InstallArea/x86_64-slc6-gcc47-opt/include"  ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/x86_64-slc6-gcc47-opt/include"  ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/GAUDI/v25r3/InstallArea/x86_64-slc6-gcc47-opt/include"  ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/LCGCMT/LCGCMT_67b/InstallArea/x86_64-slc6-gcc47-opt/include"  ,
        "-isystem", "/afs/cern.ch/user/j/jmansour/CPToolsSprint/PhysicsAnalysis/AnalysisCommon/VertexPositionReweighting" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/tools/slc6/cmt/InstallArea/include" ,
        "-isystem", "/afs/cern.ch/user/j/jmansour/CPToolsSprint/InstallArea/include" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/GAUDI/v25r3/InstallArea/include" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/LCGCMT/LCGCMT_67b/InstallArea/include" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/ElectronEfficiencyCorrection" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/ElectronPhotonFourMomentumCorrection" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/MuonSelectorTools" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/TauAnalysisTools" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/JetSelectorTools" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/xAODBTaggingEfficiency" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/PATCore" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/egammaMVACalib" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/JetResolution" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/JetUncertainties" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/JetCPInterfaces" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/JetCalibTools" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/MuonEfficiencyCorrections" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/MuonMomentumCorrections" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/PATInterfaces" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/JetInterface" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/METUtilities" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/METInterface" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/AsgTools" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/xAODMuon" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/MuonIdHelpers" ,
        "-isystem", "." ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/CalibrationDataInterface" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/JetTagCalibration" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/AthenaBaseComps" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/StoreGate" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/IOVDbDataModel" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/AthenaPoolUtilities" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/DBDataModel" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/DataModel" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/xAODEgamma" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/xAODTau" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/xAODPFlow" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/xAODCaloEvent" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/xAODJet" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/xAODBTagging" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/xAODTracking" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/xAODTruth" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/xAODMissingET" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/xAODBase" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/xAODTrigger" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/xAODEventInfo" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/xAODEventShape" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/xAODCore" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/xAODPrimitives" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/AthContainers" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/AthContainersInterfaces" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/AtlasDetDescr" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/CLIDSvc" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/AthLinks" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/SGTools" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/AthenaKernel" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/DataModelRoot" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/CaloGeoHelpers" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/GeoPrimitives" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/CxxUtils" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/AthAllocators" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/IdDict" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/Identifier" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/EventPrimitives" ,
        "-isystem", "/afs/cern.ch/user/j/jmansour/CPToolsSprint/InstallArea/x86_64-slc6-gcc47-opt/include" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/TruthUtils" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/InstallArea/include/PathResolver" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/AtlasCxxPolicy" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/sw/lcg/external/HepMC/2.06.08/x86_64-slc6-gcc47-opt/include" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/sw/lcg/external/fastjet/3.0.3/x86_64-slc6-gcc47-opt/include" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/sw/lcg/app/releases/ROOT/5.34.13/x86_64-slc6-gcc47-opt/root/include" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/sw/lcg/app/releases/ROOT/5.34.13/x86_64-slc6-gcc47-opt/root/include" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/sw/lcg/app/releases/CORAL/CORAL_2_4_1/include" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/sw/lcg/external/Boost/1.53.0_python2.7/x86_64-slc6-gcc47-opt/include/boost-1_53" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/sw/lcg/external/xrootd/3.2.7/x86_64-slc6-gcc47-opt/include" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/AtlasCommonPolicy/src" ,
        "-isystem", "." ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/sw/lcg/external/clhep/2.1.2.3-atl01/x86_64-slc6-gcc47-opt/include" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/AthAnalysisBase/2.0.15/External/AtlasCompilers/src" ,
        "-isystem", "/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc47-opt/2.0.15/GAUDI/v25r3/LCG_Platforms/src",
    ]
    flags += include_flags
#-MMD -MP -MF ../x86_64-slc6-gcc47-opt/MuonSelectionToolExample.d -MQ ../x86_64-slc6-gcc47-opt/MuonSelectionToolExample.o 





# # These are the compilation flags that will be used in case there's no
# # compilation database set (by default, one is not set).
# # CHANGE THIS LIST OF FLAGS. YES, THIS IS THE DROID YOU HAVE BEEN LOOKING FOR.
# flags = [
# '-Wall',
# '-Wextra',
# '-Werror',
# '-Wc++98-compat',
# '-Wno-long-long',
# '-Wno-variadic-macros',
# '-fexceptions',
# '-DNDEBUG',
# # You 100% do NOT need -DUSE_CLANG_COMPLETER in your flags; only the YCM
# # source code needs it.
# '-DUSE_CLANG_COMPLETER',
# # THIS IS IMPORTANT! Without a "-std=<something>" flag, clang won't know which
# # language to use when compiling headers. So it will guess. Badly. So C++
# # headers will be compiled as C headers. You don't want that so ALWAYS specify
# # a "-std=<something>".
# # For a C project, you would set this to something like 'c99' instead of
# # 'c++11'.
# '-std=c++11',
# # ...and the same thing goes for the magic -x option which specifies the
# # language that the files to be compiled are written in. This is mostly
# # relevant for c++ headers.
# # For a C project, you would set this to 'c' instead of 'c++'.
# '-x',
# 'c++',
# '-isystem',
# '../BoostParts',
# '-isystem',
# # This path will only work on OS X, but extra paths that don't exist are not
# # harmful
# '/System/Library/Frameworks/Python.framework/Headers',
# '-isystem',
# '../llvm/include',
# '-isystem',
# '../llvm/tools/clang/include',
# '-I',
# '.',
# '-I',
# './ClangCompleter',
# '-isystem',
# './tests/gmock/gtest',
# '-isystem',
# './tests/gmock/gtest/include',
# '-isystem',
# './tests/gmock',
# '-isystem',
# './tests/gmock/include',
# '-isystem',
# '/usr/include',
# '-isystem',
# '/usr/local/include',
# '-isystem',
# '/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/../include/c++/v1',
# '-isystem',
# '/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/include',
# ]


# Set this to the absolute path to the folder (NOT the file!) containing the
# compile_commands.json file to use that instead of 'flags'. See here for
# more details: http://clang.llvm.org/docs/JSONCompilationDatabase.html
#
# You can get CMake to generate this file for you by adding:
#   set( CMAKE_EXPORT_COMPILE_COMMANDS 1 )
# to your CMakeLists.txt file.
#
# Most projects will NOT need to set this to anything; you can just change the
# 'flags' list of compilation flags. Notice that YCM itself uses that approach.
compilation_database_folder = ''

if os.path.exists( compilation_database_folder ):
  database = ycm_core.CompilationDatabase( compilation_database_folder )
else:
  database = None

SOURCE_EXTENSIONS = [ '.cpp', '.cxx', '.cc', '.c', '.m', '.mm' ]

def DirectoryOfThisScript():
  return os.path.dirname( os.path.abspath( __file__ ) )


def MakeRelativePathsInFlagsAbsolute( flags, working_directory ):
  if not working_directory:
    return list( flags )
  new_flags = []
  make_next_absolute = False
  path_flags = [ '-isystem', '-I', '-iquote', '--sysroot=' ]
  for flag in flags:
    new_flag = flag

    if make_next_absolute:
      make_next_absolute = False
      if not flag.startswith( '/' ):
        new_flag = os.path.join( working_directory, flag )

    for path_flag in path_flags:
      if flag == path_flag:
        make_next_absolute = True
        break

      if flag.startswith( path_flag ):
        path = flag[ len( path_flag ): ]
        new_flag = path_flag + os.path.join( working_directory, path )
        break

    if new_flag:
      new_flags.append( new_flag )
  return new_flags


def IsHeaderFile( filename ):
  extension = os.path.splitext( filename )[ 1 ]
  return extension in [ '.h', '.hxx', '.hpp', '.hh' ]


def GetCompilationInfoForFile( filename ):
  # The compilation_commands.json file generated by CMake does not have entries
  # for header files. So we do our best by asking the db for flags for a
  # corresponding source file, if any. If one exists, the flags for that file
  # should be good enough.
  if IsHeaderFile( filename ):
    basename = os.path.splitext( filename )[ 0 ]
    for extension in SOURCE_EXTENSIONS:
      replacement_file = basename + extension
      if os.path.exists( replacement_file ):
        compilation_info = database.GetCompilationInfoForFile(
          replacement_file )
        if compilation_info.compiler_flags_:
          return compilation_info
    return None
  return database.GetCompilationInfoForFile( filename )


def FlagsForFile( filename, **kwargs ):
  if database:
    # Bear in mind that compilation_info.compiler_flags_ does NOT return a
    # python list, but a "list-like" StringVec object
    compilation_info = GetCompilationInfoForFile( filename )
    if not compilation_info:
      return None

    final_flags = MakeRelativePathsInFlagsAbsolute(
      compilation_info.compiler_flags_,
      compilation_info.compiler_working_dir_ )

    # # NOTE: This is just for YouCompleteMe; it's highly likely that your project
    # # does NOT need to remove the stdlib flag. DO NOT USE THIS IN YOUR
    # # ycm_extra_conf IF YOU'RE NOT 100% SURE YOU NEED IT.
    # try:
      # final_flags.remove( '-stdlib=libc++' )
    # except ValueError:
      # pass
  else:
    relative_to = DirectoryOfThisScript()
    final_flags = MakeRelativePathsInFlagsAbsolute( flags, relative_to )

  return {
    'flags': final_flags,
    'do_cache': True
  }
